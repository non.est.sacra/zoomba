# ZoomBA Wiki
 Welcome to <img src="../icons/ZoomBA.png" width="100" height="100" /> Wiki .

## Design Goals

The language was conceived and written by multiple angry developers, 
after looking at the extensive sad state of the tech for the core businesses. The following issues were found:

* Java is extensively verbose for the amount of work it achieves
* Scala does not play well with Java: 
   * Easy to call Java from Scala, trivial 
   * [Next to impossible](http://lampwww.epfl.ch/~michelou/scala/using-scala-from-java.html) to call [advanced Scala from Java](https://blog.akquinet.de/2011/07/20/integrating-scala-components-in-a-java-application/) code  
* Jython, the next hybrid does not play well between JVM object and Python objects 
* Groovy is ok, but [some design decisions](https://schneide.wordpress.com/2011/04/05/grails-the-good-the-bad-the-ugly/) are not to our liking - those make Groovy a terribly bad language for business programming and software validation.


### Manifesto for ZoomBA Development 

Thus, after working in Software Industry for more than 3 decades,
we have concluded the following :

 * Technology is almost always an aid to business, thus 
   * While we value technology, achieving business goal optimally is what we value more.      
 * While we understand multitudes of OOPs and Design dogmas 
   * We have rarely seen value in it for businesses.  
 * While we feel for the burning desire for the tech managers soul to create empires under them 
   * We have seen how such empires fall, when everyone stopped mapping business value of what tech does.  

### Tenets for any Language Development  

 Thus, ZoomBA was designed to :

 * Remove unnecessary Dogmas (OOPs, for example)  
 * Being minimal, thus having a code quality that, code that once written, can be forgotten about, should always run 
 * As an aid to inject scripting capabilities into underlying VM
 * As a business development language for SQL aware folks :
    * In Business 
    * In QA   
 * Abiding by strong Pareto optimality : for 90% of the cases for business need a single ZoomBA script file with less than 42 lines would suffice.

While we do not see value in dogma, we see significant value in dollars,
and in the saving of it. 

## Maven Repo 
If you want to programmatically call ZoomBA from JVM - here is the dependency:

```xml
<dependency>
    <groupId>org.zoomba-lang</groupId>
    <artifactId>zoomba.lang.core</artifactId>
    <version>${zoomba.version}</version>
</dependency>
```

Current ```zoomba.version``` is 0.3-SNAPSHOT.


## Compilation from Source 
Please have git and maven. If you have it, then :

    $git clone git@gitlab.com:non.est.sacra/zoomba.git
    $cd zoomba
    $mvn clean install -DskipTests # because some tests might not run 
    $cd target
    $java -jar zoomba.lang.core-<zoomba.version>one-jar.jar

You should be able to see the (zoomba) prompt.  
And then, you can do this :

    (zoomba)println ( "I \u2665 ZoomBA!" )
    I ♥ ZoomBA!
    null // Nil   

## Download The Latest Binary (Bleeding Edge) 

* You need to have Java 8 runtime installed.

* Download the binary distribution (one-jar) from [here](https://repo1.maven.org/maven2/org/zoomba-lang/zoomba.lang.core/0.2/)

* Unzip the archive (either beta or snapshot)

* Simply run now :

```
java -jar zoomba.lang.core-<zoomba.version>.one-jar.jar 
```     

You should see the *(zoomba)* prompt.

Alternatively, You can use the following 
[download script](../install.sh) to download the latest jars file into your `$HOME/.local/bin/zmb` directory.

```sh
bash -c "$(curl -fsSL https://gitlab.com/non.est.sacra/zoomba/-/raw/master/install.sh)"
```

## Setting Up
Setting it up means simply aliasing in \*nix platforms.
Under bash:

```sh
alias zmb='java -jar <repo-path>/target/zoomba.lang.core-<zoomba.version>.one-jar.jar'
```

That should be put into the `.bashrc` or `.profile` file.

## IDE Setup

Under the folder `ide_settings_files`, there are 

* `Intellij` : `intellij-settings.zip` : [import](https://www.jetbrains.com/help/idea/2016.2/exporting-and-importing-settings.html) it in Idea.
* Vim : `zoomba.vim` file,use appropriately as shown [here](http://superuser.com/questions/844004/creating-a-simple-vim-syntax-highlighting).
* Sublime Text : `zoomba.tmLanguage` : follow as shown [here](http://sublimetext.info/docs/en/extensibility/syntaxdefs.html)

There is also a [VS Code extension](https://www.mediafire.com/file/ul6ix5neomawu1n/zoomba-0.0.1.vsix) for syntax highlighting zoomba scripts.  

## Language Specification

The following pages would set you up for exploring features of ZoomBA.

* [Introduction to Basic Syntax](_wiki/01-basic.md)
* [Types And Conversion](_wiki/02-types.md)
* [Conditions and Iterations](_wiki/03-Iteration.md) 
* [Functions](_wiki/04-Functions.md)
* [Imports and Code ReUse](_wiki/05-Imports.md) 
* [Extended Operators](_wiki/06-Operators.md)
* [Reading and Writing](_wiki/07-IO.md) 
* [Processes and Threads](_wiki/08-Execution.md) 
* [Lots of Samples](_wiki/09-samples.md)
* [Embedding into other Java source](_wiki/10-JVM.md)
* [Introduction to Objects](_wiki/11-oops.md)  
* [Advanced Topics in Objects](_wiki/12-advanced.md)
* [ZoomBA Gotchas](_wiki/13-howto.md)


### Command Line Manual 
ZoomBA has its manual on command line, so to discuss a topic one must issue this command:

```
(zoomba)//h <topic_name>
```

This would produce the manual for the topic. 

## Solved Problems
We are using this a/c to solve some problems in Career Cup.  See the solutions listed [here](https://www.careercup.com/user?id=5687896666275840).
