# Types And Conversion

ZoomBA is almost typeless.
It does type inferencing from left side of a binary
operator. The jargon guys among us named the strategy as *conformance*.
Conformal mapping is where between transforms, the angles remain fixed.
In ZoomBA, conformance means between operations, the sense of the operation stays preserved : what was *intended idea* behind the operation,stays that way.

## Finding Types
The function *type()* returns the type of an object.

```js
(zoomba)type(_'a')
class java.lang.Character // Class
(zoomba)type(true)
class java.lang.Boolean // Class
(zoomba)type(0)
class java.lang.Integer // Class
(zoomba)type(0l)
class java.lang.Long // Class
(zoomba)type(0L)
class java.math.BigInteger // Class
(zoomba)type(1.0)
class java.lang.Double // Class
(zoomba)type(1.0D)
class java.math.BigDecimal // Class
(zoomba)type("hello")
class java.lang.String // Class
(zoomba)type([])
class zoomba.lang.core.collections.ZArray // Class
(zoomba)type(null)
class zoomba.lang.core.operations.Function$Nil // Class
(zoomba)type({:})
class zoomba.lang.core.collections.ZMap // Class
```

## Character Type 

There is `char()` function. 

```scala
c = _'a'
c isa 'char' // true 
c = char(97) // _'a'
c = char("hello") // _'h'
c = char(false,'a') // _a 
c = char(false,false) // null  
ascii = int( _'a' ) // 97 
```


## Numeric Types 
Naturally, the regex 

```js
(+|-)? (\d)+((\.)?\d+)?(l|L|D)?
```

qualifies as numeric. The [scientific/engineering notation](https://en.wikipedia.org/wiki/Engineering_notation) is also allowed.
Thus, these are legal :

```js
x = 1 // integer 
x = -1.0 // double 
x = 0.1e-42 // double, engineering notation 
```

From any type, corresponding numeric type can be obtained by *num()* function :

```js
x = num(y) // corresponding numeric
x = num("1234") // x = 1234 : Integer
x = num("123.4") // x = 123.4 : Double
```

### Infinities
There are two special values, infinities. They are to be found as such:

```js
inf = num("inf") // +ve infinity 
ninf = num("-inf") //-ve infinity 
```

The system is designed as such that, for any value *v != inf* :

```js
v < inf 
inf > v 
inf == inf 
```

while, for *v != ninf* we have :
​        
```js            
v > ninf 
ninf < v 
ninf == ninf 
```

Arithmetic is not possible with infinities. It is grossly illegal to
add, subtract, multiply or divide using infinities. They are there, to 
ensure comparison against very large number, and in supporting algorithms
requiring infinities.  In general, one can not do any other arithmetic operations using infinities.  
​     

```js
(zoomba) -1 * num('inf')
zoomba.lang.core.types.ZException$ArithmeticLogicOperation: Invalid Arithmetic Logical Operation [*] : at line 1, cols 6:15 --> ( Can not do operation ( MULTIPLY ) : ( -1 ) with ( inf ) ! ) // ArithmeticLogicOperation
```



### Integer 

If you, however want to convert other types to proper integer type,  you can do it using :

```js
 d = 1.01 
 i = int(d) // i = 1 
 i = int ( "234") // i = 234 
 i = int("23.4") // i = 23
 i = int( " boo hahah " ) // i = null 
 i = int( " boo hahah " , 42 ) // i = 42 :: answer to everything
 i = int('11',2,0) // i = 3 : base 2 notation before the default 
 i = int(34.45) // i = 34 ; truncate
```

### Floating Point 
In the same way, one can convert other types into floating point, rationals :

```js
i = 10 
d = float(i) // d = 10.0 
d = float("234") // d = 234.0 
d = float("23.4") // d = 23.4   
```

### Arbitrary Precision
ZoomBA arithmetic is by default arbitrary precision. That is,
if a value is simply too large to be contained in a smaller type, it would automatically put them into a 
larger data type.

```js
x = 0.1001001001210101008918101911 // arbitrary precision 
i = 997987894739837538753857308573873073 // very large int
z = 10 ** 100 // very large integer
```

The same can be done by literal symbols:

```js
long_integer = 1l // As java.lang.Long
big_integer = 1L // as org.jscience.mathematics.number.LargeInteger 
large_float = 1.0D // as Real 
```

See more of [Real doc](http://jscience.org/api/org/jscience/mathematics/number/Real.html).       

### Numeric Conversion 
Generic conversion is done by either int(), or float() or num() functions. If the conversion fails, the default value is passed:

```js
x = int("bo hahah") // x = null
x = int("bo hahah", 0 ) // x = 0
x = int("11" ) // x = 11
x = INT("11") // x = 11, java BigInteger
// base conversion into base  
x = int("11",  2, 0 ) // x = 3, num, base, default - we support till base 62.
x = float("bo hahah") // x = null
x = float("bo hahah" , 0 ) // x = 0.0  
x = FLOAT("11.1") // a = 11.1 java BigDecimal      
```


Like Tex conventions, *FLOAT()* and *INT()* functions converts the value to the corresponding *large/big* equivalents. As we are still under JVM and `Number` is the base type, converting anything to long is easy :

```js
l = x.longValue()
x = 10 
byte = x.byteValue() 
short = x.shortValue() 
```

Thus, there is no need for the other small integer types to be native to ZoomBA. 

#### Conformance , Narrowing , Precision 
ZoomBA hates to lose precision. So given any numeric computation it would store the information in a way that does not lose the information. We jargonfied this called this "Conformance". 

The other side of the conformance is how narrow storage can we use to reduce memory footprint. This is Narrowing. 

ZoomBA works by inspecting the number literal, or result of an operation, and finds the smallest container which can incorporate the value. 
So, ZoomBA picks the minimal container which does not loose precision of computation. This is called conformance ( by our Jargon Guys ). Thus, observe this :

```js
(zoomba)1l + 1.0
2 // Double : because precision 
(zoomba)1.0 + 1.0D
2 // Double : because of conformance & narrowing 
```

Hence, logic about *larger type + smaller type -> larger* type is not entirely true in ZoomBA, much more humane ideas about container follows. 
This was done to aid business and software testers, to remove them from the shackles of the precision loss issue in doubles. 

### Numeric to String 

The function `str()` handles any type into string conversion.
Integers can be converted to any base :

```js
x = 10
b_x = str(10,2) // 1010 : str(num, base) - we support upto base 62
b_p_x = str(10,2,8) // 00001010 -- upto 8 bit representation 
```

Floating points can be made precise upto decimal points:

```js
x = 1.2354329
s_x0 = str(x,0) // x = 1
s_x1 = str(x,1) // x = 1.2
s_x3 = str(x,2) // x = 1.24 : rounding off
```

### Ceiling and Floor

These are necessary functions, defined as such :

```js
 // floor of f: max( all integers less than of equal to f ) 
 x = floor(1.1) // x = 1
 x = floor(0.0) // x = 0
 x = floor(-1.5) // x = -2 
 // ceil of f: min ( all integers greater than or equal to f )
 y = ceil(1.1) // x = 2 
 y = ceil(-1.1) // x = -1
 y = ceil(-1.0) // x = -1
```

### Rounding 
The *round()* function comes into play (does round up by half) :

```js
(zoomba)round(0.5)
1 // Integer
(zoomba)round(0.5,2)
0.5 // Double
(zoomba)round(0.5,3)
0.5 // Double
(zoomba)round(0.51239,3)
0.512 // Double
(zoomba)round(0.51239,4)
0.5124 // Double 
```

### Absolute Value
This is done by the *size()* or the cardinality operator :

```js
x = size(10) // 10 
n = size(null) // -1 -- null is uninitialized so size is negative 
y = #|-11.1| // 11.1        
```

It is better to use cardinality operator, than the *size()*. 
​    

### Sign Function 
The classic [signum](https://en.wikipedia.org/wiki/Sign_function) function is implemented as :

```js
sign(0.0) // 0 
sign(-0.1) // -1
sign(10) // 1
```

thus, if the operand is less than zero, it would return -1, if it is zero, it would return 0, and if it is greater than zero, it would return +1.
​     

### Digit Length of an Integer

*size()* can be used to calculate the digit length of an integer in any base. Thus :

```js
(zoomba)size(10) // base 1 : same as the number itself
10 // Integer
(zoomba)size(10,2) // base 2 
4 // Integer
(zoomba)size(10,5) // base 5 
2 // Integer
(zoomba)size(10,10)
2 // Integer
(zoomba)size(10,11) // Obvious
1 // Integer
```

### Log 

Natural logarithm can be achieved by :

```js
log(x) // log to the base e, of x 
```

In the same spirit, `log(b,a)` is done by exactly that:

```js
log(b,a) // log to the base a, of b 
log(4,2)  // 2       
```

### Trigonometry 

This can be accomplished by invoking `java.lang.Math.function()`. 
Math functions can be invoked ( arbitrary precision ) with `@SCRIPT.MATH`  :



``` scala
m = @SCRIPT.MATH
m.sin( m.PI/2 ) // 1.0 BigDecimal 
m.sqrt(4,0.5) // 2.0 BigDecimal 
```



## Logical

The function *bool()* handles the boolean conversion.
As expected :

```js
 x = bool("true") // true 
 x = bool("tRuE") // true 
 x = bool("false") // false
 x = bool("FaLse") // false
 x = bool("FFFaLse") // null
 x = bool("FFFaLse" , false ) // false
 x = bool("FFFaLse" , true ) // true
```

 More fundamentally :

 ```js
 x = bool(0,[0,1]) // false [0 : false, 1: true ]
 x = bool(1,[0,1]) // true 
 ```

This way the choices can be done with and assigned one to one with 
true/false much simply.      

## Date and Time Type

ZoomBA supports time(). To get current time :

```js
t = time() // time now
```

Time can be created out of milliseconds passed since EON:

```js
t = time( 10000000 ) // time after that amount of ms
```

Time can also be created from string : as date :

```js
d = time( "20160808") // yyyyMMdd : default date format
d = time( "08-2016-08", "dd-yyyy-MM" ) // pass format 
```
Time can be created from and converted into integer type:

```js
(zoomba)t = time()
2017-07-25T22:44:26.020 // ZDate
(zoomba)l = int(t)
1501002866020 // Long
(zoomba)time(l)
2017-07-25T22:44:26.020 // ZDate
```


One can get java date by using :

```js
(zoomba)t = time()
(zoomba)t.date 
(zoomba)2016-10-02T18:02:52.897 // Date
```

ZoomBA works internally with java 8 LocalDateTime:

```js
(zoomba)time().time 
(zoomba)2016-10-02T18:04:06.722 // LocalDateTime
```

### String from Time             
One can convert time into string by str() function.

```js
d = time()
s_d = str(d) // default date format 
s_d = str(d, "dd-MM-yyyy" ) // specify date format 
```

### Comparisons on Time 

Time objects are comparable. Thus, these are perfectly legal :

```js
     t_old = time() 
     t_cur = time() 
     t_cur > t_old // true 
     t_cur < t_old // false
     t_cur == t_old // false
     t_cur != t_old // true 
```

### Arithmetic on Time

You can subtract time. You can also add time delta to the time objects.
To demonstrate :

```js
(zoomba)to = time()
2016-10-04T19:14:10.173 // ZDate
(zoomba)tc = time()
2016-10-04T19:14:16.161 // ZDate
(zoomba)dur = tc - to
PT5.988S // Duration      
```

Now, we can add milliseconds to the time slices :

```js
    (zoomba)tn = to + 100000
    2016-10-04T19:15:50.173 // ZDate
```

To add a duration, add a duration string :

```js 
    td = t + "PT-5.988S"  
```
Mutable operations are allowed on time, so it is perfectly legal to :

```js
(zoomba)t += 4000000
2017-07-25T23:51:06.020 // ZDate
(zoomba)t -= 4000000
2017-07-25T22:44:26.020 // ZDate
```

### Time Zones

Timezone handling is natively provided. 

```scala
t = time() // always gets the local time in the local time zone 
t_utc = t.at("UTC") // gets this time in a different timezone in this case UTL
```

The timezone id's supported are listed here : https://garygregory.wordpress.com/2013/06/18/what-are-the-java-timezone-ids/ and http://tutorials.jenkov.com/java-date-time/java-util-timezone.html .



## Collections

### Size Operations on Collections

```js
 x = size(null) // -1 
 y = size("") // 0 
 z = size("abc") // 3
 a = size([] ) // 0 
 empty([]) // true 
 empty(null) // true 
 #|null| // 0 
 #|[]| // 0   
 #|{:}| // 0 empty dictionary       
```

### Fixed Sized List / Array 

There are no native like arrays in ZoomBA. The arrays are conveniently wrapped in a cocoon of unmodifiable ArrayList type. Thus, sizes can not change.

```js
a = [0,1,2,3] // a Fixed List
x = a[0] // x = 0 
x = a.0 // x = 0 ; dot works too  
x = a[-1] // index -ve means a[ size(a) - index ] : x = 3 
```

#### True Array

True arrays can be created by the built in `array(string_type)` function on any standard collection.

```js
(zoomba)x = [0,1,2,3]
@[ 0,1,2,3 ] // ZArray
(zoomba)x.array('int')
0,1,2,3 // int[]
(zoomba)x.array('float')
0.0,1.0,2.0,3.0 // float[]
(zoomba)x.array('double')
0.0,1.0,2.0,3.0 // double[]
(zoomba)x = [true,false]
@[ true,false ] // ZArray
(zoomba)x.array('bool')
true,false // boolean[]
```

In case where the types are to be given full name, specify full name. As example:

```js
(zoomba)x = [0,1,2,3]
@[ 0,1,2,3 ] // ZArray
(zoomba)x.array('java.lang.Integer')
0,1,2,3 // Integer[]
```

This way, arbitrary `ZArray` (having same type items) can be converted into proper JVM array.
ZArray also abstracts out `Map.Entry` so this is legit:

```scala
e = [1,2]
e.getKey // 1 
e.getValue // 2 
```

### Mutable List 

Mutable lists are generated by, `list()` function.

```js
l = list() // empty list 
l = list(1,2,3) // list initialised with 1,2,3 
```

The indexing works same way as the array.

### Sorted List 

```js
l = slist() // empty sorted list 
l = slist(1,3,2) // list initialised with 1,2,3 
```
One can of course pass the comparators for the `comparison` for a sorted collection as follows:

```scala 
l = ["boom",  "hi", "hello",  ]
sl = slist( l ) as { size($.o) } // "hi", "boom", "hello"  
```
Here the comparator is essentially length of the string.

### Stack & Queue 

Given the `ZoomBA` list implements the  [java.util.Deque](https://docs.oracle.com/en/java/javase/23/docs/api/java.base/java/util/Deque.html) it can be used as stack as well as queue, as well as double ended queue.

```scala
// usage as stack
s = list() 
s.push(1)
s.push(2)
println(s.peek) // top element of stack : 2 
println(s.pop) // pop the value 2 
s.clear() // clear off 

// usage as Queue
q = list()
q.add(1) 
q.add(2) 
println(q.peek) // get front element of queue : 1 
println(q.poll) // get and remove front element of queue : 1 

```

It is obvious that sorted list can not be used to be using as Stack or Queue. If we do so, we would have errors:

```scala
(zoomba)x = slist(1,10,1,2)
[ 1,1,2,10 ] // BaseZSortedList
(zoomba)x.push(10)
zoomba.lang.core.types.ZException$Function: push : java.lang.UnsupportedOperationException: indexed add is not supported in Sorted List :  --> <inline>:1:3 to 10 // Function

```



### Collection Slicing

An indexable collection can be sliced by indices. We found pythonic 
way of slicing *x[:e]* revolting. That is not precision, that is convention. ZoomBA is about engineering precision over software dogma, so :

```js
sub_collection = collection[ start : end ] // slices collection
```

The indices are both included in the sub collection. Thus :

```js    
 (zoomba)a = [0,1,2,3,4]
 @[ 0,1,2,3,4 ] // ZArray
 (zoomba)a[2:4]
 [ 2,3,4 ] // ZList 
```

Now, to slice from some location to end, you do not need to do revolting code :

```js
 (zoomba)a[2:-1]
 [ 2,3,4 ] // ZList
```

### Set

Set is a non repeating collection. The set() function works:

```js
s = set() // empty set 
s = set(1,2,2,2,3) // set initialised with 1,2,3 
```

Ordinary sets do not care about the order of elements of the set. This can readily be seen :

```js
(zoomba)for ( [0:10] ) { x = random(100) ; println(x) ; s+=x }
13
23
83
12
57
74
29
32
65
16
{ 32,65,74,12,13,16,83,23,57,29 } // ZSet    
```

#### Sorted Set 

A sorted set is a set where the order of elements in the set is preserved, i.e. they are sorted. Sorted set is created by using `sset()` function :

```js
(zoomba)ss = sset()
{  } // ZSet
(zoomba)for ([0:5]) { ss += random(100) }
{ 1,56,76,84,94 } // ZSet
```

As one can see, the elements are sorted when you iterate over them! 
One can pass a comparison function to the *sset()* construct, we will talk about that later. It is same as [SortedSet : Java](https://docs.oracle.com/javase/8/docs/api/java/util/SortedSet.html).


#### Ordered Set 

What about the order of insertion? How can we ensure that the order in which the elements got inserted will be the same order in which the elements will be traversed? This is done by using Ordered Set or *oset()* :

```js
(zoomba)for ([0:5]) { v = random(100) ; println(v) ; os += v }
53
52
18
29
86
{ 53,52,18,29,86 } // ZSet
```

### Dictionary 
Dictionary, literal was discussed before. dict() function works:

```js
d = dict() // empty dictionary 
// from tuples [key,value]
tuples = [ [0,1] , [2,3] , [4,5] ]
d = dict( tuples ) // d = { 0:1, 2:3, 4:5 } 
// from matched collections
keys = [ 0, 2, 4 ]
values = [ 1, 3, 5 ]
d = dict ( keys, values ) // same as before
```

Like set, a dictionary also does not preserve the order of insertion of keys. To do so, use a sorted dictionary instead:

#### Sorted Dictionary 

As like *sset()*, the sorted dictionary gets created by the *sdict()* function (which has all the keys sorted) :

```js
(zoomba)sd = sdict()
{} // ZMap
(zoomba)for( [0:5] ){ k = random(100) ; sd[k] = k } 
21 // Integer
(zoomba)sd
{21=21, 31=31, 32=32, 43=43, 93=93} // ZMap
```

#### Ordered Dictionary 
In the same way as *oset()* we have ordered dictionary: *odict()*.
In ordered dictionary, the keys insertion order is preserved.

```js
(zoomba)od = odict()
{} // ZMap
(zoomba)for( [0:5] ){ od[$] = $ } 
4 // Integer
(zoomba)od 
{0=0, 1=1, 2=2, 3=3, 4=4} // ZMap
```

### Range 
A range is a lazy iterator. The syntax generally is :

```js
[start_included : end_excluded : optional_spacing ]
```

This is how it works (default spacing is 1):

```js
r = [0:10] // a range from 0 to 10, not including 10
lazy_list = r.asList() // gets the range as a lazy list 
l = r.list() // as list, now, evaluating everything 
```

#### In Reverse
Reverse of *start* and *end* works: 

```js
rr = [10:0] // a range from 10 to 0, not including 0
l = rr.list() // [10,9,...,1] 
```

However, there is ```reverse()``` function which actually reverses the range, and ```inverse()``` function which inverts the range. What are these?

##### Reverse
Creates a range that reverses the range:

```js
(zoomba)r = [0:10]
[0:10:1] // NumRange
(zoomba)r.reverse
[9:-1:-1] // NumRange
```

##### Inverse
If you switch [a:b] to [b:a] you have inverse:

```js
(zoomba)r.inverse
[10:0:-1] // NumRange
```

#### Spacing 

The default *spacing* is of 1. For a reverse range, the spacing is -1.
One can specify the spacing parameter:

```js
r = [0:100:2] // a range from 0 to 100 with a gap of 2
r = [10:-6:-3] // a range from 10 to -6 with a gap of 2
```

#### Numeric 

 The same one we discussed. Here is one with spacing:

```js
r = [0:10:2] // [ 0 , 2, 4, 6, 8 ] when yielded 
```
#### Character Range
Character range has end included. The spacing is 1, as default.

```js
cr = ['a':'z' ] // inclusive of 'a' and 'z'
cr.string  // "abcdefghijklmnopqrstuvwxyz" 
```

#### Date Range        
The default spacing is a day.

```js
st = time('19470815', 'yyyyMMdd' ) // Indian Independence Day 
et = time('19500126', 'yyyyMMdd' ) // Republic Day 
dr = [st:et] 
```

#### Iterating over Range
Is as easy as :

```js
for ( range ) {  println($) } // $ is the default iteration variable 
```
this, has been discussed earlier.

#### Extended Range - XRange

Sometimes, when one tries to work with seriously large numbers, 
one must deal with ranges not possible to cover by bounded integers.
Thus, XRange comes into foray. This is not different from the ordinary range syntax, simply identified by type. Thus :

```js
r = [0:10] // ordinary number range
xr = [0:10L] // note 'L' for large and Large Int, it is xRange  
```

 One can use it as precisely as the ordinary range, but this has no *list()* equivalent for obvious reasons. Like ranges, this is a *ListIterator* and thus capable of traversing both forward and backward direction. If either start or end of a range is a Large integer, the range automatically becomes an extended range.

Do not ever convert them into list, because, well, you have no idea how large they really are. Their size might be well beyond integer capability. 

### Heap And Priority Queue
ZoomBA has a fixed size priority queue : heap. The default implementation is max-heap : stores the smallest *k* numbers in the heap:

```js
(zoomba)h = heap(2) // a heap of size 2 
h<nil|[  ]> // ZHeap
(zoomba)for ( [0:5] ) { x = random(100) ; println(x) ; h += x }
45
20
9
71
49
h<9|[ 20,9 ]> // ZHeap
```

Default heap size is curiously made as *42*. We have no idea why we chose this, but probably it is because *42 = (6 X 9) base 13* . Top of the heap is given by :

```js
    (zoomba)h.top
    9 // Integer 
```

In the same way one can create the min-heap, where we store the largest of the *k* numbers :

```js
(zoomba)h = heap(2,true) 
h<nil|[  ]> // ZHeap
(zoomba)for ( [0:5] ) { x = random(100) ; println(x) ; h += x }
47
43
24
39
27
h<43|[ 43,47 ]> // ZHeap 
```

The different functionalities of heap structure can be seen easily with the following code (which is a max heap) :

```scala
h = heap(2,true)
for ( [0:5] ){
  x = random(100) // random int between 0 to 100 
  println(x)
  h += x // add to the heap 
}
println(h) // show heap 
println('min : ' + h.min ) // whats the minimum entry ?
println('max : ' + h.max ) // whats the maximum entry ?
println('top : ' + h.top ) // which entry is at the top ?
```

Which produces :

```
76
1
68
42
72
h<76|[ 72,76 ]>
min : 72
max : 76
top : 76
```

ZoomBA also has a priority queue which is a wrapper over Java's. To use that one can use :

```js
pq = pqueue() [ where { /* comparator function */ } ] 
```
without the comparator function the queue would be prioritised by comparing elements. Thus, suppose we have this :

```js
(zoomba)l = [10,1,23,12,15,0,3]
@[ 10,1,23,12,15,0,3 ] // ZArray
(zoomba)pq = pqueue()
 // PriorityQueue
(zoomba)for ( l ) { pq += $ }
0,10,1,12,15,23,3 // PriorityQueue
(zoomba)pq.peek
0 // Integer
(zoomba)while ( !empty(pq) ){ println(pq.poll) }
0
1
3
10
12
15
23
```

### Pattern Types 

A typical pattern is as follows:

```scala
"hello" =~ "^h.*$" // true
```

To access fine grain control one should use Pattern Literal.

```scala
"hello" =~ ##HEL#ig // find any "hel" (i)gnoring case and (g)lobally 
```



## Initializing Types 

The operator function *new()* exists to create new types and for compatibility with underlying JVM, if one needs to create an instance of an object. 

### new 

The syntax is simple:

```js
new (class_specifier, args...)
```

Some examples follow:

```js
hhgg = new ( 'java.lang.Integer', 42 ) 
s = new ( 'java.lang.String' , 'I like ZoomBA')
```
The operator function `new()` creates an instance of a type. 
This has implication on any shcematic type - for example define functions.
We shall talk about it then.

## Type Matching

### `isa` Operator

The operator is `isa` and it can do these:

```scala
1 isa "int" // true 
2.0 isa "float" // true 
2.0 isa "int" // false
true isa "bool" // true 
{:} isa "map" // true 
[1,2] isa "list" // true 
time() isa "time" // true 
set(1,2) isa "set" // true
"abc" isa "str" // true 
s isa ##java.lang.String# // true ( pattern matching on class name )
s isa "java.lang.String" // true ( full qualified class name )
s isa ##string#ig // true ( global find, ignore case)
s isa ##string#i // false
```

