# Functions

For being modular, functions are needed.
They are algorithms, which maps inputs to outputs, 
both being vectors.

> function : Input --> output 

Mathematically :
$$
f : X \to Y
$$


## Definition

ZoomBA has functions as first class entities.
They are entities by themselves, and do not require
any other entity. Functions are defined using *def* :

```js
  def hello(){
     println("Hello!")
  }
  // call 
  hello()
```

Arguments can be passed on : 

```js
  def hello(arg){
     println("Hello!" + arg)
  }
  // call 
  hello("ZoomBA!")
```

 Multiple arguments can be passed on using comma:

```js 
def say_hello(message, to){
 printf("%s , %s %n", message,to)
}
// call 
say_hello("Hello" , "ZoomBA!")
```


## Variable Scope
Scope rules are simple:

* Every variable defined outside the function 
   * stays read only 
   * One can override them by using variable of same name
* Every variable defined inside the function 
   * read/write 
   * Can override same name variables from outside
* Global variables 
   * are read write everywhere    

To demonstrate:

```js
def p_x(){
    println(x)
}
x = 42
p_x() // prints 42 
```

But, now, when we change the x :

```js
def p_x(){
    x = 99 // creates local, not overrides global
    println(x)
}
x = 42
p_x() // prints 99 
println(x) // prints 42
```

Thus, a local override will always create a local variable, 
which would then shadow the global one. 

### Mutability

Plainly speaking, variables are not mutable, unless on the reference you choose to do a mutable operation.
However, with the global variable, it won't happen that way: 

```js
def p_x(){
    $x = 99 // changes global
    println($x)
}
$x = 42 // global var
p_x() // prints 99 
println($x) // prints 99
```

Onto the reference of it, mutable operations are, well, mutable. They have the right to change the variable in question. For a primitive - mutability is not much, but for a collection, it is.

```js
def mutate(){
   x += 10 // effectively <-> x.add(10) ; x ;
   println(x)
} 
x = list(0,1)
mutate() // prints [0,1,10] 
println(x) // prints [0,1,10] 
```

This pose a problem. The nature of collections such that 
due to memory storage issue, mutability is used over immutability. And that one had to keep in mind.


## Arguments

### Positional Args
Generally, a function *def f(a,b)* when invoked with  *f(4,2)* implies that *a=4,b=2*. Thus, the ZoomBA maps
the arguments by positions. This mapping, is baptised by 
the jargon guys as Positional arguments.
Thus:

```js
 def f(a,b){ printf('a=%s,b=%s\n',a,b) }
 f(4,2) // will print a=4,b=2      
```

### Default Args
It would be a crime, in a modern language, if we did not
provide for named arguments, that is, you pass default 
into the arguments :

```js
def f(a=4,b=2){ printf('a=%s,b=%s\n',a,b) }
f(14,12) // will print a=14,b=12 
f(a=10) // will print a=10,b=2
f(b=10) // will print a=4,b=10
f(b=10,a=9) // will print a=9,b=10
f() // will print a=4,b=2
```

Obviously, one can change order in the default arguments, but you can not mix up positional arguments with default, 
named arguments.

### List Args

It should be evident by now that there are really no formal args for a ZoomBA function. All functions can take arbitrary number of arguments. These arguments are
accessible by the specific syntax:

```js
def f(){ println( @ARGS ) } // perl like 
f() // nothing 
f(1,2,3) // prints [ 1,2,3 ]
```

Thus, one can assign it back to any variable:

```js
def f(){ a =  @ARGS[0] ; println(a) } 
f(1,2,3) // prints 1
```

In the same way, one can make a function call using @ARGS:

```js
def f(){ println( @ARGS ) }  
arr = [1,2,3]
f( @ARGS = arr ) // prints [ 1,2,3 ]
```

This way, you can call a method using a list or an array.  

#### A Better Permutation 

With the *@ARGS = arr*  trick, one can actually create a better permutation 
program :

```js
 col = [1,2,3]
 args = list( [0:#|col|] ) as { col } // create the args 
 join( @ARGS = args ) where { #|set($.o)| == #|$.o| } // argument override     
```

### Dictionary Args

A generalisation from the previous is named arguments.
Given one can pass a dictionary into the function:

```js
def f(a){ println( a ) } // wait for it 
d = { 'a' : 42 } 
f( @ARGS = d ) // prints 42
```



### Unpopulated Arguments

What happens when an argument is not passed? Example:

```js
(zoomba)f = def(a) { println(a) }
at line 1, cols 5:25 --> def(a) { println(a) } // ZScriptMethod
(zoomba)f()
zoomba.lang.core.types.ZException$Function: parameter 'a' is not assigned!: in method : f : at line 1, cols 1:3 // Function
```

That is, it would cry foul, saying a parameter is not populated properly. In this way, it differs from JavaScript and is a bit more Pythonic.

## Return Value

A function returns the result of the last executable statement of the method block.Thus :
​    
```js    
def f(){
  i = 0 
}
x = f() // x = 0 
```

Also, *return* can be used explicitly to return from a function. 
Generally, that is needed when we require a branched return.
That is precisely the next topic.    

## Recursion

ZoomBA supports recursion. It is simply not clever enough to optimise tail call. But, yes, you can write recursive functions:

```js
 def factorial(n){
    if ( n <= 0 ) return 1
    return n * factorial(n-1) 
 }
 println( factorial(40 ) )
```

The point is, in ZoomBA, recursion is not divine. Business programming 
should not require recursion, neither does test automation. For most
of the cases - one can replace elegant recursion with elegant folds.

### Avoiding Recursion 
We start with Fibonacci series. With modern extension :

```js
def Fib(n){
    if ( n == 0 ) return 0
    if ( n == 1 ) return 1
    return ( Fib(n-1) + Fib(n-2) ) 
}
```

Observe the problem is that of a shifted / lagged term.
So, instead of keeping one variable, we should use 2. Thus:
​    
```js
// Big Endian notation : LOL 
#(big,small) = fold([2:n+1], [1,0] ) as { 
     #(b,s) = $.p
     $.p.1 += $.p.0 
     $.p.0 = b  
  }  
println(big) // holds Fib(n) 
println(small) // holds Fib(n-1) 
```

Another nice way of doing it is by using *list()* collector:

```js
fibs = list( [0:n+1] ) {
    continue( $.o <= 1 ) { $.o }
    $.p[-1] + $.p[-2] // precise definition of Fib 
}
println( fibs[n] )
```

### Maximum Recursion Depth 

It is system dependent. On our 2014 MacBookPro Retina with default memory allocation for JVM heap, the recursion ran for n ~ 540. It broke around 550.
Clearly tail call optimisation is the mission on the cards, if we choose to accept it. The message, sure does self destruct in the value addition front.
If you seriously need to use recursion, give it more memory.

## Nesting

Functions can be nested. A function can be defined inside a function.
Therefore, this sort of code is perfectly legal :

```js
 def parent(){
     def child(){
        println("I am child") 
     }
     child()
 }
 parent() // I am child 
```

But this is not legal to call child() on main script body :

     child() // error here, no function in scope!

### Partial Functions
ZoomBA supports partial functions, albeit, we have no idea why we need it, 
at all for business programming. This is one very concocted example:

```js
 def x_pos(x){
     def y_pos(y){
        return [x,y] 
     }
     return y_pos
 }
 yp = x_pos(4) // returns a partial function 
 #(x,y) = yp(2) // x = 4, y = 2 
```

Never use it, it actually means nothing useful. 
Functions with hidden states are too close to the [proverbial apple](https://en.wikipedia.org/wiki/Original_sin).

### Currying

Replacing one by one parameter, and recalculating the result, that is currying. Sure thing, ZoomBA has it. Observe the problem of say, binary operations. It is bad to write the code entirely like :

```js
 def binary_op(op, a, b ){
   switch ( op ){
     case '+' : a + b 
     case '-' : a - b 
     case '*' : a * b 
     case '/' : a / b 
     case @$ : raise('Oops! I did it again!")
   } 
 }
```

But, yes, business code is almost entirely written that way.
Well, no more. Welcome to this :

```js
result = #"a #{op} b"
```

The *#* followed by string is the operator, which generally is a string, unless it sees the *#{}* part of the expression, which it then evaluates
as sub expressions. Eventually it evaluates the whole expression one last time. So, what happens?

```js
 a = 10; b = 20; op = "+" 
 "a + b" // after first substitution of #{op} with "+" 
 //That is now executed as an expression
 30         
```

### String Formatting And Substitution 
The executable string #"abc" can be used to format strings and substitute
for simpler purposes. As an example :

```js
(zoomba)a = 10
10 // Integer
(zoomba)b = 20
20 // Integer
(zoomba)#"a is #{a}, b is #{b}"
a is 10, b is 20 // String
```

The substitution happened, and the string is formatted too. For string formatting, *str()* function can be used :

```js
(zoomba)fs = str("%d is good", a )
10 is good // String
```

## Eventing

Eventing is about asynchronous method calling. The basic idea is :

 * Add a handler (another method) to the method on which you want to snoop
 * The system notifies you by calling the handler method
    * before the method gets called
    * after the method completed calling ( including error )  

Doing so would require to understand methods as variables.

### Methods as Variables

ZoomBA has methods as first class objects, so, you can in effect store methods in variables: 

```js
method_add = def(a,b) { a + b } // assign to a variable  
```

Note that there is no name of the function,  it is bounded to the variable.
When we define a method, we also create a variable of the same name in ZoomBA,  thus :

```js
 def add(a,b){ a + b }
 x = add // valid, x gets the method add   
```

With this, we would go to method handlers.

### Handlers

Every method has two list of handlers, assigned to them. 

* before
* after 

To use them one can do this :

```js
def add(a,b) { a + b }
before_handler = def(){  printf( "before: %s\n", str(@ARGS ) ) }
after_handler = def(){  printf( "after: %s\n", str(@ARGS ) ) }
add.before += before_handler
add.after += after_handler
add ( 40, 2 )
```

The result comes in console :

```js
    before: { "m" : "add -> at line 1, cols 1:22" , "a" : [ 40, 2 ] , "e" : "zoomba.lang.core.types.ZException$Return: Unknown, No idea!"
    after: { "m" : "add -> at line 1, cols 1:22" , "a" : [ 40, 2 ] , "e" : "zoomba.lang.core.types.ZException$Return: Unknown, No idea!"
```

As we can see, the "m" signifies method information. "a" is the arguments which were passed into the function. "e" would be any exception that gets generated while executing the original function. A proper Return ( which it is in this case ) signifies, no error.


## Functions as Variables

Functions can be passed as anonymous functions, or as a variable.
This is entirely legal code :

```js
func = def(x){ println('hello : ' + x) }
d = { "some" :  func }
d.some('ZoomBA') // prints --> hello : ZoomBA 
```

### In Lambdas 
Functions can be used in lambdas and anonymous functions, like this:

```js
l = [0:5] 
ll = list(l) as def(index, item){ item ** 2 } //[0,1,4,9,16]
sl = select(l) where def(index, item) { item >= 3 } // [3,4]  
def my_selector(index, item){ item >= 2  }
sl = select(l) where my_selector  // [2,3,4]
```

#### The Lambda Protocol 
For a function to be used in lambda, the following protocol is maintained:

```js
def lambda(item, index, context, partial ){ /* body here */ }
// for no argument lambda 
def no_arg_as_lambda(){
  index = @ARGS.0 
  item = @ARGS.1
  context = @ARGS.2 
  partial = @ARGS.3 
}
```

Thus, suppose I want to select all items whose index is even :

```js
 select(collection) where def(){  @ARGS.0 % 2 == 0 }    
 select(collection) where def(index){  index % 2 == 0 }    
```

### Functions As Iterator

Functions can be acted as iterator. The protocol is :

> 1. As long as the function returns a value, it is continued.

> 2. If the function *breaks* , the iterator is finished. 

Suppose we want to create a *next\_factorial* iterator through function.

```js
// Immoral : Global States
$last_factorial = 1
$cur_n = 0
// read only, should be fine
halt_num = 6
// the function can now be used as iterator
def next_factorial(){
   break( $cur_n == halt_num )
   $last_factorial *= ( $cur_n += 1 )
}
// showcasing the factorial
for ( next_factorial ){
   println($)
}    
```

We can improve the strategy by putting the globals into a single map:

```js
// Just a bit sin : Global States
state = { 'last_factorial' : 1,  'cur_n' : 0 , 'halt_num' : 6 }
// the function can now be used as iterator
def next_factorial( ){
   break( state.cur_n == state.halt_num )
   state.last_factorial *= ( state.cur_n += 1 )
}
// showcasing the factorial
for ( next_factorial ){
   println($)
}    
```

## Functions as JVM Functional Types

In ZoomBA a function defined in a script is actually a `type` and one 
can create an instance. In fact, that is how functions gets called, 
the runtime creates instances and then appply arguments to run the instance.

### Functions as Parameters 

This is automatically done - and this anytime you are passing functions,
you are not, you are actually passing instances created when the 
function arguments were passed. Our friend `type()` comes in handy:

```scala
def func(x){x} 
println(func)
def higher_order(f,a) {
  println( type(f))
  f(a)
}
higher_order(func,42)
```
what happens?
The first print statements points to `ZScriptMethod` 
while the `println` inside the `higher_order` function prints `ZScriptMethodInstance`.

A `ZScriptMethodInstance` is actually a JVM `@FunctionalInterface` .

One easy default instance is our friend `as {}` and `where {}` blocks. 
We can start passing them around to anything JVM - here, used as `Comparator` in JVM:

```scala
import java.util.PriorityQueue as PQ
pq = new ( PQ , as { -$.o } )
println(pq)

```

### Creating Instance Manually 
But sometimes we need a bit more clean way to create instances.

```scala
ins = new ( func ) // works!
```
works, because as pointed out `ScriptMethod` are actually `Type/Schema` for computations.
It has the entire closure along with it, hence it is never a pure function,
unless you intend it to be.

### JVM Compatibility 

You actually can run this code:

```scala
l = [1,2,3] 
l.forEach() as { println($.o) }
```
Over JRE-9 you need to open the module up as follows to reflectively access `Stream` :

```
java --add-opens java.base/java.util.stream=ALL-UNNAMED -jar zmb.jar 
```
We could have very well pass it inside the `forEach` too:

```scala
l = [1,2,3] 
l.forEach( as { println($.o) } )
```
or create a singe param `Consumer`: 

```scala
def consume(x){ println(x) }
l = [1,2,3] 
l.forEach( consume )
```
and it would still work.
We can even mix it:

```scala
l.forEach() as consume 
```
#### Comparators 

`as {}` are `mappers` where as `wherer{}` are predicates. So,  they work as such. For example if we were to find out the maximum length string from a bunch of strings :

```scala
l = [ "hi", "hello", "43", "boom!" , "this is a winner"]
#(,max) = minmax(l) as { size($.o)  } // max = "this is a winner" :: Python field style 
#(,max) = minmax(l) where { size($.l) - size($.r)  } // max = "this is a winner" :: Java Comparator Style
#(,max) = minmax(l) where { size($.l) < size($.r)  } // max = "this is a winner" 
#(,max) = minmax(l) as def(x){ size(x)  } // max = "this is a winner" :: Python field style 
#(,max) = minmax(l) where def(x,y){ size(x) - size(y)  } // max = "this is a winner" :: Java Comparator Style
```

And this should explain things down. Now then one can pass default comparators to do the same:

```scala
l = [ "hi", "hello", "a", null ]
#(min) = minmax(l) as @cmp_jJn // min = "a" last null, then ascending Java Style 
#(min) = minmax(l) as @cmp_Jjn // min = "hi" last null, then descending Java Style 

```

If you notice you would see that the `code` for the comparator is passed down post `@cmp_` . The following can be combined in any way:

1.  `n` specifies the position of null
2. `jJ` specifies Java style ascending 
3. `Jj` specifies Java Style descending 
4. `zZ` specifies ascending ZoomBA comparator - this can actually compare stuff way better - for example does not get confused between `int` and `double` 
5. `Zz` descending ZoomBA comparator

We can choose to use Pure JVM style comparator at our own peril:

```scala
#(min) = minmax(l) as @cmp_Jj
//zoomba.lang.core.types.ZException$Function: Cannot read field "value" because "anotherString" is null: in method : minmax 

l = [0, 2.0, 3, 40l ]
#(min) = minmax(l) as @cmp_Jj
//zoomba.lang.core.types.ZException$Function: class java.lang.Double cannot be cast to class java.lang.Integer (java.lang.Double and java.lang.Integer are in module java.base of loader 'bootstrap'): in method : minmax 

```

So in short - unless you are very sure - please please please avoid JVM style comparator.

And that is why `ZoomBA` style comparator exists :

```scala
l = [0, 2.0, 3, 40l ]
#(min) = minmax(l) as @cmp_zZ // min = 0 
```

Now even `zZ` would have a problem dealing with `null` so .. use a variant like `nxX` or `xXn`  or whatever.

## Composition of Functions

Functions can be composed to create a new function. Now then this is not a new concept - like at all.

```scala
def successor(a){ a + 1 } // one method 
def add_two( a ) {  successor( successor(a) ) } // yes composition 
```

Now we got the idea. This is sort of sad that we need to write all of these down in the `infix` notation of sorts matching brackets.  And that to `compose` functions we need to define functions with `def`  and then keep on doing it.  What we are looking at is a rudimentary `data pipeline`.  

ZoomBA syntax makes it look like so:

```scala
add_two = #< successor | successor >
```

That is it. Now you can all it:

```scala
add_two(40) // returns 42 
```

One can naturally compose `def` ined functions this way but what about a bit .. on the prompt composition that comes to your mind?  For example  one can do this in scala:

```scala
extension(num : Int):
  def isEven  : Boolean = num % 2 == 0 
  def squared : Int = num * num 

extension(str : String):
  def toInt  : Int = str 
                     .toCharArray
                     .map( _.toInt )
                     .sum 
"Hello World"
   .toInt()        // 1052
   .pipe(_ + 1)    // 1053
   .squared()      // 1108809
   .isEven         // false 
   .pipe(println)  // prints false

```

And we want to `replicate` the same sans all the clutter scala has. Simple:

```scala
p = #< { sum($.0.toCharArray) as { int($.o) } } | { // 1052
           $.0 + 1 } | { // 1053
          $.0 ** 2 } | { // 1108809
          2 /? $.0 } | { println($.0) } >

p("Hello World") // done  
```

Now how and why this works?  Observe that there is this implicit `@ARGS` so - the system is transferring the arguments to each `block function` where the `$ = @ARGS` , in line with our implicit iteration variable `$`. 

#### Explode Operator

Now there would be cases where we would need to pass actual list - and there would be impedance mismatch. To solve that problem, there is explode operator `*`  .  

To demonstrate let's take the toy case of a very irrelevant `min_of_3` function:

```scala
def min_of_3(a,b,c){
    #(min) = minmax(a,b,c) // yes, we know 
    min // yes we know 
}
```

And suppose some function supposed to generate the input to this say by selecting random no from somewhere:

```scala
p = #< { list([0:3]) as { random(100) } }* | min_of_3  >
```

Notice the `explode` operator which says do not box the output, pass it as is `exploding` to the next function in pipe. 
