# Appendix

This documents all `ZoomBA` functionalities alphabetically with syntax.

## ##  `#` 

These are plugins.

### #atomic 

```
r = #atomic{  /* code that requires synchronization */ }
```

### #clock 

```
#(t,o) = #clock{ /* body whose execution time needs to be measured */ }
```

### #def 

```
obs = #def([mut=false]){ /* the expression body to create observable */ }
```

When `mut` is `true` mutate the environment while evaluating the expression. 

### #retry 

```
r =  #retry( [strategy='counter', 
            max=0, 
            interval= Long.MAX_VALUE,
            tracing=false, 
            err= Retry.NOP.errorHandler() ] ){ 
/* block to be retried */
}
```

#### Strategy 

One of  `[ "counter", "random" ,"exp" ]`

### #timeout 

```
r = #timeout(interval=-1, [inline=false, named_args]) { /* code does not complete within that err out */}
```

When `inline` is `true` - runs in current thread. That makes it really hard to control - one must take precaution to ensure the block is `interrupt aware` .
