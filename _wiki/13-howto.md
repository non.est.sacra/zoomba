# How to Dance in ZoomBA Tune
This page is a list of *gotcha!* you may need to use ZoomBA.

## Instrumented Build
There is an instrumented build for zoomba.
We have to use the instrumented build.
[instrumented,debuggable snapshot](https://oss.sonatype.org/content/repositories/snapshots/org/zoomba-lang/zoomba.lang.core/INSTRUMENTED-0.3-SNAPSHOT/)
This gets used for finding code coverage in ZoomBA as well as debugging zoomba scripts.

### Sample Code 
Following sections would rely on the codes in the samples directory:
1. [import_test.zm](../samples/test/import_test.zm)
2. [tbi.zm](../samples/test/tbi.zm)


## Code Coverage in ZoomBA
We have to use the instrumented build.
Once we have the coverage build we can create an alias `zmc` :

```
zmc='java --add-opens java.base/jdk.internal.loader=ALL-UNNAMED -Dzoomba.intercept=true -jar zoomba.lang.core-INSTRUMENTED-0.3-SNAPSHOT-onejar.jar'
```
And once we are done, we can now run any script with this command:

```
zmc import_test.zm
```
It would create a coverage file in the same directory location where the command was issued:

```shell
ls *.txt 
2024_10_21_11_22_12_326_coverage.txt
```
As you can see, the coverage has the time stamp upto ms in the name itself.
Now, if you open the file:

```jsonc
{
	"blocks":{
		".../import_test.zm":{
			// no blocks were found in this file 
		},
		".../tbi.zm":{
			"5:10 to 7:1":true,
			"9:15 to 13:1":true
		}
	},
	"lines":{
		".../import_test.zm":{
			1:true,
			3:true,
			4:true,
			5:true
		},
		".../tbi.zm":{
			1:true,
			3:true,
			5:true,
			6:true,
			7:true,
			9:true,
			10:true,
			11:true,
			12:true,
			13:true,
			15:true
		}
	},
	"methods":{
		".../import_test.zm":{
			
		},
		".../tbi.zm":{
			"5:1 to 7:1":true,
			"9:1 to 13:1":true
		}
	}
}                    
```
Things which are covered would be marked as `true`. 
ZoomBA coverage works on lines, methods, and blocks.


## Debugging in ZoomBA
We have to use the instrumented build.
Once we have the coverage build we can create an alias `zdb` :

```
zdb='java --add-opens java.base/jdk.internal.loader=ALL-UNNAMED -Dzoomba.debug=4242 -jar zoomba.lang.core-INSTRUMENTED-0.3-SNAPSHOT-onejar.jar'
```
And then issue the command:

```
zdb import_test.zm 
Debug Server Starting at : 4242 
Waiting for debug client connection...
```
This would start the ZMB debug server in the TCP port 4242.
Why `4242` cause we passed that as  `-Dzoomba.debug=4242`.

Now at this point one can use any network client to TCP port `4242` to start debugging!
We shall use `netcat` or `nc` command to demonstrate debugging.

```
nc localhost 4242
bp  - sets  break point  
cb  - clears  break point  
ex  - executes expression in debugger 
st  - shows current stack trace 
lb  - list all break points 
lm  - list all loaded modules 
ls  - list source code 
lv  - list context variables 
lg  - list global variables 
c   - continues in debugger 
h   - shows this help 

(zdb)

```
You would be greeted with bunch of arcane commands - which now you can use to debug the program.

### Break Points

The way debugger works, the modules ( zoomba scripts ) it keeps on loading it gives load index number from 1.
Line numbers are also from 1.
So the first loaded modules will be the script started with the `zdb`.

To set a breakpoint, simply put `module_index:line_no` :

```
bp // 1:3         
true
(zdb)
```
This puts the breakpoint in the file `import_test.zm` at line no `3`.
Note the curious use of `//` to separate out command words. That is lazy but effective way to collect commands.

Now to continue running we use :

```
(zdb)
c
continue!
(zdb)
class zoomba.lang.parser.ASTMethodNode --> .../import_test.zm:3:3 to 14

```
And if we do, we reach the breakpoint immediately.
Now we can inspect various things really, or choose to list the loaded modules `lm` ( scripts ):

```
(zdb)
lm
.../import_test.zm : 1
.../tbi.zm : 2
(zdb)

```
So it says, it loaded 2 modules at this point and listed down the load indexes.
Now, say we want to put a breakpoint in the `tbi.zm` and that too in the `hello()` method.
So, we are going to do just that:

```
(zdb)
bp // 2:10
true
(zdb)
c
continue!
(zdb)
class zoomba.lang.parser.ASTMethodNode --> .../tbi.zm:10:3 to 25
```
And we are broken again.

### Viewing Stack 
At this point looking at stack trace is a good idea, so let's just do that:

```
(zdb)
st
|- hello --> .../import_test.zm:3:3 to 14
(zdb)

```
Ah. So indeed `hello()` got called from `import_test`.
Perhaps we now want to check the variables? That is easy with `lv` :

```
(zdb)
lv
hi
@ME
hello
SYS
__FOO__
(zdb)

```
And now we just find out the breakpoints we have with `lb`:

```
(zdb)
lb
1:3
2:10
(zdb)

```
And finally, we clear the break points, all of them:

```
(zdb)
cb // all
true
(zdb)
```
And now simply continue... and close!

```
(zdb)
c
continue!
(zdb)
Process Exited!
```
And that concludes our debug session!

## How do I 

### print an object 
There are many ways. Obviously there are :
```
println(obj)
printf(obj)
```
#### As JSON 

but if you choose to see a json like output :

```
(zoomba)s = 'hi'
hi // String
(zoomba)str ( dict(s) , true )
{"serialPersistentFields" : [], "CASE_INSENSITIVE_ORDER" :  { "java.lang.String$CaseInsensitiveComparator" : "java.lang.String$CaseInsensitiveComparator@77a567e1" } , "serialVersionUID" : -6849794470754667710, "value" : [  { "java.lang.Character" : "h" } ,  { "java.lang.Character" : "i" }  ], "hash" : 0} // String
```

### Do File Operation 
The function ```file``` should be your friend. If you give it a text file, it is as if an iterator, if it is a directory, it traverses the files in the directory - returns ```java.io.File``` objects :

```
// list all files in a directory 
for ( f : file(directory) ) { println( f.name ) }
// list all lines in a file 
for ( l : file(text_file) ) { println( l ) }
```  

### Web IO 
The function ```read``` generally will help reading from web:

```
w = read('http://www.google.co.in')
println(w.status ) // HTTP status 
println(w.body) // the body 
```

### Demystify ```$``` syntax

In ZoomBA ```$``` comes in different ways.
It is the iteration variable. Here are different usage of how to use the special variable :

```
for ( [0,1,2,3] ) { println($) }  
```  
Can be used in fold like higher order function as such.





