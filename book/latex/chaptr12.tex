\chapter{Java Connectivity}\label{java-connectivity}

{\LARGE J}VM is the powering force behind ZoomBA.
This chapter illustrates various usage scenarios where ZoomBA can be
embedded inside a plain Java Program.

\begin{section}{How to Embed}

\begin{subsection}{Dependencies}
\index{maven}
In the dependency section (latest release is 0.2 ) : 
\begin{lstlisting}[style=XmlStyle]
<dependency>
  <groupId>org.zoomba-lang</groupId>
  <artifactId>zoomba.lang.core</artifactId>
   <version>0.3-SNAPSHOT</version>
</dependency>
\end{lstlisting}
That should immediately make your project a ZoomBA supported one.
\end{subsection}

\begin{subsection}{Programming Model}
\index{ java connectivity : programming model }
ZoomBA has a random access memory model.
The script interpreter is generally single threaded,
but each thread is given its own engine to avoid
the interpreter problem of \href{https://en.wikipedia.org/wiki/Global\_interpreter\_lock}{GIL}.

The memory comes in two varieties, default registers, which are used for global variables.
There is also this purely abstract 
\href{https://gitlab.com/non.est.sacra/zoomba/blob/master/src/main/java/zoomba/lang/core/interpreter/ZContext.java}{ZContext} type,
which abstracts how local storage gets used. This design makes  pretty neat to integrate with any other JVM language, specifically Java. 

Here is an example embedding.
\end{subsection}

\begin{subsection}{Example Embedding}
One sample embedding is furnished here :
\index{ java connectivity : sample embedding }


\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=myJavaStyle]
/* How to use ZoomBA Natively, i.e. not using JSR-232 */
import zoomba.lang.core.interpreter.ZContext;
import zoomba.lang.core.interpreter.ZContext.*;
import zoomba.lang.core.interpreter.ZInterpret;
import zoomba.lang.core.interpreter.ZScript;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.types.ZException;
import zoomba.lang.core.types.ZTypes;
//... can be more... 
public static void dance(String[] args) {
   String file = args[0];
   ZScript zs = null;
   try {
         zs = new ZScript(file, null);
   }catch (Throwable t){
       if ( t.getCause() instanceof ZException.Parsing ){
           ZException.Parsing p = (ZException.Parsing)t.getCause();
           System.err.printf("Parse Error: %s\n", p.errorMessage );
           System.err.printf("Options were : %s\n", ZTypes.string(p.correctionOptions) );
       }else{
           System.err.printf("Error in parsing due to : %s\n", t.getCause() );
       }
       System.exit(2);
    }
    Object[] scripArgs = ZTypes.shiftArgsLeft(args);
    Function.MonadicContainer mc = zs.execute(scripArgs);
    if (mc.isNil()) {
      // handle when return is ... Nil
    } else {
       if (mc.value() instanceof Throwable) {
           Throwable underlying = (Throwable) mc.value();
       if ( underlying instanceof ZException){
           if ( underlying instanceof ZException.ZTerminateException ){
                System.exit(3);
           }
           System.err.println(underlying);
           zs.consumeTrace(System.err::println);
           System.exit(4);
       }
       if ( underlying instanceof StackOverflowError ){
           zs.consumeTrace(System.err::println);
           System.err.println("---> Resulted in 'Stack OverFlow' Error!");
           System.exit(4);
       }
       underlying.printStackTrace();
       System.exit(1);
   }
  }
  System.exit(0);
}

\end{lstlisting}  
\end{minipage}\end{center}
\end{subsection}


\end{section}

\begin{section}{Programming ZoomBA}
Because it is JSR-223 compliant you can do this :

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=myJavaStyle]
public void javaxScripting() throws Exception {
    ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
    ScriptEngine engine = scriptEngineManager.getEngineByName("zoomba");
    assertNotNull(engine);
    Object result = engine.eval("2+2");
    assertEquals(4,result);
    Bindings bindings = engine.createBindings();
    bindings.put("x", 2 );
    bindings.put("y", 2 );
    result = engine.eval("x+y", bindings);
    assertEquals(4,result);
    ScriptContext scriptContext = engine.getContext();
    scriptContext.setAttribute("x", 2, ScriptContext.ENGINE_SCOPE );
    scriptContext.setAttribute("y", 2, ScriptContext.GLOBAL_SCOPE );
    result = engine.eval("x+y");
    assertEquals(4,result);
    CompiledScript compiledScript = ((Compilable)engine).compile("x+y");
    assertEquals( engine, compiledScript.getEngine());
    result = compiledScript.eval(bindings);
    assertEquals(4,result);
    // function invoke ?
    engine.eval("def add(a,b){ a + b }",scriptContext);
    result = ((Invocable)engine).invokeFunction("add", 2, 2);
    assertEquals(4,result);
    // method invoke ?
    Object zo = engine.eval(new StringReader("def ZO{ def add(a,b){ a + b } } ; new(ZO) ") ,scriptContext);
    assertNotNull(zo);
    assertTrue(zo instanceof ZObject);
    result = ((Invocable)engine).invokeMethod(zo,"add", 2, 2);
    assertEquals(4,result);
    // now attributes stuff
    assertEquals( 2, scriptContext.getScopes().size());
    assertNotNull(scriptContext.getAttribute("x"));
    final int scope = scriptContext.getAttributesScope("x");
    assertEquals(ScriptContext.ENGINE_SCOPE , scope);
    assertNotNull(scriptContext.getAttribute("y", ScriptContext.GLOBAL_SCOPE));
    scriptContext.removeAttribute("y", ScriptContext.GLOBAL_SCOPE);
    assertNull(scriptContext.getAttribute("y", ScriptContext.GLOBAL_SCOPE));
    assertNull(scriptContext.getAttribute("y"));
    assertEquals(-1, scriptContext.getAttributesScope("y"));
    // now let's eval a script... from external file
    zo = engine.eval("@samples/test/date.zm",scriptContext);
    assertNotNull(zo);
    compiledScript = ((Compilable)engine).compile("@samples/test/date.zm");
    assertNotNull(compiledScript);
    engine.put("y",10);
    assertEquals(10,engine.get("y"));
}
\end{lstlisting}  
\end{minipage}\end{center}


\end{section}

\begin{section}{Extending ZoomBA}

There are two extension points for Java developers
who are going to make their classes and functions ZoomBA ready.

\begin{subsection}{Implementing Named Arguments}
\index{ java connectivity : functions : named argument}

If one wants to implement a method which would mix properly with ZoomBA named arguments,
then one must accept a Java Map, whose keys are the name of the parameters while values are the values passed.

Thus, once we know that the argument is Map, we can extract the value, 
and do the due diligence.  Here is example from \href{https://gitlab.com/non.est.sacra/zoomba/-/blob/master/src/main/java/zoomba/lang/core/interpreter/ZMethodInterceptor.java#L723}{language itself}:

\begin{center}\begin{minipage}{\linewidth}
\begin{lstlisting}[style=myJavaStyle]
/* Say the call is : mset(col=l,sorted=true) */
static Map accumulatorMap(NamedArgs na){
   if ( Boolean.TRUE.equals( na.getOrDefault("sort", false ) ) ){
        return new TreeMap<>();
   }
   if ( Boolean.TRUE.equals( na.getOrDefault("order", false ) ) ){
       return  new LinkedHashMap<>();
   }
   Object o = na.get("acc");
   if ( o != null ) {
       if (o instanceof Map<?, ?>) {
           return (Map) o;
       }
       throw new IllegalArgumentException("'acc' is not a map!" );
   }
   return new HashMap<>();
}

\end{lstlisting}
\end{minipage}\end{center}
\end{subsection}


\begin{subsection}{JVM Functional Types}
\index{ java connectivity : function : functional argument }

If one wants to implement the anonymous blocks like almost all the ZoomBA functions do,
one needs to expect the last few argument to be  \emph{FunctionalInterface}:

\begin{lstlisting}[style=zmbStyle]
import java.util.ArrayList as AL
al = new ( AL, [ 1,2,3,4 ] )
al.stream.forEach( as { println($.o) }  )
\end{lstlisting}

As we can see this is a perfect way to incorporate anonymous args. 
However we can do better:

\begin{lstlisting}[style=zmbStyle]
import java.util.ArrayList as AL
al = new ( AL, [ 1,2,3,4 ] )
al.stream.forEach() as { println($.o)  }
\end{lstlisting}

As we can see, the system automatically recognizes the last few parameters 
to be functional interfaces and automatically adjusts.
However to grant reflective access to the \emph{stream()} we need to run it with the command line switch:

\begin{lstlisting}[style=all]
--add-opens java.base/jdk.internal.loader=ALL-UNNAMED  \
--add-opens java.base/java.util.stream=ALL-UNNAMED
\end{lstlisting}

\end{subsection}

\begin{subsection}{ZoomBA Plugins}
\index{ java connectivity : plugins : Plugin Functions }

\begin{subsubsection}{Basics of Syntax}
A ZoomBA plugin is characterized by \emph{\#} followed by a function style call, 
but all parameters passed are named. As you would now recall there are some default plugins 
already built in that you have encountered:

\begin{lstlisting}[style=zmbStyle]
#(t,o)=#clock{/* execution time*/} // time taken, output
#atomic{  /* atomic block with automatic revert mechanism */  }
\end{lstlisting}

There are some other plugins which we would discuss here. But before that, 
we would discuss about how said plugins can be written.

\end{subsubsection}

\begin{subsubsection}{Plugin Interface}
A plugin must conform to a particular interface \href{https://gitlab.com/non.est.sacra/zoomba/-/blob/master/src/main/java/zoomba/lang/core/interpreter/DSLPlugin.java#L38}{DSLPlugin}. 

\begin{lstlisting}[style=myJavaStyle]
/**
     * Process the plugin
     * @param interpret underlying interpreter
     * @param config configuration of the plugin
     * @param block actual plugin code block
     * @param data in case of any data needs to be passed
     * @return result of plugin execution
     * @throws Exception in case of any error
     */
    Object process(ZInterpret interpret, 
                 Map<String, Object> config, 
                 ASTBlock block, 
                 Object data) throws Exception;

    /**
     * Plugin Registry
     * Simple idea, add name and Plugin
     */
    Map<String, DSLPlugin> REGISTRY = new LinkedHashMap<>();
\end{lstlisting}


We need to do 2 things.
\begin{itemize}
\item{ implement this interface. }
\item{ Register the class by adding an entry in \emph{REGISTRY}. }
\end{itemize}

In fact here is the snippet from the same interface \emph{registering} some default \emph{Plugins}:

\begin{lstlisting}[style=myJavaStyle]
/**
  * Bunch of Default plugins to register
  */
 static void defaultRegistration() {
        REGISTRY.put("atomic", ATOMIC); // Atomic operations
        REGISTRY.put("clock", CLOCK); // perf tuning
        REGISTRY.put("def", OBSERVABLE); // reactive programming with zmb
        REGISTRY.put("retry", RETRY); // retrying a block of code
        REGISTRY.put("timeout", TIMEOUT); // timing out a block of code
    }
\end{lstlisting}

Now how to actually write some plugin code? Here is the \emph{Retry} plugin
along with the associated 
\href{https://gitlab.com/non.est.sacra/zoomba/-/blob/master/src/main/java/zoomba/lang/core/operations/Retry.java}{Retry} code:

\begin{lstlisting}[style=myJavaStyle]
/**
  * The retry construct
  */
DSLPlugin RETRY = (interpret, config, block, data) -> {
        Object handler = config.get(Retry.HANDLER);
        // special casing, in case it is defined and required
        if ( handler instanceof ZScriptMethod ){ 
            ZScriptMethod.ZScriptMethodInstance instance =
                 ((ZScriptMethod) handler).instance(interpret);
            config.put(Retry.HANDLER, instance);
        }
        Retry retry = Retry.fromConfig(config);
        Callable<Object> callable = retry.callable( () -> block.jjtAccept(interpret, data) );
        return callable.call();
    };
\end{lstlisting}

Next we are going to describe various default plugins.

\end{subsubsection}
\end{subsection}


\begin{subsection}{Atomic}
\index{plugin: atomic}
This plugin creates a local sandbox within which you can revert back any changes done on local variables
which are not references. This way you can simulate the \emph{synchronized} block, along with a primitive
rollback mechanism.

Observe the issue without any synchronized mechanism:

\begin{lstlisting}[style=zmbStyle]
def modify(){ $var += 1 }
$var = 0
tl = list ( [0:10 ] ) as { thread() as modify }
fold ( tl ) as { $.o.join() }
println( $var ) // max is 10 can be of any value
\end{lstlisting}

But the moment we do this:

\begin{lstlisting}[style=zmbStyle]
def modify(){ #atomic{ $var += 1 } }
$var = 0
tl = list ( [0:10 ] ) as { thread() as modify }
fold ( tl ) as { $.o.join() }
println( $var ) // always 10 
\end{lstlisting}
\end{subsection}

\begin{subsection}{Clock}
\index{plugin: clock}
A typical use case is as coded in \href{https://gitlab.com/non.est.sacra/zoomba/-/tree/master/samples/test/perf}{performance} test folder: 

\begin{lstlisting}[style=zmbStyle]
r = #clock{
  for ( [0:1000000] ){
    i = $
  }
}
return r.0
\end{lstlisting}

And this explains the usage of \emph{\#clock} block.


\end{subsection}

\begin{subsection}{Reactive}
\index{plugin: Reactive : def }

This starts with lazy evaluation. To evaluate any expression lazily, again and again
we use the construct:
\index{observable}

\begin{lstlisting}[style=zmbStyle]
lz = #def{ x + y } // creates an Observable 
x = 12
y = 30 
lz.value //  42 
lz + 10 //  52 
lz - 10 // 32 
10 + lz // error : can not do it 
println(lz) // it is 42
\end{lstlisting}

Now an \emph{Observable} is actually a \emph{Supplier} of values. 
But in our specific case, it is a supplier of new values if the expression value changed.
It has the same effect as the closure:

\begin{lstlisting}[style=zmbStyle]
lz = def(){ x + y } // creates a non closed supplier 
x = 12
y = 30 
println(lz()) // it is 42
\end{lstlisting}

That brings the question of what is real difference between these two then?
From a theory standpoint, not much. What can be done with an Observable, 
can be done with a non closed function. But in practice it gives us convenience.

Any time the value gets updated, there can be an event handler handling it for us:

\begin{lstlisting}[style=zmbStyle]
def update_handler( previous,current, object ){ 
  printf("[method] Observable updated --> %s --> %s %n", previous, current ) 
}
x = 20
y = 42
lz = #def{ x + y }
lz.update( update_handler )
lz.update( ) as {  
   printf("[lambda] Observable updated --> %s --> %s %n", $.p,  $.o )  
   }
println(lz) // event handlers get called because lz gets evaluated 
\end{lstlisting}

\begin{subsubsection}{ Lazy Evaluation of Assertion Arguments}
Observe this panic assert:

\begin{lstlisting}[style=zmbStyle]
panic( x != null ,  'x is not null and has length : ' + x.length ) 
\end{lstlisting}

What happens? There are two cases.
\begin{itemize}
\item{ x is not null. Then panic fires and the message string evaluates right. }
\item{ x is null. Now panic should not fire , but what happens to the message?
 Message needs to still evaluate or not? For regular programming languages, it would.
 And that would crash the panic because x is null!
 Now for ZoomBA it does not.
}
\end{itemize}

This is the lazy evaluation for assertion class of functions.
Unless the parameter needs to be evaluated, the parameters does not get evaluated, 
because for assertion class of functions, every argument is wrapped around inside an
Observable by design. In fact this is the primary reason ZoomBA has observable.

\end{subsubsection}


\begin{subsubsection}{ As Foundation of Reactive Paradigm}
Observe this  chain:

\begin{lstlisting}[style=zmbStyle]
db_data = #def{ /* get data from db */ }
// this is reactive paradigm 
user_change.update( ) as { user.data = db_data }
user_change = #def{ /* do something about user changes */ }
def login(user_name){
   /* new user came in */
   user = user_change
}
// finally 
login("zoomba")
\end{lstlisting}

What happens is a chain of evaluation.

\begin{itemize}
\item{ login triggers \emph{user\_change} }
\item{ On update of the Observable,  \emph{db\_data} gets assigned to user data }
\item{ That triggers evaluation of \emph{db\_data} itself }
\end{itemize}
 
And this chain can potentially go to any length.
This is the crux of \href{https://en.wikipedia.org/wiki/Reactive\_programming}{reactive paradigm}. 

\end{subsubsection}

\end{subsection}

\begin{subsection}{Retry}
\index{plugin: Retry}

One of the crucial component of resiliency is able to retry.
Consider connection to a website to download an image.
And it might fail. So we need to try again. This is actually a 
\href{https://learn.microsoft.com/en-us/azure/architecture/patterns/retry}{pattern} 
for the design pattern seekers.

The question is, is there a clean way to solve this problem so that code is minimal.
Turns out, we can. Here is the code snippet to do so:

\begin{lstlisting}[style=zmbStyle]
RETRY_CONFIG = { "strategy" : "exp", 
                  "max" : 1,  
                  "interval" : 750 + random(1000) 
                   }
 #(o ? e ) = #retry (RETRY_CONFIG) {  download_image( image_url) }  
 !empty(e) && eprintln("Failed : " + image_url )  
\end{lstlisting}

This shows the key ingredient of \emph{retry} plugin.
First there is a configuration that we pass to configure the behavior.
Then it is just a matter of simple method call on the block itself like clock or atomic.

\begin{subsubsection}{Strategies}

There are 3 strategies in place:
\begin{itemize}
\item{ Counter : \emph{counter}- just till count reaches \emph{max} value with fixed \emph{interval} in ms. }
\item{ Random : \emph{random} - counter, but the interval is randomized. The \emph{interval} passed is the average.  }
\item{ Exponential Back off : \emph{exp} - counter, but the interval increases exponentially.  \emph{interval} is the first item}
\end{itemize}

Counter is the default strategy.

\end{subsubsection}

\begin{subsubsection}{As a Decorator Pattern}
As told, under the hood it is a \href{https://en.wikipedia.org/wiki/Decorator\_pattern}{decorator pattern}.
This \href{https://gitlab.com/non.est.sacra/zoomba/-/blob/master/src/main/java/zoomba/lang/core/operations/Retry.java}{code here} 
decorates any \href{https://docs.oracle.com/en/java/javase/21/docs/api/java.base/java/util/function/Function.html}{java.util.function.Function} to create another \emph{Function} but with
retry enabled. 

In fact this is how one can use the retry directly from Java itself:

\begin{lstlisting}[style=myJavaStyle]
Function<Integer,Integer> errFunc = (x) -> { throw new RuntimeException(); } ;
Map config = Map.of( "debug", true , "interval", 10000 , "max", 2 );
Retry retry = Retry.fromConfig( config );
Function<Integer,Integer> withRetry = retry.function(errFunc);
\end{lstlisting}

The same is actually applicable for \emph{Timeout}.  
\end{subsubsection}

\end{subsection}

\begin{subsection}{Timeout}
\index{plugin: Timeout}

For ensuring users do get into a response rather than being \emph{hung,stuck} in a computation loop,
\emph{timeout} of any operation is needed. The trouble is, of course - how to timeout an operation?
There are two different ways to get this thing.

First of them is to use a separate thread to run the computation and then join the current thread 
so there is literally a block until the computation thread either times out or produces output,
either in terms of result or an error.

The other one is a bit more deranged and is required \href{https://en.wikipedia.org/wiki/Cooperative\_multitasking}{collaborative multitasking} - and implementing it right is real hard.

In ZoomBA we support both. 
Here is the use of \href{https://gitlab.com/non.est.sacra/zoomba/-/blob/master/src/main/java/zoomba/lang/core/operations/Timeout.java}{Timeout} decorator:

\begin{lstlisting}[style=myJavaStyle]
Timeout to = Timeout.fromConfig( Map.of( "interval", 10L, "inline", false ) );
 // regular usage
 final Function<Integer,Integer> t_id = to.function( id );
\end{lstlisting}


\begin{subsubsection}{Preemptive Timeout : Different Thread}
The configuration \emph{inline} defines the timeout strategy, \emph{false} implies
we would be using a different thread:

\begin{lstlisting}[style=zmbStyle]
def long_func(n){ /* some really long function based on n */ }
#(r ? e) = #timeout(interval=1000){ long_func(10) }
\end{lstlisting}

As one can see, the default is \emph{inline : false}.
The \emph{interval} is the amount of time in millisecond to wait before the operation gets timed out.
We must understand that the plugin block gets executed in a different thread,
so being a \href{https://en.wikipedia.org/wiki/Pure\_function}{Pure Function} helps.
We can use timeout like this with regular methods, but we should be careful about being the function pure.

\end{subsubsection}

\begin{subsubsection}{Collaborative Timeout : Same Thread}
This obviously does the inline or timeout in the same thread, however, there is a catch.

\begin{lstlisting}[style=zmbStyle]
def long_func(n){ /* some really long function based on n */ }
#(r ? e) = #timeout(interval=1000, inline=true){ long_func(10) }
\end{lstlisting}

The catch is, how does one interrupts the main thread?
This pose a \href{https://stackoverflow.com/questions/60905869/understanding-thread-interruption-in-java}{problem in any CLR language}. 

The solution is a bit tedious in case of ZoomBA - we need to write methods which are in some sense
\emph{interruptible} :

\begin{lstlisting}[style=zmbStyle]
def long_func(times, inline=false){
  for( [0:times] ){
    for ( [0:times] ){
      x = $
      // note this -- crucial
      panic( inline && thread().interrupted(), "must be co-operative!" )
    }
  }
  42 // return
}
\end{lstlisting}

What we are doing is forcing the current thread to panic on interrupt to relinquish control.
This is the key to how to do Collaborative Scheduling - or Co-Routines.

Hence the crux is to essentially \emph{catch interrupt} whenever we think it should be good enough.

\end{subsubsection}


\end{subsection}





\end{section}
