# ZoomBA README

ZoomBA syntax highlighter.

## Features

Syntax highlighter for ZoomBA scripts 

## Requirements

No further requirements.

## Extension Settings

## Known Issues

Multi line strings does not work right. 

## Release Notes

Initial draft version. 

### 0.0.1

Initial release 
