/*
 One multiline comment
*/
////////////// char ////////////////////
c = int(_'a')
println(c)
assert ( _'a' isa 'char' , 'char is not recognized as type' )
assert ( _'a' == char(c) , 'char conversion did not work' )
assert ( _'a' == char('a') , 'char conversion did not work' )
assert ( _'a' == char(_'a') , 'char conversion did not work' )
assert ( _'a' == char('',c) , 'char conversion did not work' )
assert ( empty( char() ), 'should not have produce anything!'  )
assert ( empty( char({:}) ), 'should not have produce anything!'  )
assert ( empty( char({:},[] ) ), 'should not have produce anything!'  )

///// others //////////

x = 10 // assignment
y = new ( 'java.lang.String' , 'Hello, World' )
a = [1,2,3,'Hi', false ]
assert ( x == 10 , 'Num assignment is failing!')
assert ( y == 'Hello, World' , 'String assignment is failing!')
assert ( size(y) == size('Hello, World') , 'String size is failing!')

assert ( a[0] == 1 , "Array Assignment has bombed!")
panic ( a[-1]  , "Array Assignment has bombed!")

d = { 1: 2 , 3: 4 , "foo" : "bar" }

assert ( d isa {:} , "Dictionary assignment failed!" )
assert ( size(d) == 3 , "Size on Dictionary failed!" )
assert ( d[1] == 2 , "Dictionary Access failed 1 !" )
assert ( d[3] == 4 , "Dictionary Access failed 2 !" )

assert ( d.foo.length == 3 , "Dictionary Access failed 3 !" )
assert ( d.1 == 2 , "Dictionary Access failed 4 !" )

import 'java.lang.Integer' as INT
assert( 12 == INT.parseInt('12') , 'Parse failed on Imported INT' )
// now do the error handling part...
#(o ? e) = INT.parseInt("**9797987981))")

assert( empty(o) , "Error did not happen!" )
assert( "parseInt" @ str(e) , 'Wrong exception, perhaps?' )

x = 1 + 2
assert(x == 3, "add did not work!")
x = 1 * 2
assert(x == 2, "mul did not work!")

x = 1 - 2
assert(x == -1, "sub did not work!")

x = 1 / 2
assert(x == 0, "int div did not work!")

x = 1.0 / 2
assert(x == 0.5, "float div did not work!")

x = 10 ** 2
assert(x === 100, "int pow did not work!")

x = 10.0 ** 2
assert(x === 100.0, "float pow did not work!")

x = 10 ** 2.0
assert(x === 100, "float pow did not work!")

x  = [0,1,2] * [true,false ]
assert(size(x) == 6, "col X col did not work!")

/* Test Assertions */
assert( "True should not fail in assert!" ) :: { true }
panic( "False should not fail in panic!" ) :: { false }
test( "This should not be logged" ) :: { true }

assert ( is(x) , "X is defined?" )
panic ( is(moron) , "moron is defined?" )

// infinite comparison
inf = num("inf")
ninf = num("-inf")

assert ( inf == inf , "Infinities are not equal" )
panic ( inf > inf , "Infinities are greater than one another!" )
panic ( inf < inf , "Infinities are less than one another!" )

assert ( ninf == ninf , "Infinities are not equal" )
panic ( ninf > ninf , "Infinities are greater than one another!" )
panic ( ninf < ninf , "Infinities are less than one another!" )

panic ( -999999999999999999999999999 < ninf , "a very small no can not be smaller than -infinity")
panic ( inf < 99999999999999999999999999999 , "large no can not be larger than +infinity")

c = _"A"
s  = 'A'
assert ( c isa s.0 , "Why A is not character?" )
assert ( c.equals ( s.0 ) , "Why A is not A?" )
#(o ? e ) = c.12 // should be error
panic ( empty(e) , "Accessing extra index did not generate error!" )
assert ( !empty(enum()) , "Enum returning null!"  )
#(o ? e ) = _'' // should be error
panic ( empty(e) , "Empty String did not generate error!" )
#(o ? e ) = _'42' // should be error
panic ( empty(e) , "Multiple Character String did not generate error!" )

// now wrong assignments
#(o ? e ) =  ( 2 = 10 )
panic ( empty(e) , "Assignment into Constant  did not generate error!" )
#(o ? e ) =  ( [ 2,3,4 ] = [1,2,3] )
panic ( empty(e) , "Assignment into Literal did not generate error!" )

// test multi import
import java.lang.String as String
panic ( empty(String) , "Import failed on literal entry!" )

import java.util.Map$Entry as Entry
panic ( empty(Entry) , "Import failed on $ entry!" )

my_var = 'java.lang.String' // store into a variable
import my_var as String2
panic ( empty(String2) , "Import failed on variable entry!" )

/* Test the var args functions */
import zoomba.lang.core.interpreter.InterpreterScriptTest as IT
IT.printSomeInts(1,2)
IT.printInts(1,2)
IT.printInts()
x = true
/* Import fail check */
#atomic{
  x = false
  import "foo.bar" as fb
  panic(true, "This should not have fired!")
}
// x should be reverted to true
assert ( x , "import did not throw error!" )

/* JVM connectivity */
s = "I love ZoomBA"
assert ( s.class isa 'type', 'could not call .class!' )
assert ( s.class isa 'java.lang.Class', 'could not call .class!' )
assert ( s isa 'java.lang.String', 'could not match full class name!' )
assert ( s.hashCode != 0 , "Could not find hash code!" )
assert ( s.toString == s  , "Could not match toString()" )

/* the basic type checks */
def f(){} // method
def X { } // class
assert( false isa 'bool' , 'failed isa on boolean' )
assert( 1 isa 'int' , 'failed isa on int' )
assert( 1 isa 'integer' , 'failed isa on integer')
assert( 0.0 isa 'float' , 'failed isa on float' )
assert( time() isa 'date' , 'failed isa on date')
assert( "foo".toCharArray isa 'arr' , 'failed isa on arr')
assert( [0:0] isa 'range' , 'failed isa on range')
assert( f isa 'func' , 'failed isa on method')
assert( X isa 'obj' , 'failed isa on ZObject')
assert( [] isa 'list' , 'failed isa on list')
assert( {:} isa 'dict' , 'failed isa on dict')
assert( "hello" isa 'string' , 'failed isa on String')

/* Now, we can use structured multiple return */
d = {:}
#(d.p ? d.e ) = popen('xxx')
panic( empty(d.e) , 'Error Assignment did not work' )
#(d.p , d.e ) = [0,1,2]
assert( d.p == 0 && d.e == 1 , 'Left Assignment did not work' )
eprintln("this goes to error!")
eprintf('%s goes too! %n', 'this')
#(, d.p, d.e ) = [0,1,2]
assert( d.p == 1 && d.e == 2 , 'Right Assignment did not work' )

// handle ZType related stuff
#(o ? e ) = bye(' bye bye!')
panic( empty(e), 'Bye did not work out!' )
assert( set() isa 'set', 'Isa on Set did not work!' )

import java.lang.System as SYS
home = SYS.getProperty("user.home")
// Test proper errors
import java.lang.Integer as Int
import java.math.BigDecimal as BD
#( o ? e ) = Int.parseInt("ooo")
assert( "NumberFormat" @ str(e) , "Did not catch proper Error!" )
panic( empty(e.cause) , "Did not catch Actual Cause!" )
#( o ? e ) = new (BD, "ooo")
assert( "NumberFormat" @ str(e) , "Did not catch proper Error!" )
panic( empty(e.cause) , "Did not catch Actual Cause!" )


// we need to give java command line hook for load to work under JDK-11
// thus this test case is inverted
panic( load(home+"/.m2/repository/dk/brics/automaton/1.12.1"), 'Automation was loaded - should not be possible' )
