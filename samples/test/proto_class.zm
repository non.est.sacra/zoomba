def Complex : {
     x : 0 , y : 0 ,
     $str : def(){
        str("(%s,%s)", $.x, $.y )
     },
     $eq : def(o){
        $.x == o.x && $.y == o.y
     }
}
// add method
Complex.$add = def(c){
     new ( Complex , x = $.x + c.x , y = $.y + c.y )
}
// sub method
Complex.$sub = def(c){
     new ( Complex , x = $.x - c.x , y = $.y - c.y )
}
/*
// add mutable method
Complex.$add$ = def(c){
     $.x += c.x
     $.y += c.y
     return // force return void
}
*/
// sub mutable method
Complex.$sub$ = def(c){
     $.x -= c.x
     $.y -= c.y
     return // force return void
}

// compare method
Complex.$cmp = def(c){
     ($.x **2 + $.y **2) - ( c.x **2 + c.y**2 )
}


c1 = new ( Complex , x=1, y=1)
println(c1)
assert ( c1.x == 1 && c1.y == 1, "Object created good!" )

c2 = new ( Complex , x=2, y=2)
assert ( c2.x == 2 && c2.y == 2, "Object created good!" )
println(c2)

c = c1 + c2
println(c)
assert ( c.x == 3 && c.y == 3, "Object added good!" )

c = c2 - c1
println(c)
assert ( c.x == 1 && c.y == 1, "Object subtracted good!" )

// does equality work?
assert ( c == c1 , "Object equated good!" )
assert ( c <= c1 , "Object compared good!" )

// now the mutable operations
c1 += c1 // perform mutable operation
assert ( c1.x == 2 && c1.y == 2, "Mutable add worked well!" )
c1 -= c1 // perform mutable operation
assert ( c1.x == 0 && c1.y == 0, "Mutable sub worked well!" )

// create a dummy one
def Dummy : {
      foo : 42,
      $$ : def ( f ){
         $.foo = f
      },
      print_func : def ( ){
         printf("hello : %d\n", $.foo )
      }
}
// implement some operations
Dummy.$xor = def(o){  $.foo ^ o.foo  }
Dummy.$or = def(o){  $.foo | o.foo  }
Dummy.$and = def(o){  $.foo & o.foo  }

d1 = new ( Dummy , 1)
d2 = new ( Dummy , 1)
assert ( d1 == d2 , "They should be same?")
s = str(d1)
println( s )
panic ( empty( s ) , "String should bot be empty" )
d1.print_func()
// shall we use hash code? simple put the stuff into a set
s = set( d1, d2 )
assert ( size( s) == 1, "Hash code should be same?" )

assert ( 1 == ( d1 | d2 )  , "Should be true")
assert ( 1 == ( d1 & d2) , "Should be true")
assert ( 0 == ( d1 ^ d2 ) , "Should be true")

/* To increase coverage some more tests */
def XN : {  n : 0 ,
     $$ : def(v=0) { $.n = num(v,0)  }, // the init function
     $hc   : def(){ $.n },
     $div  : def(o) { new ( XN, $.n / o.n )  },
     $mul  : def(o) { new ( XN, $.n * o.n )  },
     $div$ : def(o) { $.n /= o.n },
     $mul$ : def(o) { $.n *= o.n },
     $pow  : def(o) { new ( XN, $.n ** o.n )  },
     $eq   : def(o) { $.n == o.n }
}
x0 = new ( XN )
assert ( x0.hashCode() == 0 , "The hashCode did not work!" )
x1 = new ( XN, 1 )
assert ( x1.hashCode() == 1 , "The hashCode did not work!" )
assert ( x0 * x1 == x0 , "Multiplication did not happen!" )
assert ( x1 / x1 == x1 , "Division did not happen!" )
assert ( x1 ** x1 == x1 , "Power did not happen!" )
assert ( x1 ** x0 == x1 , "Power did not happen!" )
x1 *= x1
assert ( x1.n == 1 , "Mutable Multiplication did not work!")
x0 /= x1
assert ( x0.n == 0 , "Mutable Division did not work!")

// now new, more classical style
def Complex2 {
   def $$(x=0.0,y=0.0){
      $.x = x
      $.y = y
   }
   def $add(another){
      new ( Complex2 , $.x + another.x, $.y + another.y )
   }
   def $str(){ str('(%s,%s)', $.x,$.y) }
}

c = new ( Complex2 , 1.0, 2.0 )
d = new ( Complex2 , 2.0, 1.0 )
println(c + d )

def Empty{}
e = new ( Empty )
println(e)

// dictionary style class - init
def XXX : { 'a' : 10, 'b' : true }
assert( 'a' @ XXX , 'why a is not there in XXX ?')
assert( 'b' @ XXX , 'why b is not there in XXX ?')
y = new ( XXX )
assert( 'a' @ y , 'why a is not there in y ?')
assert( 'b' @ y , 'why b is not there in y ?')
assert( y.a == 10  , 'why a is not 10 ?')
assert( y.b , 'why b is not true')
