/*
 * Copyright 2007 Kasper B. Graversen
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.supercsv;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Interface for comment matchers.
 * 
 * @author James Bassett
 * @since 2.1.0
 */
public interface CommentMatcher {
	
	/**
	 * Determines whether a line of CSV is a comment.
	 * 
	 * @param line
	 *            the raw line of CSV
	 * @return true if it's a comment, otherwise false
	 */
	boolean isComment(String line);

	/**
	 * Constructs a new <tt>CommentMatches</tt> comment matcher. Ensure that the regex is efficient (ideally matching start/end
	 * characters) as a complex regex can significantly slow down reading.
	 *
	 * @param regex
	 *            the regular expression a line must match to be a comment
	 * @throws NullPointerException
	 *             if regex is null
	 * @throws IllegalArgumentException
	 *             if regex is empty
	 * @throws PatternSyntaxException
	 *             if the regex is invalid
	 * @return a CommentMatcher backed by regex
	 */
	static CommentMatcher regexMatch(final String regex ){
		if( regex == null ) {
			throw new NullPointerException("regex should not be null");
		} else if(regex.isEmpty()) {
			throw new IllegalArgumentException("regex should not be empty");
		}
		final Pattern pattern = Pattern.compile(regex);
		return (line) -> pattern.matcher(line).matches();
	}

	/**
	 * Constructs a new <tt>CommentStartsWith</tt> comment matcher.
	 *
	 * @param value
	 *            the String a line must start with to be a comment
	 * @throws NullPointerException
	 *             if value is null
	 * @throws IllegalArgumentException
	 *             if value is empty
	 * @return a CommentMatcher backed by a string starts with
	 */
	static CommentMatcher startsWith(final String value){
		if( value == null ) {
			throw new NullPointerException("value should not be null");
		} else if(value.isEmpty()) {
			throw new IllegalArgumentException("value should not be empty");
		}
		return (line) -> line.startsWith(value);
	}
}
