/*
 * Copyright 2007 Kasper B. Graversen
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.supercsv;

import java.util.HashSet;
import java.util.Set;

/**
 * The interface for quoting modes. The quote mode allows quoting to be enabled in cases where quotes would not normally
 * be required (because the column doesn't contain any special characters). It is not a way to disable quoting
 * altogether - to do that you must supply your own CsvEncoder.
 * 
 * @author James Bassett
 * @since 2.1.0
 */
public interface QuoteMode {
	
	/**
	 * Determines whether surrounding quotes are mandatory in cases where the CSV column would not normally be quoted
	 * (the data to be written doesn't contain special characters).
	 * 
	 * @param csvColumn
	 *            an element of a CSV file
	 * @param context
	 *            the context
	 * @param preference
	 *            the CSV preferences
	 * @return true if surrounding quotes are mandatory, otherwise false
	 */
	boolean quotesRequired(String csvColumn, CsvContext context, CsvPreference preference);

	/**
	 * When using NormalQuoteMode surrounding quotes are only applied if required to escape special characters (per
	 * RFC4180).
	 *
	 * @author James Bassett
	 * @since 2.1.0
	 */
	QuoteMode NORMAL = ( cc, c, p) -> false ;

	/**
	 * When using AlwaysQuoteMode surrounding quotes are always applied.
	 *
	 * @author James Bassett
	 * @since 2.1.0
	 */
	QuoteMode ALWAYS = ( cc, c, p) -> true ;

	/**
	 * Constructs a new <tt>ColumnQuoteMode</tt> that quotes columns by column number. If no column numbers are supplied
	 * (i.e. no parameters) then quoting will be determined per RFC4180.
	 *
	 * @param columnsToQuote
	 *            the column numbers to quote
	 * @throws NullPointerException
	 *             if columnsToQuote is null
	 */
	static QuoteMode columns(int... columnsToQuote){
		final Set<Integer> columnNumbers = new HashSet<Integer>( );
		for( final Integer columnToQuote : columnsToQuote ) {
			columnNumbers.add(columnToQuote);
		}
		final QuoteMode columnQuoteMode =
				( cc, context, p) -> columnNumbers.contains(context.getColumnNumber()) ;
		return columnQuoteMode;
	}
}
