/*
 * Copyright 2025 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.collections;

import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.ZCollection;
import zoomba.lang.core.types.ZNumber;
import zoomba.lang.core.types.ZTypes;

import java.math.BigInteger;
import java.util.*;

/**
 * A class to Handle Permutations and Combinations and Sequences
 */
public final class Combinatorics {

    final List<Comparable> items;

    /**
     * The underlying r parameter for nCr or nPr
     */
    protected final int r ;

    private Combinatorics(Object... args) {
        if ( args == null ||  args.length == 0 ) throw new UnsupportedOperationException("There should be items!");

        Object objects = args[0];
        int count = -1;
        if ( args.length > 1  ){
            count = ZNumber.integer(args[1], count).intValue();
        }
        if ( objects instanceof Iterator ){
            objects = ZCollection.iterable((Iterator) objects);
        }
        if ( objects instanceof Iterable ){
            objects = new ZList( (Iterable)objects );
        }

        if ( objects instanceof List){
            items = new ArrayList( (List)objects );
        }else if (objects.getClass().isArray() ) {
            items =  new ZArray( objects, true ) ;
        }else{
            throw new IllegalArgumentException("Items are not known collection objects!");
        }
        Set s = new HashSet<>(items);
        if (s.isEmpty() || s.size() != items.size()) {
            throw new UnsupportedOperationException("All items must be distinct!");
        }
        Collections.sort(items,Collections.reverseOrder()); // sort it off
        if ( count < 0 ){
            r = s.size();
        }else{
            r = count ;
        }
    }

    static abstract class CombinatoricsIterator implements Iterator<List>{

        protected Combinatorics combinatorics;

        protected Comparable[] next;

        protected BigInteger index ;

        private Boolean cachedHasNext;

        public CombinatoricsIterator(Combinatorics combinatorics) {
            this.combinatorics = combinatorics;
            next = new Comparable[this.combinatorics.r];
            index = BigInteger.ZERO ;
            cachedHasNext = null;
        }

        protected boolean select(){
            index = index.add( BigInteger.ONE );
            String sr = index.toString(2);
            while ( sr.length() <= combinatorics.items.size() ){
                int oneCount = 0 ;
                for ( int i = 0 ; i < sr.length(); i++ ){
                    if ( sr.charAt(i) == '1' ){
                        oneCount ++;
                    }
                }
                if ( oneCount != combinatorics.r ){
                    index = index.add( BigInteger.ONE );
                    sr = index.toString(2);
                }else{
                    int offSet = combinatorics.items.size() - sr.length();
                    StringBuilder buf = new StringBuilder();
                    for ( int i = 0 ; i < offSet ; i++ ){
                        buf.append('0');
                    }
                    buf.append(sr);
                    sr = buf.toString();
                    int start = 0 ;
                    for ( int i = sr.length() - 1; i >=0 ; i-- ){
                        if ( sr.charAt(i) == '1' ){
                            next[start] = combinatorics.items.get(i);
                            start++;
                        }
                    }
                    return true ;
                }
            }
            return false ;
        }

        public abstract boolean nextHigherCombinatorics();

        @Override
        public boolean hasNext() {
            if ( cachedHasNext == null ) {
                cachedHasNext = nextHigherCombinatorics();
            }
            return cachedHasNext;
        }

        @Override
        public List next() {
            // https://docs.oracle.com/javase/8/docs/api/java/util/Iterator.html#next--
            if ( !hasNext() ) throw new NoSuchElementException();
            final List current = new ZArray( next, true );
            cachedHasNext = nextHigherCombinatorics();
            return current;
        }
    }

    static final class CombinationIterator extends CombinatoricsIterator{

        private CombinationIterator(Combinatorics combinations) {
            super(combinations);
        }

        @Override
        public boolean nextHigherCombinatorics() {
            return select();
        }
    }

    static final class PermutationIterator extends CombinatoricsIterator {

        PermutationIterator(Combinatorics permutations) {
            super(permutations);
        }

        /**
         * http://stackoverflow.com/questions/1622532/algorithm-to-find-next-greater-permutation-of-a-given-string
         *
         * @return true if exists, false if does not
         */
        @Override
        public boolean nextHigherCombinatorics() {
            if ( next[0] == null  ){
                return select();
            }

            int i = next.length - 1;
            while (i > 0 && next[i - 1].compareTo(next[i]) >= 0) {
                i--;
            }

            if (i <= 0) {
                return select();
            }

            int j = next.length - 1;

            while (next[j].compareTo(next[i - 1]) <= 0) {
                j--;
            }

            Comparable temp = next[i - 1];
            next[i - 1] = next[j];
            next[j] = temp;

            j = next.length - 1;

            while (i < j) {
                temp = next[i];
                next[i] = next[j];
                next[j] = temp;
                i++;
                j--;
            }

            return true;
        }
    }

    static final class CombinatoricsIterable implements Iterable<List> {

        CombinatoricsIterator c;

        public CombinatoricsIterable(CombinatoricsIterator c){
            this.c = c ;
        }

        @Override
        public Iterator<List> iterator() {
            return c;
        }
    }

    /**
     * Permutation iterable
     * @return permutation
     */
    public Iterable<List> p(){
        return new CombinatoricsIterable( new PermutationIterator( this ) );
    }

    /**
     * Combination iterable
     * @return combination
     */
    public Iterable<List> c(){
        return new CombinatoricsIterable( new CombinationIterator( this ) );
    }

    /**
     * Gets an Iterable of permutations
     * @param args arguments from where permutation is to be generated
     *             usage : permutations(collection [, item_count = size_of_collection ] )
     * @return Iterable of permutations
     */
    public static Iterable<List> permutations(Object... args){
        return new Combinatorics( args ).p();
    }

    /**
     * Gets an Iterable of combinations
     * @param args arguments from where combinations is to be generated
     *             usage : combinations(collection [, item_count = size_of_collection ] )
     * @return Iterable of combinations
     */
    public static Iterable<List> combinations(Object... args){
        return new Combinatorics( args ).c();
    }

    /**
     * Gets an Iterable of Power Set of Collection
     * @param args arguments from where Power Set is to be generated
     *             usage : powerSet(collection)
     * @return Iterable of Power Set of the Collection
     */
    public static Iterable<List> powerSet(Object... args){
        return () -> new PowerSetIterator(args);
    }

    /**
     * A Power Set Generator based on any Collection
     */
    public static final class PowerSetIterator implements Iterator<List> {

        final List sequence ;

        private BigInteger index = BigInteger.ONE;

        // Java collection size is Integer.MAX_VALUE so this can be 2 ** Integer.MAX_VALUE
        final BigInteger maxSize ;

        final int len ;

        private List initSequence(Object[] args){
            if ( args.length == 0 || args[0] == null ) {
                return Collections.emptyList();
            }
            Object s = args[0];
            if ( args.length > 1 ){
                s = args ;
            }
            Iterable iter  = Converter.iterable( s,"Sorry, not a power-set-able sequence!" );
            final List l;
            if ( iter instanceof List){
                l = (List) iter;
            } else{
                l = new ZList(iter);
            }

            Set set = new HashSet(l);
            if ( set.size() != l.size() ){
                throw new UnsupportedOperationException("All items must be distinct!");
            }
            return l;
        }

        PowerSetIterator(Object[] args){
            sequence = initSequence(args);
            BigInteger two = new BigInteger( "2" );
            // ignores empty set
            maxSize = two.pow( this.sequence.size() );
            len = sequence.size();
        }

        @Override
        public boolean hasNext() {
            return maxSize.compareTo( index ) > 0 ;
        }

        @Override
        public List next() {
            if ( !hasNext() ) throw new NoSuchElementException();
            String binRep = index.toString(2);
            StringBuilder buf = new StringBuilder();
            for ( int i = binRep.length(); i < len ; i++ ){
                buf.append('0');
            }
            buf.append(binRep);
            binRep = buf.toString();
            final List subSequence = new ArrayList();
            for ( int i = 0 ; i < len ; i++ ){
                if ( binRep.charAt(i) == '1' ){
                    subSequence.add(  sequence.get(i) ) ;
                }
            }
            index = index.add( BigInteger.ONE );
            return subSequence;
        }
    }

    /**
     * A Unix Uniq like iterator on any iterable
     * <a href="https://en.wikipedia.org/wiki/Uniq">...</a>
     * <a href="https://linuxhandbook.com/uniq-command/">...</a>
     */
    public static final class UniqIterator implements Iterator<List>{
        final Iterator colIter;
        final Function hash;
        final List l;

        UniqIterator(Iterable c, Function f){
            this.colIter = c.iterator();
            this.hash = f;
            this.l = new ArrayList();
            if (colIter.hasNext()){
                l.add( colIter.next() );
            }
        }

        UniqIterator( Iterable c){
            this(c, null);
        }

        @Override
        public boolean hasNext() {
            return !l.isEmpty();
        }

        private boolean areSame(Object last, Object cur){
            if ( hash == null ) return Objects.equals( last, cur );
            final Object lh = hash.apply(last);
            final Object ch = hash.apply(cur);
            return Objects.equals( lh, ch );
        }

        @Override
        public List next() {
            if ( !hasNext() ) throw new NoSuchElementException();
            while (colIter.hasNext()) {
                Object cur = colIter.next();
                if ( areSame( l.get(l.size()-1), cur )){
                    l.add(cur);
                    continue;
                }
                List ret = new ArrayList(l);
                l.clear();
                l.add(cur);
                return ret;
            }
            List ret = new ArrayList(l);
            l.clear();
            return ret;
        }
    }

    /**
     * Gets an Iterable of Power Set of Collection
     * @param args arguments from where Power Set is to be generated
     *             usage : powerSet(collection)
     * @return Iterable of Power Set of the Collection
     */
    public static Iterable<List> uniq(List<Function> anons, Object... args){
        if ( args.length == 0 ) return Collections.emptyList() ;
        Object a = args[0];
        Function f = null ;
        if ( args[0] instanceof Function ){
            f = (Function) args[0];
            args = ZTypes.shiftArgsLeft(args);
            if ( args.length == 0 ) return Collections.emptyList();
            a = args[0];
        }
        if ( args.length > 1 ){
            a = args ; // implicit list from args, no flattening
        }
        Iterable it = Converter.iterable( a ,"Can not convert args to any iterable!" );

        if ( anons.isEmpty()  ){
            if ( f != null ){
                final Function hash = f;
                return () -> new UniqIterator(it, hash);
            }
            return () -> new UniqIterator(it);
        }
        final Function hash = anons.get(0);
        return () -> new UniqIterator(it, hash);
    }
}
