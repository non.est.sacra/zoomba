/*
 * Copyright 2025 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.collections;

import zoomba.lang.core.operations.Arithmetic;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.ZCollection;
import zoomba.lang.core.types.ZRange;
import zoomba.lang.core.types.ZTypes;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Utility class to convert arg object to various iterable types
 */
public final class Converter {

    /**
     * Converts an object to iterable if possible, else raise error with message
     * @param a object to be converted to an iterable
     * @param onFail in case conversion failed this gets called to return the value
     * @return an Iterable
     */
    public static Iterable<?> iterable(Object a, Supplier<Iterable<?>> onFail){
        if ( a instanceof Iterable){
            return  (Iterable<?>) a;
        }
        if ( a instanceof ZRange){
            return  ((ZRange)a).asList();
        }
        if (a instanceof Iterator) {
            return ZCollection.iterable((Iterator<?>) a);
        }
        if ( a.getClass().isArray() ){
            return new ZArray(a);
        }
        if ( a instanceof Stream<?>){
            return ZCollection.iterable((Stream<?>) a);
        }
        if ( a instanceof Map){
            return  ((Map<?,?>) a).entrySet();
        }
        if ( a instanceof CharSequence ){
            return new ZArray( a.toString().toCharArray() );
        }
        return onFail.get();
    }

    /**
     * Converts an object to iterable if possible, else raise error with message
     * @param a object to be converted to an iterable
     * @param whenFail in case conversion failed return this
     * @return an Iterable
     */
    public static Iterable<?> iterable(Object a, Iterable<?> whenFail){
        return iterable( a, () -> whenFail);
    }

    /**
     * Converts an object to iterable if possible, else raise error with message
     * @param a object to be converted to an iterable
     * @param errMessage message for error, in case conversion failed
     * @return an Iterable
     */
    public static Iterable<?> iterable(Object a, String errMessage){
        return iterable( a, (Supplier<Iterable<?>>) () -> { throw new IllegalArgumentException(errMessage); });
    }

    /**
     * Converts an object to a List if possible, else raise error with message
     * @param a object to be converted to a List
     * @param errMessage message for error, in case conversion failed
     * @return a List
     */
    public static List<?> list(Object a, String errMessage){
        if ( a instanceof List){
            return  (List<?>) a;
        }
        if ( a instanceof ZRange){
            return  ((ZRange)a).asList();
        }
        if ( a.getClass().isArray() ){
            return new ZArray(a);
        }
        if ( a instanceof Stream){
            return ((Stream<?>)a).collect(Collectors.toList());
        }
        if ( a instanceof CharSequence ){
            return new ZArray( a.toString().toCharArray() );
        }
        throw new IllegalArgumentException(errMessage);
    }

    /**
     * A Structure to Parse Comparator and Mapper for collections
     */
    static class ComparatorMapper {

        /**
         * The Comparator
         * Given TreeMap and TreeSet has Type issues and Null issues
         * This is the suitable default when comparator is not provided
         * <a href="https://docs.oracle.com/javase/8/docs/api/java/util/TreeMap.html#put-K-V-">...</a>
         */
        Comparator cmp = ZCollection.ASC_NULL_ZMB_COMP ;

        /**
         * The Mapper - default is just the items
         */
        Function mapper = Function.COLLECTOR_IDENTITY ;

        /**
         * Finds Comparator and mapper function
         * Java Style Comparator is done via where Predicate
         * Field style comparator is done via as Mapper
         * @param anons List of Functions which needs to be mapped to Comparator, mapper never empty
         * @return a MapperStruct
         */
        ComparatorMapper(List<Function> anons){
            if ( anons.size() == 1 ){
                if ( anons.get(0) instanceof Function.Predicate ){
                    cmp =  anons.get(0);
                    return;
                }
                mapper = anons.get(0);
                return;
            }
            // which one is which one ?
            if ( anons.get(0) instanceof Function.Predicate ){
                cmp = anons.get(0);
                mapper = anons.get(1);
                return ;
            }
            if ( anons.get(1) instanceof Function.Predicate ){
                cmp = anons.get(1);
                mapper = anons.get(0);
                return;
            }
            // both mapper
            cmp = anons.get(0);
            mapper = anons.get(1);
        }
    }

    /**
     * A Structure to Create Comparator enabled collection type and Mapper for collections
     * @param <T> Type of the Object to store
     */
    public static class SeedAnon<T>{

        /**
         * Underlying Object
         */
        public T seed;

        /**
         * The Mapper Function
         */
        public Function mapper;
    }

    /**
     * Creates a seed Collection to be used later
     * In case found a comparator applies it while creating the collection
     * @param anons list of anon functions that would be used for Comparator and Mapper
     * @param cmpConstructor a Function that abstracts a constructor creating the Collection
     * @return a SeedAnon which is a And type of Collection enriched with collector and a Mapper
     * @param <T> type of the item we are creating
     */
    public static <T> SeedAnon<T> buildWithComparator( List<Function> anons,
                                              java.util.function.Function<Comparator,T> cmpConstructor){
        final SeedAnon<T> sa = new SeedAnon<>();
        final Converter.ComparatorMapper ms =  new Converter.ComparatorMapper( anons );
        sa.mapper = ms.mapper ;
        sa.seed = cmpConstructor.apply( ms.cmp );
        if ( ms.cmp instanceof Function ){
            ((Function)ms.cmp).withIterationContext(sa.seed);
        }
        return sa;
    }

    /**
     * A class which coverts comparators into Functions
     */
    public static final class Comparator2Function {

        /**
         * An enum that matches anon method ids against predefined constants
         */
        enum ComparatorIdentifier{

            /**
             * Standard Java Natural Comparator
             */
            JVM_NATURAL_ORDER("@cmp_jJ", CMP_JVM_NATURAL_ORDER),

            /**
             * Standard Java Reverse Comparator
             */
            JVM_REVERSE_ORDER("@cmp_Jj", CMP_JVM_REVERSE_ORDER),

            /**
             * Standard ZoomBA Natural Comparator - defined by Arithmetic
             */
            ZMB_NATURAL_ORDER("@cmp_zZ", CMP_ZMB_NATURAL_ORDER),

            /**
             * Standard ZoomBA Reverse Comparator - Arithmetic in reversed order
             */
            ZMB_REVERSE_ORDER("@cmp_Zz", CMP_ZMB_REVERSE_ORDER),

            /**
             * Null ascending standard Java Natural Comparator
             */
            NULL_ASC_JVM_NATURAL_ORDER("@cmp_njJ", CMP_NULL_ASC_JVM_NATURAL_ORDER),

            /**
             * Null descending standard Java descending Comparator
             */
            NULL_DSC_JVM_REVERSE_ORDER("@cmp_Jjn", CMP_NULL_DSC_JVM_REVERSE_ORDER),

            /**
             * Null descending standard Java Natural Comparator
             */
            NULL_DSC_JVM_NATURAL_ORDER("@cmp_jJn", CMP_NULL_DSC_JVM_NATURAL_ORDER),

            /**
             * Null ascending standard Java descending Comparator
             */
            NULL_ASC_JVM_REVERSE_ORDER("@cmp_nJj", CMP_NULL_ASC_JVM_REVERSE_ORDER ),


            /**
             * Null ascending standard ZMB Default Arithmetic Comparator
             */
            NULL_ASC_ZMB_NATURAL_ORDER("@cmp_nzZ", CMP_NULL_ASC_ZMB_NATURAL_ORDER),

            /**
             * Null descending standard ZMB descending Comparator
             */
            NULL_DSC_ZMB_REVERSE_ORDER("@cmp_Zzn", CMP_NULL_DSC_ZMB_REVERSE_ORDER),

            /**
             * Null descending standard ZMB Natural Comparator
             */
            NULL_DSC_ZMB_NATURAL_ORDER("@cmp_zZn", CMP_NULL_DSC_ZMB_NATURAL_ORDER),

            /**
             * Null ascending standard ZMB descending Comparator
             */
            NULL_ASC_ZMB_REVERSE_ORDER("@cmp_nZz", CMP_NULL_ASC_ZMB_REVERSE_ORDER ),

            ;

            /**
             * identifier for the enum - same should be used in script
             */
            final String id;

            /**
             * Returns the actual underlying function
             */
            final Function cmp;

            static Map<String,ComparatorIdentifier > valueCache = new HashMap<>();

            static {
                for (ComparatorIdentifier field : ComparatorIdentifier.values()) {
                    valueCache.put(field.id, field);
                }
            }
            ComparatorIdentifier(String id, Function cmp) {
                this.id = id;
                this.cmp = cmp;
            }
        }

        /**
         * Creates a Function from Comparator
         * @param comparator a Comparator
         * @return a Function that wraps around it
         */
        public static Function of(final Comparator comparator){

            return new Function() {

                @Override
                public int definedParams() {
                    return 2;
                }

                @Override
                public String body() {
                    return "cmp_" + name() ;
                }

                @Override
                public int compare(Object o1, Object o2) {
                    return comparator.compare(o1, o2);
                }

                @Override
                public MonadicContainer execute(Object... args) {
                    return new MonadicContainerBase( comparator.compare( args[0], args[1] ));
                }

                @Override
                public String name() {
                    return comparator.getClass().getName();
                }
            };
        }

        /**
         * Creates a Function from Comparator
         * @param comparatorId a Comparator identifier as defined in ComparatorIdentifier
         * @return a Function that wraps around it
         */
        public static Function of(final String comparatorId){
            return ZTypes.safeOrNull( () -> ComparatorIdentifier.valueCache.get(comparatorId).cmp );
        }

        /**
         * A Function abstracting standard JVM comparator
         */
        public static final Function CMP_JVM_NATURAL_ORDER = of(ZCollection.JVM_NATURAL_ORDER);

        /**
         * A Function abstracting revere of the standard JVM comparator
         */
        public static final Function CMP_JVM_REVERSE_ORDER = of(ZCollection.JVM_NATURAL_ORDER.reversed());

        /**
         * A Function abstracting standard ZoomBA comparator
         */
        public static final Function CMP_ZMB_NATURAL_ORDER = of(Arithmetic.INSTANCE);

        /**
         * A Function abstracting reverse of the standard ZoomBA comparator
         */
        public static final Function CMP_ZMB_REVERSE_ORDER = of(Arithmetic.INSTANCE.reversed());

        /**
         * A Function abstracting null ascending comparator backed by standard JVM comparator
         */
        public static final Function CMP_NULL_ASC_JVM_NATURAL_ORDER = of(ZCollection.ASC_NULL_JVM_COMP);

        /**
         * A Function abstracting reverse of null ascending comparator backed by standard JVM comparator
         */
        public static final Function CMP_NULL_DSC_JVM_REVERSE_ORDER = of(ZCollection.ASC_NULL_JVM_COMP.reversed());

        /**
         * A Function abstracting null descending comparator backed by standard JVM comparator
         */
        public static final Function CMP_NULL_DSC_JVM_NATURAL_ORDER = of(ZCollection.DSC_NULL_JVM_COMP );

        /**
         * A Function abstracting null ascending comparator backed by standard JVM comparator
         */
        public static final Function CMP_NULL_ASC_JVM_REVERSE_ORDER = of(ZCollection.DSC_NULL_JVM_COMP.reversed() );

        /**
         * A Function abstracting  comparator where nulls first, then ZoomBA default order as in Arithmetic
         */
        public static final Function CMP_NULL_ASC_ZMB_NATURAL_ORDER = of(ZCollection.ASC_NULL_ZMB_COMP);

        /**
         * A Function abstracting  comparator where nulls in the end, before them ZoomBA reverse order as in Arithmetic
         */
        public static final Function CMP_NULL_DSC_ZMB_REVERSE_ORDER = of(ZCollection.ASC_NULL_ZMB_COMP.reversed());

        /**
         * A Function abstracting  comparator where nulls in the end, before them ZoomBA default order as in Arithmetic
         */
        public static final Function CMP_NULL_DSC_ZMB_NATURAL_ORDER = of(ZCollection.DSC_NULL_ZMB_COMP);

        /**
         * A Function abstracting  comparator where nulls in the front, post them ZoomBA reverse order as in Arithmetic
         */
        public static final Function CMP_NULL_ASC_ZMB_REVERSE_ORDER = of(ZCollection.DSC_NULL_ZMB_COMP.reversed());
    }
}
