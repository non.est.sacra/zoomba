package zoomba.lang.core.collections;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * <a href="https://gist.github.com/john-nash-rs/ac836b08efce01667f582b0269c8153c">...</a>
 * A class to do KMP sub list matching
 */
public class KMP {

    /**
     * The not found index
     */
    public static final int NOT_FOUND_INDEX = -1;

    /**
     * Actual KMP search
     * @param pattern pattern to search
     * @param text where to search
     * @return a non-zero index if match found
     */
    public static int findSubStringIndex( String pattern, String text){
        return findSubListIndex( new ZArray( pattern.toCharArray() ),
                new ZArray( text.toCharArray()));
    }

    /**
     * Actual KMP search on List finds the first index
     * @param subList pattern to search
     * @param parentList where to search
     * @return a non-zero index if match found
     */
    public static int findSubListIndex(List subList, List parentList) {
        if ( parentList.isEmpty() || subList.size() > parentList.size() ) return NOT_FOUND_INDEX;
        if (subList.isEmpty()) return 0;
        if ( subList.size() == parentList.size() ) {
            final int len = subList.size();
            for ( int i=0; i < len; i++ ){
                boolean same = Objects.equals( subList.get(i), parentList.get(i));
                if( !same) return NOT_FOUND_INDEX;
            }
            return 0;
        }

        int index = NOT_FOUND_INDEX;
        final int patternLength = subList.size();
        final int textLength = parentList.size();

        int[] suffixArray = generateSuffixArray(subList);

        int i = 0;
        boolean isMatched = true;
        int j = 0;
        while(j < patternLength && i < textLength){
            if(Objects.equals( parentList.get(i) , subList.get(j) )){
                j = j + 1;
                i =i + 1;
            } else{
                int newIndex = i - j + 1;
                isMatched = false;
                j = j - 1;
                if (j < 0)
                    j = 0;
                j = suffixArray[j];
                if(j==0)
                    i =newIndex;
                else
                    i = i + 1;
            }

            if(j == patternLength) {
                isMatched = true;
            }

        }
        if(isMatched)
            index = i - patternLength;

        return index;
    }

    /**
     * Actual KMP search on List finds the last index
     * @param subList pattern to search
     * @param parentList where to search
     * @return a non-zero index if match found
     */
    public static int findLastSubListIndex( List subList, List parentList){
        int inx = findSubListIndex( reversed(subList) , reversed(parentList));
        if( inx == NOT_FOUND_INDEX ) return inx;
        if (subList.isEmpty()) return parentList.size() ;

        // this is the index of the reversed sublist start in the revered parentList
        int startInx = inx + subList.size() - 1;
        // we need to reflect it
        return parentList.size()  -1 - startInx ;
    }

    private static int[] generateSuffixArray(List subList) {
        final int patternLength = subList.size();
        int[] suffixArray = new int[patternLength];
        int i = 1;
        while(i < patternLength){
            for(int j = 0; j < patternLength; j++){
                if((i < patternLength) && Objects.equals(subList.get(i) , subList.get(j))){
                    suffixArray[i] = j + 1;
                    i = i + 1;

                } else{
                    i = i + 1;
                    break;
                }

                if(i == patternLength - 1)
                    break;
            }

        }

        return  suffixArray;
    }

    private static List reversed( List l){
        List a = new ArrayList();
        for ( int i=l.size()-1; i>=0; i--){
            a.add(l.get(i));
        }
        return a;
    }
}