/*
 * Copyright 2025 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.collections;

import zoomba.lang.core.operations.ZCollection;
import zoomba.lang.core.types.ZTypes;

import java.lang.reflect.Array;
import java.util.*;

/**
 * A Size Immutable Array backed up List, works much better and faster than Arrays.asList()
 * Which supports for -ve indexing
 * Acts as a mutable fixed size Tuple
 * Also is a suitable substitute for Map.Entry so that [0,1] is actually an entry too!
 */
public final class ZArray extends BaseZCollection implements List , Map.Entry {

    @Override
    public Object getKey() {
        return get(0);
    }

    @Override
    public Object getValue() {
        return get(1);
    }

    @Override
    public Object setValue(Object value) {
        return set(1,value);
    }

    /**
     * An iterator for Arrays
     */
    public static final class ArrayIterator implements ListIterator{

        private int cursor ;

        /**
         * Underlying object array
         */
        public final Object array ;

        /**
         * Length of that array
         */
        public final int length ;

        /**
         * Creates new Iterator from underlying array
         * @param array the array
         */
        public ArrayIterator(Object array){
            length = Array.getLength(array);
            this.array = array ;
            cursor = -1;
        }

        @Override
        public boolean hasNext() {
            return (cursor + 1 < length );
        }

        @Override
        public Object next() {
            return Array.get(array, ++cursor);
        }

        @Override
        public boolean hasPrevious() {
            return ( cursor > 0 );
        }

        @Override
        public Object previous() {
            return Array.get(array, --cursor);
        }

        @Override
        public int nextIndex() {
            return (cursor+1);
        }

        @Override
        public int previousIndex() {
            return cursor;
        }

        @Override
        public void remove() {
           throw new UnsupportedOperationException("Can not remove element from array!");
        }

        @Override
        public void set(Object o) {
            Array.set(array,cursor,o);
        }

        @Override
        public void add(Object o) {
            throw new UnsupportedOperationException("Can not add element into array!");
        }
    }


    /**
     * An empty ZArray object for various usage
     */
    public static final ZArray EMPTY_Z_ARRAY = new ZArray(EMPTY_ARRAY);

    /**
     * The items stored inside the ZArray
     */
    public final Object items;

    /**
     * Length of the array
     */
    public final int length;

    /**
     * Arbitrary length parameter gets converted into ZArray
     * @param args element for the ZArray
     * @return ZArray with args as elements
     */
    public static ZArray of(Object...args){
        return new ZArray( args, false);
    }

    /**
     * Creates a new ZArray
     * Wraps around the array, does not deep copy values
     * Stores the array ref
     * In case you do not like it, use copy constructor
     * @param arr the underlying array
    */
    public ZArray(Object arr){
        this(arr,false);
    }

    /**
     * Creates a new ZArray
     * @param arr the underlying array
     * @param copy if true, deep copies it, false does not copy simply assigns
     */
    public ZArray(Object arr, boolean copy){
        super(Collections.EMPTY_LIST);
        if ( arr instanceof ZArray ){
            arr = ((ZArray) arr).items ;
        }
        if ( !arr.getClass().isArray() ){
            throw new IllegalArgumentException("Argument must be some array! Passed Type: " + arr.getClass());
        }
        length = Array.getLength(arr);
        if ( copy ){
            Object[] _arr = new Object[ length ];
            // boxing won't happen, we need to do it in normal way
            for ( int i = 0 ; i < length; i++ ){
                _arr[i] = Array.get( arr, i);
            }
            items = _arr;

        }else {
            items = arr;
        }
        col = this;
    }

    /**
     * Gets an item
     * @param index in the index, -ve int are allowed
     * @return the item
     */
    public Object get(int index){
        try {
            return Array.get(items, index);
        }catch ( ArrayIndexOutOfBoundsException e){
            if ( length != 0 && index < 0 ) {
                return Array.get(items,length + index );
            }
            throw e;
        }
    }

    /**
     * Set an item
     * @param index in the index, -ve int are allowed
     * @param o the object which needs to be placed in the array
     * @return the item
     */
    public Object set(int index,Object o){
        Object old = get(index);
        try {
            Array.set(items, index, o);
            return old;
        }catch (ArrayIndexOutOfBoundsException e){
            // only here, if the set failed, otherwise get would have failed !
            Array.set(items, length + index, o);
            return old;
        }
    }

    @Override
    public int size() {
        return length ;
    }

    @Override
    public boolean isEmpty() {
        return (0 == length);
    }

    @Override
    public boolean contains(Object o) {
        return ( indexOf(o) >= 0 );
    }

    @Override
    public Iterator iterator() {
        return new ArrayIterator(items);
    }

    @Override
    public Object[] toArray() {
        Object[] arr = new Object[length];
        for ( int i = 0 ; i < length; i++ ){
            arr[i] = Array.get(items,i);
        }
        return arr;
    }

    @Override
    public Object[] toArray(Object[] a) {
        for ( int i = 0 ; i < length; i++ ){
            a[i] = Array.get(items,i);
        }
        return a;
    }

    @Override
    public boolean add(Object o) { throw new UnsupportedOperationException(); }

    @Override
    public boolean remove(Object o) { throw new UnsupportedOperationException(); }

    @Override
    public boolean containsAll(Collection c) {
        for ( Object o : c ){
            if ( !contains(o) ) return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection c) { throw new UnsupportedOperationException(); }

    @Override
    public boolean addAll(int index, Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() { throw new UnsupportedOperationException(); }

    @Override
    public void add(int index, Object element) { throw new UnsupportedOperationException(); }

    @Override
    public Object remove(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int indexOf(Object o) {
        int i = -1 ;
        while( ++i < length ){
            Object item = Array.get(items,i);
            if ( ZTypes.equals(item,o) ) return i;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        int i = length ;
        while( --i >= 0 ){
            Object item = Array.get(items,i);
            if ( ZTypes.equals(item,o) ) return i;
        }
        return -1;
    }

    @Override
    public ListIterator listIterator() {
        return new ArrayIterator(items);
    }

    @Override
    public ListIterator listIterator(int index) {
        ArrayIterator ai = new ArrayIterator(items);
        ai.cursor = index ;
        return ai;
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        Object[] copy = new Object[toIndex-fromIndex+1];
        System.arraycopy(items,fromIndex,copy,0,copy.length);
        return new ZArray(copy);
    }

    @Override
    public ZCollection collector() {
        return new ZList();
    }

    @Override
    public Set setCollector() {
        return new ZSet();
    }

    @Override
    public String containerFormatString() {
        return "@[ %s ]";
    }

    @Override
    public ZCollection reverse() {
        Object[] arr = new Object[length];
        for ( int i = length - 1; i >= 0 ; i-- ){
            arr[ length - i -1 ] = Array.get(items,i);
        }
        return new ZArray(arr);
    }

    @Override
    public ZCollection myCopy() {
        return new ZList(items); // so that array + elements returns something meaningful
    }

    @Override
    public int hashCode(){
        return Arrays.deepHashCode( (Object[] )items ) ;
    }

    @Override
    public boolean equals(Object o){
        if ( !(o instanceof ZArray) && (o instanceof Map.Entry) ){
            return ZTypes.safeOrDefault( ()-> Objects.equals( getKey(), ((Map.Entry<?, ?>) o).getKey()) &&
                    Objects.equals( getValue(), ((Map.Entry<?, ?>) o).getValue()), false);
        }
        return super.equals(o);
    }
}
