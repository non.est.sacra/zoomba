/*
 * Copyright 2025 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.collections;

import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.ZCollection;
import zoomba.lang.core.types.ZNumber;
import zoomba.lang.core.types.ZTypes;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Objects;

/**
 * A Heap data Structure for small items,
 * Avoids the heapify procedure by sorting the items,
 * Thus making it unsuitable for large no of items in the heap
 */
public class ZHeap extends ZList {

    /**
     * Creates a heap using a comparator function and arguments
     * @param f the comparator function
     * @param args
     *       args[0] is size of the heap
     *       args[1] is the boolean flag
     *          to make it a min heap - pass false
     *          to make it max heap - pass true
     *       if args[0] is NamedArgs, then { col : Collection, max : max heap or min heap - true|false , size: max size of the heap }
     * @return a heap
     */
    public static ZHeap heap(Function f, Object...args){
        if ( args.length == 0 ) return new ZHeap(42); // waste...?
        if ( args[0] instanceof Function.NamedArgs ){
            Function.NamedArgs na = (Function.NamedArgs)args[0];
            final int maxSize = na.typed("size", 42) ;
            final boolean maxHeap = na.typed("max", false) ;
            final Collection<?> col = na.typed("col", Collections.emptyList());
            ZHeap zHeap = heap(f, maxSize, maxHeap);
            zHeap.addAll(col);
            return zHeap;
        }

        if ( f == null ){
            if ( args.length == 1 )
                return new ZHeap(ZNumber.integer(args[0],42).intValue() );
            return new ZHeap( ZNumber.integer(args[0],42).intValue(), ZTypes.bool( args[1], false ) );
        }
        if ( args.length == 1 )
            return new ZHeap(ZNumber.integer(args[0],42).intValue(), false, f );

        return new ZHeap(ZNumber.integer(args[0],42).intValue(), ZTypes.bool( args[1], false ) , f );
    }

    /**
     * Heaps underlying comparator
     */
    public final Comparator comparator;

    /**
     * flag to see if it is a min heap or max heap
     */
    public final boolean max;

    /**
     * The size of the heap
     */
    public final int maxSize;


    private final ZSortedList _slist;

    /**
     * Creates a new max heap
     * @param size with this value
     */
    public ZHeap(int size){
        this(size,false);
    }

    /**
     * Creates a new heap
     * @param size with value
     * @param maxHeap true means max, false means min
     */
    public ZHeap(int size, boolean maxHeap){
        this(size,maxHeap, ASC_NULL_ZMB_COMP);
    }

    /**
     * Creates a new heap
     * @param size with value
     * @param maxHeap true means max, false means min
     * @param comparator  using this Comparator to compare elements
     */
    public ZHeap(int size, boolean maxHeap, Comparator comparator){
        super();
        maxSize = size ;
        max = maxHeap ;
        this.comparator = comparator ;
        _slist = new ZSortedList(this.comparator);
        this.col = _slist;
    }

    /**
     * Creates a new heap
     * @param size with value
     * @param maxHeap true means max, false means min
     * @param f using this function to compare elements -1,0,1 or true/false
     */
    public ZHeap(int size, boolean maxHeap, Function f){
        super();
        maxSize = size ;
        this.comparator = f ;
        f.withIterationContext(this);
        max = maxHeap ;
        _slist = new ZSortedList(this.comparator);
        this.col = _slist;
    }

    /**
     * Gets the min value of the heap
     * Depends on the comparator, be careful
     * @return min value
     */
    public  Object min(){
        return _slist.getFirst();
    }

    /**
     * Gets the max value of the heap
     * Depends on the comparator, be careful
     * @return max value
     */
    public  Object max(){
        return _slist.getLast() ;
    }

    /**
     * Gets the top element of the heap
     * @return top value
     */
    public synchronized Object top(){
        return max? _slist.getLast() : _slist.getFirst() ;
    }

    @Override
    public synchronized boolean addAll(Collection c) {
        boolean added = true;
        for ( Object o : c ){
            added &= add(o);
        }
        return added;
    }

    @Override
    public synchronized boolean add(Object o) {
        if (  _slist.size() < maxSize ){
            return _slist.add(o);
        }

        if ( max ){
            int c = this.comparator.compare( _slist.getFirst(), o );
            if ( c >= 0 ) return false;
            _slist.removeFirst();
            return _slist.add(o);

        } else {
            int c = this.comparator.compare( o, _slist.getLast() );
            if ( c >= 0 ) return false;
            _slist.removeLast();
            return _slist.add(o);
        }
    }

    @Override
    public ZCollection myCopy() {
        ZHeap zHeap = new ZHeap( this.maxSize, this.max, this.comparator );
        zHeap.col = new ZSortedList(this.col, this.comparator);
        return zHeap;
    }

    @Override
    public ZCollection reverse() {
        ZHeap zHeap = new ZHeap( this.maxSize, !this.max, this.comparator.reversed() );
        zHeap.addAll( this.col );
        return zHeap;
    }

    @Override
    public String toString() {
        return String.format("h<%s|%s>", isEmpty()? "-" : top(), super.toString());
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        ZHeap zHeap = (ZHeap) o;
        // comparator is passed into the sorted list, hence ignored
        return max == zHeap.max && maxSize == zHeap.maxSize && Objects.equals(_slist, zHeap._slist);
    }

    @Override
    public int hashCode() {
        // comparator is passed into the sorted list, hence ignored
        return Objects.hash(super.hashCode(), max, maxSize, _slist);
    }
}
