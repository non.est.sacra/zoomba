/*
 * Copyright 2025 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.collections;

import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.ZCollection;
import zoomba.lang.core.types.ZRange;

import java.util.*;

import static zoomba.lang.core.operations.Function.NIL;

/**
 * A Generic List Class to be a wrapper over any List interface implementation
 */
public class ZList extends BaseZCollection implements List, Deque {

    /**
     * Creates a list from
     * @param anon anonymous function
     * @param args using the args
     *             either the args is empty - then empty list
     *             the args is a collection, creates a list containing collection
     *             the args themselves are taken as list
     * @return a list
     */
    public static List list( Function anon, Object... args){
        if ( args.length == 0 ) return new ZList() ;
        Object a = args[0];
        if ( args.length > 1 ){
            a = args ; // implicit list from args, no flattening
        }
        if ( anon == null ){
            if ( a instanceof ZRange) return ((ZRange) a).list();
            if ( a instanceof Collection ){
                return new ZList((Collection)a);
            }
            if ( a == null ) return Collections.emptyList();
            return new ZList(a);
        }
        final Iterable iterable = Converter.iterable(a, "Type can not be converted to a list!");
        return (List) BaseZCollection.map(new ZList(), anon, iterable);
    }

    /**
     * Creates a list from underlying array object
     * @param arr the array
     */
    public ZList(Object arr) {
        super(arr);
        col = new ArrayList<>(col);
    }

    /**
     * Creates a list out of collection
     * @param c the collection
     */
    public ZList(Collection c) {
        super(c);
        col = new ArrayList<>(col);
    }

    /**
     * Creates a list out of an Iterator
     * @param i the iterator
     */
    public ZList(Iterator i) {
        super(new ArrayList() );
        while ( i.hasNext() ){
            col.add(i.next());
        }
    }

    /**
     * Creates a list out of an Iterable
     * @param i the iterable
     */
    public ZList(Iterable i) {
        this(i.iterator());
    }

    /**
     * Generates an empty list
     */
    public ZList() {
        super(new ArrayList());
    }

    @Override
    public boolean addAll(int index, Collection c) {
        return ((List)col).addAll(index, c );
    }

    @Override
    public Object get(int index) {
        try {
            return ((List) col).get(index);
        }catch (IndexOutOfBoundsException e){
            if ( !col.isEmpty() && index < 0 ){
                return ((List) col).get(col.size() + index );
            }
            throw e;
        }
    }

    @Override
    public Object set(int index, Object element) {
        try {
            return ((List) col).set(index, element);
        }catch (IndexOutOfBoundsException e){
            if ( !col.isEmpty() && index < 0 ){
                return ((List) col).set(col.size() + index, element );
            }
            throw e;
        }
    }

    @Override
    public void add(int index, Object element) {
        ((List)col).add(index,element);
    }

    @Override
    public Object remove(int index) {
        return ((List)col).remove(index);
    }

    private List fromObject(Object o){
        List l = null;
        if ( o.getClass().isArray() ){
            l = new ZArray(o);
        }else if ( o instanceof List ){
            l = (List)o;
        }
        return l;
    }

    /**
     * Like the normal indexOf but
     * given the parameter is a list, then
     * returns the index where the list starts within itself
     * @param o the item to be found
     * @return the index (if o is a collection, returns the right most index of match )
     * [0,1,2,3,4] when indexOf [2,3], it will yield 2
     * [0,1,2,3,4] when indexOf [2,3,4], it will yield 2
     */
    @Override
    public int indexOf(Object o) {
        if ( isEmpty() ) return -1;
        if ( o == null ) return ((List)col).indexOf(o);
        List l = fromObject(o);
        if ( l == null ) return ((List)col).indexOf(o);
        if ( l.isEmpty() ) return 0;
        return KMP.findSubListIndex( l,  (List)col );
    }

    /**
     * Is the list starts with the object o?
     * This other object can be an element or can also be a list or an array
     * So, if this is [0,1,2] and o is 0, the answer would be yes
     * Same goes for [0,1,2] and o is [0,1]
     * Thus in case o is a collection of item, it tries to see if o is a prefix sequence or not
     * @param o the other object
     * @return true when this starts with the object or sequence o
     */
    public boolean startsWith(Object o){
        if ( isEmpty() ) return false;
        List l = fromObject(o);
        if ( l == null ) return Objects.equals( get(0), o);
        if ( l.isEmpty() ) return true;
        if ( l.size() > size() ) return false;
        int i = 0;
        for ( ;i < l.size(); i++){
            boolean matched = Objects.equals( l.get(i), ((List<?>)col).get(i) );
            if ( !matched) return false;
        }
        return true;
    }

    /**
     * Is the list ends with the object o?
     * This other object can be an element or can also be a list or an array
     * So, if this is [0,1,2] and o is 2, the answer would be yes
     * Same goes for [0,1,2] and o is [1,2]
     * Thus in case o is a collection of item, it tries to see if o is a suffix sequence or not
     * @param o the other object
     * @return true when this ends with the object or sequence o
     */
    public boolean endsWith(Object o){
        if ( isEmpty() ) return false;
        List l = fromObject(o);
        if ( l == null ) return Objects.equals( get(-1), o);
        if ( l.isEmpty() ) return true;
        if ( l.size() > size() ) return false;
        int i = 0;
        for ( ;i < l.size(); i++){
            int subInx = l.size() -1 -i ;
            int supInx = ((List<?>)col).size() - 1 -i;
            boolean matched = Objects.equals( l.get( subInx), ((List<?>)col).get( supInx) );
            if ( !matched) return false;
        }
        return true;
    }

    /**
     * Like the normal lastIndexOf but
     * given the parameter is a list, then
     * returns the index where the list ends within itself
     * @param o the item to be found
     * @return the index (if o is a collection, returns the right most index of match )
     * [0,1,2,3,4] when lastIndexOf [2,3], it will yield 3, instead of 2
     * [0,1,2,3,4] when lastIndexOf [2,3,4], it will yield 4, instead of 2
     */
    @Override
    public int lastIndexOf(Object o) {
        if ( isEmpty() ) return -1;
        if ( o == null ) return ((List)col).lastIndexOf(o);
        List l = fromObject(o);
        if ( l == null ) return ((List)col).lastIndexOf(o);
        if ( l.isEmpty() ) return col.size()  ;
        return KMP.findLastSubListIndex( l, (List)col);
    }

    @Override
    public ListIterator listIterator() {
        return ((List)col).listIterator();
    }

    @Override
    public ListIterator listIterator(int index) {
        return ((List)col).listIterator(index);
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        return ((List)col).subList(fromIndex,toIndex);
    }

    @Override
    public ZCollection collector() {
        return new ZList();
    }

    @Override
    public Set setCollector() {
        return new ZSet();
    }

    @Override
    public Object rightFold(Function fold, Object... arg) {
        return rightFold(this,fold,arg);
    }

    @Override
    public int rightIndex(Function predicate) {
        return rightIndex(this,predicate);
    }

    @Override
    public Object rightReduce(Function reduce) {
        return rightReduce( this, reduce);
    }

    @Override
    public ZCollection reverse() {
        ZList reverse = new ZList();
        for ( int i = size() - 1; i >= 0 ; i--){
            reverse.add( get(i));
        }
        return reverse ;
    }

    @Override
    public ZCollection myCopy() {
        return new ZList(new ArrayList<>(col));
    }

    @Override
    public String containerFormatString() {
        return "[ %s ]";
    }

    @Override
    public void addFirst(Object o) {
        ((List) col).add(0,o);
    }

    @Override
    public void addLast(Object o) {
        col.add(o);
    }

    @Override
    public boolean offerFirst(Object o) {
        addFirst(o);
        return true;
    }

    @Override
    public boolean offerLast(Object o) {
        addLast(o);
        return true;
    }

    @Override
    public Object removeFirst() {
        if ( col.isEmpty() ) throw new NoSuchElementException();
        return ((List) col).remove(0);
    }

    @Override
    public Object removeLast() {
        if ( col.isEmpty() ) throw new NoSuchElementException();
        return ((List) col).remove(col.size()-1);
    }

    @Override
    public Object pollFirst() {
        if ( col.isEmpty() ) return null ;
        return ((List) col).remove(0);
    }

    @Override
    public Object pollLast() {
        if ( col.isEmpty() ) return null;
        return ((List) col).remove(col.size()-1);
    }

    @Override
    public Object getFirst() {
        if ( col.isEmpty()) throw new NoSuchElementException();
        return ((List) col).get(0);
    }

    @Override
    public Object getLast() {
        if ( col.isEmpty()) throw new NoSuchElementException();
        return ((List) col).get( col.size()-1);
    }

    @Override
    public Object peekFirst() {
        if ( col.isEmpty()) return null;
        return  ((List) col).get(0);
    }

    @Override
    public Object peekLast() {
        if ( col.isEmpty()) return null;
        return ((List) col).get( col.size()-1);
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        List l = (List) col;
        int i = l.indexOf(o);
        if ( i < 0 ) { return false; }
        l.remove(i);
        return true;
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        List l = (List) col;
        int i = l.lastIndexOf(o);
        if ( i < 0 ) { return false; }
        l.remove(i);
        return true;
    }

    @Override
    public boolean add(Object o) {
        addLast(o);
        return true;
    }

    @Override
    public boolean offer(Object o) {
        return offerLast(o);
    }

    @Override
    public Object remove() {
        return removeFirst();
    }

    @Override
    public Object poll() {
        return pollFirst();
    }

    @Override
    public Object element() {
        return getFirst();
    }

    @Override
    public Object peek() {
        return peekFirst();
    }

    @Override
    public void push(Object o) {
        addFirst(o);
    }

    @Override
    public Object pop() {
        return removeFirst();
    }

    @Override
    public Iterator descendingIterator() {
        return new Iterator() {
            final ListIterator li = ((List) col).listIterator( col.size() );
            @Override
            public boolean hasNext() {
                return li.hasPrevious();
            }

            @Override
            public Object next() {
                return li.previous();
            }
        };
    }

    public ZList reversed() {
        return (ZList) reverse();
    }
}
