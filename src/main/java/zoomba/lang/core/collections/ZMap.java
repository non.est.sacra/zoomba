/*
 * Copyright 2025 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.collections;
import zoomba.lang.core.operations.Arithmetic;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.ZCollection;
import zoomba.lang.core.operations.ZJVMAccess;
import zoomba.lang.core.types.ZTypes;
import java.util.*;

import static zoomba.lang.core.collections.ZArray.of;

/**
 * A Map class implementing various ZoomBA protocols to make it inherently useful
 */
public class ZMap implements SortedMap, NavigableMap, Arithmetic.BasicArithmeticAware, Arithmetic.LogicAware, ZTypes.StringX<Map<?,?>> {


    /**
     * Creates a Mutable Collection from this map
     * @param synch if synchronization is needed
     * @return a collection from this map
     */
    public Collection collection( boolean synch){
        Map map = synch ? Collections.synchronizedMap(this.map) :  this.map;
        return new Collection() {
            @Override
            public int size() {
                return map.size();
            }

            @Override
            public boolean isEmpty() {
                return map.isEmpty();
            }

            @Override
            public boolean contains(Object o) {
                return map.entrySet().contains(o);
            }

            @Override
            public Iterator iterator() {
                return map.entrySet().iterator();
            }

            @Override
            public Object[] toArray() {
                return map.entrySet().toArray();
            }

            @Override
            public Object[] toArray(Object[] a) {
                return map.entrySet().toArray(a);
            }

            @Override
            public boolean add(Object o) {
                Map.Entry p = new ZArray(o);
                map.put(p.getKey(), p.getValue());
                return true;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection c) {
                return false;
            }

            @Override
            public boolean addAll(Collection c) {
                return false;
            }

            @Override
            public boolean removeAll(Collection c) {
                return false;
            }

            @Override
            public boolean retainAll(Collection c) {
                return false;
            }

            @Override
            public void clear() {
                map.clear();
            }
        };
    }

    @Override
    public Map<?,?> deepCopy() {
        ZMap zMap = new ZMap();
        ((Map<?,?>)this).forEach( (k,v) -> {
            Object key;
            Object value;
            if ( k instanceof ZTypes.StringX ){
                key = ((ZTypes.StringX<?>) k).deepCopy();
            } else {
                key = k;
            }
            if ( v instanceof ZTypes.StringX ){
                value = ((ZTypes.StringX<?>) v).deepCopy();
            } else {
                value = v;
            }
            zMap.put(key,value);
        });
        return zMap;
    }

    /**
     * An Empty immutable ZMap for various purposes
     */
    public static final ZMap EMPTY = new ZMap( Collections.emptyMap() );

    /**
     * Creates a map from a Collection of Entries
     * @param m the collector map
     * @param c the collection of entries
     * @return the m, collector map
     */
    static Map fromEntries(Map m, Collection c){
        Iterator iterator = c.iterator();
        while ( iterator.hasNext() ){
            Object o = iterator.next();
            if ( o == null ) {
                throw new UnsupportedOperationException("Map entry can not be null!");
            }
            if ( o instanceof Map.Entry ){
                m.put( ((Map.Entry) o).getKey(), ((Map.Entry) o).getValue() );
                continue;
            }
            if ( o.getClass().isArray() ){
                o = new ZArray(o);
            }
            if ( o instanceof List ){
                List l = (List)o;
                if ( l.size() < 2 ) throw new UnsupportedOperationException("Map entry can not be less than 2!");
                m.put(l.get(0) ,l.get(1));
            } else {
                throw new UnsupportedOperationException("Map entry can not be created with type : " + o.getClass());
            }
        }
        return m;
    }

    /**
     * Create a map by joining two collections, item by item
     * first collection is the key list, second is the value list
     * @param c1 key collection
     * @param c2 value collection
     * @return a map combining [ (key,value) ]
     */
    static Map joinEntries(Map m, Collection c1, Collection c2){
        Iterator iterator1 = c1.iterator();
        Iterator iterator2 = c2.iterator();

        while ( iterator1.hasNext() ){
            Object k = iterator1.next();
            if ( !iterator2.hasNext() ) {
                m.clear();
                throw new UnsupportedOperationException("Key,Value collection size mismatch!");
            }
            Object v = iterator2.next();
            m.put(k,v);
        }
        if ( iterator2.hasNext() ){
            m.clear();
            throw new UnsupportedOperationException("Key,Value collection size mismatch!");
        }
        return m;
    }

    /**
     * Creates a dictionary
     * @param f using function
     * @param args using arguments
     * @return the dictionary or map
     */
    static Map dictMaker(Map seed,  Function f,  Object...args){
        if (args.length == 0 ) return new ZMap( seed);
        if ( args[0] == null ) return EMPTY ;
        if (f == null ){
            if (  args[0] instanceof Map ){
                if ( args.length == 2 &&  ZTypes.bool(args[1],false)  ){
                    seed.putAll( (Map)args[0] ) ;
                    return new ZMap( seed  );
                }
                // now here, use this template
                Map act = ZTypes.safeOrDefault(  () -> ((ZMap)args[0]).map,  (Map)args[0] );
                if ( seed.getClass() == act.getClass() ){
                    return new ZMap( act );
                }
                seed.putAll(act);
                return new ZMap( seed );
            }
            if ( args.length == 1 ){
                if ( args[0].getClass().isArray() ){
                    return new ZMap(seed, args[0]) ;
                }
                if ( args[0] instanceof Collection ){
                    return new ZMap(seed, (Collection)args[0]);
                }
                // this will always be hash map ?
                return ZJVMAccess.dict(args[0]);
            }
            if ( args.length == 2 ){
                return new ZMap(seed, args[0],args[1]);
            }
            // are they list of pairs passing in ???
            try {
                return fromEntries( new ZMap(seed), ZArray.of(args));
            }catch ( Throwable t) {
                throw new IllegalArgumentException("Can not create a Map with these args!",t);
            }
        }

        Iterable iterable = Converter.iterable(args[0], "Can not covert to Map!" );
        return BaseZCollection.dict(new ZMap( seed ), f, iterable );
    }

    /**
     * Creates a dictionary
     * @param f using function
     * @param args using arguments
     * @return the dictionary or map
     */
    public static Map dict(Function f, Object...args){
        return dictMaker( new HashMap(), f, args);
    }

    /**
     * Creates an ordered dictionary - where entries remain in the order of inclusion
     * @param f using function
     * @param args using arguments
     * @return the ordered dictionary or map
     */
    public static Map orderedDict(Function f, Object...args){
        return dictMaker( new LinkedHashMap(), f, args);
    }

    /**
     * Creates a sorted dictionary - where keys are sorted
     * @param anons using functions list
     *              anons can be either empty or null
     *              if it has only one entry, then use this as the mapper
     *              if it is two one is the mapper, another is the comparator
     *              The type of the function matters
     * @param args using arguments
     * @return the ordered dictionary or map
     */
    public static Map sortedDict(List<Function> anons, Object...args){
        // because there are catastrophic issues with null and mixed mode objects
        final TreeMap tm = new TreeMap( ZCollection.ASC_NULL_ZMB_COMP);

        if (args.length == 0 ) return new ZMap( tm );
        if ( args.length == 1 && args[0] instanceof Comparator ){
            return new ZMap( new TreeMap( (Comparator)args[0] ) ) ;
        }
        if ( anons.isEmpty() ){
            return dictMaker( tm, null, args);
        }

        Converter.SeedAnon<TreeMap> sa = Converter.buildWithComparator( anons, TreeMap::new);
        return dictMaker( sa.seed, sa.mapper, args);
    }

    /**
     * Underlying actual map
     */
    protected Map map;

    @Override
    public Comparator comparator() {
        if ( map instanceof SortedMap ){ return ((SortedMap)map).comparator() ; }
        return null;
    }

    @Override
    public SortedMap subMap(Object fromKey, Object toKey) {
        if ( map instanceof SortedMap ){ return ((SortedMap)map).subMap(fromKey, toKey ) ; }
        return EMPTY ;
    }

    @Override
    public SortedMap headMap(Object toKey) {
        if ( map instanceof SortedMap ){ return ((SortedMap)map).headMap(toKey ) ; }
        return EMPTY;
    }

    @Override
    public SortedMap tailMap(Object fromKey) {
        if ( map instanceof SortedMap ){ return ((SortedMap)map).tailMap( fromKey ) ; }
        return EMPTY;
    }

    @Override
    public Object firstKey() {
        if ( map instanceof SortedMap ){ return ((SortedMap)map).firstKey() ; }
        return Function.NIL ;
    }

    @Override
    public Object lastKey() {
        if ( map instanceof SortedMap ){ return ((SortedMap)map).lastKey() ; }
        return Function.NIL ;
    }

    @Override
    public Entry lowerEntry(Object key) {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).lowerEntry(key) ; }
        return null;
    }

    @Override
    public Object lowerKey(Object key) {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).lowerKey(key) ; }
        return Function.NIL;
    }

    @Override
    public Entry floorEntry(Object key) {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).floorEntry(key) ; }
        return null;
    }

    @Override
    public Object floorKey(Object key) {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).floorKey(key) ; }
        return Function.NIL;
    }

    @Override
    public Entry ceilingEntry(Object key) {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).ceilingEntry(key) ; }
        return null;
    }

    @Override
    public Object ceilingKey(Object key) {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).ceilingKey(key) ; }
        return Function.NIL;
    }

    @Override
    public Entry higherEntry(Object key) {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).higherEntry(key) ; }
        return null;
    }

    @Override
    public Object higherKey(Object key) {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).higherKey(key) ; }
        return Function.NIL;
    }

    @Override
    public Entry firstEntry() {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).firstEntry() ; }
        return null;
    }

    @Override
    public Entry lastEntry() {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).lastEntry() ; }
        return null;
    }

    @Override
    public Entry pollFirstEntry() {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).pollFirstEntry() ; }
        return null;
    }

    @Override
    public Entry pollLastEntry() {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).pollLastEntry() ; }
        return null;
    }

    @Override
    public NavigableMap descendingMap() {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).descendingMap() ; }
        return EMPTY;
    }

    @Override
    public NavigableSet navigableKeySet() {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).navigableKeySet() ; }
        return ZSet.EMPTY;
    }

    @Override
    public NavigableSet descendingKeySet() {
        if ( map instanceof NavigableMap ){ return ((NavigableMap)map).descendingKeySet() ; }
        return ZSet.EMPTY;
    }

    @Override
    public NavigableMap subMap(Object fromKey, boolean fromInclusive, Object toKey, boolean toInclusive) {
        if ( map instanceof NavigableMap ){
            return ((NavigableMap)map).subMap(fromKey,fromInclusive,toKey,toInclusive) ;
        }
        return EMPTY;
    }

    @Override
    public NavigableMap headMap(Object toKey, boolean inclusive) {
        if ( map instanceof NavigableMap ){
            return ((NavigableMap)map).headMap(toKey,inclusive) ;
        }
        return EMPTY;
    }

    @Override
    public NavigableMap tailMap(Object fromKey, boolean inclusive) {
        if ( map instanceof NavigableMap ){
            return ((NavigableMap)map).tailMap(fromKey,inclusive) ;
        }
        return EMPTY;
    }

    /**
     * Relates a dictionary against another
     * @param m1 one dictionary
     * @param m2 another
     * @return Set relation between them
     */
    public static ZCollection.Relation relate(Map m1, Map m2){
        Set keySet1 = m1.keySet();
        Set keySet2 = m2.keySet();
        ZCollection.Relation relation = BaseZCollection.relate(keySet1,keySet2);
        if ( ZCollection.Relation.INDEPENDENT  == relation ) { return  relation ; }
        // do they really?
        Set intersection = BaseZCollection.intersection( keySet1,keySet2 );
        for ( Object k : intersection ){
            Object v1 = m1.get(k);
            Object v2 = m2.get(k);
            if ( !ZTypes.equals(v1,v2) ) return ZCollection.Relation.INDEPENDENT ;
        }
        return relation;
    }

    /**
     * Calculates intersection of a dictionary against another
     * Key,value must match
     * @param m1 one dictionary
     * @param m2 another
     * @return intersection between them
     */
    public static ZMap intersection( Map m1, Map m2){
        Set intersection = BaseZCollection.intersection(m1.keySet(), m2.keySet() );
        ZMap map = new ZMap();
        for ( Object k : intersection ){
            Object v1 = m1.get(k);
            Object v2 = m2.get(k);
            if ( ZTypes.equals(v1,v2) ){
                map.map.put(k,v1);
            }
        }
        return map;
    }

    /**
     * Mutably calculates intersection of a dictionary against another
     * Key,value must match
     * @param m1 one dictionary
     * @param m2 another
     * @return intersection between them as m1
     */
    public static Map intersectionMutable( Map m1, Map m2){
        Set intersection = BaseZCollection.intersection(m1.keySet(), m2.keySet() );
        Set ks = new HashSet<>( m1.keySet() );
        for ( Object k : ks ) {
            if ( intersection.contains(k) ){
                Object v1 = m1.get(k);
                Object v2 = m2.get(k);
                if ( !ZTypes.equals(v1,v2) ){
                    m1.remove(k);
                }
            } else {
                m1.remove(k);
            }
        }
        return m1;
    }

    /**
     * Calculates union of a dictionary against another
     * Key,value must match
     * @param m1 one dictionary
     * @param m2 another
     * @return union between them
     */
    public static ZMap union( Map m1, Map m2){
        Set intersection = BaseZCollection.intersection(m1.keySet(), m2.keySet() );
        ZMap map = new ZMap();
        for ( Object k : intersection ){
            Object v1 = m1.get(k);
            Object v2 = m2.get(k);
            if ( !ZTypes.equals(v1,v2) ){
                map.map.put(k, ZArray.of( v1, v2 ) );
            }else{
                map.map.put(k,v1);
            }
        }
        for ( Object k : m1.keySet() ){
            if ( !intersection.contains(k) ){
                map.map.put(k, m1.get(k));
            }
        }
        for ( Object k : m2.keySet() ){
            if ( !intersection.contains(k) ){
                map.map.put(k, m2.get(k));
            }
        }
        return map;
    }

    /**
     * Mutably calculates union of a dictionary against another
     * Key,value must match
     * @param m1 one dictionary
     * @param m2 another
     * @return union between them as m1
     */
    public static Map unionMutable( Map m1, Map m2){
        Set union = BaseZCollection.union(m1.keySet(), m2.keySet() );
        for ( Object k : union ){
            if ( m1.containsKey(k) ){
                Object v1 = m1.get(k);
                Object v2 = m2.get(k);
                if ( !Objects.equals(v1, v2 )){
                    m1.put(k,ZArray.of(v1,v2));
                }
            } else {
                m1.put( k, m2.get(k));
            }
        }
        return m1;
    }

    /**
     * Calculates difference between a dictionary against another
     * Key,value must match
     * @param m1 one dictionary
     * @param m2 another
     * @return difference between them
     */
    public static ZMap difference( Map m1, Map m2){
        if ( m1 instanceof MultiSet && m2 instanceof MultiSet ){
            return MultiSet.difference( (MultiSet)m1, (MultiSet)m2 );
        }
        Set intersection = BaseZCollection.intersection(m1.keySet(), m2.keySet() );
        ZMap map = new ZMap();
        for ( Object k : intersection ){
            Object v1 = m1.get(k);
            Object v2 = m2.get(k);
            if ( !ZTypes.equals(v1,v2) ){
                map.map.put(k, v1 );
            }
        }
        for ( Object k : m1.keySet() ){
            if ( !intersection.contains(k) ){
                map.map.put(k, m1.get(k));
            }
        }
        return map;
    }

    /**
     * Mutably calculates difference between a dictionary against another
     * Key,value must match
     * @param m1 one dictionary
     * @param m2 another
     * @return difference between them as m1
     */
    public static Map differenceMutable( Map m1, Map m2){
        // why it is not optimized for larger and smaller?
        // because there is a concurrent access problem
        // to mitigate that, one must copy the mutable map key set if required
        for ( Object k : m2.keySet() ){
            if ( m1.containsKey(k)){
                Object v1 = m1.get(k);
                Object v2 = m2.get(k);
                if ( ZTypes.equals(v1,v2)){
                    m1.remove(k);
                }
            }
        }
        return m1;
    }


    /**
     * Calculates Symmetric difference between a dictionary against another
     * Key,value must match
     * @param m1 one dictionary
     * @param m2 another
     * @return symmetric difference between them
     */
    public static ZMap symmetricDifference( Map m1, Map m2){
        Set intersection = BaseZCollection.intersection(m1.keySet(), m2.keySet() );
        ZMap map = new ZMap();
        for ( Object k : m1.keySet() ){
            if ( !intersection.contains(k) ){
                map.map.put(k, m1.get(k));
            }
        }
        for ( Object k : m2.keySet() ){
            if ( !intersection.contains(k) ){
                map.map.put(k, m2.get(k));
            }
        }
        // now the intersection ones which has different values?
        for ( Object k : intersection ){
            Object v1 = m1.get(k);
            Object v2 = m2.get(k);
            if ( !Objects.equals(v1,v2) ) {
                map.map.put(k, ZArray.of(v1,v2));
            }
        }
        return map;
    }

    /**
     * Mutably calculates Symmetric difference between a dictionary against another
     * Key,value must match
     * @param m1 one dictionary
     * @param m2 another
     * @return symmetric difference between them as m1
     */
    public static Map symmetricDifferenceMutable( Map m1, Map m2){
        Set intersection = BaseZCollection.intersection(m1.keySet(), m2.keySet() );
        for ( Object k : m1.keySet() ){
            if ( intersection.contains(k)){
                Object v1 = m1.get(k);
                Object v2 = m2.get(k);
                if ( Objects.equals(v1,v2) ) {
                    m1.remove(k);
                } else {
                    m1.put(k, ZArray.of(v1,v2));
                }
            }
        }
        for ( Object k : m2.keySet() ){
            if ( !intersection.contains(k)){
                m1.put(k,m2.get(k));
            }
        }
        return m1;
    }

    /**
     * Creates a ZMap from array
     * @param seed underlying map
     * @param arr the array
     */
    public ZMap(Map seed, Object arr){
        this( fromEntries( seed, new ZArray(arr)));
    }

    /**
     * Creates a ZMap from underlying map
     * @param m the underlying map
     */
    public ZMap(Map m) {
        if ( m instanceof ZMap ){ // clone data when copying from other ZMap
            map = fromMap(((ZMap) m).map).map ;
        }else { // wrap around, do not clone
            // is this the right behaviour?
            // will there be issues?
            map = m;
        }
    }

    /**
     * Creates a ZMap from underlying Entry collection
     * @param seed original map to be used
     * @param c the underlying Entry collection
     */
    public ZMap(Map seed, Collection c) {
        map = fromEntries(seed,c) ;
    }

    /**
     * Create a map using
     * @param seed original map to be used
     * @param keys array/collection of keys
     * @param values array/collection of values
     */
    public ZMap(Map seed, Object keys, Object values){
        if ( keys == null ) throw new IllegalArgumentException("null as keys!");
        if ( values == null ) throw new IllegalArgumentException("null as values!");
        if ( keys.getClass().isArray() ){
            keys = new ZArray(keys);
        }
        if ( values.getClass().isArray() ){
            values = new ZArray(values);
        }
        if ( keys instanceof Collection && values instanceof Collection ) {
            map = joinEntries(seed, (Collection)keys,(Collection)values);
        } else {
            throw new IllegalArgumentException("Unsupported types as keys or values!");
        }
    }

    /**
     * Create an empty map
     */
    public ZMap() {
        this(new HashMap());
    }

    @Override
    public boolean containsKey(Object key) {
        return map.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return map.containsValue(value);
    }

    @Override
    public Object get(Object key) {
        return map.get(key);
    }

    @Override
    public Object put(Object key, Object value) {
        return map.put(key,value);
    }

    @Override
    public void putAll(Map m) {
        map.putAll(m);
    }

    @Override
    public Set keySet() {
        return new ZSet(map.keySet());
    }

    @Override
    public Collection values() {
        return new ZList(map.values());
    }

    @Override
    public Set entrySet() {
        return new ZSet(map.entrySet());
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public Object remove(Object key) {
        return map.remove(key);
    }

    @Override
    public int compareTo(Object o) {
        if ( o == null ) return 1;
        if ( o == this ) return 0 ;
        if ( o instanceof Map ) {
            ZCollection.Relation r ;
            if ( o instanceof ZMap ){
                r = relate(this.map, ((ZMap) o).map);
            }else{
                r = relate(this.map, (Map) o);
            }
            if ( r == ZCollection.Relation.SUB ) return -1;
            if ( r == ZCollection.Relation.EQUAL ) return 0;
            if ( r == ZCollection.Relation.SUPER ) return 1;
            throw new UnsupportedOperationException("Can not compare unrelated Maps!");
        }
        if ( o instanceof Collection ) {
            // create another map, and forward
            Map other = fromEntries( new HashMap(), (Collection)o );
            return compareTo(other);
        }
        throw new UnsupportedOperationException("Can not compare Map against another object!");
    }


    private ZMap fromMap(Map m) {
        if (m instanceof LinkedHashMap) return new ZMap( new LinkedHashMap(m) );
        if (m instanceof TreeMap) return new ZMap(new TreeMap(m));
        return new ZMap(new HashMap(m));
    }

    /**
     * Gets a clone of the map
     * Underlying container is same as that of the container
     * @return a clone of the map
     */
    public ZMap copy(){
        return fromMap(this.map);
    }

    @Override
    public Object _add_(Object o) {
        if ( o instanceof Map ){
            return union(this.map,(Map)o);
        }
        final Map.Entry p = new ZArray(o);
        if ( this.map.containsKey(p.getKey())) {
            Object v = this.map.get(p.getKey());
            if ( Objects.equals(v, p.getValue()) ){
                // only when there is no change in the map
                return this;
            }
        }
        Map other = copy();
        other.put(p.getKey(), p.getValue());
        return other;
    }

    @Override
    public Object _sub_(Object o) {
        if ( o instanceof Map ){
            return difference(copy(),(Map)o);
        }
        if ( this.map.containsKey(o)){
            // create a new map
            Map other = copy();
            other.remove(o);
            return other;
        }
        // otherwise, no point creating a new dict
        return this;
    }

    @Override
    public Object _mul_(Object o) {
        // multiply map would have the effect of
        // https://gitlab.com/non.est.sacra/zoomba/-/issues/25
        // this should intersect the entries and then union the values in the common keys
        if ( !(o instanceof Map) ) throw new UnsupportedOperationException("Multiplier is not a dictionary!");
        Map other = (Map)o;
        Map smaller = other;
        Map larger = this;
        if ( smaller.size() > larger.size() ){
            larger = smaller;
            smaller = this;
        }
        Map result = new ZMap();
        for ( Object key : smaller.keySet() ){
            if ( larger.containsKey(key) ){
                result.put( key, of(  get( key), other.get(key)) );
            }
        }
        return result;
    }

    /**
     * Given the value find set of all keys which points to the value
     * @param o the object
     * @return set of all keys pointing to the value
     */
    @Override
    public Object _div_(Object o) {
        Set s = new ZSet();
        for ( Object k : map.keySet() ){
            Object v = map.get(k);
            if ( ZTypes.equals(v,o) ){
                s.add(k);
            }
        }
        return s;
    }

    @Override
    public Object _pow_(Object o) {
        throw new UnsupportedOperationException("No support for exponentiation in Map!");
    }

    @Override
    public void add_mutable(Object o) {
        ZArray p = new ZArray(o);
        map.put(p.get(0),p.get(1));
    }

    @Override
    public void sub_mutable(Object o) {
        if ( o instanceof Map ){
            map = differenceMutable(map,(Map)o);
            return;
        }
        if ( map.containsKey( o ) ){
            map.remove(o);
        }
    }

    @Override
    public void mul_mutable(Object o) {
        throw new UnsupportedOperationException("Can not multiply Map!");
    }

    @Override
    public void div_mutable(Object o) {
        throw new UnsupportedOperationException("Division of a Map is immutable!");
    }

    @Override
    public Object _and_(Object o) {
        if ( !(o instanceof Map) ) throw new UnsupportedOperationException("Intersection with a Non Map!");
        return intersection(this.map, (Map)o);
    }

    @Override
    public Object _or_(Object o) {
        if ( !(o instanceof Map) ) throw new UnsupportedOperationException("Union with a Non Map!");
        return union(this.map, (Map)o);
    }

    @Override
    public Object _xor_(Object o) {
        if ( !(o instanceof Map) ) throw new UnsupportedOperationException("Symmetric Difference with a Non Map!");
        return symmetricDifference(this.map, (Map)o);
    }

    @Override
    public void and_mutable(Object o) {
        if ( !(o instanceof Map) ) throw new UnsupportedOperationException("Intersection with a Non Map!");
        map = intersectionMutable(map,(Map)o);
    }

    @Override
    public void or_mutable(Object o) {
        if ( !(o instanceof Map) ) throw new UnsupportedOperationException("Union with a Non Map!");
        map = unionMutable(map,(Map)o);
    }

    @Override
    public void xor_mutable(Object o) {
        if ( !(o instanceof Map) ) throw new UnsupportedOperationException("Symmetric Difference with a Non Map!");
        map = symmetricDifferenceMutable(map,(Map)o);
    }

    /* Java 8 Exotics ... */

    @Override
    public Object getOrDefault(Object key, Object defaultValue) {
        if ( map.containsKey( key ) ) return map.get(key);
        return defaultValue;
    }

    @Override
    public Object putIfAbsent(Object key, Object value) {
        if ( map.containsKey( key ) )  return map.get(key);
        return map.put(key,value);
    }

    @Override
    public boolean remove(Object key, Object value) {
        if ( map.containsKey(key)){
            Object v = map.get(key);
            if ( Objects.deepEquals( value, v) ){
                map.remove(key);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean replace(Object key, Object oldValue, Object newValue) {
        if ( map.containsKey(key)){
            Object v = map.get(key);
            if ( Objects.deepEquals( oldValue, v) ){
                map.put(key,newValue);
                return true;
            }
        }
        return false;
    }

    @Override
    public Object replace(Object key, Object value) {
        if ( map.containsKey(key)){
            return map.put(key,value);
        }
        return null;
    }

    @Override
    public String toString() {
        return map.toString();
    }

    @Override
    public String string(Object... args) {
        if  ( args.length == 0 ) return map.toString();
        boolean json = ZTypes.bool(args[0],false);
        if ( json ){
            return ZTypes.jsonString(this.map);
        }
        return map.toString();
    }

    @Override
    public int hashCode() {
        return map.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if ( obj == null ) return false;
        if ( obj == this ) return true;
        // check the class? Nonsense...
        try {
            return compareTo(obj) == 0;
        }catch ( UnsupportedOperationException uoe){
            return false;
        }
    }

    /**
     * For a multi set implementation
     */
    public static class MultiSet extends ZMap {

        static ZCollection.Relation relate(MultiSet ms1, MultiSet ms2){
            Map<Object,Integer> m1 = ms1;
            Map<Object,Integer> m2 = ms2;
            ZSet ks1 = (ZSet) m1.keySet();
            ZSet ks2 = (ZSet) m2.keySet();
            // case 1 : ks1 and ks2 just overlap and nothing more
            ZCollection.Relation r = ks1.relate(ks2);
            if ( r == ZCollection.Relation.OVERLAP || r == ZCollection.Relation.INDEPENDENT ) return r;
            if ( r == ZCollection.Relation.SUB ){
                // is it really ?
                for ( Object key : ks1 ){
                    int leftCount = m1.get(key);
                    int rightCount = m2.get(key);
                    if ( leftCount > rightCount ) return ZCollection.Relation.OVERLAP;
                }
                return r;
            }
            if ( r == ZCollection.Relation.SUPER ){
                // is it really ?
                for ( Object key : ks2 ){
                    int leftCount = m1.get(key);
                    int rightCount = m2.get(key);
                    if ( leftCount < rightCount ) return ZCollection.Relation.OVERLAP;
                }
                return r;
            }
            // this is where they are equal, are they ?
            // it can be all of them, super, sub, equal, now?
            boolean sup = true;
            for ( Object key : ks2 ){
                int leftCount = m1.get(key);
                int rightCount = m2.get(key);
                if ( leftCount < rightCount ) {
                    sup = false ;
                    break;
                }
            }
            boolean sub = true;
            for ( Object key : ks1 ){
                int leftCount = m1.get(key);
                int rightCount = m2.get(key);
                if ( leftCount > rightCount ) {
                    sub = false;
                    break;
                }
            }
            if ( sub && sup ) return ZCollection.Relation.EQUAL ;
            if ( sub  ) return ZCollection.Relation.SUB ;
            if ( sup  ) return ZCollection.Relation.SUPER ;
            return ZCollection.Relation.OVERLAP;
        }

        /**
         * Constructs a MultiSet
         * @param src underlying counter map
         */
        public MultiSet(Map<Object,Integer> src){
            super(src);
        }

        /**
         * Constructs an empty MultiSet
         */
        public MultiSet(){
            this(new ZMap());
        }

        @Override
        public Object _and_(Object o) {
            if ( !(o instanceof MultiSet) ){
                return super._and_(o);
            }
            MultiSet ms = new MultiSet();
            Map<Object,Integer> m1 = this;
            Map<Object,Integer> m2 = (MultiSet)o;
            ZSet ks1 = (ZSet) keySet();
            ZSet ks2 = (ZSet) m2.keySet();
            ZCollection i = ks1.intersection(ks2);
            for ( Object k : i ){
                int leftCount = m1.get(k);
                int rightCount = m2.get(k);
                int count = Math.min(leftCount,rightCount);
                ms.put(k,count);
            }
            return ms;
        }

        @Override
        public Object _or_(Object o) {
            if ( !(o instanceof MultiSet) ){
                return super._or_(o);
            }
            MultiSet ms = new MultiSet();
            Map<Object,Integer> m1 = this;
            Map<Object,Integer> m2 = (MultiSet)o;
            ZSet ks1 = (ZSet) keySet();
            ZSet ks2 = (ZSet) m2.keySet();
            ZCollection u = ks1.union(ks2);
            for ( Object k : u ){
                int leftCount = m1.getOrDefault(k,0);
                int rightCount = m2.getOrDefault(k,0);
                int count = Math.max(leftCount,rightCount);
                ms.put(k,count);
            }
            return ms;
        }

        static MultiSet difference( MultiSet ms1, MultiSet ms2){
            Map<Object,Integer> m1 = (Map)ms1;
            Map<Object,Integer> m2 = (Map)ms2;
            MultiSet ms = new MultiSet();
            for ( Object lk : m1.keySet() ){
                int count = m1.get(lk) - m2.getOrDefault(lk,0);
                if ( count > 0 ){
                    ms.put(lk,count);
                }
            }
            return ms;
        }

        @Override
        public Object _xor_(Object o) {
            if ( !(o instanceof MultiSet) ){
                return super._xor_(o);
            }
            MultiSet or = (MultiSet) _or_(o);
            MultiSet and = (MultiSet) _and_(o);
            return difference( or, and );
        }


        @Override
        public void and_mutable(Object o) {
            Map and = (Map) _and_(o);
            clear();
            putAll(and);
        }

        @Override
        public void or_mutable(Object o) {
            Map or = (Map) _or_(o);
            clear();
            putAll(or);
        }

        @Override
        public void xor_mutable(Object o) {
            Map xor = (Map) _xor_(o);
            clear();
            putAll(xor);
        }

        @Override
        public int compareTo(Object o) {
            if ( o instanceof MultiSet ){
                ZCollection.Relation r = relate(this,(MultiSet)o);
                switch ( r ){
                    case SUB:
                        return -1;
                    case SUPER:
                        return 1;
                    case EQUAL:
                        return 0;
                    default:
                        throw new UnsupportedOperationException("Multisets are not comparable!");
                }
            }
            return super.compareTo(o);
        }
    }
}
