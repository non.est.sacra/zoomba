/*
 * Copyright 2025 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.interpreter;

import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.Observable;
import zoomba.lang.core.operations.Retry;
import zoomba.lang.core.operations.Timeout;
import zoomba.lang.core.sys.ZThread;
import zoomba.lang.core.types.ZException;
import zoomba.lang.core.types.ZTypes;
import zoomba.lang.parser.ASTBlock;
import zoomba.lang.parser.ASTDSLInjector;
import zoomba.lang.parser.ZoombaNode;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.Callable;

/**
 * A construct to create pluggable architecture in ZoomBA
 */
public interface DSLPlugin {

    /**
     * A constant for converting nano sec to sec
     */
    double NANO_TO_SEC = 0.000000001;

    /**
     * Process the plugin
     * @param interpret underlying interpreter
     * @param config configuration of the plugin
     * @param block actual plugin code block
     * @param data in case of any data needs to be passed
     * @return result of plugin execution
     * @throws Exception in case of any error
     */
    Object process(ZInterpret interpret, Map<String, Object> config, ASTBlock block, Object data) throws Exception;

    /**
     * Plugin Registry
     * Simple idea, add name and Plugin
     */
    Map<String, DSLPlugin> REGISTRY = new LinkedHashMap<>();

    /**
     * Process the plugin
     * @param interpret underlying interpreter
     * @param node plugin node
     * @param data optional data
     * @return result of the plugin processing
     */
    static Object visit(ZInterpret interpret, ASTDSLInjector node, Object data) {
        String pluginIdentifier = node.jjtGetChild(0).jjtAccept(interpret, data).toString();
        DSLPlugin plugin = REGISTRY.get(pluginIdentifier);
        if (plugin == null) throw new ZException.Plugin(node, pluginIdentifier);
        ASTBlock blockNode = (ASTBlock) node.jjtGetChild(node.jjtGetNumChildren() - 1);
        final Map<String, Object> config;
        if (node.jjtGetNumChildren() < 3) {
            config = Collections.emptyMap();
        } else {
            final ZoombaNode configNode = (ZoombaNode) node.jjtGetChild(1);
            try {
                Object[] args = (Object[]) configNode.jjtAccept(interpret, data);
                if ( args.length == 1 && args[0] instanceof Map){
                    config = ( Map<String, Object>) args[0];
                } else {
                    throw new IllegalArgumentException("Plugins must not have non named args!");
                }
            } catch (Throwable t) {
                throw new ZException.Plugin(configNode, pluginIdentifier, t);
            }
        }

        try {
            return plugin.process(interpret, config, blockNode, data);
        } catch (Throwable t) {
            throw new ZException.Plugin(node, pluginIdentifier, t);
        }
    }

    /**
     * The atomic construct
     */
    DSLPlugin ATOMIC = (interpret, config, block, data) -> {
        synchronized (interpret) { // lock the interpreter
            synchronized (block) { // lock the piece of code block
                // preserve state ??
                // get a new Context
                ZContext current = interpret.frames.peek();
                ZContext sandBox = new ZContext.MapContext(current);
                interpret.frames.push(sandBox);
                // save globals too?
                Map<String, Object> globalState = new HashMap<>(interpret.danceMaster.globals);
                try {
                    Object o = block.jjtAccept(interpret, data);
                    // if things are right then
                    interpret.frames.pop(); // remove sandBox
                    interpret.frames.pop(); // remove parent
                    interpret.frames.push(sandBox); // put sandBox as Ok one
                    // clear globals
                    globalState.clear();
                    return o;
                } catch (Throwable t) {
                    interpret.frames.pop(); // remove sandBox
                    // reset global
                    interpret.danceMaster.globals.clear();
                    interpret.danceMaster.globals.putAll(globalState);
                    return t;
                }
            }
        }
    };

    /**
     * The clock construct
     */
    DSLPlugin CLOCK = (interpret, config, block, data) -> {
        final int startStackSize = interpret.callStack.size();
        final Instant start = Instant.now() ;
        Instant end;
        Object o;
        try {
            o = block.jjtAccept(interpret, data);
            end = Instant.now();
        } catch (Throwable t) {
            end = Instant.now();
            ZException.consumeTrace( t, interpret, startStackSize );
            o = t;
        }
        return new Object[]{NANO_TO_SEC *  ( start.until(end, ChronoUnit.NANOS) ) , o};
    };

    /**
     * The retry construct
     */
    DSLPlugin RETRY = (interpret, config, block, data) -> {
        Object handler = config.get(Retry.HANDLER);
        if ( handler instanceof ZScriptMethod ){ // special casing, in case it is defined and required
            ZScriptMethod.ZScriptMethodInstance instance =  ((ZScriptMethod) handler).instance(interpret);
            config.put(Retry.HANDLER, instance);
        }
        Retry retry = Retry.fromConfig(config);
        Callable<Object> callable = retry.callable( () -> block.jjtAccept(interpret, data) );
        return callable.call();
    };

    /**
     * The TimeOut construct
     */
    DSLPlugin TIMEOUT = (interpret, config, block, data) -> {
        final Timeout timeout = Timeout.fromConfig(config);
        final AnonymousFunctionInstance.MapperLambda f = new AnonymousFunctionInstance.MapperLambda(interpret,block);
        final Function.NamedArgs na = Function.NamedArgs.fromMap(config);
        final ZThread zt = new ZThread(f, na );
        zt.printNoErrAsync(true);
        final Callable<Object> callable = timeout.callable( () -> zt.call().runTimeExceptionOnError().value() );
        try {
            return callable.call();
        }finally {
            interpret.callStack.addAll( zt.errorStack() );
        }
    };

    /**
     * The Observable construct
     */
    DSLPlugin OBSERVABLE = (interpret, config, block, data) -> {
        boolean mutate = ZTypes.bool(config.getOrDefault(Observable.ZObservable.MUTATE_ENVIRONMENT, false), false).booleanValue();
        return new Observable.ZObservable(block, interpret, data, mutate);
    };

    /**
     * Bunch of Default plugins to register
     */
    static void defaultRegistration() {
        REGISTRY.put("atomic", ATOMIC); // Atomic operations
        REGISTRY.put("clock", CLOCK); // perf tuning
        REGISTRY.put("def", OBSERVABLE); // reactive programming with zmb
        REGISTRY.put("retry", RETRY); // retrying a block of code
        REGISTRY.put("timeout", TIMEOUT); // timing out a block of code
    }
}
