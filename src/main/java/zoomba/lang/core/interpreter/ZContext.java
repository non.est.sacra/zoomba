/*
 * Copyright 2025 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.interpreter;

import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.operations.Function;

import javax.script.Bindings;
import java.util.*;

/**
 * The Memory and Variable Management for ZoomBA
 */
public interface ZContext {

    /**
     * An Optional Monad with nothing in it
     */
    Function.MonadicContainer NOTHING = new Function.MonadicContainerBase();

    /**
     * Parent Context
     * @return gets the parent context if any, or null
     */
    ZContext parent();

    /**
     * Given a variable name returns the value if possible, ele NOTHING
     * @param name of the variable
     * @return an Optional Monad with value if the variable name exists, else NOTHING
     */
    Function.MonadicContainer get(String name);

    /**
     * Checks if the memory map has the variable name
     * @param name of the variable
     * @return true if variable exists, false otherwise
     */
    boolean has(String name);

    /**
     * Binds a value to a name
     * @param name the name of the variable
     * @param value the value which you want to bind to
     */
    void set(String name, Object value);

    /**
     * Unbind a variable
     * @param name of the name
     * @return true if could unbind, false if such name does not exist
     */
    boolean unSet(String name);

    /**
     * Unbind all the variables
     */
    void clear();

    /**
     * Implementation of an Empty Context
     * One instance - you need a singleton
     */
    ZContext  EMPTY_CONTEXT = new ZContext(){
        @Override
        public ZContext parent() {
            return this;
        }

        @Override
        public Function.MonadicContainer get(String name) {
            return NOTHING ;
        }

        @Override
        public boolean has(String name) {
            return false;
        }

        @Override
        public void set(String name, Object value) { }

        @Override
        public boolean unSet(String name) {
            return false;
        }

        @Override
        public void clear() {}
    };

    /**
     * A Context which will be used to pass arguments to a function in ZoomBA
     */
    class ArgContext implements ZContext{

        /**
         * Empty context needed to start it up
         */
        public static final ArgContext EMPTY_ARGS_CONTEXT = new ArgContext( ZArray.EMPTY_ARRAY  );

        Object[] values;

        Map<String,Integer> lookup;

        Function.MonadicContainer instance = NOTHING ;

        Function.MonadicContainer monadicValues;

        /**
         * Handling the $his parameter - me, or self. Set that up
         * @param instance the self,this,or me parameter
         */
        public void setInstance(Object instance){
            this.instance = new Function.MonadicContainerBase(instance);
        }

        /**
         * A constructor
         * @param lookup underlying lookup
         * @param values underlying values
         */
        public ArgContext(Map<String,Integer> lookup, Object[] values){
            this.lookup = lookup;
            this.values = values;
            monadicValues = new Function.MonadicContainerBase(values);
        }


        /**
         * List of parameters
         * @param params the parameters, ordered
         * @param argValues the list of values corresponding the parameters
         */
        public ArgContext(List<String> params, List<Function.MonadicContainer> argValues){
            lookup = new HashMap<>();
            values = new Object[argValues.size()];
            for ( int i = 0 ; i < values.length; i++ ){
                Function.MonadicContainer a = argValues.get(i);
                values[i] = a.value() ;
                lookup.put(params.get(i),i);
            }
            monadicValues = new Function.MonadicContainerBase(values);
        }

        /**
         * Not named, so variable argument
         * @param args arguments to create an argument
         */
        public ArgContext(Object...args){
            lookup = Collections.emptyMap() ;
            values = new Object[args.length];
            for ( int i = 0 ; i < values.length; i++ ){
                values[i] = args[i];
            }
            monadicValues = new Function.MonadicContainerBase(values);
        }

        @Override
        public ZContext parent() {
            return EMPTY_CONTEXT;
        }

        @Override
        public Function.MonadicContainer get(String name) {
            Integer i = lookup.get(name);
            if ( i == null ) { // think multithreaded...
                if ( ZScriptMethod.ARGS_VAR.equals(name) ){
                    return monadicValues ;
                }
                if ( Function.THIS.equals(name) && !instance.isNil() ){
                    return instance ;
                }
                return NOTHING; // it does not exist...
            }
            return new Function.MonadicContainerBase(values[i]);
        }

        @Override
        public boolean has(String name) {
            return  ( !instance.isNil() && Function.THIS.equals(name) ) ||
                    ( ZScriptMethod.ARGS_VAR.equals(name) || lookup.containsKey(name) ) ;
        }

        @Override
        public void set(String name, Object value) {
            if ( Function.THIS.equals(name) || ZScriptMethod.ARGS_VAR.equals(name) ) return;
            Integer i = lookup.get(name); // think multithreaded...
            if ( i == null ) return; // can not set anything...
            values[i] = value ;
        }

        @Override
        public boolean unSet(String name) {
            return false;
        }

        @Override
        public void clear() {
            lookup.clear();
        }
    }

    /**
     * Implementation of a Context using Map
     */
    class MapContext implements ZContext, Bindings {

        /**
         * Underlying map to store context information
         * A Memory if you like
         */
        public final Map<String,Object> map ;

        @Override
        public Object put(String name, Object value) {
            return map.put(name,value);
        }

        @Override
        public void putAll(Map<? extends String, ?> toMerge) {
            map.putAll(toMerge);
        }

        @Override
        public boolean containsKey(Object key) {
            return map.containsKey(key);
        }

        @Override
        public Object get(Object key) {
            return map.get(key);
        }

        @Override
        public Object remove(Object key) {
            return map.remove(key);
        }

        @Override
        public int size() {
            return map.size();
        }

        @Override
        public boolean isEmpty() {
            return map.isEmpty();
        }

        @Override
        public boolean containsValue(Object value) {
            return map.containsValue(value);
        }

        @Override
        public Set<String> keySet() {
            return map.keySet();
        }

        @Override
        public Collection<Object> values() {
            return map.values();
        }

        @Override
        public Set<Entry<String, Object>> entrySet() {
            return map.entrySet();
        }

        final ZContext parent;

        /**
         * Create a new Context
         * @param parent from parent
         */
        public MapContext(ZContext parent){
            map = new HashMap<>();
            this.parent = parent ;
        }

        /**
         * Starts with Empty Parent Context
         */
        public MapContext(){
            this(EMPTY_CONTEXT);
        }

        /**
         * Creates a MapContext from a Bindings
         * Exposed as Map of String to Object
         * @param bindings for use with javax.Bindings to create a Context
         */
        public MapContext(Map<String,Object> bindings){
            // remember, reference copy, not clone
            map = bindings;
            // and parent does not exist...
            this.parent = EMPTY_CONTEXT;
        }

        @Override
        public ZContext parent() {
            return parent;
        }

        @Override
        public Function.MonadicContainer get(String name) {
            if ( map.containsKey(name) ){
                return new Function.MonadicContainerBase( map.get(name) );
            }
            if ( parent.has(name ) ){
                return parent.get(name);
            }
            return NOTHING ;
        }

        @Override
        public boolean has(String name) {
            // if I have ....
            if  ( map.containsKey(name) ) return true ;
            // or my parents have
            return parent.has(name);
        }

        @Override
        public void set(String name, Object value) {
            // straight forward, for this guy ...
            map.put(name,value);
        }

        @Override
        public boolean unSet(String name) {
            if ( map.containsKey(name) ){
                map.remove(name);
                return true ;
            }
            return false ;
        }

        @Override
        public void clear() {
            map.clear();
        }
    }

    /**
     * This is to be used for calling functions in ZoomBA
     */
    class FunctionContext extends MapContext{

        /**
         * The context for the arguments passed onto the function
         */
        public ArgContext argContext ;

        /**
         * Creates a function context with
         * @param parent parent context
         * @param argContext the arguments for the function
         */
        public FunctionContext(ZContext parent, ArgContext argContext){
            super(parent);
            this.argContext = argContext ;
        }

        /**
         * Creates a Function Context from a Bindings
         * Generally exposed as Map of String to Object
         * @param bindings for usage with javax.Bindings to create a Function Context
         */
        public FunctionContext(Map<String,Object> bindings){
            super(bindings);
            this.argContext = ArgContext.EMPTY_ARGS_CONTEXT;
        }

        /**
         * Creates a Function Context from empty Mutable Map
         */
        public FunctionContext(){
            super();
            this.argContext = ArgContext.EMPTY_ARGS_CONTEXT;
        }

        @Override
        public Function.MonadicContainer get(String name) {
            if ( argContext.has(name) ) return argContext.get(name);
            return super.get(name);
        }

        @Override
        public boolean has(String name) {
            return (argContext.has(name) || super.has(name) ) ;
        }

        @Override
        public void set(String name, Object value) {
            if ( argContext.has(name)){
                argContext.set(name,value);
                return;
            }
            super.set(name, value);
        }
    }
}
