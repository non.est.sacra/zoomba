/*
 * Copyright 2025 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.io;

import org.supercsv.CsvPreference;
import org.supercsv.ITokenizer;
import org.supercsv.Tokenizer;
import zoomba.lang.core.operations.ZMatrix;
import zoomba.lang.core.types.ZTypes;

import java.io.FileReader;
import java.util.*;
import java.util.regex.Pattern;

/**
 * A DataMatrix representation from Text Files
 */
public class ZTextDataFile extends ZMatrix.BaseZMatrix {

    /**
     * Headers of the columns
     */
    public final List<String> headers;
    private final boolean noHeaders;
    /**
     * Data which were cached in matrix format
     */
    protected Map<Integer, List> cachedData = new HashMap<>();

    /**
     * Creates a Matrix from
     *
     * @param path      the file path
     * @param noHeaders if true, then no header, if false, first row becomes header
     * @param pref      a CSV Parse Preference
     */
    public ZTextDataFile(String path, boolean noHeaders, CsvPreference pref) {
        Tokenizer tokenizer = ZTypes.orRaise(() -> new Tokenizer(new FileReader(path), pref));
        this.noHeaders = noHeaders;
        List<String> l = new ArrayList<>();
        boolean couldRead = ZTypes.safeOrDefault(() -> tokenizer.readColumns(l), false);
        if (!couldRead) {
            headers = Collections.emptyList();
            return;
        }
        int rowNum = 0;
        if (noHeaders) {
            headers = defaultHeaders(l.size());
        } else {
            headers = l;
        }
        cachedData.put(rowNum++, l);
        while (true) {
            final List<String> buf = new ArrayList<>();
            couldRead = ZTypes.orRaise(() -> tokenizer.readColumns(buf));
            if (!couldRead) break;
            cachedData.put(rowNum++, buf);
        }
        ZTypes.safeIgnore(() -> {
            tokenizer.close();
            return true;
        });
    }

    @Override
    public List<String> headers() {
        return headers;
    }

    @Override
    public boolean hasHeaders() {
        return !noHeaders;
    }

    @Override
    public List row(int rowNum) {
        return cachedData.get(rowNum);
    }

    @Override
    public int rows() {
        return cachedData.size();
    }

    /**
     * The TextDataMatrix loader
     */
    public static final class ZTextDataFileLoader implements ZMatrixLoader {

        /**
         * Underlying ZTextDataFileLoader
         */
        public static final ZTextDataFileLoader LOADER = new ZTextDataFileLoader();

        private ZTextDataFileLoader() {
        }

        @Override
        public ZMatrix load(String path, Object... args) {
            boolean noHeaders = false;
            boolean useNullInsteadOfEmpty = false;
            char quoteChar = '"';
            int delimChar = '\t';
            String eolStr = "\n";
            ITokenizer.EmptyColumnParsing ec = ITokenizer.EmptyColumnParsing.ParseEmptyColumnsAsEmptyString;
            if (args.length > 0) {
                if (args[0] instanceof Map) {
                    Map<String, Object> params = (Map) args[0];
                    delimChar = params.getOrDefault("d", "\t").toString().charAt(0);
                    quoteChar = params.getOrDefault("q", "\"").toString().charAt(0);
                    // no headers should be dfault false
                    noHeaders = ZTypes.bool(params.getOrDefault("nh", false), false);
                    eolStr = params.getOrDefault("eol", "\n").toString();
                    // empty column should be defaulted to empty
                    useNullInsteadOfEmpty = ZTypes.bool(params.getOrDefault("esn", false), false);
                } else {
                    delimChar = String.valueOf(args[0]).charAt(0);
                    if (args.length > 1) {
                        noHeaders = ZTypes.bool(args[1], false);
                        if (args.length > 2) {
                            eolStr = String.valueOf(args[2]);
                            if (args.length > 3) {
                                quoteChar = String.valueOf(args[3]).charAt(0);
                            }
                        }
                    }
                }
            }
            if (useNullInsteadOfEmpty) {
                ec = ITokenizer.EmptyColumnParsing.ParseEmptyColumnsAsNull;
            }
            CsvPreference csvPreference = new CsvPreference.Builder(quoteChar, delimChar, eolStr)
                    .setEmptyColumnParsing(ec)
                    .build();
            return new ZTextDataFile(path, noHeaders, csvPreference);
        }

        @Override
        public Pattern loadPattern() {
            return Pattern.compile("^.+\\.(csv|tsv|txt|text)$", Pattern.CASE_INSENSITIVE);
        }
    }
}
