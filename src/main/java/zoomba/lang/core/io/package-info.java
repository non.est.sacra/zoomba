/*
 * Copyright 2017 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Everyone needs to listen and talk, in that order, and hence IO.
 * <pre>
 * This package provides native support for
 * 1. Web calls ( rest/soap )
 * 2. File / Directory manipulation
 * 3. Database connectivity
 * </pre>
 */
package zoomba.lang.core.io;