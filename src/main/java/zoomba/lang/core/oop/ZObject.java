/*
 * Copyright 2023 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.oop;

import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.collections.ZMap;
import zoomba.lang.core.interpreter.ZInterpret;
import zoomba.lang.core.interpreter.ZMethodInterceptor;
import zoomba.lang.core.interpreter.ZScriptMethod;
import zoomba.lang.core.operations.Arithmetic;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.ZCollection;
import zoomba.lang.core.types.ZNumber;
import zoomba.lang.core.types.ZTypes;
import zoomba.lang.parser.ASTArgDef;
import zoomba.lang.parser.ZoombaNode;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * The ProtoType based Object Orientation in ZoomBA
 */
public class ZObject extends ZMap implements
        ZMethodInterceptor, Arithmetic.BasicArithmeticAware, Arithmetic.LogicAware ,Iterable {

    /*
    * What does this $ convention means?
    *  1. $ stand for the item, $his.
    *  2. Hence, a function $xxx means $his function xxx
    *  3. A function $xxx$ means mutable $his xxx
    *  4. Clearly then $$ means the Init, because it is $his, and mutates $his.
    * */

    /**
     * method for Java equals()
     */
    public static final String EQUAL = "$eq" ;

    /**
     * method for Java hashCode()
     */
    public static final String HASH_CODE = "$hc" ;

    /**
     * method for Java compareTo()
     */
    public static final String CMP = "$cmp" ;

    /**
     * method for Java toString()
     */
    public static final String STR = "$str" ;

    /**
     * method for initialising instances
     */
    public static final String INIT = "$$" ;

    /**
     * method for overloading plus operator
     */
    public static final String ADD = "$add" ;

    /**
     * method for overloading minus operator
     */
    public static final String SUB = "$sub" ;

    /**
     * method for overloading multiplication operator
     */
    public static final String MUL = "$mul" ;

    /**
     * method for overloading division operator
     */
    public static final String DIV = "$div" ;

    /**
     * method for overloading power operator
     */
    public static final String POW = "$pow" ;

    /**
     * method for overloading mutable plus operator
     */
    public static final String ADD_M = "$add$" ;

    /**
     * method for overloading mutable minus operator
     */
    public static final String SUB_M = "$sub$" ;

    /**
     * method for overloading mutable multiplication operator
     */
    public static final String MUL_M = "$mul$" ;

    /**
     * method for overloading mutable division operator
     */
    public static final String DIV_M = "$div$" ;

    /**
     * method for overloading logical and / intersection operator
     */
    public static final String AND = "$and" ;

    /**
     * method for overloading logical or / union operator
     */
    public static final String OR = "$or" ;

    /**
     * method for overloading logical xor / symmetric delta operator
     */
    public static final String XOR = "$xor" ;

    /**
     * method for overloading mutable logical and / intersection operator
     */
    public static final String AND_M = "$and$" ;

    /**
     * method for overloading mutable logical or / union operator
     */
    public static final String OR_M = "$or$" ;

    /**
     * method for overloading mutable xor  / symmetric delta operator
     */
    public static final String XOR_M = "$xor$" ;

    /**
     * method for java next() function as an iterator
     * There is no hasNext() , break in next() will halt it
     */
    public static final String NEXT = "$next" ;

    /**
     * Name of the ProtoType
     */
    public final String name ;

    ZInterpret cachedInterpreter;

    /**
     * Create a ProtoType from
     * @param name the name of the proto
     * @param m the property bucket map
     * @param interpret the interpreter to be used
     */
    public ZObject(String name, Map m, ZInterpret interpret) {
        super(m);
        this.name = name ;
        this.cachedInterpreter = interpret ;
    }

    /**
     * Creates an instance from
     * @param proto the prototype object
     * @param interpret the interpreter to be used
     */
    public ZObject(ZObject proto, ZInterpret interpret) {
        super(proto);
        this.name = proto.name ;
        this.cachedInterpreter = interpret ;
    }

    @Override
    public Object get(Object key) {
        if ( "name".equals(key) ) return name ;
        return super.get(key);
    }

    @Override
    public Object put(Object key, Object value) {
        // name is immutable
        if ("name".equals(key) ) return name ;
        return super.put(key, value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Function.MonadicContainer mc = execute(EQUAL,new Object[]{o});
        if ( !mc.isNil() ) { return ZTypes.bool(mc.value(),false) ; }
        ZObject zObject = (ZObject) o;
        return name.equals( zObject.name ) && ZMap.relate(this,zObject) == ZCollection.Relation.EQUAL ;
    }

    @Override
    public int hashCode() {
        Function.MonadicContainer mc = execute(HASH_CODE,new Object[]{});
        if ( !mc.isNil() ) {
           Number hc = ZNumber.integer( mc.value() );
            // log error about how hashCode is all wronged up ...
           if ( hc == null ) {
               throw new IllegalStateException("$hc() must return a number!");
           }
           return hc.intValue();
        }
        int result =  name.hashCode() ;
        for ( Object o : entrySet() ){
            result = 31 * result + o.hashCode();
        }
        return result;
    }

    @Override
    public String toString() {
        Function.MonadicContainer mc = execute(STR, ZArray.EMPTY_ARRAY );
        if ( mc.isNil() ){
            // make a JSON proper...
            return String.format( "{ \"%s\" : %s }" , name, ZTypes.jsonString((Map)this)) ;
        }
        return String.valueOf( mc.value() ) ;
    }

    @Override
    public String string(Object... args) {
        if ( args.length == 0 ) return toString();
        Function.MonadicContainer mc = execute(STR,args);
        if ( mc.isNil() ) return super.toString();
        return String.valueOf( mc.value() ) ;
    }

    /**
     * Given name of a method and args, executes the method
     * @param methodName name of the method
     * @param args arguments to the method
     * @return an optional
     */
    public Function.MonadicContainer execute(String methodName, Object[] args){
        Object o = get(methodName);
        if ( !(o instanceof ZScriptMethod) ) return UNSUCCESSFUL_INTERCEPT ;
        ZScriptMethod m = (ZScriptMethod)o;
        ZScriptMethod.ZScriptMethodInstance instance = m.instance(cachedInterpreter,this);
        return instance.execute(args);
    }

    /**
     * Creates an instance from
     * @param proto the prototype object
     * @param interpret the interpreter
     * @param args arguments to the constructor
     * @return the instance
     */
    public static ZObject createFrom(ZObject proto, ZInterpret interpret, Object[] args){
        ZObject tmp = new ZObject(proto,interpret);
        Object o = tmp.get( INIT );
        if ( o instanceof ZScriptMethod ){
            ZScriptMethod m = (ZScriptMethod)o;
            ZScriptMethod.ZScriptMethodInstance instance = m.instance(interpret,tmp);
            instance.execute(args);
        }else {
            for (int i = 0; i < args.length; i++) {
                // setup stuff
                if ( args[i] instanceof ASTArgDef ){
                    String p = ((ZoombaNode)((ASTArgDef)args[i]).jjtGetChild(0)).image;
                    Object v = ((ASTArgDef)args[i]).jjtGetChild(1).jjtAccept(interpret,null);
                    tmp.put(p,v);
                }
            }
        }
        return tmp;
    }

    @Override
    public Function.MonadicContainer intercept(Object context, ZInterpret interpret, String methodName,
                                               Object[] args, List<Function> anons,
                                               ZoombaNode callNode) {
        cachedInterpreter = interpret ;
        return execute(methodName,args);
    }

    private Object executeOp(Object o, String actualMethod, String opName){
        Function.MonadicContainer mc = execute(actualMethod,new Object[]{o});
        if ( UNSUCCESSFUL_INTERCEPT == mc ){
            throw new UnsupportedOperationException("Object does not implement " + opName );
        }
        return mc.value();
    }

    private void setSelf(ZObject other){
        for ( Object key : other.keySet() ){
            Object value = other.get(key);
            if ( !(value instanceof ZScriptMethod) ){
                this.put(key,value);
            }
        }
    }

    private void executeMutable(Object o, String actualMethod,String fallBackMethod, String opName){
        Function.MonadicContainer mc = execute(actualMethod,new Object[]{o});
        if ( UNSUCCESSFUL_INTERCEPT == mc ){
            mc = execute(fallBackMethod,new Object[]{o});
            if ( UNSUCCESSFUL_INTERCEPT == mc ){
                throw new UnsupportedOperationException("Object does not implement : " + opName );
            }
            Object m = mc.value();
            if ( !(m instanceof ZObject) )
                throw new UnsupportedOperationException("Result Object is not a ZObject for : " + opName ) ;
            setSelf((ZObject)m);
        }
    }


    @Override
    public Object _add_(Object o) {
        return executeOp(o,ADD,"Addition");
    }

    @Override
    public Object _sub_(Object o) {
        return executeOp(o,SUB,"Subtraction");
    }

    @Override
    public Object _mul_(Object o) {
        return executeOp(o,MUL,"Multiplication");
    }

    @Override
    public Object _div_(Object o) {
        return executeOp(o,DIV,"Division");
    }

    @Override
    public Object _pow_(Object o) {
        return executeOp(o,POW,"Power");
    }

    @Override
    public void add_mutable(Object o) {
        executeMutable(o,ADD_M,ADD, "Addition" );
    }

    @Override
    public void sub_mutable(Object o) {
        executeMutable(o,SUB_M,SUB, "Subtraction" );
    }

    @Override
    public void mul_mutable(Object o) {
        executeMutable(o,MUL_M,MUL, "Multiplication" );
    }

    @Override
    public void div_mutable(Object o) {
        executeMutable(o,DIV_M,DIV, "Division" );
    }

    @Override
    public int compareTo(Object o) {
        Function.MonadicContainer mc = execute(CMP,new Object[]{o});
        if ( UNSUCCESSFUL_INTERCEPT == mc ){
            throw new UnsupportedOperationException("Object does not implement compare!");
        }
        if ( mc.value() instanceof Boolean ){
            boolean smaller = (boolean)mc.value();
            return smaller ? -1 : 1 ;
        }
        return ZNumber.integer( mc.value() ,-1 ).intValue() ;
    }

    @Override
    public Object _and_(Object o) {
        return executeOp(o,AND,"And");
    }

    @Override
    public Object _or_(Object o) {
        return executeOp(o,OR,"Or");
    }

    @Override
    public Object _xor_(Object o) {
        return executeOp(o,XOR,"XOR");
    }

    @Override
    public void and_mutable(Object o) {
        executeMutable(o,AND_M,AND, "And" );
    }

    @Override
    public void or_mutable(Object o) {
        executeMutable(o,OR_M,OR, "Or" );
    }

    @Override
    public void xor_mutable(Object o) {
        executeMutable(o,XOR_M,XOR, "XOR" );
    }

    @Override
    public Iterator iterator() {
        Object o = get(NEXT );
        if ( !(o instanceof ZScriptMethod) ) return Collections.emptyIterator();
        return ((ZScriptMethod)o).instance(this.cachedInterpreter,this);
    }
}
