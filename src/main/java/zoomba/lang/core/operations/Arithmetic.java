/*
 * Copyright 2025 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.operations;

import zoomba.lang.core.collections.*;
import zoomba.lang.core.oop.ZObject;
import zoomba.lang.core.types.*;
import zoomba.lang.core.types.ZNumber;
import zoomba.lang.core.types.ZString;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

/**
 * The Miracle class for all ALU Operations
 */
public class Arithmetic implements Comparator<Object> {

    /**
     * The instance which can be used by anyone else - no need to create instance all around
     */
    public static final Arithmetic INSTANCE = new Arithmetic();

    /**
     * Checks if an object is infinity type
     * @param o the object
     * @return true if it is, false if it is not
     */
    public static boolean isInfinity(Object o){
        return o instanceof Infinity ;
    }

    private abstract static class Infinity extends Number implements Comparable {

        Infinity(){}

        @Override
        public int intValue() {
            throw new UnsupportedOperationException("Infinity can not be contained in container!");
        }

        @Override
        public long longValue() {
            throw new UnsupportedOperationException("Infinity can not be contained in container!");
        }

        @Override
        public float floatValue() {
            throw new UnsupportedOperationException("Infinity can not be contained in container!");
        }

        @Override
        public double doubleValue() {
            throw new UnsupportedOperationException("Infinity can not be contained in container!");
        }
    }

    /**
     * A Positive Infinity
     * All comparison with anyone else would result greater than, unless self
     */
    public static final Number POSITIVE_INFINITY = new Infinity() {
        @Override
        public int compareTo(Object o) {
            if ( this == o ) return 0 ;
            return 1;
        }

        @Override
        public String toString() {
            return "inf";
        }
    };

    /**
     * A Negative Infinity
     * All comparison with anyone else would result less than, unless self
     */
    public static final Number NEGATIVE_INFINITY  = new Infinity() {
        @Override
        public int compareTo(Object o) {
            if ( this == o ) return 0 ;
            return -1;
        }

        @Override
        public String toString() {
            return "-inf";
        }
    };

    /**
     * Basic Arithmetic interface to mingle with ZoomBA operator overloading
     */
    public interface BasicArithmeticAware extends Comparable {

        /**
         * Adds an object to self
         * @param o the object
         * @return the addition result
         */
        Object _add_(Object o);

        /**
         * Subtracts an object to self
         * @param o the object
         * @return the subtraction result
         */
        Object _sub_(Object o);

        /**
         * Multiplies an object to self
         * @param o the object
         * @return the multiplication result
         */
        Object _mul_(Object o);

        /**
         * Division of self by an object
         * @param o the object
         * @return the division result
         */
        Object _div_(Object o);

        /**
         * Exponentiation of self by an object
         * @param o the object
         * @return the exponentiation result
         */
        Object _pow_(Object o);

        /**
         * Mutable addition of self to object
         * @param o the object
         */
        void add_mutable(Object o);

        /**
         * Mutable subtraction of self to object
         * @param o the object
         */
        void sub_mutable(Object o);

        /**
         * Mutable multiplication of self to object
         * @param o the object
         */
        void mul_mutable(Object o);

        /**
         * Mutable division of self to object
         * @param o the object
         */
        void div_mutable(Object o);
    }

    /**
     * Interface capable of doing some more Numeric operations
     */
    public interface NumericAware extends Comparable {

        /**
         * Gets the absolute value as number
         * @return the absolute value
         */
        Number _abs_();

        /**
         * Gets the negation of the number
         * @return the negation
         */
        Number _not_();

        /**
         * Negates the object
         */
        void not_mutable();

        /**
         * Modulo operation using the object
         * @param o the object
         * @return the result of modulo
         */
        Number _mod_(Number o);

        /**
         * Mutable modulo of the self with the object
         * @param o the object
         */
        void mod_mutable(Number o);

        /**
         * The actual number, narrowed down
         * @return the actual number
         */
        Number actual();

        /**
         * BigDecimal representation for JDK compatibility
         * @return big decimal representation
         */
        BigDecimal bigDecimal();

        /**
         * BigInteger representation for JDK compatibility
         * @return big integer representation
         */
        BigInteger bigInteger();

        /**
         * Method to define if there is a fractional non zero part
         * @return true if it is fractional, else false
         */
        boolean fractional();

        /**
         * Ceiling of the object
         * @return ceiling
         */
        Number ceil();

        /**
         * Floor function
         * @return floor of the object
         */
        Number floor();
    }

    /**
     * The Logical Interface
     */
    public interface LogicAware {

        /**
         * And or intersection of self with object
         * @param o the object
         * @return the result
         */
        Object _and_(Object o);

        /**
         * Or or Union of self with object
         * @param o the object
         * @return the result
         */
        Object _or_(Object o);

        /**
         * XOR or Symmetric Delta of self with object
         * @param o the object
         * @return the result
         */
        Object _xor_(Object o);

        /**
         * Mutable And or intersection of self with object
         * @param o the object
         */
        void and_mutable(Object o);

        /**
         * Mutable Or or Union of self with object
         * @param o the object
         */
        void or_mutable(Object o);

        /**
         * Mutable XOR or Symmetric Delta of self with object
         * @param o the object
         */
        void xor_mutable(Object o);
    }

    static final UnsupportedOperationException NULL_NOT_SUPPORTED = new UnsupportedOperationException("Operation on null not supported!");

    /**
     * creates an unsupported operation exception for a binary Operation
     * @param l left object
     * @param r right object
     * @param op the binary operation
     * @return a properly formatted UnsupportedOperationException
     */
    public UnsupportedOperationException UNSUPPORTED_OPERATION(Object l, Object r, String op){
        final String lType = l == null ? "null" : l.getClass().getName();
        final String rType = r == null ? "null" : r.getClass().getName();
        String message = String.format( "Can not do operation ( %s ) :%n ( %s ) with ( %s ) !%n left: %s %n right: %s",
                op,l,r, lType, rType);
        return new UnsupportedOperationException(message);
    }

    /**
     * creates an unsupported operation exception for a unary Operation
     * @param o the object
     * @param op the unary operation
     * @return a properly formatted UnsupportedOperationException
     */
    public UnsupportedOperationException UNSUPPORTED_OPERATION(Object o, String op){
        final String oType = o == null ? "null" : o.getClass().getName();
        String message = String.format( "Can not do unary operation ( %s ) :%n on ( %s ) obj: %s", op, o, oType);
        return new UnsupportedOperationException(message);
    }

    /**
     * Adds two objects
     * @param left object left
     * @param right object right
     * @return result
     */
    public Object add(Object left, Object right) {
        // start with arithmetic ...
        if(left == null ) throw NULL_NOT_SUPPORTED ;
        if ( left instanceof CharSequence ){
            return left.toString() + right;
        }
        try{
            ZNumber l = new ZNumber(left,true);
            return l._add_(right);
        }catch (Exception e){
            left = customZConvert(left);
            if ( left instanceof BasicArithmeticAware ){
                try {
                    return ((BasicArithmeticAware) left)._add_(right);
                }catch (Throwable ignore){}
            }
        }
        throw  UNSUPPORTED_OPERATION(left,right,"ADD");
    }

    /**
     * Adds right object into left object and returns mutated left object
     * @param left object left
     * @param right object right
     * @return left object, mutated
     */
    public Object addMutable(Object left, Object right) {
        // start with arithmetic ...
        if(left == null ) throw NULL_NOT_SUPPORTED ;
        if ( left instanceof CharSequence ){
            if ( left instanceof ZString){
                ((ZString) left).add_mutable( right );
                return left ;
            }
            ZString zs = new ZString(left);
            zs.add_mutable( right );
            return zs;
        }
        try{
            if ( left instanceof Number ){
                ZNumber l = new ZNumber((Number) left);
                return l._add_(right);
            }
            if ( left instanceof BasicArithmeticAware ){
                ((BasicArithmeticAware) left).add_mutable(right);
                return left ;
            }
            if ( left instanceof Collection ){
                ((Collection)left).add(right);
                return left;
            }
        }catch (Exception ignore){ }
        throw  UNSUPPORTED_OPERATION(left,right,"ADD");
    }

    /**
     * Subtracts two objects
     * @param left object left
     * @param right object right
     * @return result
     */
    public Object sub(Object left, Object right) {
        if(left == null ) throw NULL_NOT_SUPPORTED ;
        try{
            ZNumber l = new ZNumber(left,true);
            return l._sub_(right);
        }catch (Exception e){
            left = customZConvert(left);
            if ( left instanceof BasicArithmeticAware ){
                return ((BasicArithmeticAware) left)._sub_(right);
            }
        }
        throw UNSUPPORTED_OPERATION(left,right,"SUBTRACT");
    }

    /**
     * Subtracts right object from left object and returns mutated left object
     * @param left object left
     * @param right object right
     * @return left object, mutated
     */
    public Object subMutable(Object left, Object right) {
        if(left == null ) throw NULL_NOT_SUPPORTED ;
        try{
            if ( left instanceof Number ){
                if ( left instanceof ZNumber ){
                    ((ZNumber) left).sub_mutable(right);
                    return left;
                }
                ZNumber l = new ZNumber((Number)left);
                return l._sub_(right);
            }
            if ( left instanceof BasicArithmeticAware ){
                ((BasicArithmeticAware) left).sub_mutable(right);
                return left ;
            }
            if ( left instanceof Collection ){
                ((Collection)left).remove(right);
                return left;
            }
            if ( left instanceof Map ){
                ((Map)left).remove(right);
                return left;
            }
        }catch (Exception e){
        }
        throw UNSUPPORTED_OPERATION(left,right,"SUBTRACT");
    }

    /**
     * Multiplies two objects
     * @param left object left
     * @param right object right
     * @return result
     */
    public Object mul(Object left, Object right) {
        if(left == null ) throw NULL_NOT_SUPPORTED ;
        try{
            ZNumber l = new ZNumber(left,true);
            return l._mul_(right);
        }catch (Throwable e){
            left = customZConvert(left); // can we convert??
            if ( left instanceof BasicArithmeticAware ){
                return ((BasicArithmeticAware) left)._mul_(right);
            }

        }
        throw UNSUPPORTED_OPERATION(left,right,"MULTIPLY");
    }

    /**
     * Multiplies right object to left object and returns mutated left object
     * @param left object left
     * @param right object right
     * @return left object, mutated
     */
    public Object mulMutable(Object left, Object right) {
        if(left == null ) throw NULL_NOT_SUPPORTED ;
        try{
            if ( left instanceof ZNumber ){
                ((ZNumber) left).mul_mutable(right);
                return left;
            }
            ZNumber l = new ZNumber(left,true);
            return l._mul_(right);
        }catch (Exception e){
            if ( left instanceof BasicArithmeticAware ){
                ((BasicArithmeticAware) left).mul_mutable(right);
                return left;
            }

        }
        throw UNSUPPORTED_OPERATION(left,right,"MULTIPLY");
    }

    /**
     * Divides two objects
     * @param left object left
     * @param right object right
     * @return result
     */
    public Object div(Object left, Object right) {
        if(left == null ) throw NULL_NOT_SUPPORTED ;
        try{
            ZNumber l = new ZNumber(left,true);
            return l._div_(right);
        }catch (Exception e){
            if ( left instanceof BasicArithmeticAware){
                return ((BasicArithmeticAware) left)._div_(right);
            }
        }
        throw UNSUPPORTED_OPERATION(left,right,"DIVISION");
    }

    /**
     * Divides left object by right object and returns mutated left object
     * @param left object left
     * @param right object right
     * @return left object, mutated
     */
    public Object divMutable(Object left, Object right) {
        if(left == null ) throw NULL_NOT_SUPPORTED ;
        try{
            if ( left instanceof ZNumber)  {
                ((ZNumber) left).div_mutable(right);
                return left;
            }
            ZNumber l = new ZNumber(left,true);
            return l._div_(right) ;
        }catch (Exception e){
            if ( left instanceof BasicArithmeticAware){
                ((BasicArithmeticAware) left).div_mutable(right);
                return left ;
            }
        }
        throw UNSUPPORTED_OPERATION(left,right,"DIVISION");
    }

    /**
     * Computers result of the left object raised to the power of right
     * @param left object left
     * @param right object right
     * @return result
     */
    public Object pow(Object left, Object right) {
        if ( left instanceof CharSequence ){
            ZString zs = new ZString((CharSequence)left);
            return zs._pow_(right);
        }
        if(left == null ) throw NULL_NOT_SUPPORTED ;
        try{
            ZNumber l = new ZNumber(left,true);
            return l._pow_(right);
        }catch (Exception e){
            // try convert...
            left = customZConvert(left);
            if ( left instanceof BasicArithmeticAware ){
                return ((BasicArithmeticAware) left)._pow_(right);
            }
        }
        throw  UNSUPPORTED_OPERATION(left,right,"EXP");
    }

    private static Object customZConvert(Object o){
        // do not convert ZObjects or Tuples and what not ...
        if ( o instanceof ZObject ||
                o instanceof ZMatrix.Tuple ) return o;

        // now rest
        if ( o.getClass().isArray() ){
            return new ZArray(o);
        }
        if ( o instanceof ZCollection || o instanceof ZMap ){
            return o;
        }
        if ( o instanceof List ){
            return new ZList((List)o);
        }
        if ( o instanceof Set ){
            return new ZSet((Set)o);
        }
        if ( o instanceof Collection ){
           // it is unknown collection
           return new BaseZCollection.ZWrappedCollection((Collection)o);
        }
        if ( o instanceof Map ){
            return new ZMap((Map)o);
        }
        if ( o instanceof Date ){
            return new ZDate((Date)o);
        }
        return o;
    }

    /**
     * Compare left object with right,
     * throws error when they are not comparable
     * @param left left object
     * @param right right object
     * @return
     *     left less than right then -1
     *     left equals right then 0
     *     left greater than right then +1
     */
    @Override
    public int compare(Object left, Object right){
        if ( left == null && right == null ) return 0;
        if ( left == null || right == null ){
            throw new UnknownFormatConversionException("null can not be compared for or against non null!");
        }
        // intercept ... infinities
        if ( left instanceof Infinity ){ return ((Comparable)left).compareTo(right) ; }
        if ( right == POSITIVE_INFINITY ) return -1;
        if ( right == NEGATIVE_INFINITY ) return 1;

        if ( left instanceof Number ){
            ZNumber l = new ZNumber((Number)left);
            return l.compareTo(right);
        }
        if ( left instanceof Character ){
            if ( right instanceof Number ){
                int i = ((Number) right).intValue();
                return ((Character) left).charValue() - i;
            }
            return left.toString().compareTo( String.valueOf(right));
        }

        if ( left instanceof CharSequence ){
            if ( right instanceof CharSequence ) {
                return left.toString().compareTo(String.valueOf(right)); // solves it
            }
            throw UNSUPPORTED_OPERATION(left,right,"COMPARING!");
        }
        left = customZConvert(left);
        if ( left instanceof Comparable ){
            return ((Comparable) left).compareTo(right);
        }
        throw  UNSUPPORTED_OPERATION(left,right,"COMPARING!");
    }

    /**
     * The operator less than
     * @param left left object
     * @param right right object
     * @return true if left is less than right
     */
    public boolean lt(Object left, Object right) {
        try{
            return compare(left,right) < 0 ;
        }catch (UnknownFormatConversionException fme){
            throw fme;
        }
        catch (Throwable t){
            return false;
        }
    }

    /**
     * The operator less than equal to
     * @param left left object
     * @param right right object
     * @return true if left is less than or equals to right
     */
    public boolean le(Object left, Object right) {
        try{
            return compare(left,right) <= 0 ;
        }catch (UnknownFormatConversionException fme){
            throw fme;
        }
        catch (Throwable t){
            return false;
        }
    }

    /**
     * The operator greater than
     * @param left left object
     * @param right right object
     * @return true if left is greater than right
     */
    public boolean gt(Object left, Object right) {
        try{
            return compare(left,right) > 0 ;
        }catch (UnknownFormatConversionException fme){
            throw fme;
        }
        catch (Throwable t){
            return false;
        }
    }

    /**
     * The operator greater than equal to
     * @param left left object
     * @param right right object
     * @return true if left is greater than or equals to right
     */
    public boolean ge(Object left, Object right) {
        try{
            return compare(left,right) >= 0 ;
        }catch (UnknownFormatConversionException fme){
            throw fme;
        }
        catch (Throwable t){
            return false;
        }
    }

    /**
     * The operator equals
     * @param left left object
     * @param right right object
     * @return true if left is equals to right
     */
    public boolean eq(Object left, Object right) {
        if ( left == null ){
            return ( right == null ) ;
        }

        try{
            if ( left instanceof Boolean ){
                if ( right instanceof Boolean ){
                    return (boolean)left == (boolean)right;
                }
                return (boolean)left == ZTypes.bool(right) ;
            }
            return ( compare(left,right) == 0 ) ;
        }catch (Throwable t){
        }
        return left.equals(right);
    }

    /**
     * The operator not equals
     * @param left left object
     * @param right right object
     * @return true if left is not equal to right
     */
    public boolean ne(Object left, Object right) {
        return !eq(left, right);
    }

    /**
     * The operator and or intersection
     * @param left left object
     * @param right right object
     * @return and or intersection of left with right object
     */
    public Object and(Object left, Object right) {
        if ( left instanceof Boolean ){
            if ( ! (boolean)left ) return false;
            if ( right instanceof Boolean ) return right;
            return ZTypes.bool(right,false);
        }
        if ( left instanceof Number ){
            ZNumber n = new ZNumber((Number) left);
            return n._and_(right);
        }
        try {
            left = customZConvert(left);
            return  ((LogicAware) left)._and_(right);
        }catch (Throwable ignore) {
        }
        throw  UNSUPPORTED_OPERATION(left,right,"AND");
    }

    /**
     * The operator and or intersection, mutably
     * @param left left object
     * @param right right object
     * @return and or intersection of left with right object, mutably stored in left object
     */
    public Object andMutable(Object left, Object right) {
        if ( left instanceof Boolean ){
            if ( ! (boolean)left ) return false;
            if ( right instanceof Boolean ) return right;
            return ZTypes.bool(right,false);
        }
        if ( left instanceof Number ){
            if ( left instanceof ZNumber ){
                ((ZNumber) left).and_mutable(right);
                return left;
            }
            ZNumber n = new ZNumber((Number) left);
            return n._and_(right);
        }
        try {
            left = customZConvert(left);
            ((LogicAware) left).and_mutable(right);
            return left ;
        } catch ( Throwable ignore){
        }
        throw UNSUPPORTED_OPERATION(left,right,"MUTABLE AND");
    }

    /**
     * The operator or or union
     * @param left left object
     * @param right right object
     * @return or or union of left with right object
     */
    public Object or(Object left, Object right) {
        if ( left instanceof Boolean ){
            if (  (boolean)left ) return true;
            if ( right instanceof Boolean ) return right;
            return ZTypes.bool(right,false);
        }
        if ( left instanceof Number ){
            ZNumber n = new ZNumber((Number) left);
            return n._or_(right);
        }
        try {
            left = customZConvert(left);
            return  ((LogicAware) left)._or_(right);
        }catch (Throwable ignore){
        }
        throw UNSUPPORTED_OPERATION(left, right,"OR");
    }

    /**
     * The operator and or intersection, mutably
     * @param left left object
     * @param right right object
     * @return the or or union of left with right object, mutably stored in left object
     */
    public Object orMutable(Object left, Object right) {
        if ( left instanceof Boolean ){
            if (  (boolean)left ) return true;
            if ( right instanceof Boolean ) return right;
            return ZTypes.bool(right,false);
        }
        if ( left instanceof Number ){
            if ( left instanceof ZNumber ){
                ((ZNumber) left).or_mutable(right);
                return left;
            }
            ZNumber n = new ZNumber((Number) left);
            return n._or_(right);
        }
        try {
            ((LogicAware) left).or_mutable(right);
            return left;
        }catch (Throwable ignore){
        }
        throw  UNSUPPORTED_OPERATION(left,right,"MUTABLE OR");
    }

    /**
     * The operator xor or symmetric delta
     * @param left left object
     * @param right right object
     * @return the and or intersection of left with right object
     */
    public Object xor(Object left, Object right) {
        if ( left instanceof Boolean ){
            return (boolean)left != ZTypes.bool(right,false);
        }
        if ( left instanceof Number ){
            ZNumber n = new ZNumber((Number) left);
            return n._xor_(right);
        }
        try {
            left = customZConvert(left);
            return  ((LogicAware) left)._xor_(right);
        }catch (Throwable ignore){
        }
        throw UNSUPPORTED_OPERATION(left,right,"XOR");
    }

    /**
     * The operator xor or symmetric delta, mutably
     * @param left left object
     * @param right right object
     * @return the xor or symmetric delta of left with right object, mutably stored in left object
     */
    public Object xorMutable(Object left, Object right) {
        if ( left instanceof Boolean ){
            return (boolean)left != ZTypes.bool(right,false);
        }
        if ( left instanceof Number ){
            if ( left instanceof ZNumber ){
                ((ZNumber) left).xor_mutable(right);
                return left;
            }
            ZNumber n = new ZNumber((Number) left);
            return n._xor_(right);
        }
        try {
            left = customZConvert(left);
            ((LogicAware) left).xor_mutable(right);
            return left;
        }catch (Throwable ignore){
        }
        throw UNSUPPORTED_OPERATION(left,right,"MUTABLE XOR");
    }

    /**
     * Negation of an object
     * @param o the object
     * @return negation of the object
     */
    public Number not(Object o) {
        if ( o instanceof Number ){
            if ( o instanceof ZNumber ) { return ((ZNumber) o)._not_() ; }
            ZNumber n = new ZNumber((Number) o);
            return n._not_();
        }
        throw UNSUPPORTED_OPERATION(o,"NEGATE");
    }

    /**
     * The modulo operation of left object modulo of right object
     * @param left left object
     * @param right right object
     * @return the modulo result of left modulo to right
     */
    public Number mod(Object left, Object right) {
        if ( left instanceof Number && right instanceof Number ) {
            ZNumber l = new ZNumber((Number) left);
            ZNumber r = new ZNumber((Number) right);
            return l._mod_(r);
        }
        throw UNSUPPORTED_OPERATION(left,right,"MOD");
    }

    /**
     * The modulo operation of left object modulo of right object
     * @param left left object
     * @param right right object
     * @return the modulo result of left modulo to right, stored in the left
     */
    public Number modMutable(Object left, Object right) {
        if ( left instanceof Number && right instanceof Number ) {
            if (left instanceof ZNumber ) {
                ((ZNumber) left).mod_mutable((Number) right);
                return (Number) left;
            }
            ZNumber l = new ZNumber((Number) left);
            ZNumber r = new ZNumber((Number) right);
            return l._mod_(r);
        }
        throw UNSUPPORTED_OPERATION(left,right,"MUTABLE MOD");
    }

    /**
     * The easy division check  left object with respect to right
     * @param left left object
     * @param right right object
     * @return true if the left object is divisible by right object
     */
    public boolean divides(Object left, Object right){
        if ( !( left instanceof Number && right instanceof Number ) ) return false ;
        ZNumber l = new ZNumber( (Number) left );
        ZNumber r = new ZNumber( (Number) right );
        if ( l.fractional() || r.fractional() ) return true ; // mathematically, we are correct
        return ( r._mod_( l ).intValue() == 0 ) ;
    }

    /**
     * The div mod operation left object with respect to right
     * Returns a ZArray (q,r) resulting from the divMod operation
     * <a href="https://math.stackexchange.com/questions/256092/can-modulus-be-negative">...</a>
     * @param left left object
     * @param right right object
     * @return a ZArray (q,r)
     *            0th element is the quotient
     *            1st element is the reminder
     */
    public ZArray divMod(Object left, Object right){
        if ( !( left instanceof Number && right instanceof Number ) ) throw UNSUPPORTED_OPERATION(left,right, "DIV_MOD");
        ZNumber l = new ZNumber( (Number) left );
        ZNumber r = new ZNumber( (Number) right );
        final ZArray res = new ZArray( new Object[]{ l._div_(r) , 0 } );
        if ( l.fractional() || r.fractional() ) return res; // mathematically, we are correct
        res.set(1, l._mod_(r));
        return res;
    }

    /**
     * Left is in as a element in the right
     * @param left element
     * @param right container
     * @return true if left in right, false if not
     */
    public boolean in(Object left, Object right){
        if ( right == null ) return false ;
        if ( right.getClass().isArray() ){
            ZArray za = new ZArray(right);
            return za.contains( left );
        }
        if ( right instanceof ZRange ){
            right = ((ZRange) right).asList();
        }
        final Object finalRight = right;
        if ( right instanceof Map){
            return ZTypes.safeOrDefault( () -> ((Map) finalRight).containsKey(left), false );
        }
        if ( right instanceof Collection){
            return ZTypes.safeOrDefault( () -> ((Collection) finalRight).contains(left), false);
        }
        if ( left == null ) return false ;

        if ( right instanceof CharSequence ){
            return right.toString().contains( String.valueOf(left));
        }
        return false ;
    }

    /**
     * Left tuple starts with the right tuple
     * @param left left tuple
     * @param right right tuple
     * @return true if left starts with right, false if not
     */
    public boolean startsWith(Object left, Object right){
        if ( left == null ) return false ;
        // TODO Ordered and Sorted Map / Set ?
        if ( left instanceof Map ) return false ;
        if ( left instanceof Set) return false ;

        if ( left instanceof CharSequence ){
            if ( right == null ) return false ;
            return left.toString().startsWith( right.toString() );
        }
        if ( left.getClass().isArray() ){
            left = new ZArray(left);
        }
        if ( left instanceof List ){
            ZList zl = new ZList((List)left);
            return ( zl.startsWith(right) );
        }
        return false ;
    }

    /**
     * Left tuple ends with the right tuple
     * @param left left tuple
     * @param right right tuple
     * @return true if left ends with right, false if not
     */
    public boolean endsWith(Object left, Object right){
        if ( left == null ) return false ;
        // TODO Ordered and Sorted Map / Set ?
        if ( left instanceof Map ) return false ;
        if ( left instanceof Set) return false ;

        if ( left instanceof CharSequence ){
            if ( right == null ) return false ;
            return left.toString().endsWith( right.toString() );
        }
        if ( left.getClass().isArray() ){
            left = new ZArray(left);
        }
        if ( left instanceof List ){
            ZList zl = new ZList((List)left);
            return ( zl.endsWith(right) );
        }
        return false ;
    }

    /**
     * Left tuple is a sub tuple in order as a sub tuple within the right tuple
     * This is entirely different from a subsequence
     * [1,2] in order [3,3,1,2,4]
     * but
     * [1,2] not in order [3,3,1,4,2]
     * That is, if left tuple is a sub tuple of the right tuple
     * @param left left tuple
     * @param right right tuple
     * @return true if left tuple is in order present within right, false if not
     */
    public boolean inOrder(Object left, Object right){
        if ( right == null ) return false ;
        if ( right instanceof Map ) return false ;
        if ( right instanceof Set) return false ;

        if ( right instanceof CharSequence ){
            return (left != null) &&
                    right.toString().contains( String.valueOf(left) );
        }
        if ( right.getClass().isArray() ){
            right = new ZArray(right);
        }
        if ( right instanceof List ){
            ZList zr = new ZList(right);
            return ( zr.indexOf(left) >= 0 );
        }
        return false ;
    }

    /**
     * Is the right arg a subsequence of the left argument ?
     * Should never throw any error
     * <a href="https://en.wikipedia.org/wiki/Subsequence">...</a>
     * @param original left argument
     * @param potentialSub right argument
     * @return a boolean deciding whether the right arg a subsequence of the left arg
     */
    public boolean isSubSequence( Object original, Object potentialSub ){
        try {
            List ol = Converter.list(original, "argument can not be converted to list!");
            List ps = Converter.list(potentialSub, "argument can not be converted to list!");
            if ( ps.isEmpty() ) return true; // force empty subsequence matching empty sequence
            final int pSize = ps.size();
            if ( pSize > ol.size() ) return false; // evidently
            for ( int i=0,j=0; i< ol.size(); i++ ){
                if ( Objects.equals( ol.get(i), ps.get(j)) ){
                    j++;
                    if ( j == pSize ) return true;
                }
            }
        }catch (Throwable ignore){
        }
        return false;
    }

    /**
     * matches left in order with right while iterating
     * This is important for array,list comparison
     * as ZoomBA always does permutation equals not order equals
     * Be careful with Set and Dict
     * Default will not maintain order
     * Sorted and Ordered ones will maintain order
     * when both are null vacuously true
     * only one of them null returns false
     * When any one can not be cast to collection then equality check happens
     * @param left any item, should be coerced to collection
     * @param right any item should be coerced to collection
     * @return when left and right matches in order while iterating
     */
    public boolean matchInIterationOrder(Object left, Object right){
        if ( left == null ) return  ( right == null );
        if ( right == null) return false;
        if ( left == right ) return true;
        // now collections
        Collection<?> lCol = BaseZCollection.collection(left);
        Collection<?> rCol = BaseZCollection.collection(right);
        // anyone failed to convert -- fallback
        if ( lCol == null || rCol == null ) return eq(left,right);
        return ZCollection.iterationOrderEquals(lCol,rCol);
    }


    /**
     * Computes difference between two collections
     * @param left a collection
     * @param right another collection
     * @return difference between these two collections
     */
    public Object collectionDifference(Object left, Object right){
        if ( left instanceof Iterable ){
            if ( right instanceof Iterable ){
                return BaseZCollection.difference((Iterable<?>) left, (Iterable<?>) right);
            }
        }
        if ( left instanceof Map ){
            if ( right instanceof Map ){
                return ZMap.difference((Map)left, (Map)right);
            }
        }
        throw UNSUPPORTED_OPERATION(left,right,"COLLECTION_DIFFERENCE");
    }

    static boolean addToAccumulator(Object x, Collection col){
        try {
            if ( x instanceof Collection){
                col.addAll((Collection) x);
                return true;
            }
            if ( x instanceof Iterable){
                ((Iterable)x).forEach( y -> col.add(y));
                return true;
            }
            if ( x instanceof Iterator){
                ((Iterator)x).forEachRemaining( y -> col.add(y));
                return true;
            }
            if ( x.getClass().isArray() ){
                ZArray za = new ZArray(x);
                col.addAll(za);
                return true;
            }
        }catch (Throwable ignore){}
        // and here...
        return false;
    }

    /**
     * Catenate one with another
     * @param left a collection
     * @param right another collection
     * @return a collection which is catenation of both the collections
     */
    public Object catenation(Object left, Object right){
        Collection<?> col = new ZList();
        if ( addToAccumulator( left, col ) && addToAccumulator(right, col) ){
            return col;
        }
        throw UNSUPPORTED_OPERATION(left,right,"COLLECTION_CATENATION");
    }

    /**
     * Catenate one with another mutably
     * @param left a collection
     * @param right another collection
     * @return a collection which is catenation of both the collections stored in let object
     */
    public Object mutableCatenation(Object left, Object right){
        if ( left instanceof Collection ){
            Collection<?> col = ((Collection<?>)left);
            if ( addToAccumulator(right, col)){
                return col;
            }
        }
        throw UNSUPPORTED_OPERATION(left,right,"COLLECTION_MUTABLE_CATENATION");
    }

    /**
     * Gets the difference between two collections modifies the left collection
     * @param left a collection
     * @param right another collection to be subtracted from the left collection
     * @return a collection that is created by subtracting all elements of right collection from left
     */
    public Object mutableCollectionDifference(Object left, Object right){
        if ( left instanceof Collection ){
            if ( right instanceof  Collection){
                ((Collection<?>) left).removeAll((Collection)right);
                return left;
            }
            if ( right instanceof Iterable ){
                Collection res = BaseZCollection.difference((Iterable<?>) left, (Iterable<?>) right);
                ((Collection<?>) left).clear();
                ((Collection<?>) left).addAll(res);
                return left;
            }
        }
        if ( left instanceof Map ){
            if ( right instanceof Map ){
                ZMap diff = ZMap.difference((Map)left, (Map)right);
                ((Map<?, ?>) left).clear();
                ((Map<?, ?>) left).putAll(diff);
                return left;
            }
        }
        throw UNSUPPORTED_OPERATION(left,right,"COLLECTION_DIFFERENCE");
    }
}
