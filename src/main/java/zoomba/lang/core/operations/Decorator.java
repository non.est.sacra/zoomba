/*
 * Copyright 2025 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.operations;

import java.util.concurrent.Callable;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * A Basic interface that can decorate over java.util.function.Function
 * <a href="https://en.wikipedia.org/wiki/Decorator_pattern">...</a>
 */
public interface Decorator {

    /**
     * Creates a Decorator with inner function
     * @param function underlying function which is to be decorated for
     * @return a decorated function
     * @param <T> input type to the Function
     * @param <R> output type of the function
     */
    <T,R> java.util.function.Function<T,R> function(Function<T,R> function);

    /**
     * Creates a callable out of the underlying retry mechanism
     * @param callable the one to wrap around
     * @return a decorated Callable
     * @param <T> type of the Callable
     */
    default <T> Callable<T> callable(Callable<T> callable ){
        Function<Object, T> wrapper = (x) -> {
            try {
                return callable.call();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };
        Function<Object,T> wrapped = function(wrapper);
        return () -> wrapped.apply(null);
    }

    /**
     * Creates a retriable predicate
     * @param predicate the one to wrap around
     * @return a decorated predicate
     * @param <T> type of the predicate
     */
    default <T> Predicate<T> predicate(Predicate<T> predicate ){
        Function<T, Boolean> wrapper = predicate::test;
        Function<T,Boolean> wrapped = function(wrapper);
        return wrapped::apply;
    }

}
