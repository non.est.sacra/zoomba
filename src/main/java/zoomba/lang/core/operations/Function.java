/*
 * Copyright 2025 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.operations;

import zoomba.lang.core.interpreter.ZContext;
import zoomba.lang.core.types.ZNumber;
import zoomba.lang.core.types.ZTypes;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.UnaryOperator;

/**
 * The Functional Interface in ZoomBA
 * We do not use Java Functional interfaces because we do not like much types
 */
public interface Function extends UnaryOperator<Object>, ZJVMFunctionalInterface {

    @Override
    default Function zmb() {
        return this;
    }

    @Override
    default int definedParams(){
        return Integer.MAX_VALUE ;
    }

    /**
     * Help raise error by smartly casting it to RuntimeException
     * @param t underlying error
     * @return a RuntimeException
     */
    static RuntimeException runTimeException(Throwable t){
        if ( t instanceof RuntimeException ) return (RuntimeException)t;
        return new RuntimeException(t);
    }


    /**
     * Creates a Consumer out of this function
     * @param context any context you need to pass to the consumer
     * @return a Consumer
     * @param <T> type of the consumer
     */
    default  <T> Consumer<T> consumer(Object context){
        return  (value) -> ZTypes.safeIgnore( () -> execute( value, context) );
    }

    /**
     * Creates a BiConsumer out of this function
     * @param context any context you need to pass to the bi consumer
     * @return a Consumer
     * @param <U> first consumable type of the consumer
     * @param <T> second consumable type of the consumer
     */
    default  <U,T> BiConsumer<U,T> biConsumer(Object context){
        return  (left,right) -> ZTypes.safeIgnore( () -> execute( left, right , context) );
    }

    /**
     * A Static Field to encompass an error that function:apply must return a value
     */
    RuntimeException NO_NIL_VALUE_ALLOWED =  new UnknownFormatConversionException("function::apply can not return void!");

    @Override
    default Object apply(Object arg) {
        return execute(arg).runTimeExceptionOnError().whenNilRaise( NO_NIL_VALUE_ALLOWED ).value();
    }

    /**
     * The id representing this parameter
     */
    String THIS = "$";

    /**
     * The id representing current function in the context
     */
    String ME = "@ME";

    /**
     * The id representing the default parameter
     */
    String SWITCH_VALUE = "@$";

    /**
     * A class defined to replace null issues in ZoomBA
     * This defines a lack of value in many areas without exception
     * One single instant of NIL is good enough - singleton
     * An Alias to ZTypes.NilEnum.NIL.value
     */
    Object NIL = ZTypes.NilEnum.NIL.value ;

    /**
     * Sets the precision for Arbitrary precision
     * @param precision no of digits precision
     * @return previous precision
     */
    default int precision(int precision){
        return ZNumber.precision(precision);
    }

    /**
     * Gets the precision for Arbitrary precision
     * @return no of digits precision
     */
    default int  precision(){
        return ZNumber.precision();
    }

    /**
     * One Arg class to Handle the default and named parameters
     * to any functions
     */
    final class NamedArgs extends HashMap<String, Object> {

        /**
         * Applies a BiConsumer if a key was present
         * First arg of the bi consumer would be key, Second arg would be value associated with the key
         * @param key the key
         * @param biConsumer the bi consumer
         * @return true if it could apply false if it could not
         */
        public boolean doWhenPresent(String key, BiConsumer<String,Object> biConsumer){
            final Object value = get(key);
            if ( value == null ) { return false; }
            biConsumer.accept ( key, value);
            return true;
        }

        /**
         * Applies a Consumer if a key was NOT present
         * Arg of the bi consumer would be key
         * @param key the key
         * @param consumer the  consumer
         * @return true if it could apply false if it could not
         */
        public boolean doWhenAbsent(String key, Consumer<String> consumer){
            if ( containsKey(key)){ return false; }
            consumer.accept ( key);
            return true;
        }

        /**
         * Create a NamedArgs from a Map
         *
         * @param from map, if it is already NamedArgs, returns that
         * @return a NamedArgs object
         */
        public static NamedArgs fromMap(Map<String, Object> from) {
            if (from instanceof NamedArgs) return (NamedArgs) from;
            return new NamedArgs(from);
        }

        /**
         * Constructs a NamedArgs
         */
        public NamedArgs() {
        }

        private NamedArgs(Map<String, Object> from) {
            super(from);
        }

        /**
         * Gets a Typed value from the underlying dictionary
         * @param name of the property
         * @param defaultValue if not found, or not usable, what default to pass
         * @return if name is found and matches type, then value, else defaultValue
         * @param <T> type of the property
         */
        public <T> T typed(String name, T defaultValue ){
            return ZTypes.safeOrDefault( () -> (T)super.getOrDefault(name,defaultValue) , defaultValue );
        }
    }

    /**
     * A class to wrap around a function to create another
     */
    abstract class Wrapper implements Function {

        /**
         * Actual function
         */
        protected Function f;

        /**
         * Function wrapper
         * @param f underlying function
         */
        public Wrapper(Function f) {
            this.f = f;
        }

        @Override
        public String body() {
            return f.body();
        }

        @Override
        public MonadicContainer execute(Object... args) {
            return f.execute(args);
        }

        @Override
        public String name() {
            return f.name();
        }

        @Override
        public int definedParams() {
            return f.definedParams();
        }

        @Override
        public void withIterationContext(Object o){
            f.withIterationContext(o);
        }
    }

    /**
     * A function which is mapper
     * A generic mapper
     */
    interface Mapper extends Function {

        /**
         * A function wrapper to base class the mapper from any function
         */
        abstract class MapperWrapper extends Wrapper implements Mapper {
            /**
             * Constructs a mapper wrapper using underlying function
             * @param f underlying function
             */
            public MapperWrapper(Function f) {
                super(f);
            }
        }

        /**
         * Maps the arguments to an object
         *
         * @param args the variable length arguments
         * @return a single object, the mapped value of the arguments
         */
        Object map(Object... args);

        /**
         * A function wrapper to create a mapper from any function
         * @param f underlying function to use
         * @return mapper instance
         */
        static Mapper from(Function f) {
            if (f instanceof Mapper) return (Mapper) f;
            return new MapperWrapper(f) {
                @Override
                public Object map(Object... args) {
                    MonadicContainer container = f.execute(args);
                    if (container.isNil() && args.length != 0) {
                        return args[0]; // must
                    }
                    return container.value();
                }
            };
        }
    }

    /**
     * A generic predicate function
     */
    interface Predicate extends Function {

        /**
         * A function wrapper to base class the predicate from any function
         */
        abstract class PredicateWrapper extends Wrapper implements Predicate {
            /**
             * Constructs a predicate wrapper with underlying function
             * @param f underlying function
             */
            public PredicateWrapper(Function f) {
                super(f);
            }
        }

        /**
         * Given arguments maps to a boolean
         *
         * @param args variable length arguments
         * @return boolean true or false
         */
        boolean accept(Object... args);

        /**
         * A function wrapper to create a Predicate from any function
         * @param f underlying function to use
         * @return Predicate instance
         */
        static Predicate from(Function f) {
            if (f instanceof Predicate) return (Predicate) f;
            return new PredicateWrapper(f) {
                @Override
                public boolean accept(Object... args) {
                    MonadicContainer container = f.execute(args);
                    return ZTypes.bool(container.value(), false);
                }
            };
        }
    }

    /**
     * An Optional Container defining Maybe Monad
     */
    interface MonadicContainer {

        /**
         * Is the container empty?
         *
         * @return true if it is, false if not
         */
        boolean isNil();

        /**
         * Given the container is non empty returns the value
         *
         * @return either the value or NIL
         */
        Object value();

        /**
         * The JAVA 8 style optional conversion.
         * Optional is not good because, it is a final class
         * Monadic Containers are not class, they are interfaces
         *
         * @return Java 8 Optional
         */
        default Optional<Object> asOptional() {
            return isNil() ? Optional.empty() : Optional.of(value());
        }

        /**
         * Raise RuntimeException
         * in case MonadicContainer has error value
         * @return a MonadicContainer in case no error returns the container itself
         */
        default MonadicContainer runTimeExceptionOnError(){
            if ( value() instanceof Throwable ) throw runTimeException((Throwable) value());
            return this;
        }

        /**
         * Raise Error
         * in case MonadicContainer has error value
         * @return a MonadicContainer in case no error returns the container
         * @throws Throwable in case the value() is any type of Throwable, raise that
         */
        default MonadicContainer throwOnError() throws Throwable {
            if ( value() instanceof Throwable ) throw (Throwable)value();
            return this;
        }

        /**
         * When the container value is nil, then runs the runnable
         * @param then the runnable
         * @return after running return this
         */
        default  MonadicContainer whenNil(Runnable then){
            if ( isNil() ) then.run();
            return this;
        }

        /**
         * When the container value is nil, then throws the runtime exception passed
         * @param rex the RuntimeException
         * @return in case of no error, return this
         */
        default  MonadicContainer whenNilRaise(RuntimeException rex){
            return whenNil( () -> { throw rex ;} );
        }
    }

    /**
     * A basic implementation of Container
     */
    class MonadicContainerBase implements MonadicContainer {

        /**
         * The actual value of the container object
         */
        public final Object value;

        @Override
        public final boolean isNil() {
            return value == NIL ;
        }

        /**
         * Default constructor - empty container
         */
        public MonadicContainerBase() {
            this(NIL);
        }

        /**
         * Constructor containing an object
         *
         * @param v the object as the value
         */
        public MonadicContainerBase(Object v) {
            value = v;
        }

        @Override
        public Object value() {
            return value;
        }

        @Override
        public boolean equals(Object obj) {
            return (obj instanceof MonadicContainer) &&
                    ZTypes.equals(value, ((MonadicContainer) obj).value());
        }

        @Override
        public int hashCode() { return value == null || isNil() ? 0 : value.hashCode(); }

        @Override
        public String toString() {
            return String.format("<%s>", this.value);
        }
    }

    /**
     * Defines a NO Operation
     */
    Constant NOP = new Constant();

    /**
     * Identity for the collectors
     */
    SingleProjection COLLECTOR_IDENTITY = new SingleProjection(1);

    /**
     * Creates a SingleProjection function
     * @param inx the indexed value to be taken
     * @return a SingleProjection function
     */
    static SingleProjection singleProjection( int inx){
        return new SingleProjection(inx);
    }

    /**
     * A predicate that always is true
     */
    Constant TRUE = new Constant(true);

    /**
     * A predicate that always is false
     */
    Constant FALSE = new Constant(false);

    /**
     * Creates a constant function
     * @param constantValue the constant value
     * @return a Constant function
     */
    static Constant constant( Object constantValue){
        return new Constant(constantValue);
    }

    /**
     * A container for Success with inner value true
     */
    MonadicContainer SUCCESS = TRUE.v;

    /**
     * A container for Failure with inner value false
     */
    MonadicContainer FAILURE = FALSE.v;

    /**
     * A container for void type with inner value NIL
     */
    MonadicContainer Void = NOP.v;

    /**
     * A container for NOTHING same as a reference to Void
     */
    MonadicContainer NOTHING = Void;

    /**
     * The body of a function, not used as of now
     *
     * @return the body
     */
    String body();

    /**
     * Executes a function with the arguments
     *
     * @param args the variable length arguments
     * @return a container containing the value
     */
    MonadicContainer execute(Object... args);

    /**
     * Gets the last run context from the function
     * @return last run context
     */
    default ZContext runContext(){ return ZContext.EMPTY_CONTEXT; }

    /**
     * Sets the impending run context for the function
     * @param  zContext run context to set
     */
    default void runContext(ZContext zContext){ }

    /**
     * Name of the function
     *
     * @return the name
     */
    String name();

    /**
     * In case the method is getting used for iteration
     * Sets the collection on which iteration is happening
     * @param o Object on which iteration is happening
     */
    default void withIterationContext(Object o){}

}

/**
 * Implementation of Single Projection
 */
final class SingleProjection implements Function.Mapper {

    /**
     * The args Index which it will return
     */
    public final int argIndex;

    public SingleProjection(int index){
        this.argIndex = index ;
    }

    @Override
    public String body() {
        return String.format( "return @ARGS[%d] ;" , argIndex) ;
    }

    @Override
    public MonadicContainer execute(Object... args) {
        return new MonadicContainerBase(args[argIndex]);
    }

    @Override
    public String name() {
        return "SingleProjection";
    }

    @Override
    public Object map(Object... args) {
        return args[argIndex];
    }
}

/**
 * Implementation of a Constant function
 */
final class Constant implements Function {
    /**
     * This is the constant value which will be stored
     */
    public final MonadicContainer v;

    Constant() {
        v = new MonadicContainerBase();
    }

    public Constant(Object value) {
        this.v = new MonadicContainerBase(value);
    }

    @Override
    public String body() {
        return String.format("def _const_(){ %s }", v.value() );
    }

    @Override
    public Function.MonadicContainer execute(Object... args) {
        return v;
    }

    @Override
    public String name() {
        return "const";
    }
}
