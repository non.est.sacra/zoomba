/*
 * Copyright 2025 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.operations;

import zoomba.lang.core.interpreter.ZContext;
import zoomba.lang.core.interpreter.ZInterpret;
import zoomba.lang.core.types.ZNumber;
import zoomba.lang.core.types.ZTypes;
import zoomba.lang.parser.ZoombaNode;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * A ZoomBA Observable for Reactive Paradigm
 * @param <T> the type of the observable
 */
public interface Observable<T> extends Arithmetic.LogicAware, Arithmetic.BasicArithmeticAware, Arithmetic.NumericAware {

    /**
     * Underlying generator for value
     * @return a Callable
     */
    Callable<T> lazy();

    /**
     * List of BiConsumer observers ( last, current )
     * @return list of BiConsumer observers
     */
    List<BiConsumer<T, T>> updateObservers();

    /**
     * List of Consumer acting as observers for error ( error )
     * @return list of error observer
     */
    List<Consumer<Throwable>> errorObservers();

    /**
     * Gets the last value
     * @return last value of the observable
     */
    T lastValue();

    /**
     * Used to update the value of the observable
     * @param r with this new value
     */
    void updateValue(T r);

    /**
     * Gets the current value of the observable
     * @return current value
     */
    default T value() {
        try {
            synchronized (this) {
                T current = lazy().call();
                T last = lastValue();
                updateValue(current);
                updateObservers().stream().parallel().forEach(bc -> bc.accept(last, current));
                return current;
            }
        } catch (Throwable t) {
            errorObservers().stream().parallel().forEach(consumer -> consumer.accept(t));
            throw Function.runTimeException(t);
        }
    }

    /**
     * If possible, convert the Observable into a Comparable by the underlying value
     * @return a Comparable
     * @param <R> type of the comparable
     */
    default <R> Comparable<R> comparable() {
        T v = value();
        if (v instanceof Comparable) return (Comparable) v;
        throw new UnsupportedOperationException("lazy pointer can not be cast to comparable!");
    }

    /**
     * If possible, convert the Observable into a Number by the underlying value
     * @return a Number
     */
    default Number numeric() {
        return new ZNumber(value());
    }

    @Override
    default Object _and_(Object o) {
        return Arithmetic.INSTANCE.and(value(), o);
    }

    @Override
    default Object _or_(Object o) {
        return Arithmetic.INSTANCE.or(value(), o);
    }

    @Override
    default Object _xor_(Object o) {
        return Arithmetic.INSTANCE.xor(value(), o);
    }

    @Override
    default void and_mutable(Object o) {
        throw new UnsupportedOperationException("mutable operation on reactive pointers are not supported!");
    }

    @Override
    default void or_mutable(Object o) {
        throw new UnsupportedOperationException("mutable operation on reactive pointers are not supported!");
    }

    @Override
    default void xor_mutable(Object o) {
        throw new UnsupportedOperationException("mutable operation on reactive pointers are not supported!");
    }

    @Override
    default Object _add_(Object o) {
        return Arithmetic.INSTANCE.add(value(), o);
    }

    @Override
    default Object _sub_(Object o) {
        return Arithmetic.INSTANCE.sub(value(), o);
    }

    @Override
    default Object _mul_(Object o) {
        return Arithmetic.INSTANCE.mul(value(), o);
    }

    @Override
    default Object _div_(Object o) {
        return Arithmetic.INSTANCE.div(value(), o);
    }

    @Override
    default Object _pow_(Object o) {
        return Arithmetic.INSTANCE.pow(value(), o);
    }

    @Override
    default void add_mutable(Object o) {
        throw new UnsupportedOperationException("mutable operation on reactive pointers are not supported!");
    }

    @Override
    default void sub_mutable(Object o) {
        throw new UnsupportedOperationException("mutable operation on reactive pointers are not supported!");
    }

    @Override
    default void mul_mutable(Object o) {
        throw new UnsupportedOperationException("mutable operation on reactive pointers are not supported!");
    }

    @Override
    default void div_mutable(Object o) {
        throw new UnsupportedOperationException("mutable operation on reactive pointers are not supported!");
    }

    @Override
    default int compareTo(Object o) {
        return comparable().compareTo(o);
    }

    @Override
    default Number _abs_() {
        return ((ZNumber) numeric())._abs_();
    }

    @Override
    default Number _not_() {
        return ((ZNumber) numeric())._not_();
    }

    @Override
    default void not_mutable() {
        throw new UnsupportedOperationException("mutable operation on reactive pointers are not supported!");
    }

    @Override
    default Number _mod_(Number o) {
        return ((ZNumber) numeric())._mod_(o);
    }

    @Override
    default void mod_mutable(Number o) {
        throw new UnsupportedOperationException("mutable operation on reactive pointers are not supported!");
    }

    @Override
    default Number actual() {
        return ((ZNumber) numeric()).actual();
    }

    @Override
    default BigDecimal bigDecimal() {
        return ((ZNumber) numeric()).bigDecimal();
    }

    @Override
    default BigInteger bigInteger() {
        return ((ZNumber) numeric()).bigInteger();
    }

    @Override
    default boolean fractional() {
        return ((ZNumber) numeric()).fractional();
    }

    @Override
    default Number ceil() {
        return ((ZNumber) numeric()).ceil();
    }

    @Override
    default Number floor() {
        return ((ZNumber) numeric()).floor();
    }

    /**
     * A Base Observable Class
     * @param <T> type of the Observable
     */
    abstract class ObservableBase<T> implements Observable<T> {

        /**
         * List of the Update Listeners
         */
        protected final List<BiConsumer<T, T>> updateListeners = Collections.synchronizedList(new ArrayList<>());

        /**
         * List of the Error Listeners
         */
        protected final List<Consumer<Throwable>> errorListeners = Collections.synchronizedList(new ArrayList<>());

        private volatile T last = null;

        @Override
        public T lastValue() {
            return last;
        }

        @Override
        public void updateValue(T r) {
            last = r;
        }

        @Override
        public List<BiConsumer<T, T>> updateObservers() {
            return updateListeners;
        }

        @Override
        public List<Consumer<Throwable>> errorObservers() {
            return errorListeners;
        }

        @Override
        public String toString() {
            return String.valueOf(value());
        }

        @Override
        public int hashCode() {
            return lazy().hashCode();
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof Observable) {
                if (o == this) return true;
                return this.lazy().equals(((Observable<?>) o).lazy());
            }
            return false;
        }
    }

    /**
     * A Concrete class implemented for ZoomBA style Observable
     */
    class ZObservable extends ObservableBase<Object> {

        /**
         * Key to check whether we should mutate environment
         */
        public static final String MUTATE_ENVIRONMENT = "mut" ;

        /**
         * Underlying code node that would produce the value
         */
        protected final ZoombaNode zoombaNode;

        /**
         * Underling Callable which got created out of the zoombaNode
         */
        protected final Callable<Object> cached;

        /**
         * Underling String definition of the zoombaNode
         */
        public final String defString;

        /**
         * Constructs a ZObservable
         * @param node underling code node
         * @param interpret using this interpreter
         * @param data with this data
         * @param mutate whether to mutate the environment or not
         */
        public ZObservable(ZoombaNode node, ZInterpret interpret, Object data, boolean mutate) {
            this.zoombaNode = node;
            defString = this.zoombaNode.body( interpret.script().body() );
            if ( mutate ){
                // runs and destroys the same context, e.g. mutates it
                cached = () -> node.jjtAccept(interpret, data);
            }else{
                cached = () -> {
                    // this is how mutation is prevented
                    ZContext.MapContext sandBoxContext = new ZContext.MapContext(interpret.context());
                    interpret.prepareCall(sandBoxContext);
                    Object r = node.jjtAccept(interpret, data);
                    interpret.endCall();
                    return r;
                };
            }
        }

        /**
         * Adds a Function to the list of update handler
         * @param instance a Function to be added to the update handler list
         * @return a BiConsumer
         */
        public BiConsumer<Object, Object> update(Function instance) {
            final BiConsumer<Object, Object> listener = instance.biConsumer(this);
            this.updateListeners.add(listener);
            return listener;
        }

        /**
         * Adds a Function to the list of error handler
         * @param instance a Function to be added to the error handler list
         * @return a Consumer
         */
        public Consumer<Throwable> error(Function instance) {
            final Consumer<Throwable> listener = instance.consumer(this);
            this.errorListeners.add(listener);
            return listener;
        }

        @Override
        public Callable<Object> lazy() {
            return cached;
        }

        @Override
        public String toString(){
            try {
                return super.toString();
            } catch (Throwable t){
                return defString + ": #eval-error# " + t ;
            }
        }
    }
}
