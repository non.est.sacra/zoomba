/*
 * Copyright 2025 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.operations;


import zoomba.lang.core.sys.ZThread;
import zoomba.lang.core.types.ZNumber;
import zoomba.lang.core.types.ZTypes;

import java.util.Map;
import java.util.concurrent.*;
import java.util.function.Function;


/**
 * Implementation of things which are timed out-able
 * <a href="https://stackoverflow.com/questions/19456313/simple-timeout-in-java">...</a>
 */
public interface Timeout extends Decorator{

    /**
     * How much time to wait before it crashes out - in milli sec
     * @return wait till function responds time in ms
     */
    long waitUntil();

    /**
     * Whether the function would be executed using current thread
     * If set to false then spawn another thread to run the function
     * @return use current thread for actual execution
     */
    boolean useCurrentThread();

    /**
     * Creates a Decorator with inner function using other thread
     * @param timeout long ms
     * @param function underlying function which is to be decorated for Timeout
     * @return a decorated function which automatically errors out when waitUntil() exceeds
     * @param <T> input type to the Function
     * @param <R> output type of the function
     */
    static <T,R> Function<T,R> otherThread(final long timeout, final Function<T,R> function){
        return (t) ->{
            ExecutorService executor = ZThread.getExecutor(-1);
            final Future<R> handler = executor.submit(() -> function.apply(t) );
            try {
                return handler.get(timeout, TimeUnit.MILLISECONDS);
            } catch (Throwable e) {
                if ( e instanceof TimeoutException ) {
                    handler.cancel(true);
                }
                throw zoomba.lang.core.operations.Function.runTimeException(e);
            }finally {
                executor.shutdownNow();
            }
        };
    }

    /**
     * Creates a Decorator with inner function using same thread where the method was called
     * This is dastardly dangerous -- <a href="https://stackoverflow.com/questions/60905869/understanding-thread-interruption-in-java">...</a>
     * The method MUST do collaboration - every now and then it should check to be interrupted and if it is - then throw error
     * @param timeout long ms
     * @param function underlying function which is to be decorated for Timeout
     * @return a decorated function which automatically errors out when waitUntil() exceeds
     * @param <T> input type to the Function
     * @param <R> output type of the function
     */
    static <T,R> Function<T,R> thisThread(final long timeout, final Function<T,R> function){
        final Thread parentThread = Thread.currentThread() ;
        return (t) -> {
            final Thread snoop = new Thread(() -> {
                boolean completed = false;
                try {
                    Thread.sleep(timeout);
                } catch (InterruptedException iex) {
                    completed = true;
                    // do nothing really
                }
                if (!completed) {
                    parentThread.interrupt();
                }
            });
            try {
                snoop.start();
                return function.apply(t);
            } catch (Throwable ex) {
                throw zoomba.lang.core.operations.Function.runTimeException(ex);
            } finally {
                snoop.interrupt();
            }
        };
    }

    /**
     * Creates a Decorator with inner function
     * @param function underlying function which is to be decorated for Timeout
     * @return a decorated function which automatically errors out when waitUntil() exceeds
     * @param <T> input type to the Function
     * @param <R> output type of the function
     */
    @Override
    default  <T,R> Function<T,R> function(Function<T,R> function){
        if ( waitUntil() < 0 ) return function; // there is no timeout
        if ( useCurrentThread() ) return thisThread( waitUntil(), function);
        return otherThread(waitUntil(), function);
    }

    /**
     * Key for the Timeout in ms for the Timeout in Config
     */
    String INTERVAL  = "interval";

    /**
     * Key for the enabling same thread execution of the function for the Timeout in Config
     */
    String SAME_THREAD  = "inline";

    /**
     * Creates a Timeout out of a Configuration
     * Typical configuration is as follows:
     * {  "timeout" : 3000, "inline" : false }
     * Defaults are
     * {  "timeout" : -1, "inline" : false }
     * @param config a configuration map
     * @return a Timeout
     */
    static Timeout fromConfig(Map<String,Object> config){
        final long timeOut = ZNumber.integer(config.getOrDefault(INTERVAL, -1L),-1L).longValue();
        final boolean inline = ZTypes.bool( config.getOrDefault(SAME_THREAD, false),false ).booleanValue();
        return new Timeout() {
            @Override
            public long waitUntil() {
                return timeOut;
            }

            @Override
            public boolean useCurrentThread() {
                return inline;
            }
        };
    }
}