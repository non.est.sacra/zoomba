/*
 * Copyright 2025 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.operations;

import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.collections.ZList;
import zoomba.lang.core.collections.ZMap;
import zoomba.lang.core.interpreter.ZScript;
import zoomba.lang.core.types.ZException;
import zoomba.lang.core.types.ZNumber;
import zoomba.lang.core.types.ZTypes;

import java.lang.reflect.*;
import java.util.*;
import java.util.function.Predicate;

/**
 * The one who connects ZoomBA to JVM
 */
public final class ZJVMAccess {

    private static final Map<String, Method> classMethodMap = new HashMap<>();

    @SuppressWarnings("unchecked")
    private static void disableAccessWarnings() {
        /*
        https://stackoverflow.com/questions/46454995/how-to-hide-warning-illegal-reflective-access-in-java-9-without-jvm-argument
        * */
        try {
            Class unsafeClass = Class.forName("sun.misc.Unsafe");
            Field field = unsafeClass.getDeclaredField("theUnsafe");
            field.setAccessible(true);
            Object unsafe = field.get(null);

            Method putObjectVolatile = unsafeClass.getDeclaredMethod("putObjectVolatile", Object.class, long.class, Object.class);
            Method staticFieldOffset = unsafeClass.getDeclaredMethod("staticFieldOffset", Field.class);

            Class loggerClass = Class.forName("jdk.internal.module.IllegalAccessLogger");
            Field loggerField = loggerClass.getDeclaredField("logger");
            Long offset = (Long) staticFieldOffset.invoke(unsafe, loggerField);
            putObjectVolatile.invoke(unsafe, loggerClass, offset, null);
        } catch (Exception ignored) {
        }
    }

    static {
        disableAccessWarnings();
        Method[] methods = Class.class.getDeclaredMethods();
        for (Method m : methods) {
            classMethodMap.put(m.getName(), m);
        }
    }

    /**
     * Given a type name returns the class type
     *
     * @param type type name
     * @return type class
     * @throws Exception when any error has occurred - for example class not found exception
     */
    public static Class<?> findClass(String type) throws Exception {
        switch (type) {
            case "int":
                return int.class;
            case "byte":
                return byte.class;
            case "short":
                return short.class;
            case "long":
                return long.class;
            case "float":
                return float.class;
            case "double":
                return double.class;
            case "char":
                return char.class;
            case "boolean":
                return boolean.class;
        }
        return Class.forName(type);
    }

    /**
     * Creates a dictionary out of an object as if it is a property bucket
     *
     * @param o the object
     * @return a map with key fields and value the value of the fields
     */
    public static Map dict(Object o) {
        if (o == null) return null;

        Map map = new HashMap();
        map.put("type", o.getClass().getName());
        map.put("hash", o.hashCode());
        Map mFields = new HashMap();
        map.put("fields", mFields);
        Field[] fields = o.getClass().getDeclaredFields();
        for (Field f : fields) {
            if (tryIfNotAccessible(f)) continue;
            ZTypes.safeIgnore( () ->{
                String name = f.getName();
                Object value = f.get(o);
                return mFields.put(name, ZArray.of(f.getType().getName(), value));
            });
        }
        Method[] methods = o.getClass().getDeclaredMethods();
        Set mMethods = new HashSet();
        map.put("methods", mMethods);
        for (Method m : methods) {
            if (tryIfNotAccessible(m)) continue;
            mMethods.add(m.toString());
        }
        return Collections.unmodifiableMap(map);
    }
    /**
     * Constructs an object from arguments using constructor
     *
     * @param classType the type of the object
     * @param args      the constructor arguments
     * @return an instance of the classType
     */
    public static Object construct(Object classType, Object[] args) {
        try {
            Class<?> clazz = null;
            if (classType instanceof CharSequence) {
                clazz = Class.forName(classType.toString());
            }
            if (clazz == null) {
                if (classType instanceof Class) {
                    clazz = (Class) classType;
                }
            }
            if (clazz == null)
                throw new IllegalArgumentException("Unknown type to create : " + classType.getClass());

            if (args == null || args.length == 0) {
                // even if private :: I am no mood for privacy
                Constructor<?> c = clazz.getDeclaredConstructor();
                c.setAccessible(true);
                return c.newInstance();
            }
            PriorityQueue<MethodScore> pq = MethodScore.scoreConstructors(clazz, args);
            if ( !pq.isEmpty() ) {
                return pq.peek().constructor.newInstance(args);
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getCause() == null? e: e.getCause());
        }
        throw new UnsupportedOperationException("args did not match any known constructor for class : " + classType);
    }

    private static void before(ZEventAware before, Method m, Object[] args) {
        ZEventAware.ZEventArg event = new ZEventAware.ZEventArg(before, ZEventAware.When.BEFORE, m, args);
        try {
            before.before(event);
        } catch (Throwable t) {

        }
    }

    private static void after(ZEventAware after, Method m, Object[] args, Object r, Throwable th) {
        ZEventAware.ZEventArg event = new ZEventAware.ZEventArg(after, ZEventAware.When.AFTER, m, args,
                new Function.MonadicContainerBase(r), th);
        try {
            after.after(event);
        } catch (Throwable t) {

        }
    }

    private static Object wrapForVarArgs(Class c, Object arr, int srcIndex, int len) {
        Class arrType = c.getComponentType();
        // if (arrType == null) { } -- this should never happen, never.
        // we are already in the array (varargs) zone, hence..
        Object o = Array.newInstance(arrType, len);
        for (int i = 0; i < len; i++) {
            Object v = Array.get(arr, srcIndex++);
            Array.set(o, i, v);
        }
        return o;
    }

    /**
     * Calls a method on an object
     *
     * @param instance the object, or a class type to call static method
     * @param name     name of the method
     * @param args     arguments to the method
     * @return result of the function call
     */
    public static Object callMethod(Object instance, String name, Object[] args) {
        boolean isStatic = false;
        Class<?> clazz = null;
        try {
            if (instance == null) { throw new NullPointerException("Can not call method on null! "); }

            if (instance instanceof Class) {
                clazz = ((Class<?>) instance);
                isStatic = true;
            } else {
                clazz = instance.getClass();
            }
            PriorityQueue<MethodScore> methodScores = MethodScore.scoreMethods(clazz, name, args);
            // if nothing exists, we are out of args,
            for (MethodScore ms : methodScores) {
                final Method m = ms.method;
                boolean isVarArgs = m.isVarArgs();
                Class<?>[] pTypes = m.getParameterTypes();
                Object[] myArgs = args;
                if (isVarArgs) {
                    int numPar = m.getParameterCount();
                    Object[] newArgs = new Object[numPar];
                    final int expectedNonVarArgsCount = numPar - 1;
                    if (myArgs.length >= expectedNonVarArgsCount) { // protect against bad indices Issue #65
                        System.arraycopy(args, 0, newArgs, 0, expectedNonVarArgsCount);
                        final int varLen = args.length - expectedNonVarArgsCount;
                        newArgs[expectedNonVarArgsCount] = wrapForVarArgs(pTypes[expectedNonVarArgsCount], args, expectedNonVarArgsCount, varLen);
                        myArgs = newArgs;
                    }
                }
                Object instanceObject = isStatic ? null : instance;
                if (instanceObject instanceof ZEventAware) {
                    before((ZEventAware) instanceObject, m, args);
                }
                Object r = Function.NIL;
                Throwable th = ZException.Return.RETURN;
                try {
                    r = m.invoke(instanceObject, myArgs);
                    if (m.getReturnType() == Void.TYPE) {
                        return (r = Function.Void);
                    }
                    return r;
                } catch (Throwable t) {
                    if (t instanceof IllegalArgumentException) {
                        Object ret = handleJVMNumericArgFunctions(instanceObject, m, myArgs);
                        if (ret != INVALID_INTERCEPT) { // could match and could call method
                            return ret;
                        }
                        // our smart matching failed, so continue to find new method
                        continue;
                    }
                    Object ret = handleFormattingFunction(t.getCause(), instanceObject, m, myArgs);
                    if (ret != INVALID_INTERCEPT) { // could match and could call
                        return ret;
                    }
                    th = t;

                    // for debugging, we should keep it
                    //System.err.println(t);
                    throw t;
                } finally {
                    if (instanceObject instanceof ZEventAware) {
                        after((ZEventAware) instanceObject, m, args, r, th.getCause());
                    }
                }
            }


        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        final String errorMessage = "args did not match any known method! : ";

        final String addendum;
        if (instance instanceof ZScript) {
            addendum = " : in Script : " + ((ZScript) instance).location();
        } else {
            addendum = " : in class : " +  ( isStatic? instance :  instance.getClass() );
        }
        final String hint = MethodScore.formatInvalidArgMessage(clazz, name, args);
        throw new NoSuchMethodError(errorMessage + name + addendum + hint );
    }

    private final static Set<String> formattingMethods = new HashSet<>(Arrays.asList("printf", "format"));

    private static Object handleJVMNumericArgFunctions(Object instance, Method m, Object[] args) throws Exception {
        if ( m.isVarArgs() ) return INVALID_INTERCEPT; // can  not do this for varargs... so ignore for all format functions
        Class<?>[] params = m.getParameterTypes();
        boolean numericPresent = false;
        for (int i = 0; i < args.length ; i++) {
            if (args[i] instanceof Number) {
                numericPresent = true;
                args[i] = ZNumber.convertNum( (Number) args[i], params[i]);
            }
        }
        if (!numericPresent) return INVALID_INTERCEPT;
        return m.invoke(instance, args);
    }

    // this is for formatted printf functions to succeed, will add more later
    private static Object handleFormattingFunction(Throwable t, Object instanceObject, Method m, Object[] args)
            throws Exception {
        if (!m.isVarArgs() || args.length == 0 ||
                !formattingMethods.contains(m.getName()) ||
                !(t instanceof IllegalFormatConversionException) ||
                !(args[0] instanceof String) ||
                !t.getMessage().contains("f !=")) {
            return INVALID_INTERCEPT;
        }
        // varargs, so unwrap and then wrap...
        Object wrappedArgs = args[1];
        for (int i = 0; i < Array.getLength(wrappedArgs); i++) {
            Object arg = Array.get(wrappedArgs, i);
            if (arg instanceof Number) {
                Array.set(wrappedArgs, i, ((Number) arg).doubleValue());
            }
        }
        return m.invoke(instanceObject, args);
    }

    /**
     * Invalid Return Type for a method interceptor
     */
    public final static Function.MonadicContainer INVALID_INTERCEPT = new Function.MonadicContainerBase();


    /**
     * Invalid accessor
     */
    public final static Function.MonadicContainer INVALID_ACCESSOR = new Function.MonadicContainerBase();

    /**
     * Invalid property
     */
    public final static Function.MonadicContainer INVALID_PROPERTY = new Function.MonadicContainerBase();

    private static boolean VALID_ACCESS(Function.MonadicContainer c) {
        return  (c instanceof ZException.MonadicException) || !c.isNil()  ;
    }

    private static boolean tryIfNotAccessible(Field f) {
        try {
            f.setAccessible(true);
            return false;
        } catch (Throwable t) {
            return true;
        }
    }

    private static boolean tryIfNotAccessible(Method m) {
        try {
            m.setAccessible(true);
            return false;
        } catch (Throwable t) {
            return true;
        }
    }

    static Field getField(Object o, String prop) {
        if (o == null) return null;
        if (o instanceof Class) {
            if (o == Object.class) return null;
            Field[] fields = ((Class) o).getDeclaredFields();
            for (Field f : fields) {
                if (tryIfNotAccessible(f)) continue;
                if (f.getName().equals(prop)) return f;
            }
            return getField(((Class) o).getSuperclass(), prop);
        }
        return getField(o.getClass(), prop);
    }

    /**
     * Finds matching method of the object which matches with a predicate
     * @param o object whose method needs to be found
     * @param match the predicate
     * @return a method matching the predicate or null
     */
    public static Method findMatchingMethod(Object o, Predicate<Method> match) {
        if (o == null || o == Object.class) return null;
        if (!(o instanceof Class)) {
            return findMatchingMethod(o.getClass(), match);
        }
        Class<?> clazz = ((Class<?>) o);
        MethodCache mc = MethodCache.getAllMethods(clazz, false, false);
        Optional<Method> om = mc.methodList.stream().filter(m -> !tryIfNotAccessible(m) && match.test(m)).findFirst();
        return om.orElseGet(() -> findMatchingMethod(clazz.getSuperclass(), match));
    }

    static Method getGetterMethod(Object o, String prop) {
        return findMatchingMethod(o, (m) -> m.getName().equals(prop) && m.getParameterCount() == 0);
    }

    static Method getSetterMethod(Object o, String prop) {
        return findMatchingMethod(o, (m) -> m.getName().equals(prop) && m.getParameterCount() > 0);
    }

    /**
     * Named Property
     */
    public interface NameProperty {

        /**
         * Gets the value
         *
         * @param instance     from the instance
         * @param propertyName of the property name
         * @return the value
         */
        Function.MonadicContainer getValue(Object instance, String propertyName);

        /**
         * Sets value
         *
         * @param instance     of the instance
         * @param propertyName name of the property
         * @param value        the new value
         * @return Function.SUCCESS if successful else Function.FAILURE
         */
        Function.MonadicContainer setValue(Object instance, String propertyName, Object value);

    }

    final static class FieldAccess implements NameProperty {

        @Override
        public Function.MonadicContainer getValue(Object instance, String propertyName) {
            Field f = getField(instance, propertyName);
            if (f == null) return INVALID_PROPERTY;
            try {
                return new Function.MonadicContainerBase(f.get(instance));
            } catch (Exception e) {
                return INVALID_PROPERTY;
            }
        }

        @Override
        public Function.MonadicContainer setValue(Object instance, String propertyName, Object value) {
            Field f = getField(instance, propertyName);
            if (f == null) return INVALID_PROPERTY;
            try {
                f.set(instance, value);
                return Function.SUCCESS;
            } catch (Exception e) {
                return Function.FAILURE;
            }
        }
    }

    final static FieldAccess FIELD_ACCESS = new FieldAccess();

    final static class MethodAccess implements NameProperty {

        public static String doCamelCasing(String propertyName) {
            return Character.toUpperCase(propertyName.charAt(0)) + propertyName.substring(1);
        }

        @Override
        public Function.MonadicContainer getValue(Object instance, String propertyName) {
            String methodName = "get" + doCamelCasing(propertyName);
            Method m = getGetterMethod(instance, methodName);
            if (m == null) {
                // perhaps it has a function instance.propertyName()?
                m = getGetterMethod(instance, propertyName);
                if (m == null) {
                    // perhaps boolean isXXX?
                    methodName = "is" + doCamelCasing(propertyName);
                    m = getGetterMethod(instance, methodName);
                    if (instance instanceof Class && m == null) {
                        methodName = "get" + doCamelCasing(propertyName);
                        m = classMethodMap.get(methodName);
                    }
                    if (m == null) return INVALID_PROPERTY;
                }
            }
            try {
                return new Function.MonadicContainerBase(m.invoke(instance));
            } catch (Exception e) {
                // a method is found and then -- no args were passed.
                // this is a telltale method call failure
                return new ZException.MonadicException(e.getCause());
            }
        }

        @Override
        public Function.MonadicContainer setValue(Object instance, String propertyName, Object value) {
            String methodName = "set" + doCamelCasing(propertyName);
            Method m = getSetterMethod(instance, methodName);
            if (m == null) {
                // perhaps it has a function instance.propertyName()?
                m = getSetterMethod(instance, propertyName);
                if (m == null) return INVALID_PROPERTY;
            }
            try {
                Object o = m.invoke(instance, value);
                if (Void.TYPE.equals(m.getReturnType())) {
                    return Function.SUCCESS;
                }
                return new Function.MonadicContainerBase(o);
            } catch (Exception e) {
                return Function.FAILURE;
            }
        }
    }

    final static MethodAccess METHOD_ACCESS = new MethodAccess();

    /**
     * Indexed property - object index
     */
    public interface IndexProperty {

        /**
         * Gets the value
         *
         * @param instance of the instance
         * @param index    at the index
         * @return the value
         */
        Function.MonadicContainer getIndexedValue(Object instance, Object index);

        /**
         * Sets the value
         *
         * @param instance of the instance
         * @param index    at the index
         * @param value    the new value
         * @return Function.SUCCESS if successful else Function.FAILURE
         */
        Function.MonadicContainer setIndexedValue(Object instance, Object index, Object value);
    }

    final static class PropertyBucket implements IndexProperty {

        public static Method getDuckGet(Object o) {
            if (o == null) return null;
            if (o instanceof Class) {
                if (o == Object.class) return null;
                Method[] methods = ((Class) o).getDeclaredMethods();
                for (Method m : methods) {
                    m.setAccessible(true);
                    if (m.getName().equals("get") && m.getParameterCount() == 1) return m;
                }
                return getDuckGet(((Class) o).getSuperclass());
            }
            return getDuckGet(o.getClass());
        }

        public static Method getDuckSet(Object o) {
            if (o == null) return null;
            if (o instanceof Class) {
                if (o == Object.class) return null;
                Method[] methods = ((Class) o).getDeclaredMethods();
                for (Method m : methods) {
                    m.setAccessible(true);
                    if (m.getName().equals("set") && m.getParameterCount() == 2) return m;
                }
                return getDuckSet(((Class) o).getSuperclass());
            }
            return getDuckSet(o.getClass());
        }


        @Override
        public Function.MonadicContainer getIndexedValue(Object instance, Object propertyName) {
            try {
                if (instance == null) return INVALID_PROPERTY;
                if (instance instanceof Map) return MAP_ACCESS.getIndexedValue(instance, propertyName);
                Method m = getDuckGet(instance);
                if (m == null) return INVALID_PROPERTY;
                return new Function.MonadicContainerBase(m.invoke(instance, propertyName));
            } catch (Exception e) {
                if ( e.getCause() == null || e instanceof IllegalArgumentException ) return INVALID_PROPERTY ;
                return new ZException.MonadicException(e.getCause()) ;
            }
        }

        @Override
        public Function.MonadicContainer setIndexedValue(Object instance, Object propertyName, Object value) {
            try {
                if (instance == null) return INVALID_PROPERTY;
                if (instance instanceof Map) return MAP_ACCESS.setIndexedValue(instance, propertyName, value);
                Method m = getDuckSet(instance);
                if (m == null) return INVALID_PROPERTY;
                Object o = m.invoke(instance, propertyName, value);
                if (Void.TYPE.equals(m.getReturnType())) {
                    return Function.SUCCESS;
                }
                return new Function.MonadicContainerBase(o);
            } catch (Exception e) {
                return Function.FAILURE;
            }
        }
    }

    final static PropertyBucket PROPERTY_BUCKET = new PropertyBucket();

    final static class MapAccess implements IndexProperty {

        @Override
        public Function.MonadicContainer getIndexedValue(Object instance, Object index) {
            if (!(instance instanceof Map)) return INVALID_ACCESSOR;
            try {
                if (((Map) instance).containsKey(index)) // can throw exception in case of sorted maps
                    return new Function.MonadicContainerBase(((Map) instance).get(index));
            } catch (Throwable t) {

            }
            return INVALID_PROPERTY;
        }

        @Override
        public Function.MonadicContainer setIndexedValue(Object instance, Object index, Object value) {
            if (!(instance instanceof Map)) return INVALID_ACCESSOR;
            Object o = ((Map) instance).put(index, value);
            return new Function.MonadicContainerBase(o);
        }
    }

    final static MapAccess MAP_ACCESS = new MapAccess();

    final static class ArrayAccess implements IndexProperty {

        @Override
        public Function.MonadicContainer getIndexedValue(Object instance, Object index) {
            if (!instance.getClass().isArray()) return INVALID_ACCESSOR;
            if (index instanceof Integer) {
                try {
                    return new Function.MonadicContainerBase(Array.get(instance, (int) index));
                } catch (ArrayIndexOutOfBoundsException e) {
                    try {
                        int len = Array.getLength(instance);
                        return new Function.MonadicContainerBase(Array.get(instance, len + (int) index));
                    } catch (ArrayIndexOutOfBoundsException e1) {
                    }
                    return INVALID_PROPERTY;
                }
            }

            if (index != null && index.getClass().isArray()) {
                try {
                    int left = (int) ((Object[]) index)[0];
                    int right = (int) ((Object[]) index)[1];
                    int len = Array.getLength(instance);
                    if (left < 0) {
                        left += len;
                    }
                    if (right < 0) {
                        right += len;
                    }
                    if (left <= right && left >= 0) {
                        // inclusive
                        Object[] arr = new Object[right - left + 1];
                        for (int i = left; i <= right; i++) {
                            arr[i - left] = Array.get(instance, i);
                        }
                        return new Function.MonadicContainerBase(arr);
                    }

                } catch (Exception e) {

                }
            }
            return INVALID_PROPERTY;
        }

        @Override
        public Function.MonadicContainer setIndexedValue(Object instance, Object index, Object value) {
            if (!instance.getClass().isArray()) return INVALID_ACCESSOR;
            if (index instanceof Integer) {
                try {
                    Array.set(instance, (int) index, value);
                    return Function.SUCCESS;
                } catch (ArrayIndexOutOfBoundsException e) {
                    try {
                        int len = Array.getLength(instance);
                        Array.set(instance, len + (int) index, value);
                        return Function.SUCCESS;
                    } catch (ArrayIndexOutOfBoundsException e1) {
                    }
                    return INVALID_PROPERTY;
                }
            }
            return INVALID_ACCESSOR;
        }
    }

    final static ArrayAccess ARRAY_ACCESS = new ArrayAccess();

    final static class ListAccess implements IndexProperty {

        @Override
        public Function.MonadicContainer getIndexedValue(Object instance, Object index) {
            if (!(instance instanceof List)) return INVALID_ACCESSOR;
            List myList = (List) instance;
            if (index instanceof Integer) {
                try {
                    return new Function.MonadicContainerBase(myList.get((int) index));
                } catch (IndexOutOfBoundsException e) {
                    if ((int) index < 0) {
                        try {
                            return new Function.MonadicContainerBase(myList.get(myList.size() + (int) index));
                        } catch (IndexOutOfBoundsException e1) {
                        }
                    }
                    return INVALID_PROPERTY;
                }
            }
            if (index != null && index.getClass().isArray()) {
                try {
                    int left = (int) ((Object[]) index)[0];
                    int right = (int) ((Object[]) index)[1];
                    int len = myList.size();
                    if (left < 0) {
                        left += len;
                    }
                    if (right < 0) {
                        right += len;
                    }
                    List slice = new ZList();
                    if (left <= right && left >= 0) {
                        // inclusive
                        for (int i = left; i <= right; i++) {
                            slice.add(myList.get(i));
                        }
                        return new Function.MonadicContainerBase(slice);
                    }

                } catch (Exception e) {

                }
            }
            return INVALID_PROPERTY;
        }

        @Override
        public Function.MonadicContainer setIndexedValue(Object instance, Object index, Object value) {
            if (!(instance instanceof List)) return INVALID_ACCESSOR;
            List myList = (List) instance;
            if (index instanceof Integer) {
                try {
                    Object o = myList.set((int) index, value);
                    return new Function.MonadicContainerBase(o);
                } catch (IndexOutOfBoundsException e) {
                    if ((int) index < 0) {
                        try {
                            return new Function.MonadicContainerBase(myList.set(myList.size() + (int) index, value));
                        } catch (IndexOutOfBoundsException e1) {
                        }
                    }
                    return INVALID_PROPERTY;
                }
            }
            return INVALID_PROPERTY;
        }
    }

    final static ListAccess LIST_ACCESS = new ListAccess();

    /**
     * Gets the value of the property of the object instance
     *
     * @param instance the object instance
     * @param property the name of the property
     * @return the property value in a container, or INVALID_PROPERTY
     */
    public static Function.MonadicContainer getProperty(Object instance, Object property) {
        if (instance == null) throw new NullPointerException("Can not get property of null!");
        String prop = String.valueOf(property);
        switch (prop) {
            case "hashCode":
                return new Function.MonadicContainerBase(instance.hashCode());
            case "class":
                return new Function.MonadicContainerBase(instance.getClass());
            case "toString":
                return new Function.MonadicContainerBase(instance.toString());
        }

        Function.MonadicContainer r;
        if (instance.getClass().isArray()) {
            r = ARRAY_ACCESS.getIndexedValue(instance, property);
            if (VALID_ACCESS(r)) return r;
            if ("length".equals(property)) return new Function.MonadicContainerBase(Array.getLength(instance));
        }
        boolean isMap = false;
        if (instance instanceof Map) {
            isMap = true;
            r = MAP_ACCESS.getIndexedValue(instance, property);
            if (VALID_ACCESS(r)) return r;

            switch (prop) {
                case "size":
                case "length":
                    return new Function.MonadicContainerBase(((Map) instance).size());
                case "keys":
                case "keySet":
                    return new Function.MonadicContainerBase(((Map) instance).keySet());
                case "values":
                    return new Function.MonadicContainerBase(((Map) instance).values());
                case "entries":
                case "entrySet":
                    return new Function.MonadicContainerBase(((Map) instance).entrySet());
            }

        }
        if (instance instanceof List) {
            r = LIST_ACCESS.getIndexedValue(instance, property);
            if (VALID_ACCESS(r)) return r;
            switch (prop) {
                case "size":
                case "length":
                    return new Function.MonadicContainerBase(((List) instance).size());
            }
        }
        // for CharSequences
        if (instance instanceof CharSequence) {
            String s = instance.toString();
            if (property instanceof Integer) {
                int index = (int) property;
                try {
                    return new Function.MonadicContainerBase(s.charAt(index));
                } catch (IndexOutOfBoundsException e) {
                    index += s.length(); // perhaps -ve index ?
                    try {
                        return new Function.MonadicContainerBase(s.charAt(index));
                    } catch (IndexOutOfBoundsException e1) {
                    }
                }
            }
            if (property.getClass().isArray()) {
                try {
                    int start = (int) Array.get(property, 0);
                    int end = (int) Array.get(property, 1);
                    int len = s.length();
                    if (start < 0) {
                        start = len + start;
                    }
                    if (end < 0) {
                        end = len + end;
                    }
                    return new Function.MonadicContainerBase(s.substring(start, end + 1));
                } catch (Exception e) {
                }
            }
        }
        // for Map.Entry
        if (instance instanceof Map.Entry) {
            if (property instanceof CharSequence) {
                switch (property.toString()) {
                    case "k":
                    case "key":
                        return new Function.MonadicContainerBase(((Map.Entry) instance).getKey());
                    case "v":
                    case "value":
                        return new Function.MonadicContainerBase(((Map.Entry) instance).getValue());
                }
            }
            if (property instanceof Number) {
                switch (((Number) property).intValue()) {
                    case 0:
                        return new Function.MonadicContainerBase(((Map.Entry) instance).getKey());
                    case 1:
                        return new Function.MonadicContainerBase(((Map.Entry) instance).getValue());
                }
            }
        }
        /*
        * Order is as follows:
        * try methods getXXX() , setXXX() or xxx()
        * Next fields
        * Next duckGet duckSet
        *
        * */

        // now try getter/setter
        r = METHOD_ACCESS.getValue(instance, prop);
        if (VALID_ACCESS(r)) return r;
        // and now, go back to field access
        r = FIELD_ACCESS.getValue(instance, prop);
        if (VALID_ACCESS(r)) return r;
        // Now Property Bucket Access
        if (!isMap) {
            // try Bucket Access only when NOT map?
            r = PROPERTY_BUCKET.getIndexedValue(instance, property);
            if (VALID_ACCESS(r)) return r;
        }
        // nothing... hence...
        return INVALID_PROPERTY;
    }

    /**
     * Sets the value of the property of the object instance
     *
     * @param instance the instance of the object
     * @param property name of the property
     * @param value    the value we want to set
     * @return Function.SUCCESS or INVALID_PROPERTY
     */
    public static Function.MonadicContainer setProperty(Object instance, Object property, Object value) {
        if (instance == null) throw new NullPointerException("Can not set property of null!");
        if (instance.getClass().isArray()) {
            return ARRAY_ACCESS.setIndexedValue(instance, property, value);
        }
        if (instance instanceof List) {
            return LIST_ACCESS.setIndexedValue(instance, property, value);
        }
        if (instance instanceof Map) {
            return MAP_ACCESS.setIndexedValue(instance, property, value);
        }
        String prop = String.valueOf(property);

        Function.MonadicContainer r = METHOD_ACCESS.setValue(instance, prop, value);
        if (VALID_ACCESS(r)) return r;

        r = PROPERTY_BUCKET.setIndexedValue(instance, property, value);
        if (VALID_ACCESS(r)) return r;

        r = FIELD_ACCESS.setValue(instance, prop, value);
        if (VALID_ACCESS(r)) return r;

        return INVALID_PROPERTY;
    }
}
