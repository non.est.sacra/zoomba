/*
 * Copyright 2025 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.operations;

import zoomba.lang.core.types.ZNumber;
import zoomba.lang.core.types.ZTypes;

import java.lang.reflect.Proxy;
import java.util.Comparator;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.function.*;

import static zoomba.lang.core.operations.Function.NO_NIL_VALUE_ALLOWED;

/**
 * A bridge proxying JVM functional interfaces to ZMB
 * so that ZMB Methods can be used in their place
 * A Work in progress
 */
public interface ZJVMFunctionalInterface extends Runnable, Callable, Comparator, Supplier {

    /**
     * Creates a Proxy Object
     * @param src the source around which to create proxy
     * @param interceptOnly  boolean whether you want just an interceptor or interloper
     * @param interceptor with this Proxy Interceptor
     * @return a Proxy Object
     */
    static Object proxy(Object src, boolean interceptOnly, Function interceptor) {
        final Class<?> clazz = src.getClass();
        Class<?>[] interfaceClasses = clazz.getInterfaces();
        return Proxy.newProxyInstance(
                clazz.getClassLoader(),
                interfaceClasses ,
                (proxy, method, methodArgs) -> {
                    Map<String, Object> payLoad = Map.of("src", src,
                            "proxy", proxy,
                            "method", method,
                            "args", methodArgs);
                    if (interceptOnly) {
                        ZTypes.safeIgnore(() -> interceptor.execute(payLoad));
                        return ZTypes.orRaise(() -> method.invoke(src, methodArgs));
                    } else {
                        return interceptor.execute(payLoad).runTimeExceptionOnError().value();
                    }
                });
    }

    /**
     * Underlying ZMB Function
     *
     * @return a zmb functiion
     */
    Function zmb();

    /**
     * No of parameters the function takes
     * This has implication in Comparator
     * def cmp(x,y){} has 2 defined parameters
     *
     * @return number of defined parameters for the ZMB function
     */
    int definedParams();

    @Override
    default void run() {
        zmb().execute().runTimeExceptionOnError();
    }

    @Override
    default Object call() throws Exception {
        return zmb().execute().runTimeExceptionOnError().whenNilRaise( NO_NIL_VALUE_ALLOWED ).value();
    }

    @Override
    default int compare(Object o1, Object o2) {
        final Function f = zmb();
        if (definedParams() == 1) {
            // this int did not work out, now mapper comparator
            final Object m1 = f.apply(o1);
            final Object m2 = f.apply(o2);
            return Arithmetic.INSTANCE.compare(m1, m2);
        }
        final Object cmp = f.execute(o1, o2).runTimeExceptionOnError().whenNilRaise(NO_NIL_VALUE_ALLOWED).value();
        return ZNumber.sign(cmp);
    }

    @Override
    default Object get() {
        return zmb().execute().runTimeExceptionOnError().whenNilRaise( NO_NIL_VALUE_ALLOWED).value();
    }

    /**
     * A method to coerce into various Java Types
     * Tries first with itself, if it works return itself
     * Then tries with UniParam, if it does not work falls back on BiParam
     *
     * @param functionalType java functional type to coerce into
     * @return an appropriate ZJVMFunctionalInterface Type to coerce into
     */
    default ZJVMFunctionalInterface coerce(Class<?> functionalType) {
        // if it can itself do it, so be it
        if (functionalType.isAssignableFrom(getClass())) return this;
        // otherwise
        final ZJVMFunctionalInterface me = this;
        if (functionalType.isAssignableFrom(UniParam.class)) {
            return new UniParam() {
                @Override
                public Function zmb() {
                    return me.zmb();
                }

                @Override
                public int definedParams() {
                    return me.definedParams();
                }
            };
        }
        // finally
        return new BiParam() {
            @Override
            public Function zmb() {
                return me.zmb();
            }

            @Override
            public int definedParams() {
                return me.definedParams();
            }
        };
    }

    /**
     * Due to terrible design of JVM functional one must split the functions into these two parts
     * These are methods which takes only single parameter
     */
    interface UniParam extends ZJVMFunctionalInterface,
            Predicate, Consumer {

        @Override
        default boolean test(Object o) {
            return (boolean) zmb().apply(o);
        }

        @Override
        default void accept(Object o) {
            zmb().execute(o).runTimeExceptionOnError();
        }
    }

    /**
     * Due to terrible design of JVM functional one must split the functions into these two parts
     * These are methods which takes two parameters
     */
    interface BiParam extends ZJVMFunctionalInterface,
            BiPredicate, BiConsumer, BinaryOperator {

        @Override
        default Object apply(Object o1, Object o2) {
            final Function f = zmb();
            if (definedParams() == 1) {
                Object o = new Object[]{o1, o2};
                return f.apply(o);
            }
            return f.execute(o1, o2).runTimeExceptionOnError().whenNilRaise(NO_NIL_VALUE_ALLOWED).value();
        }

        @Override
        default void accept(Object o1, Object o2) {
            final Function f = zmb();
            if (definedParams() == 1) {
                Object o = new Object[]{o1, o2};
                f.execute(o).runTimeExceptionOnError();
            } else {
                f.execute(o1, o2).runTimeExceptionOnError();
            }
        }

        @Override
        default boolean test(Object o1, Object o2) {
            return (boolean) apply(o1, o2);
        }
    }
}
