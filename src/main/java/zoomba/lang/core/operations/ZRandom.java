/*
 * Copyright 2025 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.operations;

import zoomba.lang.core.collections.BaseZCollection;
import zoomba.lang.core.collections.Converter;
import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.operations.generator.Generex;
import zoomba.lang.core.types.ZException;
import zoomba.lang.core.types.ZNumber;
import zoomba.lang.core.collections.ZList;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import static zoomba.lang.core.operations.Function.NIL;
import static zoomba.lang.core.types.ZTypes.safeOrNull;

import java.math.BigInteger;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Random Implementation
 */
public class ZRandom extends SecureRandom {

    /**
     * The MD5 algo name
     */
    public static final String HASH_MD5 = "MD5";

    /**
     * We need only a singleton for random
     */
    public static final ZRandom RANDOM = new ZRandom();

    class RandomIterator implements Iterator {

        final ZRandom r;

        final Object arg;

        RandomIterator(ZRandom r, Object arg) {
            this.r = r;
            this.arg = arg;
        }

        @Override
        public boolean hasNext() {
            return true;
        }

        @Override
        public Object next() {
            return r.num(arg);
        }
    }

    /**
     * Generate an iterator out of the random numbers
     *
     * @param arg argument - ZRandom.num(arg) will be called on next()
     * @return a random iterator
     */
    public Iterator iterator(Object arg) {
        return new RandomIterator(this, arg);
    }

    /**
     * See https://github.com/mifmif/Generex
     *
     * @param regex   the regular expression
     * @param minSize minimum size of the regex
     * @return a random string matching the regex
     */
    public String string(final String regex, final int minSize) {
        Generex generex = new Generex(regex, this);
        return generex.random(minSize);
    }

    /**
     * See https://github.com/mifmif/Generex
     *
     * @param regex   the regular expression
     * @param minSize minimum size of the regex
     * @param maxSize minimum size of the regex
     * @return a random string matching the regex
     */
    public String string(final String regex, final int minSize, final int maxSize) {
        Generex generex = new Generex(regex, this);
        return generex.random(minSize, maxSize);
    }

    /**
     * Random select function
     *
     * @param args arguments
     * @return Selects random from collection or returns a random object
     */
    public static Object random(Object... args) {
        if (args.length == 0) return RANDOM;
        Object o = args[0];
        if (o == null) return RANDOM;
        if (args.length == 1) return RANDOM.select(args[0]);
        int minLen = ZNumber.integer(args[1], 1).intValue();
        if ( args.length > 2 ){
            // string probably ?
            int maxLen = ZNumber.integer(args[2], minLen).intValue();
            return RANDOM.string( String.valueOf(args[0]), minLen, maxLen);
        }
        return RANDOM.select(args[0], minLen);
    }

    /**
     * Given a mutable collection shuffles it
     *
     * @param o the collection
     * @return the probably shuffled object
     */
    public static Object shuffle(Object o) {
        if (o == null) return null;
        boolean wasArr = false;
        if (o.getClass().isArray()) {
            wasArr = true;
            o = new ZArray(o);
        }
        if (!(o instanceof List)) {
            return o;
        }
        List l = (List) o;
        /* https://en.wikipedia.org/wiki/Fisher–Yates_shuffle  */
        Collections.shuffle(l);
        if ( wasArr ){
            return ((ZArray) o).items;
        }
        return o;
    }

    /**
     * Generates has based on arguments
     *
     * @param args the string and the encoding
     *             hash(string)
     *             hash(algorithm, string)
     * @return the hash of the string
     */
    public static String hash(Object... args) {
        if (args.length == 0) return "";
        String algorithm = HASH_MD5;
        String string = String.valueOf(args[0]);
        if (args.length > 1) {
            algorithm = string;
            string = String.valueOf(args[1]);
        }
        final String text = string;

        final String smallCaseAlgorithmName = algorithm.toLowerCase();
        switch ( smallCaseAlgorithmName ){
            case "hex":
                // convert string or byte array to hex
                final byte[] data;
                if ( args[1] instanceof byte[] ){
                    data = (byte[]) args[1];
                } else {
                    data = text.getBytes();
                }
                StringBuilder builder = new StringBuilder();
                for (byte b: data) {
                    builder.append(String.format("%02x", b));
                }
                return builder.toString();

            case "e64":
                // do base 64 encoding
                return Base64.getEncoder().encodeToString(text.getBytes(StandardCharsets.UTF_8));
            case "em64":
                // do base 64 mime encoding
                return Base64.getMimeEncoder().encodeToString(text.getBytes(StandardCharsets.UTF_8));
            case "d64" :
                // do base 64 decoding
                return safeOrNull( () ->{
                    byte[] barr = Base64.getDecoder().decode(text);
                    return new String(barr);
                });
            case "dm64" :
                // do mime base 64 decoding
                byte[] barr = Base64.getMimeDecoder().decode(text);
                return new String(barr);
            case "eu" :
                return safeOrNull( () -> URLEncoder.encode(text, StandardCharsets.UTF_8.toString()) );
            case "du" :
                return safeOrNull( () -> URLDecoder.decode(text, StandardCharsets.UTF_8.toString()) );

            case "hmac":
                // do hmac hashing
                if ( args.length <= 3 ){
                    throw new IllegalArgumentException("HMAC Needs (algorithm, text, secret-key)");
                }
                return safeOrNull(()->{
                    String algo = String.valueOf(args[1]);
                    String txt = String.valueOf(args[2]);
                    String key = String.valueOf(args[3]);
                    SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(), algo);
                    Mac mac = Mac.getInstance(algo);
                    mac.init(secretKeySpec);
                    return hash("hex", mac.doFinal(txt.getBytes()));
                });
        }

        try {
            MessageDigest m = MessageDigest.getInstance(algorithm);
            m.update(text.getBytes(), 0, text.length());
            BigInteger bi = new BigInteger(1, m.digest());
            return bi.toString(16);
        } catch (Exception e) {

        }
        return Integer.toString(text.hashCode());
    }

    /**
     * Random selects an item from collection
     *
     * @param o the collection
     * @return a random selection
     */
    public Object select(Object o) {
        if (o instanceof Enum) {
            o = o.getClass().getEnumConstants();
        }
        if (o.getClass().isArray()) {
            o = new ZArray(o);
        }
        if (o instanceof CharSequence) {
            int size = ((CharSequence) o).length();
            return ((CharSequence) o).charAt(RANDOM.nextInt(size));
        }
        if (o instanceof List) {
            int size = ((List) o).size();
            return ((List) o).get(RANDOM.nextInt(size));
        }
        if (o instanceof Number) {
            return RANDOM.num(o);
        }
        if (o instanceof Boolean) {
            return RANDOM.bool();
        }
        return RANDOM;
    }

    /**
     * Random selects count no of items from collection
     *
     * @param o     the collection
     * @param count the number of items
     * @return a random selection
     */
    public Object select(Object o, int count) {
        if (count <= 0) return RANDOM;
        if (o instanceof String) {
            // generate the string based on regex ... how cool is that?
            return string((String) o, count);
        }
        if (count == 1) return select(o);

        if (o.getClass().isArray()) {
            o = new ZArray(o);
        }
        if (o instanceof List) {
            int size = ((List) o).size();
            if (count >= size) return o;
            HashSet<Integer> indices = new HashSet<>();
            while (indices.size() != count) {
                int i = RANDOM.nextInt(size);
                indices.add(i);
            }
            ZList l = new ZList();
            for (Integer i : indices) {
                l.add(((List) o).get(i));
            }
            indices.clear();
            return l;
        }
        return RANDOM;
    }

    /**
     * Gets a random boolean value
     * @return a random boolean value
     */
    public boolean bool() {
        return nextBoolean();
    }

    /**
     * Gets a random number within the specified range
     * @param fromIncluded from this no ( included )
     * @param toExcluded to this no ( excluded )
     * @return a random number in this half open range
     */
    public Number numRange(ZNumber fromIncluded, ZNumber toExcluded) {
        Number range = (Number) toExcluded._sub_(fromIncluded);
        if (range instanceof Integer) {
            int i = nextInt(range.intValue());
            return (Number) fromIncluded._add_(i);
        }
        ZNumber r = new ZNumber(range);
        Number partitions = (Number) r._div_(Integer.MAX_VALUE);
        // pick a random partition first ?
        Number p = numRange(new ZNumber(0), new ZNumber(partitions));
        ZNumber result = new ZNumber(p);
        result.mul_mutable(Integer.MAX_VALUE);
        result.add_mutable(fromIncluded);
        result.add_mutable(nextInt(Integer.MAX_VALUE));
        return result.actual();
    }

    /**
     * Gets a random number
     *
     * @param args arguments
     *             with no args returns a random integer
     *             with two arguments a number between (start,end)
     *             with one arg
     *             integer an integer between (0,thatInt)
     *             long, nextLong()
     *             double, nextDouble()
     *             String, very long Integer
     *             Any other type, nextGaussian()
     * @return a random number
     */
    public Number num(Object... args) {
        if (args.length == 0) return nextInt();
        if (args.length == 2) return numRange(new ZNumber(args[0]), new ZNumber(args[1]));
        if (args[0] instanceof Integer) {
            return nextInt((int) args[0]);
        }
        if (args[0] instanceof Long) {
            return nextLong();
        }

        if (args[0] instanceof Double) {
            return nextDouble();
        }
        if (args[0] instanceof String) {
            String s = (String) args[0];
            Number n = ZNumber.number(s);
            if (n == null) return nextInt();
            if (n instanceof Integer) {
                // large... INT
                int in = n.intValue();
                StringBuilder buf = new StringBuilder();
                while (in > 0) {
                    buf.append(nextInt(Integer.MAX_VALUE));
                    --in;
                }
                return new BigInteger(buf.toString());
            }
        }
        return nextGaussian();
    }

    final static Comparator<Object> DEFAULT_COMPARATOR = Arithmetic.INSTANCE;


    /**
     * Sorts a collection Ascending with
     *
     * @param args the collection
     * @param cmp  comparator function
     * @param reversed when true sort using descending
     * @return the sorted object
     */
    public static Object sort(Object[] args, Function cmp, boolean reversed) {
        if(args == null || args.length == 0) return ZArray.EMPTY_ARRAY;
        Object o;
        if ( args.length > 1 ){
            // sort the arguments
            o = args;
        } else {
            o = args[0];
            if ( o == null ) return null;
        }
        boolean wasArray = false;
        if (o.getClass().isArray()) {
            o = new ZArray(o);
            wasArray = true;
        }
        if ( o instanceof List ){
            Comparator cl;
            if ( cmp == null ){
                cl = DEFAULT_COMPARATOR;
            } else {
                cmp.withIterationContext(o);
                cl = cmp;
            }
            if ( reversed ){
                cl = cl.reversed();
            }
            Collections.sort((List)o, cl );
        }
        if ( wasArray ){
            return ((ZArray) o).items;
        }
        return o;
    }

    /**
     * Sorts a collection Ascending with
     *
     * @param args the collection
     * @param cmp  comparator function
     * @return the sorted object
     */
    public static Object sortAscending(Object[] args, Function cmp) {
        return sort(args, cmp,false);
    }

    /**
     * Sorts a collection Descending with
     *
     * @param args the collection
     * @param cmp  comparator function
     * @return the sorted object
     */
    public static Object sortDescending(Object[] args, Function cmp) {
        return sort(args, cmp, true);
    }


    /**
     * Java convert to numeric over a context
     * Used by sum() function
     * @param o       the object which is a pair left,right
     * @param index   the index
     * @param ctx iteration context
     * @param partial the partial result
     * @return a numeric result
     */
    static Number num(Function f, Object o, Number index, Object ctx , Object partial) {
        Object[] args = new Object[]{index, o, ctx, partial};
        Object r = f.execute(args)
                .runTimeExceptionOnError()
                .whenNilRaise( new UnknownFormatConversionException("Scalar can not return void!") )
                .value();
        if (r instanceof Number) return ((Number) r);
        return ZNumber.number(r, 0);
    }

    /**
     * Sums a collection with
     * You can  not break or continue
     * @param args   the collection
     * @param scalar scalar mapping function
     * @return sum of the collection
     */
    public static Number sum(Function scalar, Object[] args) {
        if (args.length == 0) return 0;
        Object o;
        if (args.length == 1) {
            o = args[0];
        } else {
            o = args;
        }
        Iterator i = Converter.iterable(o, ZArray.EMPTY_Z_ARRAY).iterator();
        if ( !i.hasNext() ) return 0;

        ZNumber zz = new ZNumber(0);
        if (scalar != null) {
            final Object ctx = o;
            int index = -1;
            while (i.hasNext()) {
                Object x = i.next();
                index++;
                Number n;
                n = num(scalar, x, index, ctx, zz.actual());
                zz.add_mutable(n);
            }
        } else {
            while (i.hasNext()) {
                Object x = i.next();
                Number n = ZNumber.number(x, 0);
                zz.add_mutable(n);
            }
        }
        return zz.actual();
    }

    /**
     * Finds Min and Max of a collection with
     * You can not break or continue
     * @param args  the collection
     * @param anons one scalar comparator function
     * @return a pair [min,max]
     */
    public static Object[] minMax(List<Function> anons, Object[] args) {
        if (args.length == 0) return ZArray.EMPTY_ARRAY;
        Object o;
        if (args.length == 1) {
            o = args[0];
        } else {
            o = args;
        }
        Iterator i = Converter.iterable(o, ZArray.EMPTY_Z_ARRAY).iterator();

        if (!i.hasNext()) {
            return ZArray.EMPTY_ARRAY;
        }

        Object min = i.next();
        Object max = min;
        Function cmp = null;

        if (!anons.isEmpty()) {
            cmp = anons.get(0);
        }

        // these two branches can be merged, they are not due to optimization reason
        // merging them would require a generic comparator and that would take a hit in perf
        if (cmp != null) {
            cmp.withIterationContext(o);
            Comparator cl = cmp;
            while (i.hasNext()) {
                Object x = i.next();
                boolean smaller = cl.compare(x, min) < 0;
                if (smaller) {
                    min = x;
                } else {
                    boolean larger = cl.compare(max, x) < 0;
                    if (larger) {
                        max = x;
                    }
                }
            }
        } else {
            while (i.hasNext()) {
                Object x = i.next();
                boolean smaller = ((Comparable) x).compareTo(min) < 0;
                if (smaller) {
                    min = x;
                } else {
                    boolean larger = ((Comparable) x).compareTo(max) > 0;
                    if (larger) {
                        max = x;
                    }
                }
            }
        }
        return new Object[]{min, max};
    }

    /**
     * Isolate and tokenizes a collection
     *
     * @param anons mapper and then another mapper function
     * @param args  the collection
     * @return result collection of the tokenization
     */
    public static Object tokens(List<Function> anons, Object[] args) {
        if ( args == null || args.length == 0) return "";
        Pattern p;
        if (args[0] instanceof Pattern) {
            p = (Pattern) args[args.length - 1];
        } else {
            p = Pattern.compile(String.valueOf(args[args.length - 1]));
        }
        if (args.length == 1) return p;
        String matchString = String.valueOf(args[0]);
        ArrayList l = new ArrayList();
        Matcher m = p.matcher(matchString);
        while (m.find()) {
            l.add(m.group());
        }
        if ( anons == null || anons.isEmpty()) return l;
        return BaseZCollection.compose(l, new ZList(), anons, 1);
    }

    /**
     * Does generic binary search, w/o any conditional expression
     *
     * @param col the collection can be array or list
     * @param key the key to search for
     * @return index if found, -1 if not
     */
    public static int binarySearchBasic(Object col, Object key) {
        if (col == null) return -1;
        if (col instanceof List) {
            return Collections.binarySearch((List) col, key);
        }
        if (col.getClass().isArray()) {
            try {
                return Arrays.binarySearch((Object[]) col, key);
            }catch (Throwable ignore){
                final ZArray za = new ZArray(col);
                return Collections.binarySearch(za, key);
            }

        }
        return -1;
    }

    /**
     * Does generic binary search, w/o any conditional expression
     *
     * @param args args[0] the collection can be array or list,
     *             args[1] the key
     * @param f    the comparator function
     * @return index if found, -1 if not
     */
    public static int binarySearch(Object[] args, Function f) {
        if ( args == null ) return -1;
        if (args.length != 2 || args[0] == null) return -1;
        if (f == null) {
            return binarySearchBasic(args[0], args[1]);
        }
        f.withIterationContext(args[0]);
        final Comparator cmp = f ;
        if (args[0] instanceof List) {
            return Collections.binarySearch((List) args[0], args[1], cmp);
        }
        if (args[0].getClass().isArray()) {
            try {
                return Arrays.binarySearch((Object[]) args[0], args[1], cmp);
            }catch (Throwable ignore){
                final ZArray za = new ZArray(args[0]);
                return Collections.binarySearch(za, args[1], cmp);
            }
        }
        return -1;
    }
}
