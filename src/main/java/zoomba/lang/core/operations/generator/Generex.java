/*
 * Copyright 2014 y.mifrah
 *
 * https://github.com/mifmif/Generex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.operations.generator;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dk.brics.automaton.Automaton;
import dk.brics.automaton.RegExp;
import dk.brics.automaton.State;
import dk.brics.automaton.Transition;

/**
 * A Java utility class that help generating string values that match a given regular expression.It generate all values
 * that are matched by the Regex, a random value, or you can generate only a specific string based on it's
 * lexicographical order .
 *
 * @author y.mifrah, zoomba-lang.org
 */
public class Generex {

    /**
     * The predefined character classes supported by {@code Generex}.
     * <p>
     * An immutable map containing as keys the character classes and values the equivalent regular expression syntax.
     *
     * @see #createRegExp(String)
     */
    private static final Map<String, String> PREDEFINED_CHARACTER_CLASSES;
    private Automaton automaton;

    static {
        Map<String, String> characterClasses = new HashMap<String, String>();
        characterClasses.put("\\\\d", "[0-9]");
        characterClasses.put("\\\\D", "[^0-9]");
        characterClasses.put("\\\\s", "[ \t\n\f\r]");
        characterClasses.put("\\\\S", "[^ \t\n\f\r]");
        characterClasses.put("\\\\w", "[a-zA-Z_0-9]");
        characterClasses.put("\\\\W", "[^a-zA-Z_0-9]");
        PREDEFINED_CHARACTER_CLASSES = Collections.unmodifiableMap(characterClasses);
    }


    /**
     * Constructs a Generex
     * @param regex with this regex
     * @param random with this Random implementation
     */
    public Generex(String regex, Random random) {
        regex = requote(regex);
        RegExp regExp = createRegExp(regex);
        automaton = regExp.toAutomaton();
        this.random = random;
    }

    /**
     * Creates a {@code RegExp} instance from the given regular expression.
     * <p>
     * Predefined character classes are replaced with equivalent regular expression syntax prior creating the instance.
     *
     * @param regex the regular expression used to build the {@code RegExp} instance
     * @return a {@code RegExp} instance for the given regular expression
     * @throws NullPointerException     if the given regular expression is {@code null}
     * @throws IllegalArgumentException if an error occurred while parsing the given regular expression
     * @throws StackOverflowError       if the regular expression has to many transitions
     * @see #PREDEFINED_CHARACTER_CLASSES
     *
     */
    private static RegExp createRegExp(String regex) {
        String finalRegex = regex;
        for (Entry<String, String> charClass : PREDEFINED_CHARACTER_CLASSES.entrySet()) {
            finalRegex = finalRegex.replaceAll(charClass.getKey(), charClass.getValue());
        }
        return new RegExp(finalRegex);
    }

    private Random random;

    /**
     * Generate and return a random String that match the pattern used in this Generex.
     *
     * @return no args random string
     */
    public String random() {
        return prepareRandom(automaton.getInitialState(), 1, Integer.MAX_VALUE);
    }

    /**
     * Generate and return a random String that match the pattern used in this Generex, and the string has a length &gt;=
     * <code>minLength</code>
     *
     * @param minLength minimal length of the string
     * @return the generated string
     */
    public String random(int minLength) {
        return prepareRandom(automaton.getInitialState(), minLength, Integer.MAX_VALUE);
    }

    /**
     * Generate and return a random String that match the pattern used in this Generex, and the string has a length &gt;=
     * <code>minLength</code> and &lt;= <code>maxLength</code>
     *
     * @param minLength min length of the string
     * @param maxLength max length of the string
     * @return the generated string
     */
    public String random(int minLength, int maxLength) {
        return prepareRandom(automaton.getInitialState(), minLength, maxLength);
    }

    // recursion removed by ZoomBA developers, to ensure it will work for arbitrary lengths
    private String prepareRandom(State initialState, int minLength, int maxLength) {
        State state = initialState;
        StringBuilder result = new StringBuilder();
        boolean loopOver = true;
        while (loopOver) {
            loopOver = false;
            List<Transition> transitions = state.getSortedTransitions(false);
            Set<Integer> selectedTransitions = new HashSet<Integer>();

            for (int resultLength = -1;
                 transitions.size() > selectedTransitions.size()
                         && (resultLength < minLength || resultLength > maxLength);
                 resultLength = result.length()) {

                if (randomPrepared(result.toString(), state, minLength, maxLength, transitions)) {
                    return result.toString();
                }

                int nextInt = random.nextInt(transitions.size());
                if (!selectedTransitions.contains(nextInt)) {
                    selectedTransitions.add(nextInt);

                    Transition randomTransition = transitions.get(nextInt);
                    int diff = randomTransition.getMax() - randomTransition.getMin() + 1;
                    int randomOffset = diff > 0 ? random.nextInt(diff) : diff;
                    char randomChar = (char) (randomOffset + randomTransition.getMin());
                    // we definitely want to improve here. Let's see how it works out
                    result.append(randomChar);
                    state = randomTransition.getDest();
                    loopOver = true;
                    break;
                }
            }
        }
        return result.toString();
    }


    private boolean randomPrepared(
            String strMatch,
            State state,
            int minLength,
            int maxLength,
            List<Transition> transitions) {

        if (state.isAccept()) {
            if (strMatch.length() == maxLength) {
                return true;
            }
            if (random.nextInt() > 0.3 * Integer.MAX_VALUE && strMatch.length() >= minLength) {
                return true;
            }
        }

        return transitions.size() == 0;
    }

    /**
     * Requote a regular expression by escaping some parts of it from generation without need to escape each special
     * character one by one. <br> this is done by setting the part to be interpreted as normal characters (thus, quote
     * all meta-characters) between \Q and \E , ex : <br> <code> minion_\d{3}\Q@gru.evil\E </code> <br> will be
     * transformed to : <br> <code> minion_\d{3}\@gru\.evil </code>
     *
     * @param regex
     * @return
     */
    private static String requote(String regex) {
        final Pattern patternRequoted = Pattern.compile("\\\\Q(.*?)\\\\E");
        // http://stackoverflow.com/questions/399078/what-special-characters-must-be-escaped-in-regular-expressions
        // adding "@" prevents StackOverflowError inside generex: https://github.com/mifmif/Generex/issues/21
        final Pattern patternSpecial = Pattern.compile("[.^$*+?(){|\\[\\\\@]");
        StringBuilder sb = new StringBuilder(regex);
        Matcher matcher = patternRequoted.matcher(sb);
        while (matcher.find()) {
            sb.replace(matcher.start(), matcher.end(), patternSpecial.matcher(matcher.group(1)).replaceAll("\\\\$0"));
            //matcher.reset();
        }
        return sb.toString();
    }

}
