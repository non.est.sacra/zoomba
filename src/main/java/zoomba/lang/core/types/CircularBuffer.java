/*
 * Copyright 2025 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.types;

import java.util.*;

/**
 * A bounded Circular Ring Buffer queue backed by an array.
 * This queue orders elements FIFO (first-in-first-out).
 * The head of the queue is that element that has been on the queue the longest time.
 * The tail of the queue is that element that has been on the queue the shortest time.
 * New elements are inserted at the tail of the queue, and the queue retrieval operations obtain elements at the head of the queue.
 * This is a classic "circular buffer", in which a fixed-sized array holds elements inserted by producers and extracted by consumers.
 * <a href="https://en.wikipedia.org/wiki/Circular_buffer">...</a>
 * Once created, the capacity cannot be changed.
 * Attempts to put an element into a full queue will result in element overwriting;
 * attempts to take an element from an empty queue will throw error depends on method
 * Can not remove specific elements from this queue
 * @param <E> the type of elements held in this queue
 */
public final class CircularBuffer<E> implements Queue<E> {

    private final E[] buffer;

    private final int maxSize;

    private int head;

    private int size;

    public int bufSize(){
        return maxSize;
    }

    @Override
    public void clear() {
        head = 0;
        size = 0;
    }

    @SuppressWarnings("unchecked")
    public CircularBuffer(int maxSize){
        this.maxSize = maxSize ;
        buffer = (E[])new Object[ maxSize ];
        clear();
    }

    @Override
    public boolean add(E e) {
        return offer(e);
    }

    @Override
    public boolean offer(E e) {
        int tail = (head + size) % maxSize ;
        buffer[tail] = e;
        final boolean isFilled = size == maxSize ;
        if ( isFilled ){
            head = (head + 1) % maxSize;
        }else {
            size ++;
        }
        return true;
    }

    @Override
    public E remove() {
        if ( isEmpty() ) throw new NoSuchElementException();
        E val = buffer[head];
        head = (head + 1) % maxSize;
        size--;
        return val;
    }

    @Override
    public E poll() {
        if ( isEmpty() ) return null;
        final E val = buffer[head];
        head = (head + 1) % maxSize;
        size--;
        return val;
    }

    @Override
    public E element() {
        if ( isEmpty() ) throw new NoSuchElementException();
        return buffer[head];
    }

    @Override
    public E peek() {
        if ( isEmpty() ) return null;
        return buffer[head];
    }

    @Override
    public int size() {
        return size ;
    }

    @Override
    public boolean isEmpty() {
        return 0 == size ;
    }

    @Override
    public boolean contains(Object o) {
        final int n = size ;
        for ( int i=0; i < n; i++){
            if (Objects.equals(o, buffer[i])) return true;
        }
        return false;
    }

    @Override
    public Iterator<E> iterator() {
        final int beginInx = head;
        final int thisSize = size;
        return new Iterator<E>() {
            int i = 0;
            @Override
            public boolean hasNext() {
                return i < thisSize;
            }

            @Override
            public E next() {
                if ( !hasNext() ) throw new NoSuchElementException();
                int curInx = (beginInx+i) % maxSize ;
                E value = buffer[curInx];
                i++;
                return value;
            }
        };
    }

    @Override
    public Object[] toArray() {
        final Object[] arr = new Object[size()];
        int i=0;
        for (E e : this) {
            arr[i++] = e;
        }
        return arr;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T[] toArray(T[] a) {
        final Object[] elementData = toArray();
        if (a.length < size)
            // Make a new array of a's runtime type, but my contents:
            return (T[]) Arrays.copyOf(elementData, size, a.getClass());
        System.arraycopy(elementData, 0, a, 0, size);
        if (a.length > size)
            a[size] = null;
        return a;
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for ( Object o : c ){
            if ( !contains(o) ) return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        c.forEach(this::add);
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        CircularBuffer<?> that = (CircularBuffer<?>) o;
        if ( this.size() != that.size() ) return false;
        Iterator<?> thisI = iterator();
        Iterator<?> thatI = that.iterator();
        while ( thisI.hasNext() ){
            Object o1 = thisI.next();
            Object o2 = thatI.next();
            if ( !Objects.equals(o1,o2) ) return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return (Arrays.hashCode(toArray()));
    }

    @Override
    public String toString() {
        return "<*"  + ZTypes.string(this,",") + "_>";
    }
}
