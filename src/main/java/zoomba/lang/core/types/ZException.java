/*
 * Copyright 2025 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.types;

import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.interpreter.ZInterpret;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.Observable;
import zoomba.lang.parser.ZoombaNode;

import java.util.*;

/**
 * ZoomBA Error Handling and Assertion Classes
 */
public class ZException extends RuntimeException {

    /**
     * A list depicting the trace value
     */
    private List<String> trace = Collections.emptyList();

    /**
     * Trace for the error - immutable List
     * @return the stack trace of the execution for the error
     */
    public List<String> stack(){
        return trace;
    }

    /**
     * Sets up the trace for the error in case the error is ZException
     * @param ex an error, trace will be set only when ex would be ZException
     * @param interpret a ZInterpret having the current trace of execution
     * @param trimSize trim the trace of interpret to this size
     */
    public static void consumeTrace(Throwable ex, ZInterpret interpret, int trimSize){
        final List<String> callStack = interpret.callStack();
        if ( !(ex instanceof ZException) ) {
            ex = new ZException(ex); // wrap it around
        }
        ((ZException)ex).trace = Collections.unmodifiableList(new ArrayList<>(callStack));
        while  ( callStack.size() != trimSize ){
            callStack.remove(0);
        }
    }

    /**
     * Constructs a ZException
     */
    protected ZException(){
        super("Unknown, No idea!");
    }

    /**
     * Constructs a ZException from a Throwable
     * @param e a Throwable
     */
    protected ZException(Throwable e){ super(e);}

    /**
     * Constructs a ZException from a Message
     * @param message string message
     */
    protected ZException(String message){ super(message);}

    /**
     * Constructs a ZException from a Throwable, and a Message
     * @param e the root cause Throwable
     * @param message the string message
     */
    protected ZException(Throwable e, String message){
        super(message,e);
    }

    /**
     * Tokenizing error
     */
    public static final class Tokenization extends ZException{
        /**
         * Constructs Tokenization error
         * @param e underlying error
         * @param message with this message
         */
        public Tokenization(Throwable e, String message) {
            super(e,message);
        }
    }

    /**
     * Parsing Error
     */
    public static final class Parsing extends ZException{

        /**
         * Error message
         */
        public final String errorMessage;

        /**
         * What are the options which were expected there
         */
        public final Set<String> correctionOptions;

        /**
         * Location of the error in the script
         */
        private String location = "" ;

        /**
         * Get the file location
         * @return file location or empty string
         */
        public String location(){
            return location;
        }

        /**
         * Sets the file location for a parse error
         * @param location non empty file location
         */
        public void withLocation(String location){
            this.location = location;
        }

        /**
         * Parsing Exception
         * @param e underlying exception
         * @param message the message
         */
        public Parsing(Throwable e, String message) {
            super(e,message);
            correctionOptions = new HashSet<>();
            String s = e.getMessage();
            int firstNewLine = s.indexOf("\n");
            if ( firstNewLine >= 0 ) {
                errorMessage = s.substring(0, firstNewLine);
                String opts = s.substring(firstNewLine + 1);
                firstNewLine = opts.indexOf("\n");
                opts = opts.substring(firstNewLine + 1);
                String[] arr = opts.split("\n");
                for (String string : arr) {
                    final String opt = string.trim().replace("...", "");
                    if (opt.isEmpty()) continue;
                    correctionOptions.add(opt);
                }
            } else{
                errorMessage = message;
            }
        }

        @Override
        public String getMessage(){
            if ( location.isEmpty() ) return errorMessage;
            if  ( correctionOptions.isEmpty() ){
                return super.getMessage() + " => " + location ;
            }
            return super.getMessage() + " => " + errorMessage + " => " + location ;
        }

        /**
         * Produces a printable message
         * @return a printable message for the error
         */
        public String printableMessage(){
           final String msg =  String.format("Parse Error: %s", getMessage() );
           if ( correctionOptions.isEmpty() ) return msg;
           return  String.format("%s %n possible options: %s", msg, ZTypes.string(correctionOptions) );
        }
    }

    /**
     * Variable Not Found Exception
     */
    public static final class Variable extends ZException{
        /**
         * Creates a Variable error
         * @param node which caused the error
         * @param name name of the variable
         */
        public Variable(ZoombaNode node, String name) {
            super( name + " : "  + node.locationInfo());
        }
    }

    /**
     * Arithmetic Execution Exception
     */
    public static final class ArithmeticLogicOperation extends ZException{

        private static String message(Throwable t, ZoombaNode node, String op){
            return String.format("Invalid Arithmetic Logical Operation [%s] : %s --> ( %s )",
                    op, node.locationInfo(), t.getMessage());
        }

        /**
         * Constructs ArithmeticLogicOperation
         * @param node which caused the error
         * @param t what error happened
         * @param op operation which caused it
         */
        public ArithmeticLogicOperation(ZoombaNode node, Throwable t, String op ) {
            super(t, message(t,node,op) );
        }
    }

    /**
     * Function Execution Exception
     */
    public static final class Function extends ZException{

        /**
         * Constructs a Function 'not found' error
         * @param node which caused the error
         * @param method name of the method which caused it
         */
        public Function(ZoombaNode node, String method ) {
            super( method + " : " + node.locationInfo() );
        }

        /**
         * Constructs a Function 'in error ' error
         * @param node which caused the error
         * @param method name of the method which caused it
         * @param t underlying error which surfaced
         */
        public Function(ZoombaNode node, String method, Throwable t ) {
            super(t,  method + " : " + node.locationInfo() );
        }
    }

    /**
     * Plugin Execution Exception
     */
    public static final class Plugin extends ZException{
        /**
         * Constructs a Plugin 'not found' Error
         * @param node which caused the error
         * @param name of the plugin
         */
        public Plugin(ZoombaNode node, String name ) {
            super( "Unregistered Plugin! " + name + " : " + node.locationInfo() );
        }

        /**
         * Constructs a Plugin 'in error' Error
         * @param node which caused the error
         * @param name of the plugin
         * @param t underlying cause of the failure
         */
        public Plugin(ZoombaNode node, String name, Throwable t ) {
            super(t,  name + " : " + node.locationInfo() );
        }
    }

    /**
     * Property Not Found Exception
     */
    public static final class Property extends ZException{

        private static String makeString(ZoombaNode node, Object targetObject, Object property, Throwable err ){
            final String fmtString;
            if ( err != null ){
                fmtString = "Object{ %s  (%s)} failed to evaluate property '%s' of %s" ;
            }else{
                fmtString = "Object{ %s  (%s)} does not have property '%s' of %s" ;
            }
            final Object tgt;
            if ( targetObject != null ){
                tgt = targetObject.getClass();
            }else{
                tgt = "nil" ;
            }
            final Object prop;
            if ( property != null ){
                prop = property.getClass();
            }else{
                prop = "null" ;
            }
            final String baseMessage = String.format(fmtString, targetObject, tgt, property, prop) + node.locationInfo();
            if ( err != null ){
                return baseMessage + "\nCaused By: " + err ;
            }
            return baseMessage;
        }

        /**
         * Constructs a property error
         * @param node which caused the error
         * @param targetObject object whose property is missing
         * @param property the actual property which is missing
         * @param err the error that was encountered while extracting the property
         */
        public Property(ZoombaNode node, Object targetObject, Object property, Throwable err ) {
            super( makeString(node, targetObject, property, err) );
        }

        /**
         * Constructs a property error
         * @param node which caused the error
         * @param targetObject object whose property is missing
         * @param property the actual property which is missing
         */
        public Property(ZoombaNode node, Object targetObject, Object property ) {
            this(node, targetObject, property, null);
        }

        /**
         * Constructs a property error
         * @param node which caused the error
         * @param propertyText the actual property which is missing
         */
        public Property(ZoombaNode node, Object propertyText ) {
            super( propertyText + " : " + node.locationInfo() );
        }

    }

    /**
     * Import Not Successful Exception
     */
    public static final class Import extends ZException{

        /**
         * Constructs an import error
         * @param node which caused the error
         * @param imported object which was tried to be imported
         * @param cause why the issue was in import
         */
        public Import(ZoombaNode node, Object imported, String cause ) {
            super( cause + "\n" + imported + " : " + node.locationInfo()  );
        }
    }

    /**
     * The Assertion Infrastructure
     */
    public abstract static class ZRuntimeAssertion extends ZException{

        /**
         * Not an Exception
         */
        public static final ZAssertionException NOT_AN_EXCEPTION = new ZAssertionException();

        /**
         * Returning the cause of the Assertion
         * @return the cause
         */
        public final Object cause(){
           if ( args.length == 0 ) return NOT_AN_EXCEPTION;
           return args[0];
        }

        /**
         * The node which generated the Exception
         */
        public final ZoombaNode here;

        /**
         * The extra arguments which were passed
         */
        public Object[] args;

        /**
         * Creates an Assertion
         * @param n the node who raised it
         * @param o the auxiliary objects
         */
        public ZRuntimeAssertion(ZoombaNode n, Object[] o){
            if ( o == null || o.length == 0 ){

                here = n;
                args = ZArray.EMPTY_ARRAY;
                return;
            }

            args = o;

            Object c = args[0];

            if ( c instanceof CharSequence ){
                args[0] = new ZAssertionException(c.toString());
            } else if ( c instanceof Throwable ){
                args[0] = new ZAssertionException((Throwable)c);
            } else if ( c instanceof Observable){
                args[0] = ((Observable<?>) c).value();
            }
            else {
                args[0] = o;
            }
            here = n ;
        }

        /**
         * Type of the assertion ( 'Assert' , 'Panic' , 'Test' )
         * Classifying the type of the assertion
         * @return a string - one of the above text
         */
        public abstract String myType();

        @Override
        public String getMessage() {
            return ((Throwable)cause()).getMessage();
        }

        @Override
        public String toString() {
            return String.format( "%s --> [ %s ] caused by : %s", myType() , here.locationInfo(), cause() );
        }
    }

    /**
     * The runtime Assertion Panic
     */
    public static class Panic extends ZRuntimeAssertion{

        /**
         * Constructs a Panic error
         * @param n which caused it
         * @param o bunch of objects which are parameters
         */
        public Panic(ZoombaNode n, Object[] o) {
            super(n, o);
        }

        @Override
        public String myType() {
            return "Panic";
        }
    }

    /**
     * The runtime Assertion Assert
     */
    public static class Assertion extends ZRuntimeAssertion{

        /**
         * Constructs an Assertion error
         * @param n which caused it
         * @param o bunch of objects which are parameters
         */
        public Assertion(ZoombaNode n, Object[] o) {
            super(n, o);
        }

        @Override
        public String myType() {
            return "Assertion Failure";
        }
    }

    /**
     * The runtime Exceptions to handle control flow structure
     */
    public static class MonadicException extends ZException implements zoomba.lang.core.operations.Function.MonadicContainer {


        public static Throwable throwable(zoomba.lang.core.operations.Function.MonadicContainer mc){
            if ( mc instanceof MonadicException ){
                return ((MonadicException) mc).getCause();
            }
            return null;
        }

        /**
         * Underlying actual value
         */
        public final Object value;

        /**
         * Construct the exception
         */
        public MonadicException(){
            super();
            value = zoomba.lang.core.operations.Function.NIL;
        }

        /**
         * Construct the exception
         * @param o underling value of the error
         */
        public MonadicException(Object o){
            super();
            value = o ;
        }

        /**
         * Construct the exception
         * @param o underlying root cause of the error
         */
        public MonadicException(Throwable o){
            super(o);
            value = zoomba.lang.core.operations.Function.NIL;
        }

        @Override
        public boolean isNil(){ return value == zoomba.lang.core.operations.Function.NIL; }

        @Override
        public Object value() {
            return value;
        }
    }

    /**
     * The Return Exception
     */
    public static final class Return extends MonadicException{

        /**
         * A field to have one instance of Return
         */
        public static final Return RETURN = new Return();

        private Return(){ super();}

        /**
         * Construct the Return type
         * @param o actual return value
         */
        public Return(Object o){ super(o);}

    }

    /**
     * The Continue Exception
     */
    public static final class Continue extends MonadicException{

        /**
         * A field to have one instance of Continue
         */
        public static final Continue CONTINUE = new Continue();

        private Continue(){ super();}

        /**
         * Construct the Continue type
         * @param o actual return value for Continue
         */
        public Continue(Object o){ super(o);}

    }

    /**
     * The Break Exception
     */
    public static final class Break extends MonadicException{

        /**
         * A field to have one instance of Break
         */
        public static final Break BREAK = new Break();

        private Break(){ super();}

        /**
         * Construct the Break type
         * @param o actual return value for Break
         */
        public Break(Object o){ super(o);}

    }

    /**
     * The Termination Exception
     */
    public static final class ZTerminateException extends ZAssertionException {

        /**
         * Construct the ZTerminateException
         * @param m message we need to pass
         */
        public ZTerminateException(String m){
            super(m);
        }
    }
}

/**
 * Generic Assertion Exception
 */
class ZAssertionException extends ZException{

    /**
     * Construct a non exception
     */
    public ZAssertionException(){
        super("NO_EXCEPTION");
    }

    /**
     * Construct a ZAssertionException
     * @param t underlying root cause
     */
    public ZAssertionException(Throwable t){
        super(t);
    }

    /**
     * Construct a ZAssertionException
     * @param m with this message
     */
    public ZAssertionException(String m){
        super(m);
    }
}


