/*
 * Copyright 2025 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.types;

import ch.obermuhlner.math.big.BigDecimalMath;
import ch.obermuhlner.math.big.DefaultBigDecimalMath;
import zoomba.lang.core.operations.Arithmetic;
import zoomba.lang.core.operations.Arithmetic.BasicArithmeticAware;
import zoomba.lang.core.operations.Arithmetic.LogicAware;
import zoomba.lang.core.operations.Arithmetic.NumericAware;
import zoomba.lang.core.operations.Function;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;

/**
 * Wrapper over Numbers, so that we can have unlimited precision Arithmetic
 * It holds a long value, and a BigInteger value and a BigDecimal value to do all the tricks
 * All integers can grow to arbitrary large
 * All floating points are always BigDecimal which makes floating point computations slow but accurate
 * Idea was to use Rational but that idea was dropped because of no practical need to do so
 */
public class ZNumber extends Number implements
        BasicArithmeticAware, NumericAware , LogicAware , ZTypes.StringX<Number> {

    static MathContext MATH_CONTEXT = MathContext.DECIMAL128;

    /**
     * Sets the precision for the arithmetic, default is 34 digits
     * Specifically check the test here
     * samples/test/precision_e.zm
     * to see how it is getting used to compute "e" with 256 digits of precision
     * @param p no of digits of precision for floating point arithmetic
     * @return previous precision
     */
    public synchronized static int precision(int p){
        final int precision = MATH_CONTEXT.getPrecision();
        MATH_CONTEXT = new MathContext( p, MATH_CONTEXT.getRoundingMode() );
        DefaultBigDecimalMath.setDefaultMathContext(MATH_CONTEXT);
        return precision;
    }

    /**
     * Gets the precision for the arithmetic, default is 34 digits
     * @return current precision
     */
    public synchronized static int precision(){
        return MATH_CONTEXT.getPrecision();
    }

    @Override
    public Number deepCopy() {
        if ( isPrimitive() ){
            return new ZNumber(lValue, noCoercion);
        }
        return new ZNumber(toString(),noCoercion);
    }

    static final DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols(Locale.getDefault());

    static final String decimalSeparator = String.valueOf(decimalFormatSymbols.getDecimalSeparator());

    private static final String VALUE = "value";

    private static final String BASE = "base";

    private static final String DEFAULT = "default";


    /**
     * From args converts into an Integer type ( can be Long or even BigInteger also )
     * @param args the arguments
     *      args[0] string or another object
     *      args[1] default integer object, when conversion fails, returns that
     * @return null, or an integer type ( or default )
     */
    public static Number integer(Object...args){
        if ( args.length == 0 ) return null;
        if ( args.length == 3 ){
            // num,base,default
            try{
                BigInteger i = intRadix( args[0].toString(), ((Number)args[1]).intValue() );
                return narrowBigInteger(i);
            }catch (Exception e){
                return integer(args[2]);
            }
        }
        if ( args[0] instanceof Map){
            Function.NamedArgs na = Function.NamedArgs.fromMap((Map)args[0]);
            return integer( na.getOrDefault(VALUE,null),
                    na.getOrDefault(BASE,10) , na.getOrDefault(DEFAULT,null));
        }

        if ( args[0] instanceof Number ){
            if ( args[0] instanceof Integer  ||
                    args[0] instanceof Short ||
                    args[0] instanceof  Byte ||
                    args[0] instanceof Long  ||
                    args[0] instanceof BigInteger ){
                return (Number)args[0];
            }
        }
        try {
            return new ZNumber(args[0]).integer();
        }catch (Exception e){
            try {
                return integer(args[1]);
            }catch (Exception ee){}
        }
        return null;
    }

    /**
     * From args converts into BigInteger type
     * @param args the arguments
     *      args[0] string or another object
     *      args[1] default integer object, when conversion fails, returns that
     * @return null, or a BigInteger ( or default )
     */
    public static BigInteger INT(Object...args){
        if ( args.length > 0 && args[0] instanceof ZDate ){ // special casing for Date to Nanos
            return ((ZDate)args[0]).toNano();
        }
        Number n = integer(args);
        if ( n == null) return null;
        return new BigInteger(n.toString());
    }

    /**
     * From args converts into a BigDecimal type
     * @param args the arguments
     *      args[0] string or another object
     *      args[1] default floating object, when conversion fails, returns that
     * @return null, or a floating ( or default )
     */
    public static BigDecimal FLOAT(Object...args){
        if ( args.length > 0 && args[0] instanceof ZDate ){
            return new BigDecimal( ((ZDate) args[0]).toNano() );
        }
        Number n = floating(args);
        if ( n == null) return null;
        return new BigDecimal(n.toString());
    }

    /**
     * Converts to an integer like type: one of int, long, BigInteger
     * In the minimal container where the value can be contained
     * @return an integer like object
     */
    public Number integer(){
        if ( isPrimitive() ) return narrowLong(lValue);
        if ( bigInteger != null ) return narrowBigInteger(bigInteger);
        return narrowBigInteger( bigDecimal.toBigInteger() );
    }

    /**
     * Converts to a floating point like object double, BigDecimal
     * In the minimal container where the value can be precisely contained
     * @return a floating like object
     */
    public Number floating(){
        return narrowBigDecimal( bigDecimal() );
    }

    /**
     * From args converts into a floating type float/double/BigDecimal/ZRational
     * @param args the arguments
     *      args[0] string or another object
     *      args[1] default floating object, when conversion fails, returns that
     * @return null, or a floating ( or default )
     */
    public static Number floating(Object...args){
        if ( args.length == 0 ) return null;
        if ( args[0] instanceof Character){
            Integer i = (int)((Character)args[0]).charValue() ;
            return i.doubleValue();
        }
        if ( args[0] instanceof Number ){
            Number num = (Number)args[0];
            if ( num instanceof Double || num instanceof Float || num instanceof BigDecimal ){
                return num ;
            }
        }
        try {
            return new ZNumber(args[0]).floating() ;
        }catch (Exception e){
            try {
                return floating(args[1]);
            }catch (Exception ee){}
        }
        return null;
    }

    /**
     * From args converts into a numeric type
     * @param args the arguments
     *      args[0] string or another object
     *      args[1] default number object, when conversion fails, returns that
     * @return null, or a numeric ( or default )
     */
    public static Number number(Object...args){
        if ( args.length == 0 ) return null;
        if ( args[0] instanceof Number ){
            return (Number)args[0];
        }
        try {
            ZNumber zn = new ZNumber(args[0]);
            return zn.actual();
        }catch (Exception e){
            String s = String.valueOf(args[0]);
            if ( "inf".equalsIgnoreCase(s) ) return Arithmetic.POSITIVE_INFINITY ;
            if ( "-inf".equalsIgnoreCase(s) ) return Arithmetic.NEGATIVE_INFINITY ;

            try {
                return number(args[1]);
            }catch (Exception ignore){}
        }
        return null;
    }

    /**
     * Rounds a number
     * http://mathforum.org/library/drmath/view/71202.html
     * @param n the number
     * @param args optional, num of digits to round into
     * @return the rounded number
     */
    public static Number round(Number n, Object...args){
        if ( n instanceof Integer || n instanceof Long
                || n instanceof BigInteger || n instanceof Short || n instanceof Byte ){
            return n;
        }
        int numDigits = 0;
        if ( args.length > 0 ){
            numDigits = integer( args[0], 0 ).intValue() ;
        }
        if ( n instanceof ZNumber){
            return round(((ZNumber) n).actual(), numDigits );
        }
        BigDecimal bd = new BigDecimal(String.valueOf(n));
        if ( numDigits < 0  ){
            return narrowBigInteger(bd.toBigInteger());
        }
        bd = bd.setScale(numDigits, RoundingMode.HALF_UP );
        return narrowBigDecimal(bd);
    }

    /**
     * Floor of a number
     * @param n the number
     * @return the floored number
     */
    public static Number floor(Number n){
        if ( n instanceof Integer || n instanceof Long || n instanceof BigInteger || n instanceof Short || n instanceof Byte ){
            return n;
        }
        if ( n instanceof Double || n instanceof Float ){
            return integer( Math.floor( n.doubleValue() ) );
        }
        return new ZNumber(n).floor();
    }

    /**
     * Ceiling of a number
     * @param n the number
     * @return the ceil number
     */
    public static Number ceil(Number n){
        if ( n instanceof Integer || n instanceof Long || n instanceof BigInteger || n instanceof Short || n instanceof Byte ){
            return n;
        }
        if ( n instanceof Double || n instanceof Float ){
            return integer( Math.ceil( n.doubleValue() ) );
        }
        return new ZNumber(n).ceil();
    }

    static boolean numberEquals(Number r1, Number r2){
        BigDecimal b1 = new BigDecimal(String.valueOf(r1));
        BigDecimal b2 = new BigDecimal(String.valueOf(r2));
        return 0 == b1.compareTo(b2);
    }

    /**
     * Narrow down a Large float into double precision if possible
     * @param n the large float
     * @return a narrowed down double or the same float if it can not be done
     */
    public static Number narrowNumber(Number n){
        if ( n instanceof Integer || n instanceof Short || n instanceof Byte) return n;
        if ( n instanceof Long ) return narrowLong(n.longValue());
        if ( n instanceof BigInteger ) return narrowBigInteger((BigInteger) n );

        final BigDecimal f = n instanceof BigDecimal ? (BigDecimal)n : new BigDecimal(String.valueOf(n));

        if ( integerValued(f) ){
            return narrowBigInteger( f.toBigInteger() );
        }
        else{
            return narrowBigDecimal(f);
        }
    }

    /**
     * Underlying number a Real when we store fractional data
     */
    protected BigDecimal bigDecimal = null;

    /**
     * Underlying integer val if overcomes long max or goes below long min
     */
    protected BigInteger bigInteger = null ;

    /**
     * Underlying long val if value is small enough
     */
    protected long lValue;

    /**
     * Current value is a primitive
     * @return true in case primitive, false otherwise
     */
    public boolean isPrimitive(){
        return bigInteger == null && bigDecimal == null;
    }

    /**
     * This is different from fractional.
     * Storing 1 as integer would set fractional to false
     * Storing 1.0 , an integer, we are storing to float, so fractional would be true
     * But this is different. integerValued() both cases would return true
     * @param bigDecimal is that value an integer
     * @return true if integer type , false otherwise
     */
    public static boolean integerValued(BigDecimal bigDecimal){
        if  (  bigDecimal.scale() <= 0  ) return true;
        return bigDecimal.remainder(BigDecimal.ONE).compareTo(BigDecimal.ZERO) == 0;
    }

    /**
     * This is different from fractional.
     * Storing 1 as integer would set fractional to false
     * Storing 1.0 , an integer, we are storing to float, so fractional would be true
     * But this is different. integerValued() both cases would return true
     * @return true if integer type , false otherwise
     */
    public boolean integerValued(){
        return bigDecimal == null || integerValued( bigDecimal );
    }

    private void populateNumber (Number o){
        bigDecimal = null;
        bigInteger = null;
        lValue = 0;
        if ( o instanceof Integer || o instanceof Long || o instanceof Short || o instanceof Byte ){
            lValue = o.longValue() ;
        } else if ( o instanceof Double || o instanceof Float ) {
            bigDecimal = new BigDecimal(String.valueOf(o));
        }else if ( o instanceof ZNumber){
            bigDecimal = ((ZNumber) o).bigDecimal;
            if ( bigDecimal != null ) {
                // ensure we have a normalized form
                bigDecimal = bigDecimal.stripTrailingZeros();
            }
            bigInteger = ((ZNumber) o).bigInteger;
            lValue = ((ZNumber) o).lValue;
        } else if ( o instanceof BigInteger ){
            bigInteger = (BigInteger)o;
        } else if ( o instanceof BigDecimal ){
            bigDecimal = (BigDecimal)o;
        }
        else{
            // reason we do this is to ensure the precision passed gets preserved
            // there should be a better way. but right now, this works
            // if we go with creating double, float problems galore abs(0.0) would become 0.
            bigDecimal = new BigDecimal(String.valueOf(o));
            bigDecimal = bigDecimal.stripTrailingZeros();
        }
    }

    /**
     * Allow or Disallow
     * <a href="https://developer.mozilla.org/en-US/docs/Glossary/Type_coercion">...</a>
     */
    public final boolean noCoercion;

    /**
     * <a href="https://developer.mozilla.org/en-US/docs/Glossary/Type_coercion">...</a>
     * No Coercion in this case
     * Creates a Wrapper number from an object
     * @param n the number
     */
    public ZNumber(Number n){
        this.noCoercion = true;
        populateNumber(n);
    }

    /**
     * Creates a Wrapper number from an object by Coercion
     * <a href="https://developer.mozilla.org/en-US/docs/Glossary/Type_coercion">...</a>
     * @param o the object
     */
    public ZNumber(Object o){
        this(o,false);
    }

    /**
     * Coercion function
     * <a href="https://developer.mozilla.org/en-US/docs/Glossary/Type_coercion">...</a>
     * @param o the object to be coerced into Numeric
     * @return a Number or throws error
     */
    static Number coerce2Numeric(Object o){
        if ( o instanceof Character) return (int)((Character)o);
        if ( o instanceof Enum<?>) return ((Enum<?>)o).ordinal();
        if ( o instanceof Date ) return ((Date) o).getTime();
        if ( o instanceof ZDate ) return ((ZDate) o).date().getTime();
        if ( o instanceof Boolean ) return ((boolean)o) ? 1 : 0 ;
        String v = String.valueOf(o);
        if ( o instanceof CharSequence && v.contains(decimalSeparator)  ){
            return narrowBigDecimal( new BigDecimal(v));
        }
        return narrowBigInteger( new BigInteger(v));
    }

    /**
     * <a href="https://developer.mozilla.org/en-US/docs/Glossary/Type_coercion">...</a>
     * Creates a Wrapper number from an object
     * @param o the object
     * @param noCoercion should not allow coercion in any operation
     */
    public ZNumber(Object o, boolean noCoercion){
        this.noCoercion = noCoercion;
        if ( o instanceof Number ){
            populateNumber((Number) o);
            return; // no bickering
        }
        if ( noCoercion ){
            // a polite go away message
            final String msg = String.format( "[Object(%s) of %s] is not a Number!", o, o.getClass() );
            throw new UnknownFormatConversionException(msg);
        }
        // coerce
        try {
            populateNumber( coerce2Numeric(o) );
        }catch (Exception e){
            final String msg = String.format( "[Object(%s) of %s] can of coerced into a Number!", o, o.getClass() );
            throw new UnknownFormatConversionException(msg);
        }
    }

    @Override
    public int intValue() {
        if ( isPrimitive() ) return (int)lValue;

        if ( bigInteger == null ) {
            return bigDecimal.intValue();
        }
        return bigInteger.intValue();
    }

    @Override
    public long longValue() {
        if ( isPrimitive() ) return lValue;

        if ( bigInteger == null ) {
            return bigDecimal.longValue();
        }
        return bigInteger.longValue();
    }

    @Override
    public float floatValue() {
        if ( isPrimitive() ) return (float) lValue;
        if ( bigDecimal == null ){
            return bigInteger.floatValue();
        }
        return bigDecimal.floatValue();
    }

    @Override
    public double doubleValue() {
        if ( isPrimitive() ) return (double) lValue;

        if ( bigDecimal == null ){
            return bigInteger.doubleValue();
        }
        return bigDecimal.doubleValue();
    }

    /**
     * Narrows down a BigInteger value progressively till it can fit into a container best for it.
     * The absolute minimum is integer. So it can return wither int, long, or BigInteger
     * @param api a BigInteger value
     * @return absolute minimal numeric storage that can store the value int,long, or BigInteger
     */
    public static Number narrowBigInteger(BigInteger api){
        long l = api.longValue();
        if ( !api.equals( BigInteger.valueOf(l)) ) return api;
        int i = (int)l;
        if ( (long)i == l ) return i;
        return l;
    }

    static Number narrowLong(long l){
        int i = (int)l;
        if ( (long)i == l ) return i;
        return l;
    }

    /**
     * Narrows down a BigDecimal value progressively till it can fit into a container best for it.
     * The absolute minimum is double. So it can return a double or BigDecimal
     * Remember - precision is the key. In case of loss of precision, it would store BigDecimal
     * @param r a BigDecimal value
     * @return absolute minimal numeric storage that can store the value double or BigDecimal
     */
    public static Number narrowBigDecimal(BigDecimal r){
        final double d = r.doubleValue();
        if ( numberEquals( d, r) ) return d;
        return r;
    }

    static boolean safeAdd(long x, long y, long[] res){
        final long r = res[0] = x + y ;
        return  (((x ^ r) & (y ^ r)) >= 0);
    }

    static boolean safeSubtract(long x, long y, long[] res){
        final long r = res[0] = x - y ;
        return  (((x ^ y) & (x ^ r)) >= 0);
    }

    static boolean safeMultiply(long x, long y, long[] res) {
        long r = res[0] = x * y;
        long ax = Math.abs(x);
        long ay = Math.abs(y);
        if (((ax | ay) >>> 31 != 0)) {
            // Some bits greater than 2^31 that might cause overflow
            // Check the result using the divide operator
            // and check for the special case of Long.MIN_VALUE * -1
            if (((y != 0) && (r / y != x)) ||
                    (x == Long.MIN_VALUE && y == -1)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Object _add_(Object o) {
        ZNumber zn = new ZNumber(o, noCoercion);
        if ( isPrimitive() && zn.isPrimitive() ){
            long[] res = new long[1];
            if (safeAdd(lValue, zn.lValue, res)) {
                return narrowLong(res[0]);
            }
        }
        if ( fractional() || zn.fractional() ){
            return narrowBigDecimal( bigDecimal().add(zn.bigDecimal()));
        }
        return narrowBigInteger( bigInteger().add( zn.bigInteger()) );
    }

    @Override
    public Object _sub_(Object o) {
        ZNumber zn = new ZNumber(o, noCoercion);
        if ( isPrimitive() && zn.isPrimitive() ){
            long[] res = new long[1];
            if (safeSubtract(lValue, zn.lValue, res)) {
                return narrowLong(res[0]);
            }
        }
        if ( fractional() || zn.fractional() ){
            return narrowBigDecimal( bigDecimal().subtract(zn.bigDecimal()));
        }
        return narrowBigInteger( bigInteger().subtract( zn.bigInteger()) );
    }

    @Override
    public Object _mul_(Object o) {
        ZNumber zn = new ZNumber(o, noCoercion);
        if ( isPrimitive() && zn.isPrimitive() ){
            long[] res = new long[1];
            if (safeMultiply(lValue, zn.lValue, res)) {
                return narrowLong(res[0]);
            }
        }
        if ( fractional() || zn.fractional() ){
            return narrowBigDecimal( bigDecimal().multiply(zn.bigDecimal()));
        }
        return narrowBigInteger( bigInteger().multiply( zn.bigInteger()) );
    }

    @Override
    public Object _div_(Object o) {
        ZNumber zn = new ZNumber(o, noCoercion);
        if ( isPrimitive() && zn.isPrimitive() ){
            return narrowLong( lValue / zn.lValue );
        }
        if ( fractional() || zn.fractional() ){
            return narrowBigDecimal( bigDecimal().divide(zn.bigDecimal(), MATH_CONTEXT ));
        }
        return narrowBigInteger( bigInteger().divide( zn.bigInteger()) );
    }

    static BigInteger fastExp(BigInteger l, long exp){
        if ( exp < 0 ) throw new IllegalArgumentException("exp can not be negative : " + exp);
        if ( exp == 0 ) return BigInteger.ONE;
        if ( l.equals(BigInteger.ONE) ) return BigInteger.ONE;
        BigInteger tmp = BigInteger.ONE;
        while ( exp > 1 ){
            if ( exp % 2 == 1 ){ // is odd
                tmp = tmp.multiply(l);
            }
            exp = exp/2;
            l = l.multiply(l);
        }
        final BigInteger res = l.multiply(tmp);
        return res;
    }

    static BigDecimal fastExp(BigDecimal l, long exp){

        if ( exp == 0 ) return BigDecimal.ONE;
        if ( l.equals(BigDecimal.ONE) ) return BigDecimal.ONE;
        final boolean invert ;
        if ( exp < 0 ){
            // negate
            exp = -exp;
            invert = true;
        }else{
            invert = false;
        }

        BigDecimal tmp = BigDecimal.ONE;
        while ( exp > 1 ){
            if ( exp % 2 == 1 ){ // is odd
                tmp = tmp.multiply(l);
            }
            exp = exp/2;
            l = l.multiply(l);
        }
        BigDecimal res = l.multiply(tmp);
        if ( invert ){
            res = BigDecimal.ONE.divide( res );
        }
        return res;
    }

    @Override
    public Object _pow_(Object o) {
        ZNumber zn = new ZNumber(o, noCoercion);
        if ( zn.isZero() ) {
            if ( fractional() ) return 1.0D;
            return 1;
        }

        if ( integerValued() && zn.integerValued() ){
            final boolean invert ;
            if ( zn.compareTo(0) < 0 ){
                // negate
                zn.not_mutable();
                invert = true;
            }else{
                invert = false;
            }
            BigInteger bi = fastExp(bigInteger(), zn.longValue() );
            if ( invert ) {
                BigDecimal bd = BigDecimal.ONE.divide( new BigDecimal(bi) , MATH_CONTEXT );
                return narrowBigDecimal( bd);
            }
            if ( fractional() ){
                return narrowBigDecimal( new BigDecimal(bi) );
            }
            return narrowBigInteger(bi);
        }

        if ( fractional() && zn.integerValued()){
            return narrowBigDecimal( fastExp( bigDecimal, zn.longValue() ) );
        }
        final BigDecimal exp = BigDecimalMath.pow( bigDecimal(), zn.bigDecimal(), MATH_CONTEXT );
        return narrowBigDecimal( exp );
    }

    @Override
    public void add_mutable(Object o) {
        populateNumber( (Number) _add_(o));
    }

    @Override
    public void sub_mutable(Object o) {
        populateNumber( (Number) _sub_(o));
    }

    @Override
    public void mul_mutable(Object o) {
        populateNumber( (Number) _mul_(o));
    }

    @Override
    public void div_mutable(Object o) {
        populateNumber( (Number) _div_(o));
    }

    @Override
    public Object _and_(Object o) {
        ZNumber zn = new ZNumber(o, noCoercion);
        if ( isPrimitive() && zn.isPrimitive() ){
            return narrowLong( lValue & zn.lValue ) ;
        }
        BigInteger bi = zn.bigInteger();
        BigInteger me = bigInteger();
        return  integer( me.and(bi) ) ;
    }

    @Override
    public Object _or_(Object o) {
        ZNumber zn = new ZNumber(o, noCoercion);
        if ( isPrimitive() && zn.isPrimitive() ){
            return narrowLong( lValue | zn.lValue ) ;
        }
        BigInteger bi = zn.bigInteger();
        BigInteger me = bigInteger();
        return  integer( me.or( bi )) ;
    }

    @Override
    public Object _xor_(Object o) {
        ZNumber zn = new ZNumber(o, noCoercion);
        if ( isPrimitive() && zn.isPrimitive() ){
            return narrowLong( lValue ^ zn.lValue ) ;
        }
        BigInteger bi = zn.bigInteger();
        BigInteger me = bigInteger();
        return  integer( me.xor( bi )) ;
    }

    @Override
    public void and_mutable(Object o) {
        populateNumber( (Number) _and_(o));
    }

    @Override
    public void or_mutable(Object o) {
        populateNumber( (Number) _or_(o));
    }

    @Override
    public void xor_mutable(Object o) {
        populateNumber( (Number) _xor_(o));
    }

    @Override
    public Number _abs_() {
        if ( isPrimitive() ) return narrowLong( Math.abs(lValue) );
        if ( bigDecimal != null ){
            return narrowBigDecimal( bigDecimal.abs() );
        }
        return narrowBigInteger(bigInteger.abs());
    }

    @Override
    public Number _not_() {
        if ( isPrimitive() ) {
            long[] res = new long[1];
            if ( safeSubtract(0, lValue , res)){
                return narrowLong(res[0]);
            }
            return narrowBigInteger( bigInteger().negate());
        }
        if ( bigDecimal != null ){
            return narrowBigDecimal( bigDecimal.negate() );
        }
        return narrowBigInteger(bigInteger.negate());
    }

    @Override
    public void not_mutable() {
        populateNumber(_not_());
    }

    @Override
    public Number _mod_(Number o ) {
        ZNumber zn = o instanceof ZNumber? (ZNumber)o : new ZNumber(o, noCoercion);
        if ( zn.isZero() ){
            throw new IllegalArgumentException("modulo by 0.0!");
        }
        if ( bigDecimal != null || zn.bigDecimal != null ){
            return 0;
        }
        
        if ( isPrimitive() && zn.isPrimitive() ){
            return narrowLong(lValue % zn.lValue);
        }
        BigInteger mod = bigInteger().mod(zn.bigInteger());
        return narrowBigInteger(mod);
    }

    @Override
    public void mod_mutable(Number o) {
        populateNumber( _mod_(o));
    }

    /**
     * Whether this number is zero or not
     * Can be int 0, long 0, or BigInteger 0 or even BigDecimal 0.0*
     * @return true in case this is zero false otherwise
     */
    public boolean isZero(){
        if ( isPrimitive() ) return lValue == 0;
        if ( bigInteger != null ) return bigInteger.equals( BigInteger.ZERO);
        return bigDecimal.compareTo( BigDecimal.ZERO) == 0;
    }

    @Override
    public Number actual() {
        if ( isPrimitive() ) return narrowLong(lValue);
        if ( bigDecimal != null ){
            return narrowBigDecimal(bigDecimal);
        }
        return narrowBigInteger(bigInteger);
    }

    @Override
    public BigDecimal bigDecimal() {
        if ( isPrimitive() ) return BigDecimal.valueOf(lValue);
        if ( bigDecimal != null ) {
            return bigDecimal;
        }
        return new BigDecimal(bigInteger);
    }

    @Override
    public BigInteger bigInteger() {
        if ( isPrimitive() ) return BigInteger.valueOf(lValue);
        if ( bigInteger != null ) {
            return bigInteger;
        }
        // produces truncated BigInteger -- should we ?
        return bigDecimal.toBigInteger();
    }

    @Override
    public boolean fractional() {
        return bigDecimal != null ;
    }

    @Override
    public int compareTo(Object obj) {
        if ( obj == null ) throw  new IllegalArgumentException("other object can not be null!");
        if ( obj == this ) return 0 ;
        ZNumber o = new ZNumber(obj, noCoercion);
        if ( isPrimitive() && o.isPrimitive() ){
            return Long.compare(lValue, o.lValue);
        }
        if ( integerValued() && o.integerValued() ){
            return bigInteger().compareTo( o.bigInteger() );
        }
        return bigDecimal().compareTo( o.bigDecimal() );
    }

    @Override
    public Number ceil() {
        if ( isPrimitive() ) return narrowLong(lValue);
        if ( integerValued() || bigDecimal.compareTo( BigDecimal.ZERO ) < 0 ) return narrowBigInteger(bigInteger());
        BigInteger bi = bigInteger().add(BigInteger.ONE);
        return narrowBigInteger(bi);
    }

    @Override
    public Number floor() {
        if ( isPrimitive() ) return narrowLong(lValue);
        if ( integerValued() || bigDecimal.compareTo( BigDecimal.ZERO ) > 0 ) return narrowBigInteger(bigInteger());
        BigInteger bi = bigInteger().subtract(BigInteger.ONE);
        return narrowBigInteger(bi);
    }

    @Override
    public String toString() {
        if ( isPrimitive() ) return Long.toString(lValue);

        if ( bigInteger != null ){
            return bigInteger.toString();
        }
        BigDecimal bd = new BigDecimal(bigDecimal.toString());
        return bd.toString();
    }

    static final String charSet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz" ;

    /**
     * <a href="https://en.wikipedia.org/wiki/Base62">...</a>
     * @param bi Big Integer to encode
     * @param r radix, the base of the encoding
     * @return a String rep for the number bi in base r
     */
    public static String radix(BigInteger bi, int r){
        if ( r <= 36 || r > 62 ) return bi.toString(r);
        int sgn = bi.compareTo(BigInteger.ZERO);
        if ( sgn == 0 ) return "0";
        String sign = "";
        if ( sgn < 0 ){
            bi = bi.negate(); // make pos
            sign = "-" ;
        }
        StringBuilder ret = new StringBuilder();
        final BigInteger br = BigInteger.valueOf(r);
        while ( !bi.equals( BigInteger.ZERO )){
            int rem = bi.mod( br).intValue() ;
            bi = bi.divide(br);
            ret.insert(0, charSet.charAt(rem));
        }
        return sign + ret ;
    }


    /**
     * <a href="https://en.wikipedia.org/wiki/Base62">...</a>
     * @param s A String may be an int encoded in base r
     * @param r radix, the base of the encoding
     * @return a BigInteger
     */
    public static BigInteger intRadix(String s, int r){
        if ( r <= 36 || r > 62 ) return new BigInteger(s, r );
        int i = 0 ;
        boolean neg = false;
        if ( s.startsWith("-") ){
            neg = true;
            i++;
        }

        BigInteger cur = BigInteger.ZERO ;
        BigInteger radix = BigInteger.valueOf(r);
        while ( i < s.length() ){
            int ord = charSet.indexOf( s.charAt(i));
            if ( ord < 0 ) throw new IllegalArgumentException("Invalid char : " + s.charAt(i));
            cur = cur.multiply(radix);
            cur = cur.add( BigInteger.valueOf(ord ));
            i++;
        }
        return  neg? cur.negate() : cur;
    }

    @Override
    public String string(Object... args) {
        if ( args.length == 0 ) return toString();
        if ( args[0] instanceof Number ){
            int i = ((Number)args[0]).intValue() ;
            if ( !fractional() ){
                BigInteger bi = bigInteger();
                final String unPadded = radix(bi, i);
                if ( args.length == 1 ) return unPadded;
                int numPad = ZNumber.integer(args[1],0).intValue() ;
                numPad -= unPadded.length() ;
                if ( numPad <= 0 ) return unPadded;
                StringBuffer sb = new StringBuffer();
                while ( numPad > 0 ){
                    numPad --;
                    sb.append('0');
                }
                sb.append(unPadded);
                return sb.toString();
            }
            // the precision float thingie
            BigDecimal bd = bigDecimal();
            String format = "%%.%df" ;
            format = String.format(format, i);
            String v = String.format(format,bd);
            return v;
        }
        if ( args[0] instanceof String ){
            String pattern = (String)args[0];
            DecimalFormat df = new DecimalFormat(pattern);
            return df.format(bigDecimal());
        }

        return toString();
    }

    @Override
    public int hashCode() {
        Number n = actual();
        return n.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if ( obj == this ) return true ;
        if ( obj == null ) return false ;
        return compareTo(obj) == 0;
    }

    /**
     * Log base e
     * @return log(this,e)
     */
    public Number ln(){
        BigDecimal bd = bigDecimal();
        BigDecimal res = BigDecimalMath.log( bd, MATH_CONTEXT );
        return narrowNumber(res);
    }

    /**
     * Log of this base b
     * @param b the base
     * @return log(this,b)
     */
    public Number log(Object b){
        BigDecimal bd = bigDecimal();
        BigDecimal myLog = BigDecimalMath.log( bd, MATH_CONTEXT );
        BigDecimal base = new ZNumber(b, noCoercion).bigDecimal();
        BigDecimal baseLog = BigDecimalMath.log( base, MATH_CONTEXT );
        BigDecimal res = myLog.divide( baseLog, MATH_CONTEXT );
        return narrowNumber(res);
    }

    /**
     * The sign function for the number returns -1, 0, or +1
     * based on negative, zero, or positive
     * @return -1,0,1
     */
    public int sign(){
        if ( isPrimitive() ) {
            if ( lValue < 0 ) return -1;
            if ( lValue > 0 ) return  1;
            return 0;
        }
        if ( bigInteger != null ){
            return bigInteger.compareTo( BigInteger.ZERO );
        }
        return bigDecimal.compareTo( BigDecimal.ZERO );
    }

    /**
     * Computes bitwise complement for the number
     * @return the bitwise complement of the number
     */
    public Number bitwiseComplement(){
        BigInteger bi = bigInteger();
        return narrowBigInteger( bi.not() );
    }

    /**
     * Log of this base b
     * @param args the optional base
     *     if not specified treats base as e,
     *     else takes args[0] as base b
     * @return log(this,b)
     */
    public static Number log(Object...args){
        if  ( args.length == 0 ) return Math.E ;
        ZNumber zn = new ZNumber( args[0], true);
        if ( args.length == 1 ) return zn.ln();
        return zn.log(args[1]);
    }

    /**
     * Sign of a numeric value
     *  -1 if argument is negative 0 if 0, and 1 if positive
     * @param args argument only args[0] is useful
     * @return -1 if args[0] is negative 0 if 0, and 1 if positive
     */
    public static int sign(Object ... args){
        if ( args.length == 0 ) return 0;
        ZNumber zn = new ZNumber( args[0] , true);
        return zn.sign();
    }

    /**
     * Converts a numeric type to a target type
     * @param num numeric object Number
     * @param type target type
     * @return converted object or failing to convert, input back
     */
    public static Object convertNum(Number num, Class<?> type){
        if (type.equals(double.class) || type.equals(Double.class)) {
            return num.doubleValue();
        }
        if (type.equals(long.class) || type.equals(Long.class)) {
            return num.longValue();
        }
        if (type.equals(int.class) || type.equals(Integer.class)) {
            return num.intValue();
        }
        if (type.equals(float.class) || type.equals(Float.class)) {
            return num.floatValue();
        }
        if (type.equals(short.class) || type.equals(Short.class)) {
            return num.shortValue();
        }
        if (type.equals(byte.class) || type.equals(Byte.class)) {
            return num.byteValue();
        }
        if (type.equals(BigInteger.class) ) {
            return new BigInteger( num.toString() );
        }
        if (type.equals(BigDecimal.class) ) {
            return new BigDecimal( num.toString() );
        }
        return num;
    }

    /**
     * A singleton to do BigDecimal math for ZoomBA
     * Wraps around DefaultBigDecimalMath
     */
    public enum BigMath{
        /**
         * The one single instance we shall use to do it
         */
        INSTANCE
        ;

        /**
         * Converts a Number into a BigDecimal w/o loss of any precision
         * @param n a Number
         * @return a BigDecimal
         */
        public static BigDecimal BD(Number n){
            if ( n instanceof BigDecimal ) return (BigDecimal)n;
            if ( n instanceof Long ||  n instanceof Integer || n instanceof Short || n instanceof Byte ){
                return BigDecimal.valueOf(n.longValue());
            }
            return new BigDecimal( n.toString() );
        }

        /**
         * Computes <a href="https://en.wikipedia.org/wiki/Gamma_function">...</a>
         * @param n a Number
         * @return a BigDecimal value
         */
        public BigDecimal gamma( Number n){
            return DefaultBigDecimalMath.gamma( BD(n) );
        }

        /**
         * Computes <a href="https://en.wikipedia.org/wiki/Bernoulli_number">...</a>
         * @param n a Number
         * @return a BigDecimal value
         */
        public BigDecimal bernoulli( Number n){
            return DefaultBigDecimalMath.bernoulli( n.intValue() );
        }

        /**
         * Computes Power
         * @param n a Number whose power we need to compute
         * @param p a Number who is the power
         * @return a BigDecimal value
         */
        public BigDecimal pow(Number n , Number p){
            ZNumber zn = new ZNumber(n);
            ZNumber zp = new ZNumber(p);
            if ( zp.isPrimitive() ){
                return DefaultBigDecimalMath.pow( zn.bigDecimal(), p.longValue() );
            }
            return DefaultBigDecimalMath.pow( zn.bigDecimal(), zp.bigDecimal() );
        }

        /**
         * Computes square root of a number
         * @param n a Number
         * @return a BigDecimal value
         */
        public BigDecimal sqrt( Number n){
            return DefaultBigDecimalMath.sqrt( BD(n) );
        }

        /**
         * Computes root of a Number
         * @param n a Number whose root we would be computing
         * @param r a Number - the root
         * @return a BigDecimal value
         */
        public BigDecimal root( Number n, Number r){
            return DefaultBigDecimalMath.root( BD(n), BD(r) );
        }

        /**
         * Computes Natural Logarithm  <a href="https://en.wikipedia.org/wiki/Natural_logarithm">...</a>
         * @param n a Number
         * @return a BigDecimal value
         */
        public BigDecimal ln( Number n){
            return DefaultBigDecimalMath.log( BD(n) );
        }

        /**
         * Computes Base 2 Logarithm <a href="https://en.wikipedia.org/wiki/Logarithm">...</a>
         * @param n a Number
         * @return a BigDecimal value
         */
        public BigDecimal log2( Number n){
            return DefaultBigDecimalMath.log2( BD(n));
        }

        /**
         * Computes Base 10 Logarithm <a href="https://en.wikipedia.org/wiki/Logarithm">...</a>
         * @param n a Number
         * @return a BigDecimal value
         */
        public BigDecimal log10( Number n){
            return DefaultBigDecimalMath.log10( BD(n) );
        }

        /**
         * Computes using current math context the value of <a href="https://en.wikipedia.org/wiki/Pi">...</a>
         * @return a BigDecimal value
         */
        public BigDecimal PI(){
            return DefaultBigDecimalMath.pi() ;
        }

        /**
         * Computes using current math context the value of <a href="https://en.wikipedia.org/wiki/E_(mathematical_constant)">...</a>
         * @return a BigDecimal value
         */
        public BigDecimal E(){
            return DefaultBigDecimalMath.e() ;
        }

        /**
         * Computes <a href="https://en.wikipedia.org/wiki/Exponential_function">...</a>
         * @param n a Number
         * @return a BigDecimal value
         */
        public BigDecimal exp( Number n){
            return DefaultBigDecimalMath.exp( BD(n) );
        }

        /**
         * Computes sine <a href="https://en.wikipedia.org/wiki/Trigonometric_functions">...</a>
         * @param n a Number
         * @return a BigDecimal value
         */
        public BigDecimal sin( Number n){
            return DefaultBigDecimalMath.sin( BD(n) );
        }

        /**
         * Computes arc-sine <a href="https://en.wikipedia.org/wiki/Trigonometric_functions">...</a>
         * @param n a Number
         * @return a BigDecimal value
         */
        public BigDecimal asin( Number n){
            return DefaultBigDecimalMath.asin( BD(n) );
        }

        /**
         * Computes sine-hyperbolic <a href="https://en.wikipedia.org/wiki/Hyperbolic_functions">...</a>
         * @param n a Number
         * @return a BigDecimal
         */
        public BigDecimal sinh( Number n){
            return DefaultBigDecimalMath.sinh( BD(n) );
        }

        /**
         * Computes arc-sin-hyperbolic <a href="https://en.wikipedia.org/wiki/Hyperbolic_functions">...</a>
         * @param n a Number
         * @return a BigDecimal
         */
        public BigDecimal asinh( Number n){
            return DefaultBigDecimalMath.asinh( BD(n) );
        }

        /**
         * Computes cosine <a href="https://en.wikipedia.org/wiki/Trigonometric_functions">...</a>
         * @param n a Number
         * @return a BigDecimal value
         */
        public BigDecimal cos( Number n){
            return DefaultBigDecimalMath.cos( BD(n) );
        }

        /**
         * Computes arc-cosine <a href="https://en.wikipedia.org/wiki/Trigonometric_functions">...</a>
         * @param n a Number
         * @return a BigDecimal value
         */
        public BigDecimal acos( Number n){
            return DefaultBigDecimalMath.acos( BD(n) );
        }

        /**
         * Computes cosine-hyperbolic <a href="https://en.wikipedia.org/wiki/Hyperbolic_functions">...</a>
         * @param n a Number
         * @return a BigDecimal
         */
        public BigDecimal cosh( Number n){
            return DefaultBigDecimalMath.cosh( BD(n) );
        }

        /**
         * Computes arc-cosine-hyperbolic <a href="https://en.wikipedia.org/wiki/Hyperbolic_functions">...</a>
         * @param n a Number
         * @return a BigDecimal
         */
        public BigDecimal acosh( Number n){
            return DefaultBigDecimalMath.acosh( BD(n) );
        }

        /**
         * Computes tan <a href="https://en.wikipedia.org/wiki/Trigonometric_functions">...</a>
         * @param n a Number
         * @return a BigDecimal value
         */
        public BigDecimal tan( Number n){
            return DefaultBigDecimalMath.tan( BD(n) );
        }

        /**
         * Computes arc-tan <a href="https://en.wikipedia.org/wiki/Trigonometric_functions">...</a>
         * @param n a Number
         * @return a BigDecimal value
         */
        public BigDecimal atan( Number n){
            return DefaultBigDecimalMath.atan( BD(n) );
        }

        /**
         * Computes tan-hyperbolic <a href="https://en.wikipedia.org/wiki/Hyperbolic_functions">...</a>
         * @param n a Number
         * @return a BigDecimal
         */
        public BigDecimal tanh( Number n){
            return DefaultBigDecimalMath.tanh( BD(n) );
        }

        /**
         * Computes arc-tan-hyperbolic <a href="https://en.wikipedia.org/wiki/Hyperbolic_functions">...</a>
         * @param n a Number
         * @return a BigDecimal
         */
        public BigDecimal atanh( Number n){
            return DefaultBigDecimalMath.atanh( BD(n) );
        }

        /**
         * Computes <a href="https://en.wikipedia.org/wiki/Atan2">...</a>
         * @param y first param
         * @param x second param
         * @return a BigDecimal
         */
        public BigDecimal atan2( Number y, Number x ){
            return DefaultBigDecimalMath.atan2( BD(y), BD(x) );
        }

        /**
         * Computes cot <a href="https://en.wikipedia.org/wiki/Trigonometric_functions">...</a>
         * @param n a Number
         * @return a BigDecimal value
         */
        public BigDecimal cot( Number n){
            return DefaultBigDecimalMath.cot( BD(n) );
        }

        /**
         * Computes arc-cot <a href="https://en.wikipedia.org/wiki/Trigonometric_functions">...</a>
         * @param n a Number
         * @return a BigDecimal value
         */
        public BigDecimal acot( Number n){
            return DefaultBigDecimalMath.acot( BD(n) );
        }

        /**
         * Computes cot-hyperbolic <a href="https://en.wikipedia.org/wiki/Hyperbolic_functions">...</a>
         * @param n a Number
         * @return a BigDecimal
         */
        public BigDecimal coth( Number n){
            return DefaultBigDecimalMath.coth( BD(n) );
        }

        /**
         * Computes arc-cot-hyperbolic <a href="https://en.wikipedia.org/wiki/Hyperbolic_functions">...</a>
         * @param n a Number
         * @return a BigDecimal
         */
        public BigDecimal acoth( Number n){
            return DefaultBigDecimalMath.acoth( BD(n) );
        }
    }
}
