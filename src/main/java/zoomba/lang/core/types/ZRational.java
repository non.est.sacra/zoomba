/*
 * Copyright 2025 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.types;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 A Rational type required to handle ZXRange
 */
public class ZRational extends Number implements Comparable<ZRational> {

    /**
     * A constant Zero for the ZRational
     */
    public final static ZRational ZERO = new ZRational( 0);

    /**
     * A constant One for the ZRational
     */
    public final static ZRational ONE = new ZRational( 1);

    /**
     * Data fields for numerator
     *  r := n/d
     */
    private BigInteger numerator = BigInteger.ZERO;

    /**
     * Data fields for denominator
     *  r := n/d
     */
    private BigInteger denominator = BigInteger.ONE;

    /**
     * Construct a rational with default property of 1 for denominator
     * @param numerator the dividend
     */
    public ZRational(long numerator) {
        this(numerator, 1);
    }

    /**
     * Construct a rational with specified numerator and denominator
     * @param numerator dividend
     * @param denominator  divisor
     */
    public ZRational(long numerator, long denominator) {
        this( BigInteger.valueOf(numerator), BigInteger.valueOf(denominator));
    }

    private void init( BigInteger numerator, BigInteger denominator){
        if  ( numerator.equals( BigInteger.ZERO ) ){
            this.numerator = numerator ;
            this.denominator = BigInteger.ONE ;
        } else if ( denominator.equals( BigInteger.ONE ) ){
            this.numerator = numerator;
            this.denominator = denominator;
        } else {
            BigInteger gcd = numerator.gcd(denominator);
            this.numerator = numerator.divide(gcd);
            this.denominator = denominator.divide(gcd);
        }
        if ( this.denominator.compareTo( BigInteger.ZERO ) < 0 ){
            this.denominator = this.denominator.abs();
            this.numerator = this.numerator.negate();
        }
    }

    /**
     * Construct a rational with specified numerator and denominator
     * @param numerator dividend
     * @param denominator  divisor
     */
    public ZRational(BigInteger numerator, BigInteger denominator) {
        init(numerator, denominator);
    }

    /**
     * Construct a rational with specified numerator and denominator extrapolated from a BigDecimal
     * @param num a fractional number
     */
    public ZRational(BigDecimal num ){
        if ( new ZNumber(num).integerValued() ){
            this.numerator = num.toBigInteger();
            this.denominator = BigInteger.ONE;
        } else {
            num = num.stripTrailingZeros();
            BigDecimal d = BigDecimal.ONE ;
            int s = num.scale() ;
            while ( s > 0 ){
                d = d.multiply( BigDecimal.TEN );
                num = num.multiply( BigDecimal.TEN );
                s --;
            }
            init( num.toBigInteger(), d.toBigInteger());
        }
    }

    /**
     * Gets the denominator
     * @return denominator
     */
    public BigInteger getDivisor(){
        return denominator;
    }

    /**
     * Gets the numerator
     * @return numerator
     */
    public BigInteger getDividend(){
        return numerator;
    }
    /**
     * Add a rational number to this rational return the result
     * @param secondZRational  other ZRational
     * @return the result
     */
    public ZRational add(ZRational secondZRational) {
        BigInteger n = numerator.multiply( secondZRational.denominator )
                .add( denominator.multiply(secondZRational.numerator ) ) ;
        BigInteger d = denominator.multiply(  secondZRational.denominator );
        return new ZRational(n, d);
    }

    /**
     * Subtract a rational number to this rational return the result
     * @param secondZRational  other ZRational
     * @return the result
     */
    public ZRational subtract(ZRational secondZRational) {
        BigInteger n = numerator.multiply( secondZRational.denominator )
                .subtract( denominator.multiply(secondZRational.numerator ) ) ;
        BigInteger d = denominator.multiply(  secondZRational.denominator );
        return new ZRational(n, d);
    }

    /**
     * Multiply a rational number by this rational
     * @param secondZRational  other ZRational
     * @return the result
     */
    public ZRational multiply(ZRational secondZRational) {
        BigInteger n = numerator.multiply(secondZRational.numerator);
        BigInteger d = denominator.multiply( secondZRational.denominator );
        return new ZRational(n, d);
    }

    /**
     * Divide a rational number by this rational
     * @param secondZRational  other ZRational
     * @return the result
     */
    public ZRational divide(ZRational secondZRational) {
        BigInteger n = numerator.multiply( secondZRational.denominator) ;
        BigInteger d = denominator.multiply( secondZRational.numerator ) ;
        return new ZRational(n, d);
    }

    /**
     * Return this as a BigDecimal number
     * @return a BigDecimal number which holds approximately this
     */
    public BigDecimal bigDecimal(){
        return new BigDecimal( numerator ).divide( new BigDecimal(denominator), ZNumber.MATH_CONTEXT );
    }

    @Override
    public String toString() {
        if ( BigInteger.ONE.equals( denominator) )
            return numerator + "";
        else
            return numerator + "/" + denominator;
    }

    @Override // Implement the abstract intValue method in Number
    public int intValue() {
        return (int) doubleValue();
    }

    @Override // Implement the abstract floatValue method in Number
    public float floatValue() {
        return (float) doubleValue();
    }

    @Override // Implement the doubleValue method in Number
    public double doubleValue() {
        return bigDecimal().doubleValue();
    }

    @Override // Implement the abstract longValue method in Number
    public long longValue() {
        return (long) doubleValue();
    }

    @Override // Implement the compareTo method in Comparable
    public int compareTo(ZRational o) {
        BigInteger n = numerator.multiply( o.denominator )
                .subtract( denominator.multiply(o.numerator ) ) ;
        return n.compareTo(BigInteger.ZERO);
    }

    @Override
    public boolean equals(Object o){
        if ( o == this ) return true;
        if ( o == null ) return false;
        if ( o instanceof ZRational) return compareTo((ZRational)o) == 0;
        return false;
    }

    /**
     * Creates a copy of this
     * @return a Clone  of this
     */
    public ZRational copy(){
        return new ZRational( new BigInteger( numerator.toString()) , new BigInteger(denominator.toString() ));
    }

    /**
     * Negation of this
     * @return a negative version of the current number
     */
    public ZRational negate(){
        BigInteger num = numerator.negate();
        return new ZRational( num, denominator );
    }

    static ZRational fromZNumber(ZNumber zn){
        BigInteger sig;
        BigInteger div;
        if ( zn.fractional() ){
            return new ZRational( zn.bigDecimal );
        }else{
            sig = zn.bigInteger();
            div = BigInteger.ONE ;
        }
        return new ZRational( sig, div);
    }
}