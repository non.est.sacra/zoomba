/*
 * Copyright 2025 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.types;

import com.esotericsoftware.yamlbeans.YamlConfig;
import com.esotericsoftware.yamlbeans.YamlException;
import com.esotericsoftware.yamlbeans.YamlReader;
import com.esotericsoftware.yamlbeans.YamlWriter;
import com.github.cliftonlabs.json_simple.Jsoner;
import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.collections.ZList;
import zoomba.lang.core.interpreter.ZInterpret;
import zoomba.lang.core.interpreter.ZMethodInterceptor;
import zoomba.lang.core.interpreter.ZScriptMethod;
import zoomba.lang.core.io.ZWeb;
import zoomba.lang.core.oop.ZObject;
import zoomba.lang.core.operations.*;
import zoomba.lang.parser.ASTPatternLiteral;

import java.io.File;
import java.io.FileReader;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.net.URLClassLoader;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Callable;

import static zoomba.lang.core.operations.Function.FAILURE;
import static zoomba.lang.core.operations.Function.NIL;
import static zoomba.lang.core.operations.Function.SUCCESS;

/**
 * Basic Type Conversion Utility Functions
 */
public final class ZTypes {


    static boolean LOG_ERR = false;

    /**
     * Ensures safe call for a block of code returning a value
     * When failed to do so, returns the default value
     * @param callable block of code
     * @param defaultValue default value to be returned if error encountered
     * @return value from the callable block when failed default value
     * @param <T> type parameter of the callable
     */
    public static <T> T  safeOrDefault(Callable<T> callable, T defaultValue) {
        try {
            return callable.call();
        }catch (Throwable t){
            if ( LOG_ERR ){
                // TODO for now
                t.printStackTrace();
            }
            return defaultValue;
        }
    }

    /**
     * Ensures safe call for a block of code returning a value
     * When failed to do so, raise a runtime exception
     * In case the thrown exception is already runtime exception, throws that
     * Otherwise, creates a new Runtime exception
     * @param callable block of code
     * @return value from the callable block when failed default value
     * @param <T> type parameter of the callable
     */
    public static <T> T orRaise(Callable<T> callable) {
        try {
            return callable.call();
        }catch (Throwable t){
            throw Function.runTimeException(t);
        }
    }

    /**
     * Ensures safe call for a block of code returning a value
     * When failed to do so, returns null
     * @param callable block of code
     * @return value from the callable block when failed null
     * @param <T> type parameter of the callable
     */
    public static <T> T safeOrNull( Callable<T> callable) {
        return safeOrDefault(callable, null);
    }


    /**
     * Ensures safe call for a block of code
     * When failed to do so, ignores the error
     * @param callable block of code
     * @param <T> type parameter of the callable
     */
    public static <T> void safeIgnore( Callable<T> callable) {
        safeOrDefault(callable, null);
    }
    
    /**
     * Extended String Object
     * @param <T> type of the cloned object
     */
    public interface StringX<T> {

        /**
         * Format into String based on arguments
         *
         * @param args the arguments
         * @return a String representation of the object
         */
        String string(Object... args);

        /**
         * Generates a Deep copy of the Object
         * @return A Deep copy of the object
         */
        T deepCopy();
    }

    private static boolean isa(Object target, String literal) {
        String type = literal.toLowerCase();
        switch (type) {
            case "bool":
            case "boolean":
                return target instanceof Boolean;
            case "char":
                return target instanceof Character;
            case "int":
            case "integer":
                return (target instanceof Integer ||
                        target instanceof Short ||
                        target instanceof Long ||
                        target instanceof Byte ||
                        target instanceof BigInteger ||
                        (target instanceof ZNumber && !((ZNumber) target).fractional())
                );

            case "float":
                return (target instanceof Float ||
                        target instanceof Double ||
                        target instanceof BigDecimal ||
                        (target instanceof ZNumber && ((ZNumber) target).fractional())
                );

            case "enum":
                return target.getClass().isEnum();
            case "arr":
            case "array":
                return target.getClass().isArray();
            case "list":
                return target instanceof List;
            case "obj":
                return target instanceof ZObject;

            case "map":
            case "dict":
                return target instanceof Map;

            case "set":
                return target instanceof Set;

            case "range":
                return target instanceof ZRange;

            case "func":
            case "method":
                return target instanceof Function || target instanceof ZScriptMethod;

            case "dt":
            case "time":
            case "date":
                return (target instanceof Date ||
                        target instanceof LocalDateTime ||
                        target instanceof Duration ||
                        target instanceof Instant ||
                        target instanceof ZDate
                );

            case "str":
            case "string":
                return target instanceof CharSequence;
            case "err":
                return target instanceof Throwable;
            case "type":
                return target instanceof Class<?> ;
            default:
                if( target instanceof Class ) return "java.lang.Class".equals(literal);
                return target.getClass().getName().equals(literal);
        }
    }

    /**
     * Is object a is of type b
     *
     * @param a object left
     * @param b object right
     *          if b is a string, it would be specially treated, the types are then :
     *          int, float, bool, arr, enum, list, set, map/dict , dt/time/date, range, obj
     * @param i Interpreter
     * @return true if a isa b, else false
     */
    public static boolean isa(Object a, Object b, ZInterpret i) {
        if (a == null) return (b == null);
        if (b == null) return false;
        Class ca;
        if (a instanceof Class) {
            ca = (Class) a;
        } else {
            ca = a.getClass();
        }
        if (b instanceof Class) {
            return ((Class) b).isAssignableFrom(ca);
        } else {
            if (b instanceof CharSequence) {
                return isa(a, b.toString());
            }
            if (b instanceof ASTPatternLiteral) {
                ASTPatternLiteral pl = (ASTPatternLiteral) b;
                return pl.matches(ca.getName(), i);
            }
        }
        Class cb = b.getClass();
        return cb.isAssignableFrom(ca);
    }

    /**
     * Converts arguments to boolean
     *
     * @param args when single arg try to cast to boolean, failing returns null
     *             with 2 args, if fails, args[1] passed as the result
     *             with args[1] a pair, use that as matching [true,false]
     * @return Boolean or null
     */
    public static Boolean bool(Object... args) {

        if (args.length == 0) return null;
        if (args[0] == null) {
            if (args.length > 1) {
                return bool(args[1]);
            }
            return null;
        }

        if (args[0] instanceof Boolean) {
            return (Boolean) args[0];
        }
        Boolean fallBack = null;
        if (args.length > 1) {
            if (args[1] != null && args[1].getClass().isArray()) {
                args[1] = new ZArray(args[1]);
            }
            if (args[1] instanceof List) {
                List l = (List) args[1];
                if (l.size() < 2) return null;
                Object f = l.get(0);
                Object t = l.get(1);
                if (args[0].equals(t)) return true;
                if (args[0].equals(f)) return false;
                return null;
            }
            fallBack = bool(args[1]);
        }
        if (args[0] instanceof CharSequence) {
            String s = String.valueOf(args[0]);
            s = s.toLowerCase().trim();
            if ("true".equals(s)) return true;
            if ("false".equals(s)) return false;
            return fallBack;
        }
        return fallBack;
    }

    /**
     * Converts arguments to Character
     *
     * @param args when single arg try to cast to Character, failing returns null
     *             with 2 args, if fails, args[1] passed as the result
     *
     * @return Character or null
     */
    public static Character character(Object...args){
        try {
            if ( args[0] instanceof Character ) return (char)args[0];
            if ( args[0] instanceof CharSequence ) return ((CharSequence)args[0]).charAt(0);
            if ( args[0] instanceof Number ) return (char) ((Number) args[0]).intValue();
        }catch (Throwable ignore){}
        if ( args.length > 1 ){
            return character(args[1]);
        }
        return null;
    }

    /**
     * Makes a string out of an iterable
     * string( [1,2,3] , ":" ) : 1:2:3
     *
     * @param c   the iterable
     * @param sep the separator
     * @return a string
     */
    public static String string(Iterable c, String sep) {
        Iterator i = c.iterator();
        if (!i.hasNext()) return "";
        StringBuilder buf = new StringBuilder();
        Object o = i.next();
        buf.append(o);
        while (i.hasNext()) {
            o = i.next();
            buf.append(sep).append(o);
        }
        return buf.toString();
    }

    /**
     * Makes a string out of an iterable after mapping each item by the mapping function
     * string(  [1,2,3] , ":" ) : 1:2:3
     *
     * @param map the mapping function
     * @param col the iterable
     * @param sep the separator
     * @return a string
     */
    public static String string(Function map, Object col, String sep) {
        if (col == null) return "";
        if ( col instanceof Map ){
            col = ((Map) col).entrySet();
        }
        Iterable c;
        if (col instanceof Iterator) {
            c = ZCollection.iterable((Iterator) col);
        } else if (col instanceof Iterable) {
            c = (Iterable) col;
        } else {
            if (col instanceof CharSequence) {
                col = col.toString().toCharArray();
            }
            c = new ZArray(col);
        }

        Iterator i = c.iterator();
        if (!i.hasNext()) return "";
        // now rest
        StringBuilder buf = new StringBuilder();
        int index = 0;
        Object o = i.next();
        Function.MonadicContainer result = map.execute(index, o, c, NIL);
        if (!result.isNil()) {
            buf.append(result.value());
        }
        if (result instanceof ZException.Break) {
            return buf.toString();
        }
        while (i.hasNext()) {
            index++;
            o = i.next();
            result = map.execute(index, o, c, NIL);
            if (!result.isNil()) {
                buf.append(sep).append(result.value());
            }
            if (result instanceof ZException.Break) {
                break;
            }
        }
        return buf.toString();
    }

    /**
     * Converts to string arguments
     *
     * @param args arguments
     * @return a string
     */
    public static String string(Object... args) {
        if (args.length == 0) return "";
        if (args[0] == null) return "null";
        if (args[0] instanceof ZString) {
            return args[0].toString();
        }
        if (args[0] instanceof Date) {
            ZDate dt = new ZDate((Date) args[0]);
            args = shiftArgsLeft(args);
            return dt.string(args);
        }
        if (args[0] instanceof Number) {
            if (Arithmetic.isInfinity(args[0])) return String.valueOf(args[0]);
            Number n = (Number) args[0];
            args = shiftArgsLeft(args);
            if (n instanceof ZNumber) {
                return ((ZNumber) n).string(args);
            }
            ZNumber zn = new ZNumber(n);
            return zn.string(args);
        }
        Iterable c = null;
        if (args[0].getClass().isArray()) {
            c = new ZArray(args[0]);
        } else if (args[0] instanceof Iterable) {
            c = (Iterable) args[0];
        }
        String sep = ",";
        if (args.length > 1) {
            sep = String.valueOf(args[1]);
            if (args.length > 2 && args[2] instanceof Function) {
                return string((Function) args[2], c, sep);
            }
        }
        if (args[0] instanceof StringX) {
            StringX sx = (StringX) args[0];
            args = shiftArgsLeft(args);
            return sx.string(args);
        }

        if (args[0] instanceof String) {
            String fmt = (String) args[0];
            final boolean possibleFmtFunc = (args.length > 1 && fmt.contains("%"));
            if (possibleFmtFunc) {
                // confirm
                args = shiftArgsLeft(args);
                try {
                    return String.format(fmt, args);
                }catch (Exception e){
                    if ( e instanceof java.util.IllegalFormatConversionException ){
                        // here, we should fix the formats, if need be
                        if ( e.getMessage().contains("d != ") ){
                            // here, change format...
                            fmt = fmt.replace("%d", "%s");
                        }
                        boolean toFloatCue = e.getMessage().contains("f != ");
                        if ( toFloatCue ) { // floating point problem...
                            // here, change format...
                            fmt = fmt.replace("%f", "%s");
                            // try again
                        }
                        return String.format(fmt, args);
                    }
                }
            }
        }
        // rest fall back
        if (c != null) {
            if (args.length > 2) {
                System.err.println("Fallback impl. Use jstr() instead to cast to json string");
                return jsonString(c);
            }
            return string(c, sep);
        }
        if (args.length > 1) {
            System.err.println("Fallback impl. Use jstr() instead to cast to json string");
            return jsonString(args[0]);
        }
        return String.valueOf(args[0]);
    }

    private static String hex(char ch) {
        return Integer.toHexString(ch).toUpperCase(Locale.ENGLISH);
    }

    /**
     * https://commons.apache.org/proper/commons-lang/javadocs/api-2.6/src-html/org/apache/commons/lang/StringEscapeUtils.html#line.62
     *
     * @param str the input string
     * @return the escaped string
     */
    private static String escapeJavaStyleString(String str) {
        final boolean escapeSingleQuote = false;
        final boolean escapeForwardSlash = false;

        StringWriter out = new StringWriter();
        int sz;
        sz = str.length();
        for (int i = 0; i < sz; i++) {
            char ch = str.charAt(i);

            // handle unicode
            if (ch > 0xfff) {
                out.write("\\u" + hex(ch));
            } else if (ch > 0xff) {
                out.write("\\u0" + hex(ch));
            } else if (ch > 0x7f) {
                out.write("\\u00" + hex(ch));
            } else if (ch < 32) {
                switch (ch) {
                    case '\b':
                        out.write('\\');
                        out.write('b');
                        break;
                    case '\n':
                        out.write('\\');
                        out.write('n');
                        break;
                    case '\t':
                        out.write('\\');
                        out.write('t');
                        break;
                    case '\f':
                        out.write('\\');
                        out.write('f');
                        break;
                    case '\r':
                        out.write('\\');
                        out.write('r');
                        break;
                    default:
                        if (ch > 0xf) {
                            out.write("\\u00" + hex(ch));
                        } else {
                            out.write("\\u000" + hex(ch));
                        }
                        break;
                }
            } else {
                switch (ch) {
                    case '\'':
                        if (escapeSingleQuote) {
                            out.write('\\');
                        }
                        out.write('\'');
                        break;
                    case '"':
                        out.write('\\');
                        out.write('"');
                        break;
                    case '\\':
                        out.write('\\');
                        out.write('\\');
                        break;
                    case '/':
                        if (escapeForwardSlash) {
                            out.write('\\');
                        }
                        out.write('/');
                        break;
                    default:
                        out.write(ch);
                        break;
                }
            }
        }
        return out.toString();
    }

    /**
     * Deep equals two objects
     *
     * @param o1 object 1
     * @param o2 object 2
     * @return true if they are deep equal, false if they are not
     */
    public static boolean equals(Object o1, Object o2) {
        if (o1 == null) {
            return  (o2 == null);
        }
        if (o2 == null) return false;
        try {
            if (o1 instanceof Comparable) {
                // java is retarded. Even if o1 is not null, if o2 is null it throws nullpointer. How pathetic is that?
                // mathematically, x.compareTo(null) must return 1, anything is greater than null.
                return (((Comparable) o1).compareTo(o2) == 0);
            }
        } catch (Throwable e) {
            // yep, can not compare...
        }
        return Objects.equals(o1, o2);
    }

    /**
     * Shift arguments left by 1
     *
     * @param args the arguments
     * @return shifter args - an array
     */
    public static Object[] shiftArgsLeft(Object[] args) {
        if (args.length == 0) return args;
        Object[] newArgs = new Object[args.length - 1];
        for (int i = 0; i < newArgs.length; i++) {
            newArgs[i] = args[i + 1];
        }
        return newArgs;
    }

    /**
     * Creates ZRange type from arguments
     *
     * @param args the arguments
     * @return a ZRange instance
     */
    public static ZRange range(Object... args) {
        if ( args.length == 0 ) return ZRange.EMPTY_RANGE ;
        if (args[0] instanceof Number) {
            final boolean xr = ZXRange.hasXtype(args);
            if (xr) {
                // extended range
                return ZXRange.zxRange(args);
            }

            long b = ((Number) args[0]).longValue();
            long e = Long.valueOf(args[1].toString());

            if (args.length == 3) {
                long s = Long.valueOf(args[2].toString());
                return new ZRange.NumRange(b, e, s);
            }
            return new ZRange.NumRange(b, e);
        }
        if (args[0] instanceof String) {
            char b = args[0].toString().charAt(0);
            char e = args[1].toString().charAt(0);

            if (args.length == 3) {
                int s = Integer.valueOf(args[2].toString());
                return new ZRange.CharRange(b, e, s);
            }
            return new ZRange.CharRange(b, e);
        }
        if (args[0] instanceof ZDate) {
            ZDate s = (ZDate) args[0];
            ZDate e = (ZDate) args[1];
            if (args.length > 2) {
                String d = String.valueOf(args[2]);
                return new ZRange.DateRange(s, e, d);
            }
            return new ZRange.DateRange(s, e);
        }
        return ZRange.EMPTY_RANGE;
    }

    private final static String DEFAULT_EXCEPTION_MESSAGE = "Vogons destroyed Earth!";

    /**
     * Raise errors from arguments
     *
     * @param args arguments
     */
    public static void raise(Object... args) {
        if (args.length == 0) {
            throw new RuntimeException(DEFAULT_EXCEPTION_MESSAGE);
        }
        String exceptClassName = String.valueOf(args[0]);
        Class exceptClass;
        try {
            exceptClass = Class.forName(exceptClassName);
        } catch (Exception e) {
            // treat like message
            throw new ZAssertionException(exceptClassName);
        }
        if (!Throwable.class.isAssignableFrom(exceptClass)) {
            // the case of a custom exception not derived from Throwable?
            throw new ZAssertionException(exceptClass.getName());
        }
        Object[] params = ZTypes.shiftArgsLeft(args);
        Throwable t;
        try {
            t = (Throwable) ZJVMAccess.construct(exceptClass, params);
        } catch (Exception e) {
            throw new ZAssertionException("Error creating Exception class : "  +  exceptClass + " : " + e.getMessage() );
        }
        throw new ZAssertionException(t);
    }

    /**
     * When called, throws Error to come out from execution
     *
     * @param args arguments
     */
    public static void bye(Object... args) {
        if (args.length == 0) {
            throw new ZException.ZTerminateException(DEFAULT_EXCEPTION_MESSAGE);
        }
        System.err.printf("%s\n", args[0]);
        throw new ZException.ZTerminateException(String.valueOf(args[0]));
    }

    /**
     * A special NIL enum
     * Never use this out of context
     * Always use Function.NIL
     * As part of <a href="https://gitlab.com/non.est.sacra/zoomba/-/issues/114">...</a>
     */
    public enum NilEnum {
        /**
         * Only value in the enum depicting non-existence of any value
         */
        NIL(new Object() {
            @Override
            public String toString() {
                return "nil";
            }
        });

        /**
         * Underlying Object Value, Depicts absence of Object in ZoomBA
         */
        public final Object value;

        NilEnum(Object o){
            value = o;
        }
    }

    /**
     * Cast arguments into enum
     *
     * @param args arguments
     * @return an appropriate enum
     */
    public static Object enumCast(Object... args) {
        if (args.length == 0) return NilEnum.NIL;
        Object o = args[0];
        if (o instanceof String) {
            try {
                o = Class.forName((String) o);
            } catch (Exception e) {
                // may be the whole name is there?
                throw new IllegalArgumentException(e);
            }
        }
        if (o instanceof Class) {
            Class ce = (Class) o;
            if (!ce.isEnum()) {
                return NilEnum.NIL;
            }
            Object[] ca = ce.getEnumConstants();
            HashMap m = new HashMap();
            for (int i = 0; i < ca.length; i++) {
                m.put(ca[i].toString(), ca[i]);
                m.put(i, ca[i]);
            }
            Map em = Collections.unmodifiableMap(m);
            if (args.length > 1) {
                return em.get(args[1]);
            }
            return em;
        }
        return NilEnum.NIL;
    }

    /**
     * Reads Yaml file
     *
     * @param args 0 the string or file path
     *             1 true, if you want to read the file
     * @return the POJO object after parsing Yaml
     */
    public static Object yaml(Object... args) {
        if (args.length == 0) return null;
        boolean isFile = false;
        if (args.length > 1) {
            isFile = bool(args[1], false);
        }
        try {
            YamlReader reader;
            YamlConfig yamlConfig = new YamlConfig();
            // so that we can automatically figure out what sort of numbers we are having
            // also support arbitrary precision numbers
            yamlConfig.readConfig.setGuessNumberTypes(true);
            String data = String.valueOf(args[0]);
            if (isFile) {
                reader = new YamlReader(new FileReader(data), yamlConfig);
            } else {
                reader = new YamlReader(data, yamlConfig);
            }
            Object o = reader.read();
            List<Object> list = new ArrayList();
            list.add(o);
            Object next = null;
            while ( (next = reader.read())!= null ){
                list.add(next);
            }
            if ( list.size() == 1 ) return o;
            return list;

        } catch (Exception e) {
            if (e instanceof YamlException) {
                throw new RuntimeException(e.getCause());
            }
            throw new RuntimeException(e);
        }
    }

    /**
     * Serialize an object to Yaml
     * @param args arguments args[0] is the object
     * @return a Yaml Serialization of the object
     */
    public static String yamlString(Object... args) {
        if (args.length <= 0) return "";
        if (args.length > 1) return yamlString(new ZArray(args));
        if ( args[0] instanceof ZXml ){
            // special casing, for xml to yaml conversion
            args[0] = ((ZXml)args[0]).json();
            // carry on...
        }

        StringWriter writer = new StringWriter();
        YamlWriter yamlWriter = new YamlWriter(writer);
        yamlWriter.getConfig().writeConfig.setIndentSize(2);
        yamlWriter.getConfig().writeConfig.setAutoAnchor(false);
        yamlWriter.getConfig().writeConfig.setWriteDefaultValues(true);
        yamlWriter.getConfig().writeConfig.setWriteRootTags(false);
        yamlWriter.getConfig().writeConfig.setWriteClassname(YamlConfig.WriteClassName.NEVER);
        try {
            yamlWriter.write(args[0]);
            writer.flush();
            return writer.getBuffer().toString();
        } catch (Throwable th) {
            throw new RuntimeException(th);
        }
    }

    /**
     * From String or Path convert into JSON which are either List or Map
     *
     * @param args arguments
     * @return List/Map or null
     */
    public static Object json(Object... args) {
        try {
            if (args.length == 0) return Collections.emptyMap();
            if ( args[0] == null ) return null;
            if ( args[0] instanceof ZXml ){
                return ((ZXml)args[0]).json();
            }
            if (args.length > 1) {
                boolean file = bool(args[1], false);
                if (file) {
                    try {
                        Object obj = Jsoner.deserialize(new FileReader(args[0].toString()));
                        return obj;
                    } catch (Throwable t) {

                    }

                    Object o = ZMethodInterceptor.Default.read(args[0]);
                    String text;
                    if (o instanceof ZWeb.ZWebCom) {
                        text = ((ZWeb.ZWebCom) o).body();
                    } else {
                        text = String.valueOf(o);
                    }
                    args[0] = text;
                }
            }

            if (args[0] instanceof CharSequence) {
                String f = args[0].toString();
                Object obj = Jsoner.deserialize(f);
                // for debug ...
                return obj;
            }
            if ( args[0] instanceof Map || args[0] instanceof List ) return args[0];
            if ( args[0] instanceof Collection ) return new ArrayList((Collection) args[0]);
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
        System.err.printf("Type is not valid %s - json() returning null! %n", args[0].getClass());
        return null;
    }

    /**
     * Converts an iterable to json string
     *
     * @param i iterable
     * @return the json string form
     */
    public static String jsonString(Iterable i) {
        StringBuilder buf = new StringBuilder();
        Iterator iterator = i.iterator();
        if (!iterator.hasNext()) return "[]";
        buf.append("[ ");
        Object o = iterator.next();
        buf.append(jsonString(o));
        while (iterator.hasNext()) {
            o = iterator.next();
            buf.append(", ").append(jsonString(o));
        }
        buf.append(" ]");
        return buf.toString();
    }

    /**
     * Converts a Map to json string
     *
     * @param m the map
     * @return the json string form
     */
    public static String jsonString(Map m) {
        if (m.isEmpty()) return "{}";

        StringBuilder buf = new StringBuilder();
        buf.append("{");
        Iterator<Map.Entry> i = m.entrySet().iterator();
        Map.Entry e = i.next();
        Object keyVal = e.getKey();
        String key = jsonString(keyVal);
        if ( key.charAt(0) != '"' ){
            key = "\"" + escapeJavaStyleString(key) + "\"" ;
        }
        String value = jsonString(e.getValue());
        buf.append(key).append(" : ").append(value);
        while (i.hasNext()) {
            e = i.next();
            keyVal = e.getKey();
            key = jsonString(keyVal);
            if ( key.charAt(0) != '"' ){
                key = "\"" + escapeJavaStyleString(key) + "\"" ;
            }
            value = jsonString(e.getValue());
            buf.append(", ").append(key).append(" : ").append(value);
        }
        buf.append("}");
        return buf.toString();
    }

    /**
     * Formatted Prints into JSON String
     * Can be used to compress the json repr by removing spaces
     * @param args
     *    args[0] object to be made into a JSON String
     *    args[1] when false use native stringification, if true, use Jsoner - default false
     *    args[2] the string to be used as space - default TAB "\t"
     * @return pretty printed JSON string, if args[2] is specified as empty, compress the json string
     */
    public static String jsonFormatted(Object... args){
        Object o = args[0];
        boolean useJSONer =  args.length > 1 &&  bool(args[1],false );
        final String indentSpace = args.length > 2 ? String.valueOf(args[2] ) : "\t" ;
        final String jsonString;
        if ( useJSONer ){
            jsonString = Jsoner.serialize(o);
        } else {
            jsonString = jsonString(o);
        }
        return Jsoner.prettyPrint(jsonString, indentSpace);
    }

    /**
     * Converts an Object to json string
     *
     * @param a the object
     *
     * @return the json string form
     */
    public static String jsonString(Object a) {
        if (a == null) return "null";
        if ( a instanceof ZXml ){ // special casing here ...
            return ((ZXml) a).jsonString();
        }
        if (a instanceof CharSequence) {
            return "\"" + escapeJavaStyleString(a.toString()) + "\"";
        }
        if (a instanceof Number) {
            ZNumber zn;
            if (a instanceof ZNumber) {
                zn = (ZNumber) a;
            } else {
                zn = new ZNumber((Number) a);
            }
            return String.valueOf(zn.actual());
        }
        if (a instanceof Boolean) {
            return String.valueOf(a);
        }
        if (a instanceof Date || a instanceof Instant || a instanceof ZDate) {
            return "\"" + String.valueOf(a) + "\"";
        }
        if (a.getClass().isArray()) {
            a = new ZArray(a);
        }

        // ZObject is also iterable, so there are problems...
        if (a instanceof Map) {
            return jsonString((Map) a);
        }

        if (a instanceof Iterable) {
            return jsonString((Iterable) a);
        }

        //  I do not know how to handle this, so...
        return String.format(" { \"%s\" : \"%s\" } ", a.getClass().getName(), a);
    }

    /**
     * Gets a Proxy Object
     * @param anon with this Proxy Interceptor
     * @param args arguments
     *             args0 the source around which to create proxy
     *             args1 boolean whether you want just an interceptor or interloper
     * @return a Proxy
     */
    public static Object jvmProxy(Function anon, Object... args) {
        if ( args.length == 0 ) return null;
        boolean interceptOnly = false;
        if ( anon == null ){
            if ( args.length < 2 ) return null;
            if ( args.length > 2 ){
                interceptOnly = bool(args[1], interceptOnly);
                anon = (Function) args[2] ;
            } else{
                anon = (Function) args[1] ;
            }
            return ZJVMFunctionalInterface.proxy(args[0], interceptOnly, anon);
        }
        if ( args.length > 1 ){
            interceptOnly = bool(args[1], interceptOnly);
        }
        return ZJVMFunctionalInterface.proxy(args[0], interceptOnly, anon);
    }

    /**
     * Gets a Queue
     * @param args arguments
     *             With no args returns a ArrayDeque
     *             With args[0] as non int or negative int returns a ZList
     *             With args[0] is an int - it is the fixed capacity of the queue
     *             args[1] boolean blocking nature of the queue
     *                false : CircularBuffer queue - no blocking
     *                true :  ArrayBlockingQueue - blocking write on full and blocking read on empty
     *             args[2] : set fairness for the queue
     * @return a Queue
     */
    public static Queue<?> queue(Object[] args){
        if ( args.length == 0 ) return new ArrayDeque<>();
        Number capacity = ZNumber.integer(args[0]);
        if ( capacity == null || capacity.intValue() <= 0 ){
            return new ZList();
        }
        boolean blocking = args.length > 1 && bool(args[1], false );
        if ( blocking ){
            boolean fair = args.length > 2 && bool(args[2], false );
            return new ArrayBlockingQueue<>(capacity.intValue(), fair);
        }
        return new CircularBuffer<>(capacity.intValue());
    }

    /**
     * Gets a Priority Queue
     * @param anon with this comparator
     * @param args arguments
     * @return a PriorityQueue
     */
    public static PriorityQueue pqueue(Function anon, Object... args) {
        if (anon != null) {
            PriorityQueue<?> pq = new PriorityQueue<>(anon);
            anon.withIterationContext(pq);
            return pq;
        }
        if (args.length == 0) {
            return new PriorityQueue();
        }
        if (args[0] instanceof Comparator) {
            return new PriorityQueue((Comparator) args[0]);
        }
        return new PriorityQueue();
    }

    private static boolean jarLoad(File file) {
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        if (classLoader instanceof URLClassLoader) {
            try {
                URL url = file.toURI().toURL();
                URLClassLoader urlClassLoader = (URLClassLoader) ClassLoader.getSystemClassLoader();
                Method method = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
                method.setAccessible(true);
                method.invoke(urlClassLoader, url);
                return true;
            } catch (Exception ignore) {
                return false;
            }
        } else {
            try {

                Method method = classLoader.getClass().
                        getDeclaredMethod("appendToClassPathForInstrumentation", String.class);
                method.setAccessible(true);
                method.invoke(classLoader, file.getAbsolutePath());
                return true;

            } catch (Exception ignore) {
                System.err.printf("Failure to load Jar : %s %n", file.getAbsolutePath() );
                System.err.println("Possible fix : run java with: java --add-opens java.base/jdk.internal.loader=ALL-UNNAMED");
                return false;
            }
        }
    }

    /**
     * Loads a jar
     *
     * @param args arguments
     * @return a MonadicContainer of SUCCESS or FAILURE
     */
    public static Function.MonadicContainer loadJar(Object... args) {
        try {
            File file = new File(String.valueOf(args[0]));
            if (!file.exists()) {
                System.err.printf("Load Jar failure: File : %s : PATH DOES NOT EXIST!\n", file.getAbsolutePath());
                return FAILURE;
            }
            if (file.isDirectory()) {
                // load all the jars found under it...
                File[] children = file.listFiles((dir, name) -> name.endsWith(".jar"));
                if (children == null) return FAILURE;
                boolean multiLoad = true;
                for (File f : children) {
                    multiLoad &= jarLoad(f.getCanonicalFile());
                }
                return multiLoad ? SUCCESS : FAILURE;
            }
            return jarLoad(file) ? SUCCESS : FAILURE;
        } catch (Exception ignore) {

        }
        System.err.println("Load Jar : UnSuccessful !!!");
        return FAILURE;
    }
}
