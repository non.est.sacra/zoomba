/*
 * Copyright 2025 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.types;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import static zoomba.lang.core.operations.Function.NIL;
import static zoomba.lang.core.types.ZRational.fromZNumber;

/**
 * Arbitrary large range in ZoomBA
 * With Large Precision, Arbitrary Precision support
 */
public class ZXRange extends ZRange{

    /**
     * Checks if the range should be extended or not
     * @param args Object array of args
     * @return true if any arg is extended type
     */
    static boolean hasXtype(Object[] args){
        for (Object o : args) {
            if (o instanceof Double || o instanceof Float
                    || o instanceof BigInteger || o instanceof BigDecimal
            ) return true;
        }
        return false;
    }

    final ZRational b;

    final ZRational e;

    final ZRational s;

    ZRational c;

    final boolean fractional;

    final boolean isEmpty;

    @Override
    public void reset() {
        c = b.copy();
    }

    /**
     * Creates a ZXRange from ZNumbers
     * @param args begin, end, [step]
     * @return a ZXRange
     */
    public static ZXRange zxRange(Object...args){
        ZRational b = fromZNumber( new ZNumber(args[0]) );
        ZRational e = fromZNumber( new ZNumber((args[1])));
        if (args.length == 3) {
            ZRational s = fromZNumber( new ZNumber(args[2]));
            return new ZXRange(b, e, s);
        }
        return new ZXRange(b, e);
    }

    private ZXRange(ZRational b, ZRational e, ZRational s) {
        super();
        this.b = b ;
        this.e = e ;
        this.s = s ;
        decreasing = ( b.compareTo( e) > 0 );
        fractional = !b.getDivisor().equals( BigInteger.ONE ) || !s.getDivisor().equals( BigInteger.ONE ) ;
        reset();
        computeSize();
        isEmpty = zs.longValue() == 0;
    }

    private ZXRange(ZRational b, ZRational e) {
        super();
        this.b = b ;
        this.e = e ;
        decreasing = ( b.compareTo( e) > 0 );
        fractional = !b.getDivisor().equals( BigInteger.ONE );
        if ( decreasing ) {
            s = ZRational.ONE.negate();
        }else{
            s = ZRational.ONE;
        }
        reset();
        computeSize();
        isEmpty = zs.longValue() == 0;
    }

    @Override
    public ZRange yielding() {
        return new ZXRange( b, e, s );
    }

    @Override
    public Object object(long l) {
        return NIL;
    }

    @Override
    public String toString() {
        return String.format("[%s:%s:%s]" , b,e,s);
    }

    @Override
    public void toEnd() {
        // jumps to the start,
        ZRational sz = new ZRational((BigInteger)zs , BigInteger.ONE);
        c = sz.multiply(s).add(b);
    }

    @Override
    public boolean hasNext() {
        if ( isEmpty ) return false;
        if ( decreasing ) {
            return c.compareTo( e ) > 0 ;
        }
        return c.compareTo(e) < 0 ;
    }

    private Number appropriate( ZRational next){
        if ( fractional ){
            return ZNumber.narrowBigDecimal( next.bigDecimal() );
        }
        return next.getDividend() ;
    }

    @Override
    public Object next() {
        ZRational next = c.copy();
        c = c.add(s);
        return appropriate(next);
    }

    @Override
    public Object get(Number n) {
        // n * s + b
        BigInteger lInx = new BigInteger(n.toString());
        ZRational l = new ZRational( lInx, BigInteger.ONE);
        if ( lInx.compareTo( BigInteger.ZERO) < 0 ){ // support negative indexers
            lInx = ((BigInteger)zs).add( lInx );
            // this is still a problem
            if ( lInx.compareTo( BigInteger.ZERO) < 0  ) return NIL;
            l = new ZRational( lInx, BigInteger.ONE);
        }

        l = l.multiply(s);
        l = l.add( b );

        if ( decreasing ){
            if ( l.compareTo( e ) >= 0 ) return appropriate(l);
            return NIL;
        }
        if ( l.compareTo( e ) <= 0 ) return appropriate(l);
        return NIL;
    }

    @Override
    public boolean hasPrevious() {
        if ( isEmpty ) return false;
        if ( decreasing ) {
            return c.compareTo( b ) <= 0 ;
        }
        return c.compareTo(b) > 0 ;
    }

    @Override
    public Object previous() {
        if ( decreasing ){
            ZRational prev = c ;
            c = c.subtract(s);
            return appropriate(prev) ;
        }
        c = c.subtract(s);
        return appropriate(c) ;
    }

    private synchronized void computeSize(){
        if ( (b.equals(e)) ||
                ( s.compareTo(ZRational.ZERO) >= 0 && b.compareTo(e) > 0) ||
                ( s.compareTo(ZRational.ZERO) <= 0 && b.compareTo(e) < 0 ) ) {
            zs = BigInteger.ZERO; // ensure always large integer in size
            return;
        }

        ZRational r = e.subtract(b);
        ZRational sz = r.divide(s);
        BigInteger li = sz.getDividend().divide( sz.getDivisor() );
        if ( !sz.getDivisor().equals( BigInteger.ONE) ){
            li = li.add(BigInteger.ONE);
        }
        zs = li;
    }

    @Override
    public synchronized Number zSize(){
        return zs;
    }

    @Override
    public ZRange reverse() {
        return new ZXRange( e.subtract(s)  , b.subtract(s)  , s.negate() );
    }

    @Override
    public ZRange inverse() {
        return new ZXRange(e, b, s.negate());
    }

    @Override
    public List<?> asList(){
        return new ZRangeList(this){
            @Override
            public boolean isIn(ZNumber n){
                final ZRational r = fromZNumber(n);
                if ( decreasing ){
                    int i = r.compareTo( e );
                    if ( i <= 0 ) return false; // item <= to end
                    i = r.compareTo(b);
                    if ( i > 0 ) return false; // item > begin
                    if ( i == 0 ) return true ; // item same as begin
                }else{
                    int i = r.compareTo( b );
                    if ( i <= -1 ) return false; // item smaller then begin
                    if ( i == 0 ) return true; // item same as begin
                    i = r.compareTo(e);
                    if ( i >= 0 ) return false; // item >= end
                }
                // now we might have one item that is inside the range
                final ZRational x = r.subtract(b).divide(s);
                // if multiplier x is exactly an integer, then the item is in the range
                return BigInteger.ONE.equals( x.getDivisor() );
            }
        };
    }
}
