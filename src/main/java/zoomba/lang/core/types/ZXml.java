/*
 * Copyright 2025 zoomba-lang.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package zoomba.lang.core.types;

import org.w3c.dom.*;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import zoomba.lang.Main;
import zoomba.lang.core.interpreter.ZMethodInterceptor;
import zoomba.lang.core.io.ZWeb;
import zoomba.lang.core.operations.Arithmetic;
import zoomba.lang.core.operations.Function;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The XML Based Infrastructure
 */
public final class ZXml {

    /**
     * Applying XSL transform to xml
     * @param xmlDoc ml doc
     * @param xslFile path of file where xsl is stored, or the xsl string itself
     * @param outFile file path of the result, if null, result is the return Object
     * @return boolean or string based on whether outFile is null or not
     */
    public static Object xsl(Document xmlDoc, String xslFile, String outFile) {
        return ZTypes.orRaise(() -> {

            TransformerFactory factory = TransformerFactory.newInstance();
            File tFile = new File(xslFile);
            Source xslt;
            if (tFile.exists()) {
                xslt = new StreamSource(tFile);
            } else {
                xslt = new StreamSource(new ByteArrayInputStream(xslFile.getBytes(StandardCharsets.UTF_8.name())));
            }
            Transformer transformer = factory.newTransformer(xslt);
            Source text = new DOMSource(xmlDoc);
            Result outputTarget;
            ByteArrayOutputStream outStream = null;
            if (outFile != null) {
                outputTarget = new StreamResult(new File(outFile));
            } else {
                outStream = new ByteArrayOutputStream();
                outputTarget = new StreamResult(outStream);
            }
            transformer.transform(text, outputTarget);
            if (outStream != null) {
                return outStream.toString();
            }
            return true;
        });
    }

    /**
     * Parses a string into xml
     * @param xmlString string which is to be converted as ZXml object
     * @param encoding of the string
     * @param validateDTD should we have DTD validation on or off
     * @return a ZXml
     * @throws Exception in case of error
     */
    public static ZXml string2xml(final String xmlString, final String encoding, final boolean validateDTD)
            throws Exception {
        // this is funny ...
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setValidating(validateDTD);
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(new ByteArrayInputStream(xmlString.getBytes(encoding)));
        doc.getDocumentElement().normalize();
        return new ZXml(doc);
    }

    /**
     * The underlying Xml Document
     */
    public final Document document;

    /**
     * The wrapped around ZXmlElement root
     */
    public final ZXmlElement root;

    private final LSSerializer serializer;

    private LSSerializer ls(){
        DOMImplementationLS lsImpl = (DOMImplementationLS)document.getImplementation().getFeature("LS", "3.0");
        LSSerializer serializer = lsImpl.createLSSerializer();
        serializer.getDomConfig().setParameter("xml-declaration", false); //by default its true, so set it to false to get String without xml-declaration
        return serializer;
    }

    /**
     * Creates a ZXml from
     * @param doc the document
     */
    public ZXml(Document doc) {
        document = doc;
        root = new ZXmlElement(document.getDocumentElement());
        serializer = ls();
        document.setUserData( "#ser#", serializer, (a,b,c,d,e) ->{});
    }

    /**
     * Xml Element representation
     */
    public static class ZXmlElement {

        /**
         * Instance of an XPath made into a singleton
         */
        public static final XPath X_PATH = XPathFactory.newInstance().newXPath();

        /**
         * Does the xpath exist
         * @param expression the xpath expression
         * @return true if present, false if does not
         * @throws Exception when wrong xpath is given
         */
        public boolean exists(String expression) throws Exception {
            boolean exists = (boolean) X_PATH.compile("boolean(" + expression + ")").evaluate(this.node,
                    XPathConstants.BOOLEAN);
            return exists;
        }

        /**
         * Underlying node
         */
        public final Node node;

        /**
         * Create element from a node
         * @param n the node
         */
        public ZXmlElement(Node n) {
            node = n;
        }

        static boolean hasNoOrSingleTextChild(Node node ){
            if ( !node.hasChildNodes() ) return true;
            NodeList nl = node.getChildNodes();
            if ( nl.getLength() > 1 ) return false;
            Node child = nl.item(0);
            return child.getNodeType() == Node.TEXT_NODE ;
        }

        static Object getNodeValue(Node node ){
            if ( hasNoOrSingleTextChild(node) ){
                final String text = node.getTextContent();
                Object r = ZTypes.bool(text);
                if ( r != null ) return r;
                r = ZNumber.number(text);
                if ( r != null ) return r;
                return text;
            }
            // the value is this
            return node;
        }

        /**
         * As appropriate value for the node
         * @return a bool, numeric or text
         */
        public Object value(){
            final Object v = getNodeValue(node);
            return v == node ? this : v;
        }

        /**
         * List of child elements
         * @return list of child elements
         */
        public List<ZXmlElement> children() {
            NodeList nodeList = node.getChildNodes();
            ArrayList<ZXmlElement> elements = new ArrayList<>();
            int size = nodeList.getLength();
            for (int i = 0; i < size; i++) {
                Node node = nodeList.item(i);
                ZXmlElement e = new ZXmlElement(node);
                elements.add(e);
            }
            return elements;
        }

        /**
         * Gets properties of an element
         * @param name name of the property, use "@" before to specify attributes
         * @return the value of the property
         * @throws Exception in case of error
         */
        public Object get(String name) throws Exception {
            if (name.contains("/")) {
                return element(name);
            }
            if (name.startsWith("@")) {
                // attributes
                NamedNodeMap map = node.getAttributes();
                name = name.substring(1);
                Node n = map.getNamedItem(name);
                if (n != null) {
                    return n.getTextContent();
                }
            }
            switch (name) {
                case "text":
                    return node.getTextContent();
                case "child":
                    return children();
                case "parent":
                    return new ZXmlElement(node.getParentNode());
                default:
                    return Function.NIL;
            }
        }

        /**
         * Given xpath finds value of the xpath
         * @param path the xpath expression
         * @param defaultValue if the xpath fails, returns default
         * @return the result of xpath or if not found, defaultValue
         * @throws Exception in case of expression is wrong
         */
        public String xpath(String path, String defaultValue) throws Exception {
            boolean exists = exists(path);
            if (!exists) return defaultValue;
            String val = (String) X_PATH.compile(path).evaluate(this.node, XPathConstants.STRING);
            return val;
        }

        /**
         * Given xpath finds value of the xpath
         * @param path the xpath expression
         * @return the result of xpath or if not found, null
         * @throws Exception in case of expression is wrong
         */
        public String xpath(String path) throws Exception {
            return xpath(path, null);
        }

        /**
         * Given xpath finds first element matching the xpath
         * @param path the xpath expression
         * @return the result of xpath or if not found, null
         * @throws Exception in case of expression is wrong
         */
        public ZXmlElement element(String path) throws Exception {
            NodeList nodeList = (NodeList) X_PATH.compile(path).evaluate(
                    this.node, XPathConstants.NODESET);
            if (nodeList.getLength() > 0) {
                return new ZXmlElement(nodeList.item(0));
            }
            return null;
        }

        /**
         * Extract right value from a path
         * @param path xpath expression
         * @param multiple when true returns collection of matching elements
         * @return matching objects
         */
        public Object jxPath( String path, boolean multiple) {
            if ( multiple ){
                return ZTypes.safeOrDefault( () -> elementsTransformer(path, ZXmlElement::getNodeValue), Collections.emptyList());
            }
            return ZTypes.safeOrNull( () -> element(path).value());
        }

        /**
         * Extract ZXmlElement from a path
         * @param path xpath expression
         * @param multiple when true returns collection of matching elements
         * @return matching objects
         */
        public Object jxElement( String path, boolean multiple) {
            if ( multiple ){
                return ZTypes.safeOrDefault( () -> elements(path), Collections.emptyList());
            }
            return ZTypes.safeOrNull( () -> element(path) );
        }

        /**
         * Given xpath finds all elements matching the xpath
         * @param path the xpath expression
         * @param transformer a java.util.function.Function that transforms the Node to another Type
         * @return the result of xpath as list or if not found, Empty Collection
         * @throws Exception in case of expression is wrong
         * @param <T> type of the return value for the transformer
         */
        public <T> List<T> elementsTransformer(String path, java.util.function.Function<Node,T> transformer ) throws Exception {
            NodeList nodeList = (NodeList) X_PATH.compile(path).evaluate(
                    this.node, XPathConstants.NODESET);
            int size = nodeList.getLength();
            if ( size == 0 ) return Collections.emptyList();
            List<T> elements = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                Node node = nodeList.item(i);
                T e = transformer.apply(node);
                elements.add(e);
            }
            return elements;
        }

        /**
         * Given xpath finds all elements matching the xpath
         * @param path the xpath expression
         * @return the result of xpath as list or if not found, Empty Collection
         * @throws Exception in case of expression is wrong
         */
        public List<ZXmlElement> elements(String path) throws Exception {
            return elementsTransformer(path, ZXmlElement::new) ;
        }

        @Override
        public String toString() {
            LSSerializer serializer = (LSSerializer)node.getOwnerDocument().getUserData("#ser#");
            return serializer.writeToString(node);
        }
    }

    /**
     * Given xpath finds value of the xpath
     * @param path the xpath expression
     * @param defaultValue if the xpath fails, returns default
     * @return the result of xpath or if not found, defaultValue
     * @throws Exception in case of expression is wrong
     */
    public String xpath(String path, String defaultValue) throws Exception {
        return root.xpath(path, defaultValue);
    }

    /**
     * Given xpath finds first element matching the xpath
     * @param path the xpath expression
     * @return the result of xpath or if not found, null
     * @throws Exception in case of expression is wrong
     */
    public String xpath(String path) throws Exception {
        return xpath(path, null);
    }

    /**
     * Given xpath finds first element matching the xpath
     * @param path the xpath expression
     * @return the result of xpath or if not found, null
     * @throws Exception in case of expression is wrong
     */
    public ZXmlElement element(String path) throws Exception {
        return root.element(path);
    }

    /**
     * Given xpath finds all elements matching the xpath
     * @param path the xpath expression
     * @return the result of xpath as list or if not found, Empty Collection
     * @throws Exception in case of expression is wrong
     */
    public List<ZXmlElement> elements(String path) throws Exception {
        return root.elements(path);
    }

    /**
     * Applying XSL transform to xml
     * @param xslFile path of file where xsl is stored
     * @param outFile file path of the result
     * @return boolean or string
     * @throws Exception in case of error scenarios
     */
    public Object xsl(String xslFile, String outFile) throws Exception {
        return xsl(document, xslFile, outFile);
    }

    /**
     * Generates a JSON String of the XML
     * @return JSON String
     */
    public String jsonString(){
        String xslString = new BufferedReader(new InputStreamReader(Main.class.getResourceAsStream("/xml2json.xsl"))).
                lines().parallel().collect(Collectors.joining("\n"));
        return ZTypes.safeOrDefault( ()->{
            Object retVal = xsl(xslString, null);
            return retVal.toString();
        }, "");
    }

    private Object _cacheJSON = null;

    /**
     * JSON version of the Xml object
     * @return json object from the xml object
     */
    public synchronized Object json(){
        if ( _cacheJSON != null ) return _cacheJSON ;
        _cacheJSON = ZTypes.json(jsonString());
        return _cacheJSON;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        Object me = json();
        Object other = ((ZXml) o).json();
        return Arithmetic.INSTANCE.eq(me,other);
    }

    @Override
    public int hashCode() {
        return document.hashCode();
    }

    /**
     * Creates an ZXml object from args
     * @param args arguments
     *       when args[0] is map, then
     *       { "text" : "xml text" , "loc" : "url/file location",
     *             "encoding" : "string encoding" , "validate" : true/false dtd validation  }
     *       else
     *         args[0] string or file location
     *         args[1] true/false depicting whether args[0] is a location or not ( optional default false )
     *         args[2] String encoding  ( optional - default UTF-8 )
     *         args[3] boolean validate  ( optional - default false )
     *
     * @return ZXml object
     */
    public static ZXml xml(Object... args) {
        if (args.length == 0) return null;
        String location = "";
        String xmlText ;
        String encoding = "UTF-8";
        boolean validate = false ;

        if ( args[0] instanceof Map ){
            Function.NamedArgs param = Function.NamedArgs.fromMap((Map)args[0]) ;
            location = param.typed("loc", "");
            xmlText = param.typed("text", "");
            encoding = param.getOrDefault("encoding", "UTF-8").toString();
            validate = param.typed("validate", false);
        } else {
            xmlText = String.valueOf(args[0]);
            if (args.length > 1) {
                if ( ZTypes.bool(args[1],false) ){
                    location = String.valueOf(args[0]);
                }
                if (args.length > 2) {
                    encoding = String.valueOf(args[2]);
                    if (args.length > 3) {
                        validate = ZTypes.bool(args[3],false );
                    }
                }
            }
        }

        if (!location.isEmpty()) {
            Object o = ZMethodInterceptor.Default.read(location);
            if (o instanceof ZWeb.ZWebCom) {
                xmlText = ((ZWeb.ZWebCom) o).body();
            } else {
                xmlText = String.valueOf(o);
            }
        }
        final String xmlStr = xmlText ;
        final String enc  = encoding ;
        final boolean valid = validate ;
        return ZTypes.safeOrNull( () -> string2xml(xmlStr, enc, valid) );
    }
}
