package zoomba.lang.parser;

import zoomba.lang.core.types.ZException;

/**
 */
public class ZoombaParser extends StringParser{

    public void Identifier(boolean top) throws ParseException {
        // Overriden by generated code
    }

    final public void Identifier() throws ParseException {
        Identifier(false);
    }

    public Token getToken(int index) {
        return null;
    }

    void jjtreeOpenNodeScope(ZoombaNode n) {
    }

    /**
     * Ambiguous statement detector.
     *
     * @param n the node
     * @throws ParseException
     */
    void jjtreeCloseNodeScope(ZoombaNode n) throws ParseException {
        if (n instanceof ASTAmbiguous && n.jjtGetNumChildren() > 0) {
            ParseException e;
            Token tok = this.getToken(0);
            String message = "Not sure what happened!";
            if (tok != null) {
                e = new ParseException(tok, new int[][]{}, new String[]{});
                message = String.format("Unexpected token '%s' line %d, column %d ", tok.image, tok.beginLine, tok.beginColumn);
            } else {
                e = new ParseException(message);
            }
            throw new ZException.Parsing(e, message);
        }
    }
}
