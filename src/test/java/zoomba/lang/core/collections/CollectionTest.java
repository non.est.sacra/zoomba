package zoomba.lang.core.collections;

import org.junit.Assert;
import org.junit.Test;
import zoomba.lang.core.operations.ZCollection;
import zoomba.lang.core.interpreter.InterpreterScriptTest;

import java.util.Collections;

/**
 */
public class CollectionTest {

    final String COLLECTION_TEST_SCRIPT_LOCATION = "samples/test/collections/" ;

    final String COLLECTION_BASIC = COLLECTION_TEST_SCRIPT_LOCATION + "collection_basic.zm" ;

    final String MAP_BASIC = COLLECTION_TEST_SCRIPT_LOCATION + "map_basic.zm" ;

    final String STRUCTURED_DATA = "samples/test/structured_data.zm" ;

    final String ITERATOR_TEST = "samples/test/iterator_method.zm" ;

    final String CARDINALITY_TEST = "samples/test/cardinality.zm" ;

    final String EXOTIC_TEST = COLLECTION_TEST_SCRIPT_LOCATION + "collections_exotic.zm" ;

    final String JOIN_TEST = COLLECTION_TEST_SCRIPT_LOCATION + "collections_join.zm" ;

    final String GROUP_TEST = COLLECTION_TEST_SCRIPT_LOCATION + "groups.zm" ;

    final String LOGICAL = COLLECTION_TEST_SCRIPT_LOCATION + "collections_logical.zm" ;

    final String RANDOM = COLLECTION_TEST_SCRIPT_LOCATION + "collections_random.zm" ;

    final String SORTED = COLLECTION_TEST_SCRIPT_LOCATION + "collections_sorted.zm" ;

    final String SORTED_LIST = COLLECTION_TEST_SCRIPT_LOCATION + "collections_sorted_list.zm" ;

    final String RANGE_TEST = COLLECTION_TEST_SCRIPT_LOCATION + "collections_range.zm" ;

    final String COMBINATORICS = COLLECTION_TEST_SCRIPT_LOCATION + "combinatorics.zm" ;

    final String COMPOSE = COLLECTION_TEST_SCRIPT_LOCATION + "compose.zm" ;

    final String ORDER_ITER = COLLECTION_TEST_SCRIPT_LOCATION + "in_order_iter.zm" ;

    final String LIST_AS_Q = COLLECTION_TEST_SCRIPT_LOCATION + "list_queue.zm" ;

    final String JSON_STRING = COLLECTION_TEST_SCRIPT_LOCATION + "json_str.zm" ;

    @Test
    public void testCollections()  {
        InterpreterScriptTest.danceWithScript( COLLECTION_BASIC );
    }

    @Test
    public void testZListAsDq()  {
        InterpreterScriptTest.danceWithScript( LIST_AS_Q );
    }

    @Test
    public void testJsonString(){
        InterpreterScriptTest.danceWithScript( JSON_STRING );
    }

    @Test
    public void testMap()  {
        InterpreterScriptTest.danceWithScript( MAP_BASIC );
    }

    @Test
    public void testStructuredData()  {
        InterpreterScriptTest.danceWithScript( STRUCTURED_DATA );
    }

    @Test
    public void testIteratorMethods()  {
        InterpreterScriptTest.danceWithScript( ITERATOR_TEST );
    }

    @Test
    public void testCardinality()  {
        InterpreterScriptTest.danceWithScript( CARDINALITY_TEST );
    }

    @Test
    public void testExotic()  {
        InterpreterScriptTest.danceWithScript( EXOTIC_TEST );
    }

    @Test
    public void testJoin()  {
        InterpreterScriptTest.danceWithScript( JOIN_TEST );
    }

    @Test
    public void testLogical()  {
        InterpreterScriptTest.danceWithScript( LOGICAL );
    }

    @Test
    public void testRandom()  {
        InterpreterScriptTest.danceWithScript( RANDOM );
    }

    @Test
    public void testSortedCollections()  {
        InterpreterScriptTest.danceWithScript( SORTED );
    }

    @Test
    public void testSortedList()  {
        InterpreterScriptTest.danceWithScript( SORTED_LIST );
    }

    @Test
    public void testZRangeCollections()  {
        InterpreterScriptTest.danceWithScript( RANGE_TEST );
    }

    @Test
    public void testCombinatorics(){
        InterpreterScriptTest.danceWithScript( COMBINATORICS );
    }

    @Test
    public void testGroupedCollections(){
        InterpreterScriptTest.danceWithScript( GROUP_TEST );
    }

    @Test
    public void testCompose(){  InterpreterScriptTest.danceWithScript( COMPOSE ); }

    @Test
    public void testInOrderIteratorEqualOperator(){  InterpreterScriptTest.danceWithScript( ORDER_ITER ); }

    @Test
    public void testZArrayImmutability(){
        ZArray zArray = new ZArray( new Object[] { 1,2,3,4 }, false );
        Throwable exception = Assert.assertThrows( UnsupportedOperationException.class , zArray::clear);
        Assert.assertNotNull(exception);
        exception = Assert.assertThrows( UnsupportedOperationException.class , () -> zArray.add(0));
        Assert.assertNotNull(exception);
        exception = Assert.assertThrows( UnsupportedOperationException.class , () -> zArray.add(0,1));
        Assert.assertNotNull(exception);
        exception = Assert.assertThrows( UnsupportedOperationException.class , () -> zArray.remove(1));
        Assert.assertNotNull(exception);
        exception = Assert.assertThrows( UnsupportedOperationException.class , () -> zArray.remove(""));
        Assert.assertNotNull(exception);

        exception = Assert.assertThrows( UnsupportedOperationException.class , () -> zArray.addAll(Collections.emptyList()));
        Assert.assertNotNull(exception);
        exception = Assert.assertThrows( UnsupportedOperationException.class , () -> zArray.addAll(1, Collections.emptyList()));
        Assert.assertNotNull(exception);

        exception = Assert.assertThrows( UnsupportedOperationException.class , () -> zArray.removeAll(  Collections.emptyList() ));
        Assert.assertNotNull(exception);

    }

    static class CloneTest implements Cloneable {
        ZCollection l = new ZList();
        @Override
        public Object clone(){
            CloneTest o = new CloneTest();
            o.l = l.deepCopy();
            return o;
        }

        @Override
        public boolean equals(Object o){
            if ( !(o instanceof CloneTest) ) return false;
            if ( this == o ) return true;
            return l.equals(((CloneTest)o).l);
        }

        @Override
        public int hashCode() {
            return l.hashCode();
        }
    }
    @Test
    public void deepCopyTest(){
        CloneTest ct = new CloneTest();
        ct.l.add(0);
        ct.l.add(42);
        ZSet zs = new ZSet();
        zs.add(ct);
        ZCollection zc = zs.deepCopy();
        CloneTest ctOther = (CloneTest) zc.iterator().next();
        Assert.assertNotSame(ct, ctOther);
    }

}
