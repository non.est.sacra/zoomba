package zoomba.lang.core.collections;

import org.junit.Test;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.types.ZRange;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.junit.Assert.*;

public class CombinatoricsTest {

    @Test
    public void initTest(){
        assertThrows( UnsupportedOperationException.class, Combinatorics::permutations);
        assertThrows( UnsupportedOperationException.class, () -> Combinatorics.permutations((Object[]) null));
        assertThrows( IllegalArgumentException.class, () -> Combinatorics.permutations( new Object[]{ "hi", 2 } ));
        assertThrows( UnsupportedOperationException.class, () -> Combinatorics.permutations( new Object[]{Collections.emptyList() } ));
        assertThrows( UnsupportedOperationException.class, () -> Combinatorics.permutations( new Object[]{Arrays.asList(1,1,2)} ));
    }

    @Test
    public void powerSetInitTest(){
        assertEquals( Collections.emptyList(), new Combinatorics.PowerSetIterator (new Object[]{ null } ).sequence );
        assertEquals( Collections.emptyList(),  new Combinatorics.PowerSetIterator(ZArray.EMPTY_ARRAY).sequence );
        assertEquals( 1,  new Combinatorics.PowerSetIterator( new Object[] { new ZRange.NumRange(1,2) }).sequence.size() );
        Iterable it = () -> new ZRange.NumRange(1,2) ;
        assertEquals( 1,  new Combinatorics.PowerSetIterator( new Object[] { it }).sequence.size() );
        assertEquals( 3,  new Combinatorics.PowerSetIterator( new Object[] { new HashSet<>( Arrays.asList(1,2,3)) }).sequence.size() );
        assertEquals( 3,  new Combinatorics.PowerSetIterator( new Object[] { "hi!" }).sequence.size() );
        assertThrows( UnsupportedOperationException.class, () -> new Combinatorics.PowerSetIterator( new Object[]{ 1,1,2 } ));
    }

    @Test
    public void uniqFunctionTest(){
        assertEquals( Collections.emptyList(), Combinatorics.uniq( Collections.emptyList()) );

        Iterable iterable = Combinatorics.uniq( Collections.emptyList(), Arrays.asList(1,1,2,1,3,3)) ;
        List l = (List)StreamSupport.stream(iterable.spliterator(), false).collect(Collectors.toList());
        assertEquals( 4, l.size() );

        iterable = Combinatorics.uniq( Collections.emptyList(), Arrays.asList(1,1,2,1,3,3,0)) ;
        l = (List)StreamSupport.stream(iterable.spliterator(), false).collect(Collectors.toList());
        assertEquals( 5, l.size() );

        iterable = Combinatorics.uniq( Collections.emptyList(), Arrays.asList(1,2,3,4,5)) ;
        l = (List)StreamSupport.stream(iterable.spliterator(), false).collect(Collectors.toList());
        assertEquals( 5, l.size() );

        iterable = Combinatorics.uniq( Collections.emptyList(), Arrays.asList(1,1,1,1,1)) ;
        l = (List)StreamSupport.stream(iterable.spliterator(), false).collect(Collectors.toList());
        assertEquals( 1, l.size() );

        iterable = Combinatorics.uniq( List.of(Function.singleProjection(0)), Arrays.asList(1,1,1,1,1)) ;
        l = (List)StreamSupport.stream(iterable.spliterator(), false).collect(Collectors.toList());
        assertEquals( 1, l.size() );

        iterable = Combinatorics.uniq( Collections.emptyList(), Function.singleProjection(0), Arrays.asList(1,1,1,1,1 )) ;
        l = (List)StreamSupport.stream(iterable.spliterator(), false).collect(Collectors.toList());
        assertEquals( 1, l.size() );

        iterable = Combinatorics.uniq( Collections.emptyList(), Function.singleProjection(0), 1,1,1,1,1 ) ;
        l = (List)StreamSupport.stream(iterable.spliterator(), false).collect(Collectors.toList());
        assertEquals( 1, l.size() );

        iterable = Combinatorics.uniq( Collections.emptyList(), Function.singleProjection(0) ) ;
        l = (List)StreamSupport.stream(iterable.spliterator(), false).collect(Collectors.toList());
        assertEquals( 0, l.size() );

        iterable = Combinatorics.uniq( Collections.emptyList(), Collections.emptyList() ) ;
        l = (List)StreamSupport.stream(iterable.spliterator(), false).collect(Collectors.toList());
        assertEquals( 0, l.size() );

    }

    @Test
    public void hasNextNotEatingValueTest(){
        // Perm
        Iterator<List> iter =  Combinatorics.permutations(Arrays.asList(1,2) ).iterator();
        // do not remove this code -- https://gitlab.com/non.est.sacra/zoomba/-/issues/143
        for ( int i=0; i< 10; i++ ) {
            assertTrue(iter.hasNext());
        }
        for ( int i=0; i< 2; i++){
            assertTrue( iter.hasNext() );
            assertFalse( iter.next().isEmpty() );
        }
        assertFalse( iter.hasNext() );
        assertThrows( NoSuchElementException.class, iter::next);

        // Comb
        iter =  Combinatorics.combinations(Arrays.asList(1,2,3),2 ).iterator();
        // do not remove this code -- https://gitlab.com/non.est.sacra/zoomba/-/issues/143
        for ( int i=0; i< 10; i++ ) {
            assertTrue(iter.hasNext());
        }
        for ( int i=0; i< 3; i++){
            assertTrue( iter.hasNext() );
            assertFalse( iter.next().isEmpty() );
        }
        assertFalse( iter.hasNext() );
        assertThrows( NoSuchElementException.class, iter::next);


        //PowerSet
        iter =  Combinatorics.powerSet(Arrays.asList(1,2) ).iterator();
        // do not remove this code -- https://gitlab.com/non.est.sacra/zoomba/-/issues/143
        for ( int i=0; i< 10; i++ ) {
            assertTrue(iter.hasNext());
        }
        for ( int i=0; i< 3; i++ ) {
            assertTrue(iter.hasNext());
            assertFalse( iter.next().isEmpty() );
        }
        assertFalse( iter.hasNext() );
        assertThrows( NoSuchElementException.class, iter::next);

        // Uniq
        iter =  Combinatorics.uniq(Collections.emptyList(), Arrays.asList(1,2,2,2,1,1) ).iterator();
        // do not remove this code -- https://gitlab.com/non.est.sacra/zoomba/-/issues/143
        for ( int i=0; i< 10; i++ ) {
            assertTrue(iter.hasNext());
        }
        for ( int i=0; i< 3; i++ ) {
            assertTrue(iter.hasNext());
            assertFalse( iter.next().isEmpty() );
        }
        assertFalse( iter.hasNext() );
        assertThrows( NoSuchElementException.class, iter::next);
    }
}
