package zoomba.lang.core.collections;


import org.junit.Test;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.types.ZRange;

import java.util.Arrays;
import java.util.Iterator;
import java.util.stream.Stream;

import static org.junit.Assert.*;

public class ConverterTest {

    @Test
    public void iterTest(){
        Iterable<Integer> it = () -> new ZRange.NumRange(1,5);
        assertSame( it, Converter.iterable( it, "foo" ));
        assertSame( it, Converter.iterable(Function.NIL, () -> it  ));
        assertSame( it, Converter.iterable(Function.NIL, it  ));
        Iterator<Integer> iter = Arrays.asList(1,2,3).listIterator();
        assertNotNull( Converter.iterable(iter, "foo"));
    }

    @Test
    public void listTest(){
        Stream<Integer> si = Stream.of(1,2,3);
        assertNotNull(Converter.list(si, "foo"));
        assertThrows( IllegalArgumentException.class, () -> Converter.list(Function.NIL, "foo") );
    }
}
