package zoomba.lang.core.collections;


import org.junit.Test;
import zoomba.lang.core.operations.ZCollection;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class MultiSetTest {

    @Test
    public void relateFunctionTest(){
        ZMap.MultiSet ms1 = new ZMap.MultiSet();
        ZMap.MultiSet ms2 = new ZMap.MultiSet();
        ms1.put(1, 2);
        ms1.put(2, 2);
        ms2.put(1, 2);
        ms2.put(3, 2);
        assertEquals(ZCollection.Relation.OVERLAP, ZMap.MultiSet.relate(ms1, ms2));
        ms2.remove(1);
        assertEquals(ZCollection.Relation.INDEPENDENT, ZMap.MultiSet.relate(ms1, ms2));
        // in case of independent comparison can not be made
        assertThrows(UnsupportedOperationException.class, () -> ms1.compareTo( ms2 ));

        ms2.remove(3);
        ms2.put(1,1);
        assertEquals(ZCollection.Relation.SUB, ZMap.MultiSet.relate(ms2, ms1));
        ms2.put(1,3);
        assertEquals(ZCollection.Relation.OVERLAP, ZMap.MultiSet.relate(ms2, ms1));

        ms1.clear();
        ms2.clear();

        ms1.put(1, 2);
        ms1.put(2, 2);
        ms2.put(1, 2);
        assertEquals(ZCollection.Relation.SUPER, ZMap.MultiSet.relate(ms1, ms2));
        ms1.put(1, 1);
        assertEquals(ZCollection.Relation.OVERLAP, ZMap.MultiSet.relate(ms1, ms2));

        ms1.clear();
        ms2.clear();
        ms1.put(1, 2);
        ms1.put(2, 1);
        ms2.put(2, 3);
        ms2.put(1, 1);

        assertEquals(ZCollection.Relation.OVERLAP, ZMap.MultiSet.relate(ms1, ms2));

        // in case of overlap comparison can not be made
        assertThrows(UnsupportedOperationException.class, () -> ms1.compareTo( ms2 ));
    }

    @Test
    public void logicFunctionsTest(){
        ZMap.MultiSet ms1 = new ZMap.MultiSet();
        ms1.put(1, 2);
        ms1.put(2, 2);
        assertEquals(Collections.emptyMap(), ms1._and_( Collections.emptyMap() ) );
        Map m = new HashMap();
        m.put(2,2);
        assertEquals(ms1, ms1._or_( m ) );
        Object o = ms1._xor_( m );
        assertEquals(1, ((Map)o).size() );

        // mutable
        ms1.or_mutable( m );
        assertEquals(2, ms1.size());
        ms1.and_mutable( m );
        assertEquals(1, ms1.size());
        ms1.xor_mutable( m );
        assertEquals(0, ms1.size());
    }
    
}
