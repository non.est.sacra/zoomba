package zoomba.lang.core.collections;


import org.junit.Test;
import zoomba.lang.core.operations.ZCollection;

import java.security.SecureRandom;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.*;


public class SortedListTest {

    final Integer[] ints = new Integer[]{ 100, 2, 4, 5, 1000, 6 , 1, 100, 100, 6, 6 } ;

    @Test
    public void noComparatorTest(){
        ZSortedList<Integer> zsi = new ZSortedList<>();
        zsi.addAll(Arrays.asList(ints));
        assertEquals( ints.length, zsi.size() );
        assertEquals( 1, zsi.getFirst().longValue() );
        assertEquals( 1000, zsi.getLast().longValue() );
        // now remove and check
        zsi.removeFirst();
        assertEquals( 2, zsi.getFirst().longValue() );
        zsi.removeLast();
        assertEquals( 100, zsi.getLast().longValue() );
        zsi.removeLast();
        assertEquals( 100, zsi.getLast().longValue() );
        zsi.removeLast();
        assertEquals( 100, zsi.getLast().longValue() );
        zsi.removeLast();
        assertEquals( 6, zsi.getLast().longValue() );
    }

    @Test
    public void withComparatorTest(){
        ZSortedList<Integer> zsi = new ZSortedList<Integer>(Comparator.reverseOrder());
        zsi.addAll(Arrays.asList(ints));
        assertFalse( zsi.isEmpty() );
        // contains ?
        Arrays.stream(ints).forEach( i -> assertTrue( zsi.contains(i) ) );
        assertEquals( ints.length, zsi.size() );
        assertEquals( 1000, zsi.getFirst().longValue() );
        assertEquals( 1, zsi.getLast().longValue() );
        assertTrue( zsi.containsAll( Arrays.asList( 100, 1000)));
        assertFalse( zsi.containsAll( Arrays.asList( 42, 1000)));
        assertFalse( new ZSortedList<>().containsAll( Arrays.asList( 42, 1000)));

    }

    @Test
    public void removeTest(){
        ZSortedList<Integer> zsi = new ZSortedList<>();
        // check against empty
        zsi.addAll(Arrays.asList(ints));
        zsi.retainAll(Collections.emptyList());
        assertTrue( zsi.isEmpty() );
        assertThrows( UnsupportedOperationException.class, () -> zsi.addAll(0, Arrays.asList(1,2,3)));

        // check against itself
        zsi.clear();
        zsi.addAll(Arrays.asList(ints));
        zsi.retainAll(Arrays.asList(ints));
        assertEquals( ints.length, zsi.size() );

        // something in between
        zsi.clear();
        zsi.addAll(Arrays.asList(ints));
        zsi.retainAll(Arrays.asList(6,6, 100, 42 ));
        assertEquals( 3, zsi.size() );

        // now remove all
        zsi.clear();
        zsi.addAll(Arrays.asList(ints));
        assertFalse( zsi.removeAll( Collections.emptyList() ));

        assertTrue( zsi.removeAll(Arrays.asList(ints)) );
        assertTrue( zsi.isEmpty() );
        assertFalse( zsi.removeAll( Arrays.asList(1,2)));

        // again
        zsi.clear();
        zsi.addAll(Arrays.asList(ints));
        assertFalse( zsi.removeAll(Arrays.asList(2,4,6, 10)) );
        assertEquals( ints.length- 3, zsi.size() );
    }

    @Test
    public void getElementTest(){
        List<Integer> list = Arrays.asList(ints) ;
        ZSortedList<Integer> zsi = new ZSortedList<>( list );
        list.sort( Comparator.naturalOrder() );
        IntStream.range(0, ints.length ).forEach( inx -> {
            Integer act = zsi.get(inx);
            Integer exp = list.get(inx);
            assertEquals( exp, act );
        });
        assertThrows( IndexOutOfBoundsException.class, () -> zsi.get(-1));
        assertThrows( IndexOutOfBoundsException.class, () -> zsi.get(42));
    }

    @Test
    public void findElementTest(){
        ZSortedList<Integer> zsi = new ZSortedList<>( Arrays.asList(ints)  );
        assertEquals(0, zsi.indexOf(1));
        assertEquals(4, zsi.indexOf(6));
        assertEquals( -1, zsi.indexOf(5000) );
        // now the reverse
        assertEquals(0, zsi.lastIndexOf(1));
        assertEquals(6, zsi.lastIndexOf(6));
        assertEquals( -1, zsi.lastIndexOf(-30) );
    }

    @Test
    public void iteratorTest(){
        ZSortedList<Integer> zsi = new ZSortedList<>( Arrays.asList(ints) );
        Iterator<Integer> it = zsi.iterator();
        assertTrue( it instanceof  ListIterator);
        ListIterator<Integer> li = zsi.listIterator();
        assertTrue( li.hasNext() );
        assertEquals( 1, li.next().intValue() );
        // indexed
        li = zsi.listIterator(0);
        assertTrue( li.hasNext() );
        assertEquals( 1, li.next().intValue() );
        // offset and check
        li = zsi.listIterator(1);
        assertTrue( li.hasNext() );
        assertEquals( 2, li.next().intValue() );
    }

    @Test
    public void performanceTest(){
        final int n = 500;  // default value
        final int times = 10000;
        final Random r = new SecureRandom();
        List<Integer> list = IntStream.range(0,n).map(x -> r.nextInt(10000) ).boxed().collect(Collectors.toList());
        // for zoomba SortedList
        long start = System.currentTimeMillis();
        IntStream.range(0,times).forEach( (x)->{
            ZSortedList<Integer>  heap = new ZSortedList();
            heap.addAll(list);
        });
        long delta1 = System.currentTimeMillis() - start;
        // for Java PriorityQueue
        start = System.currentTimeMillis();
        IntStream.range(0,times).forEach( (x)->{
            PriorityQueue<Integer> heap = new PriorityQueue<>(n);
            heap.addAll(list);
        });
        long delta2 = System.currentTimeMillis() - start;
        long f = delta1 / delta2 ;
        final long limit = 15;
        System.out.printf("Sorted List took %d ms while PriorityQueue took %d ms, %d is the fraction with limit %d", delta1, delta2, f, limit);
        assertTrue( f <= limit );
    }

    @Test
    public void toArrayTest(){
        ZSortedList zsl = new ZSortedList( Arrays.asList(3,4,1) );
        Object[] arr = zsl.toArray(new Object[0]);
        assertEquals(3, arr.length );
        assertEquals(1, arr[0] );
        assertEquals(3, arr[1] );
        assertEquals(4, arr[2] );

    }

    @Test
    public void equalTest(){
        ZSortedList zsl = new ZSortedList();
        assertFalse( zsl.equals(null ));
        assertFalse( zsl.equals(42 ));
        assertTrue( zsl.equals( zsl ));

        ZSortedList zsl2 = new ZSortedList();
        assertEquals(zsl, zsl2);
        zsl.add(1);
        assertNotEquals(zsl, zsl2);
        zsl2.add(2);
        assertNotEquals(zsl, zsl2);
    }

    @Test
    public void othersTest(){
        ZSortedList zsl = new ZSortedList();
        assertThrows( UnsupportedOperationException.class, () -> zsl.add(0, 10));
        assertThrows( UnsupportedOperationException.class, () -> zsl.set(0, 10));
        zsl.addAll( Arrays.asList(1,2,3,4,5,6));
        assertEquals( Arrays.asList(3,4), zsl.subList( 2, 4) );
    }

    @Test
    public void baseZSortedListTest(){
        ZSortedList.BaseZSortedList bsl = new ZSortedList.BaseZSortedList( new ZSortedList( Arrays.asList(1,3,1)) );
        assertEquals(3, bsl.size());

        ZSortedList.BaseZSortedList cp = (ZSortedList.BaseZSortedList)bsl.myCopy();
        assertNotSame( cp.col, bsl.col );

        assertTrue( bsl.toSet() instanceof NavigableSet );
        assertTrue( bsl.collector() instanceof ZSortedList.BaseZSortedList );
        assertTrue( bsl.setCollector() instanceof ZSet );

        assertEquals(Arrays.asList(1,2,3), ZSortedList.BaseZSortedList.list( Collections.emptyList(), 3,2,1));
        // copy test
        ZCollection zc = bsl.myCopy();
        assertTrue( zc instanceof ZSortedList.BaseZSortedList );
        assertEquals(Arrays.asList(1,1,3), zc);

        // reverse test
        zc = bsl.reverse();
        assertTrue( zc instanceof ZSortedList.BaseZSortedList );
        assertEquals(Arrays.asList(3,1,1), zc);
        bsl = new ZSortedList.BaseZSortedList( new ZSortedList( Arrays.asList(1,2,3), Comparator.naturalOrder() ) );
        assertEquals(Arrays.asList(3,2,1), bsl.reverse() );
    }
}
