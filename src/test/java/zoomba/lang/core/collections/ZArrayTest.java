package zoomba.lang.core.collections;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

import static org.junit.Assert.*;

public class ZArrayTest {

    @Test
    public void arrayIteratorTest(){
        ZArray.ArrayIterator iter = new ZArray.ArrayIterator( "hello".toCharArray() );
        assertTrue(iter.hasNext());
        assertFalse(iter.hasPrevious());
        assertEquals(0, iter.nextIndex() );
        assertEquals(-1, iter.previousIndex() );
        for (ZArray.ArrayIterator it = iter; it.hasNext(); ) {
            it.next();
        }
        assertTrue(iter.hasPrevious());
        assertEquals(4, iter.previousIndex());
        assertThrows( UnsupportedOperationException.class, iter::remove);
        assertThrows( UnsupportedOperationException.class, () -> iter.add('f') );
    }

    @Test
    public void initTest(){
        assertThrows( IllegalArgumentException.class, () -> new ZArray("hello", false) );
        assertTrue( ZArray.EMPTY_Z_ARRAY.setCollector() instanceof ZSet);
        ZArray a = ZArray.of(1,2);
        assertEquals( 1, a.getKey() );
        assertEquals( 2, a.getValue() );
        a.setValue(42);
        assertEquals( 42, a.getValue() );
    }

    @Test
    public void accessTest(){
        ZArray za = new ZArray( "hello".toCharArray() , false );

        assertThrows( ArrayIndexOutOfBoundsException.class, () -> za.get(15));
        assertThrows( ArrayIndexOutOfBoundsException.class, () -> za.set(15, 'c'));
        assertThrows( ArrayIndexOutOfBoundsException.class, () -> za.get(-15));
        assertThrows( ArrayIndexOutOfBoundsException.class, () -> za.set(-15, 'c'));
        assertThrows( ArrayIndexOutOfBoundsException.class, () -> ZArray.EMPTY_Z_ARRAY.get(-15));
        assertThrows( ArrayIndexOutOfBoundsException.class, () -> ZArray.EMPTY_Z_ARRAY.set(-15, 'c'));
        assertThrows( ArrayIndexOutOfBoundsException.class, () -> ZArray.EMPTY_Z_ARRAY.get(15));
        assertThrows( ArrayIndexOutOfBoundsException.class, () -> ZArray.EMPTY_Z_ARRAY.set(15, 'c'));

        assertEquals( 'o', za.set(-1, 'i'));
    }

    @Test
    public void containsTest(){
        ZArray za = new ZArray( "hii!".toCharArray() , false );
        assertTrue( za.containsAll(Arrays.asList('i','h')));
        assertFalse( za.containsAll(Arrays.asList('i','j')));
        assertThrows( UnsupportedOperationException.class,
                () -> ZArray.EMPTY_Z_ARRAY.retainAll( Collections.emptyList() ));
        assertEquals(2, za.lastIndexOf( 'i') );
        assertEquals(-1, za.lastIndexOf( 'a') );
    }

    @Test
    public void mapEntryTest(){
        assertEquals( ZArray.of(1,2), Map.entry(1,2));
        assertNotEquals( ZArray.of(1,2), Map.entry(1,3));
        assertNotEquals( ZArray.of(1,2), Map.entry(2,1));
        assertEquals( ZArray.of(1,2), ZArray.of(1,2));
        assertEquals( ZArray.of(1,2), ZArray.of(2,1));
    }
}
