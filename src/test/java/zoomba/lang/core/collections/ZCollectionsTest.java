package zoomba.lang.core.collections;

import org.junit.Test;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.ZCollection;
import zoomba.lang.core.types.ZException;
import zoomba.lang.core.types.ZRange;

import java.util.*;

import static org.junit.Assert.*;

public class ZCollectionsTest {

    @Test
    public void initTest(){
        ZList bzc = new ZList(1);
        assertEquals(Arrays.asList(1), bzc.col );
    }

    @Test
    public void arrayFunctionTest(){
       Object[] arr = BaseZCollection.array( Arrays.asList(1L,2L,3L));
       assertEquals( 3, arr.length);
       BaseZCollection.ZWrappedCollection col = new BaseZCollection.ZWrappedCollection( Arrays.asList(1L,2L,3L));
       Object o = col.array( int.class);
       assertNotNull(o);
       assertNotNull(o.getClass().getComponentType() );
       assertEquals( int.class, o.getClass().getComponentType() );

        o = col.array( byte.class);
        assertNotNull(o);
        assertNotNull(o.getClass().getComponentType() );
        assertEquals( byte.class, o.getClass().getComponentType() );

        BaseZCollection.ZWrappedCollection col2 = new BaseZCollection.ZWrappedCollection( Arrays.asList("hi","hello", "boom!"));
        o = col2.array( String.class);
        assertNotNull(o);
        assertNotNull(o.getClass().getComponentType() );
        assertEquals( String.class, o.getClass().getComponentType() );

        assertThrows( RuntimeException.class, () -> col.array("foo-bar"));
    }

    @Test
    public void setFunctionsTest(){
        HashSet hs = new HashSet( Arrays.asList(1,2,3));
        // intersection
        assertEquals(Collections.emptySet(), BaseZCollection.intersection( Collections.emptySet(), hs )  );
        assertEquals(Collections.emptySet(), BaseZCollection.intersection(  hs, Collections.emptySet() )  );

        // union
        assertEquals(hs, BaseZCollection.union(  hs, Collections.emptySet() )  );

        // minus
        assertEquals(Collections.emptySet(), BaseZCollection.minus( Collections.emptySet(), hs )  );
    }

    @Test
    public void composeFunctionTest(){
        Set s = new HashSet(Arrays.asList(1,2,3));
        Collection col = BaseZCollection.compose( s, null, Arrays.asList(Function.COLLECTOR_IDENTITY ), 1 );
        assertTrue( col instanceof Set );
        List l = new ArrayList();
        col = BaseZCollection.compose( s, l, Arrays.asList(Function.COLLECTOR_IDENTITY ), 1 );
        assertSame( l, col);
        assertSame( Collections.emptyList(),
                BaseZCollection.compose( null , l, Arrays.asList(Function.COLLECTOR_IDENTITY ), 1 ));

        BaseZCollection.ZWrappedCollection bzc = new BaseZCollection.ZWrappedCollection( Arrays.asList(1,2,3));
        col = bzc.compose( Function.COLLECTOR_IDENTITY );
        assertEquals( Arrays.asList(1,2,3), col );
    }

    static final class XPredicate implements Function.Predicate {

        int[] throwAt = { -1, -1 };
        boolean useValue = false;
        boolean value = true;

        @Override
        public boolean accept(Object... args) {
            return false;
        }

        @Override
        public String body() {
            return "";
        }

        @Override
        public MonadicContainer execute(Object... args) {
            int i = (int)args[1];
            if ( i == throwAt[0] ){
                if ( useValue ) {
                    return new ZException.Break(value);
                }
                return new ZException.Break(Function.NIL);
            }
            if ( i == throwAt[1] ){
                if ( useValue ) {
                    return new ZException.Continue(value);
                }
                return new ZException.Continue(Function.NIL);
            }
            return new MonadicContainerBase(true);
        }

        @Override
        public String name() {
            return "";
        }
    }

    @Test
    public void applyComposeFunctionTest(){
        List l = new ArrayList();
        XPredicate p = new XPredicate();
        p.throwAt[0] = 2 ;
        p.throwAt[1] = 1 ;
        BaseZCollection.compose(l, Arrays.asList(0,1,2,3),  p );
        assertEquals( Arrays.asList(0), l );
        l.clear();
        p.throwAt[0] = 2 ;
        p.throwAt[1] = 1 ;
        p.useValue = true;
        BaseZCollection.compose(l, Arrays.asList(0,1,2,3),  p );
        assertEquals( Arrays.asList(0,1,2), l );

        l.clear();
        p.throwAt[0] = 2 ;
        p.throwAt[1] = 1 ;
        p.useValue = true;
        p.value = false;
        BaseZCollection.compose(l, Arrays.asList(0,1,2,3),  p );
        assertEquals( Arrays.asList(0), l );
    }

    @Test
    public void dictAuxiliaryMethodTest(){
        Map  m = new HashMap();
        BaseZCollection.putBackInMap( m, new Object[]{ 1, 1 } );
        assertEquals( 1, m.size() );
        Object me = m.entrySet().iterator().next();
        BaseZCollection.putBackInMap( m, me );
        assertEquals( 1, m.size() );
    }

    @Test
    public void relationFunctionTest(){
        assertEquals(ZCollection.Relation.SUPER, BaseZCollection.relate( Arrays.asList(1,2), Collections.emptyList() ) );
        assertEquals(ZCollection.Relation.INDEPENDENT, BaseZCollection.relate( Arrays.asList(1,2), Arrays.asList(3,4) ) );
        assertEquals(ZCollection.Relation.EQUAL, BaseZCollection.relate( Arrays.asList(1,2,2,1), Arrays.asList(2,1,1,2) ) );
        assertEquals(ZCollection.Relation.SUPER, BaseZCollection.relate( Arrays.asList(1,2,2), Arrays.asList(2,1 ) ) );
        assertEquals(ZCollection.Relation.SUB, BaseZCollection.relate( Arrays.asList(1,2), Arrays.asList(2,1,2 ) ) );
        assertEquals(ZCollection.Relation.OVERLAP, BaseZCollection.relate( Arrays.asList(1,2,3,4,1), Arrays.asList(1,2,3,4,4 ) ) );
        // this - that is empty
        assertEquals(ZCollection.Relation.OVERLAP, BaseZCollection.relate( Arrays.asList(1,2,1), Arrays.asList(1,2,3 ) ) );
        assertEquals(ZCollection.Relation.OVERLAP, BaseZCollection.relate(  Arrays.asList(1,2,3 ), Arrays.asList(1,2,1) ) );

    }

    @Test
    public void intersectionTest(){
        List l = new ArrayList(Arrays.asList(1,1,1,2,2,3));
        BaseZCollection.intersectionMutable( l, Arrays.asList(1,2,3,4,5));
        assertEquals(Arrays.asList(1,2,3), l);
    }

    @Test
    public void diffIntoTest(){
        Iterable iter1 = () -> new ZRange.NumRange(1,5);
        Iterable iter2 = () -> new ZRange.NumRange(1,3);
        Collection diff = BaseZCollection.difference( iter1, iter2 );
        assertTrue( diff instanceof ZList);
        assertEquals( 2, diff.size());
    }

    @Test
    public void zWrappedCollectionTest(){
        BaseZCollection.ZWrappedCollection col = new BaseZCollection.ZWrappedCollection( Arrays.asList(1,2,3));
        assertTrue( col.collector() instanceof ZList );
        assertEquals( Arrays.asList(1,2,3), col.myCopy() );
        assertTrue( col.setCollector() instanceof ZSet );
        assertEquals( "[ 1,2,3 ]", col.toString() );
        assertEquals( 3, col.toArray(new Object[]{} ).length );
        assertTrue( col.containsAll(Arrays.asList(1,2,3)));
        assertFalse( col.retainAll(Arrays.asList(1,2,3)));

        assertEquals( 3, col.toMultiSet().size() );
        assertEquals( 3, col.toMultiSet( Function.COLLECTOR_IDENTITY ).size() );
        assertEquals( 3, col.groupBy( Function.COLLECTOR_IDENTITY , Function.COLLECTOR_IDENTITY ).size() );

        assertEquals( 3, col.leftFold( Function.COLLECTOR_IDENTITY ) );
        assertThrows( UnsupportedOperationException.class, () -> col.rightFold( Function.COLLECTOR_IDENTITY ) );
        assertThrows( UnsupportedOperationException.class, () -> col.rightIndex( Function.COLLECTOR_IDENTITY ) );
        assertThrows( UnsupportedOperationException.class, () -> col.rightReduce( Function.COLLECTOR_IDENTITY ) );

        // check this
        assertEquals(1, col.compareTo( Collections.emptyList() ));
        col.forEach( Function.COLLECTOR_IDENTITY );
    }

    @Test
    public void zWrappedCollectionArithmeticTest(){
        BaseZCollection.ZWrappedCollection col = new BaseZCollection.ZWrappedCollection( Arrays.asList(1,2,3));

        assertThrows( UnsupportedOperationException.class, () -> col._div_( Function.NIL ) );
        assertThrows( UnsupportedOperationException.class, () -> col.div_mutable(Function.NIL ) );

        assertThrows( UnsupportedOperationException.class, () -> col._mul_( Function.NIL ) );
        assertThrows( UnsupportedOperationException.class, () -> col.mul_mutable( Function.NIL ) );

        assertNull( col._mul_( null ));
        Collection cr =  (Collection) col._mul_( new Boolean[]{ false, true } ) ;
        assertEquals( 6, cr.size());

        assertNull( col._pow_( null ));
        assertThrows( UnsupportedOperationException.class, () -> col._pow_( Function.NIL ) );

        cr =  (Collection) col._pow_(0L)  ;
        assertEquals( 0, cr.size());
        cr =  (Collection) col._pow_(1)  ;
        assertEquals( col.size(), cr.size());

        // now some crazy stuff
        col.mul_mutable(null);
        assertEquals(3, col.size());

        col.add_mutable( col );
        assertEquals( 6, col.size());
        col.sub_mutable( col );
        assertEquals( 0, col.size());

    }

    @Test
    public void zWrappedCollectionLogicTest(){
        BaseZCollection.ZWrappedCollection col = new BaseZCollection.ZWrappedCollection( Arrays.asList(1,2,3));
        Integer[] arr = new Integer[]{ 1, 2 , 4 };

        assertNull( col._and_(null));
        assertEquals( 2, ((Collection)col._and_( arr)).size() );
        assertThrows( UnsupportedOperationException.class, () -> col._and_( Function.NIL ) );

        assertNull( col._or_(null));
        assertEquals( 4, ((Collection)col._or_( arr)).size() );
        assertThrows( UnsupportedOperationException.class, () -> col._or_( Function.NIL ) );

        assertNull( col._xor_(null));
        assertEquals( 2, ((Collection)col._xor_( arr)).size() );
        assertThrows( UnsupportedOperationException.class, () -> col._xor_( Function.NIL ) );

        col.and_mutable(null);
        assertEquals( 3, col.size() );
        col.or_mutable(null);
        assertEquals( 3, col.size() );
        col.xor_mutable(null);
        assertEquals( 3, col.size() );


        col.and_mutable(col);
        assertEquals( 3, col.size() );
        col.or_mutable(col);
        assertEquals( 3, col.size() );
        col.xor_mutable(col);
        assertEquals( 0, col.size() );

        assertThrows( UnsupportedOperationException.class, () -> col.and_mutable( Function.NIL ) );
        assertThrows( UnsupportedOperationException.class, () -> col.or_mutable( Function.NIL ) );
        assertThrows( UnsupportedOperationException.class, () -> col.xor_mutable( Function.NIL ) );

        col.or_mutable( new Object[]{ 1, 2, 3 });
        assertEquals( 3, col.size() );
        col.and_mutable( new Object[]{ 1, 2  });
        assertEquals( 2, col.size() );
        col.xor_mutable( new Object[]{  2, 3  });
        assertEquals( 2, col.size() );

    }

    @Test
    public void equalsTest(){
        ZList l = new ZList();
        BaseZCollection.ZWrappedCollection col = new BaseZCollection.ZWrappedCollection(l);
        col.col = l;
        assertEquals( l.col.hashCode(), col.hashCode() );
        assertFalse( col.equals(null));
        assertFalse( col.equals(Arrays.asList(1,2,3)));
        // same list
        assertTrue( ZCollection.iterationOrderEquals(l,l));
    }
}
