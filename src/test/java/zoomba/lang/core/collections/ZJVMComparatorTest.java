package zoomba.lang.core.collections;

import org.junit.Test;
import zoomba.lang.core.interpreter.InterpreterScriptTest;
import zoomba.lang.core.operations.Function;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static zoomba.lang.core.collections.Converter.Comparator2Function.*;

public class ZJVMComparatorTest {

    public static final String COMPARATOR_TESTS = "samples/test/collections/comparators_test.zm" ;

    @Test
    public void comparatorMapperTest(){
        Function.Predicate p = Function.Predicate.from( Converter.Comparator2Function.CMP_JVM_NATURAL_ORDER );
        Converter.ComparatorMapper m = new Converter.ComparatorMapper(List.of( p ) );
        assertSame( p, m.cmp);
        m = new Converter.ComparatorMapper(List.of( p , Function.COLLECTOR_IDENTITY ) );
        assertSame( p, m.cmp);
        m = new Converter.ComparatorMapper(List.of( Function.COLLECTOR_IDENTITY , Function.COLLECTOR_IDENTITY ) );
        assertSame( Function.COLLECTOR_IDENTITY, m.cmp);
    }

    @Test
    public void lookUpFunctionComparatorTest(){
        assertNull( Converter.Comparator2Function.of("foo") );
        // JVM standard reverse
        Function f = Converter.Comparator2Function.of("@cmp_Jj");
        assertEquals( 2, f.definedParams() );
        assertTrue( f.body().startsWith("cmp_"));
        assertEquals( 1, f.execute(0,1).value() );
        assertEquals( 1, f.compare(0,1) );

    }

    @Test
    public void jvmComparatorTest(){
        List l = new ArrayList(Arrays.asList(2,3,1,null,null));
        Collections.sort(l, CMP_NULL_ASC_JVM_NATURAL_ORDER );
        assertEquals(Arrays.asList(null, null, 1,2,3), l );

        l = new ArrayList(Arrays.asList(2,null, 3,1,null));
        Collections.sort(l, CMP_NULL_DSC_JVM_NATURAL_ORDER );
        assertEquals(Arrays.asList( 1,2,3,null, null), l );

        l = new ArrayList(Arrays.asList(null, 2,1,null, 3));
        Collections.sort(l, CMP_NULL_DSC_JVM_REVERSE_ORDER );
        assertEquals(Arrays.asList( 3,2,1,null, null), l );

        l = new ArrayList(Arrays.asList(2,null,1,3, null));
        Collections.sort(l, CMP_NULL_ASC_JVM_REVERSE_ORDER );
        assertEquals(Arrays.asList(null, null, 3,2,1), l );
    }

    @Test
    public void zmbComparatorTest(){

        List l = new ArrayList(Arrays.asList(1, null, 2.0, null, 3L ));
        Collections.sort(l, CMP_NULL_ASC_ZMB_NATURAL_ORDER );
        assertEquals(Arrays.asList(null, null, 1, 2.0, 3L), l );

        l = new ArrayList(Arrays.asList(null, 2,3.0,null, 1L));
        Collections.sort(l, CMP_NULL_DSC_ZMB_NATURAL_ORDER );
        assertEquals(Arrays.asList( 1L,2,3.0,null, null), l );

        l = new ArrayList(Arrays.asList(2,null,BigInteger.ONE,null,3));
        Collections.sort(l, CMP_NULL_DSC_ZMB_REVERSE_ORDER );
        assertEquals(Arrays.asList( 3,2,BigInteger.ONE,null, null), l );

        l = new ArrayList(Arrays.asList(BigDecimal.valueOf(2), null,1,3, null));
        Collections.sort(l, CMP_NULL_ASC_ZMB_REVERSE_ORDER );
        assertEquals(Arrays.asList(null, null, 3,BigDecimal.valueOf(2),1), l );
    }

    @Test
    public void scriptBasedTest(){
        InterpreterScriptTest.danceWithScript( COMPARATOR_TESTS );
    }
}
