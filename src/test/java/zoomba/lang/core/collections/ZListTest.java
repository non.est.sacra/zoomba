package zoomba.lang.core.collections;

import org.junit.Test;
import zoomba.lang.core.operations.Function;

import java.util.*;

import static org.junit.Assert.*;

public class ZListTest {

    @Test
    public void listFunctionTest(){
        assertEquals(Collections.emptyList(), ZList.list(null, (Object) null));
    }

    @Test
    public void queueFunctionsTest(){
        Queue q = ((ZList) ZList.list( null, 1,2,3));
        assertEquals(3, q.size());
        assertEquals( 1, q.peek());
        assertTrue( q.offer(4));
        assertEquals( 1, q.poll());
        assertEquals( 2, q.element());
        assertEquals( 2, q.remove());
        q.remove();
        q.poll();
        assertTrue( q.isEmpty() );
        assertThrows( NoSuchElementException.class , q::element);
        assertThrows( NoSuchElementException.class , q::remove);
        assertNull( q.poll());
        assertNull( q.peek());
    }

    @Test
    public void dqFunctionsTest(){
        Deque q = ((ZList) ZList.list( null, 1,2,1,3));
        assertFalse( q.removeFirstOccurrence(10));
        assertFalse( q.removeLastOccurrence(10));
        assertTrue( q.removeFirstOccurrence(1));
        assertEquals(2, q.getFirst() );
        assertTrue( q.removeLastOccurrence(1));
        assertEquals(2, q.size() );
        assertTrue( q.contains(2));
        assertTrue( q.remove(2));

        q.clear(); // make it empty
        assertFalse( q.iterator().hasNext());
        assertFalse( q.descendingIterator().hasNext());
        q.addAll( Arrays.asList(1,2,3));
        Iterator it = q.descendingIterator();
        for (int i=3; i > 0; i--){
            assertTrue(it.hasNext());
            assertEquals(i, it.next());
        }
        assertFalse( it.hasNext());
        Deque r = ((ZList) q).reversed();
        for (int i=3; i > 0; i--){
            assertEquals(i, r.pollFirst());
        }
    }

    @Test
    public void addTest(){
        ZList zl = new ZList(Arrays.asList(1,2,3,4));
        zl.addAll(3, Arrays.asList( "a", "b") );
        assertEquals(3, zl.indexOf("a"));
        assertEquals(4, zl.indexOf("b"));
        assertEquals(5, zl.indexOf(4));
        assertEquals(6, zl.size());
        zl.add( 1, "x");
        assertEquals(1, zl.indexOf("x"));
    }

    @Test
    public void accessorTest(){
        ZList zl = new ZList(Arrays.asList(1,2,3,4));
        assertThrows( IndexOutOfBoundsException.class, () -> zl.get(10));
        assertThrows( IndexOutOfBoundsException.class, () -> zl.set(10,10));

        assertThrows( IndexOutOfBoundsException.class, () -> new ZList().get(10));
        assertThrows( IndexOutOfBoundsException.class, () -> new ZList().set(10,10));

        assertThrows( IndexOutOfBoundsException.class, () -> new ZList().get(-10));
        assertThrows( IndexOutOfBoundsException.class, () -> new ZList().set(-10,10));
    }

    @Test
    public void KMPTest(){
        // forward
        assertEquals(-1, KMP.findSubListIndex( Collections.emptyList(), Collections.emptyList() ) );
        assertEquals(0, KMP.findSubListIndex( Collections.emptyList(), Arrays.asList(1,2 ) ) );
        assertEquals(-1, KMP.findSubListIndex( Arrays.asList(1,2), Collections.emptyList() ) );
        assertEquals(-1, KMP.findSubListIndex( Arrays.asList(1,2), Arrays.asList(1)) );
        assertEquals(0, KMP.findSubListIndex( Arrays.asList(1,2), Arrays.asList(1,2)) );
        //backward
        assertEquals(-1, KMP.findLastSubListIndex( Collections.emptyList(), Collections.emptyList() ) );
        assertEquals(2, KMP.findLastSubListIndex( Collections.emptyList(), Arrays.asList(1,2 ) ) );
        assertEquals(0, KMP.findLastSubListIndex( Arrays.asList(1,2), Arrays.asList(1,2 ) ) );
        assertEquals(-1, KMP.findLastSubListIndex( Arrays.asList(1,2,3), Arrays.asList(1,2 ) ) );
        assertEquals(3, KMP.findLastSubListIndex( Arrays.asList(2,3), Arrays.asList(1,2,3,2,3 ) ) );
        assertEquals(2, KMP.findLastSubListIndex( Arrays.asList(3,2,3), Arrays.asList(1,2,3,2,3 ) ) );
        assertEquals(1, KMP.findLastSubListIndex( Arrays.asList(2,3), Arrays.asList(1,2,3) ) );
    }

    @Test
    public void containsTest(){
        ZList zl = new ZList( Arrays.asList(1,2,4,5,6, null,3, 10, 12 ));
        // index of
        assertEquals(5, zl.indexOf(null));
        assertEquals(5, zl.indexOf( new Object[] { null, 3 } ));
        assertEquals(-1, new ZList().indexOf('a') );
        assertEquals(0, zl.indexOf( new ZList() ) );

        // last index
        assertEquals(5, zl.lastIndexOf(null));
        assertEquals(5, zl.lastIndexOf( new Object[] { null, 3 } ));
        assertEquals(zl.size(), zl.lastIndexOf( new ZList() ) );
        assertEquals(-1, zl.lastIndexOf('a') );
        assertEquals(-1, new ZList().lastIndexOf('a') );

        // starts with
        assertFalse( new ZList( ).startsWith(Arrays.asList(1,2,3)) );
        assertFalse( new ZList( Arrays.asList(1,2)).startsWith(Arrays.asList(1,2,3)) );
        // ends with
        assertFalse( new ZList( ).endsWith(Arrays.asList(1,2,3)) );
        assertFalse( new ZList( Arrays.asList(1,2)).endsWith(Arrays.asList(1,2,3)) );
    }

    @Test
    public void foldFunctionTest(){
        ZList zl = new ZList(Arrays.asList(1,2,3));
        assertEquals( true, zl.rightFold( Function.TRUE) );
        assertEquals( false, zl.rightReduce( Function.FALSE) );
    }

}
