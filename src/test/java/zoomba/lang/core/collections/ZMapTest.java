package zoomba.lang.core.collections;

import org.junit.Test;
import zoomba.lang.core.operations.Arithmetic;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.ZCollection;
import zoomba.lang.core.types.ZTypes;

import java.util.*;
import java.util.stream.Stream;

import static org.junit.Assert.*;

public class ZMapTest {

    @Test
    public void collectionMethodTest(){
        ZMap m = new ZMap();
        Collection col = m.collection(false);
        assertEquals(m.size(), col.size());
        assertEquals(m.isEmpty(), col.isEmpty());
        assertEquals(m.entrySet().contains(null), col.contains(null));
        assertFalse( col.iterator().hasNext() );
        assertEquals( 0, col.toArray().length );
        assertEquals( 0, col.toArray( new Object[]{}).length );

        assertFalse( col.remove( null ) );
        assertFalse( col.removeAll(Collections.emptyList() ) );
        assertFalse( col.containsAll( Collections.emptyList() ) );
        assertFalse( col.addAll( Collections.emptyList() ) );
        assertFalse( col.retainAll( Collections.emptyList() ) );
        col.clear();
    }

    @Test
    public void deepCopyTest(){
        ZTypes.StringX foo = new ZTypes.StringX() {
            @Override
            public String string(Object... args) {
                return "";
            }

            @Override
            public Object deepCopy() {
                return "hi";
            }
        };
        ZMap m = new ZMap();
        m.put( foo, "hello");
        Map m2 =  m.deepCopy();
        assertTrue( m2.containsKey("hi"));
    }

    @Test
    public void fromEntriesTest(){
        Map m = new HashMap();
        List l = new ArrayList();
        l.add(null);
        assertThrows( UnsupportedOperationException.class, ()-> ZMap.fromEntries( m, l) );
        assertThrows( UnsupportedOperationException.class, ()-> ZMap.fromEntries( m, Arrays.asList( Arrays.asList(1))) );
        assertThrows( UnsupportedOperationException.class, ()-> ZMap.fromEntries( m, Arrays.asList(Function.NIL)) );

        ZMap.fromEntries( m, Arrays.asList( new Object[]{1,2}, new Object[]{3,4} ));
        assertEquals(2, m.size());
    }

    @Test
    public void joinEntriesTest(){
        Map m = new HashMap();
        assertThrows( UnsupportedOperationException.class, ()-> ZMap.joinEntries( m, Arrays.asList(1,2), Arrays.asList(1,2,3)) );
        assertThrows( UnsupportedOperationException.class, ()-> ZMap.joinEntries( m, Arrays.asList(1,2,3), Arrays.asList(1,2)) );
    }

    @Test
    public void dictFunctionTest(){
        Map m = new HashMap();
        m.put(1,2);
        Map m2 = ZMap.dict( null, m, true);
        assertEquals(1, m2.size());
        m2.clear();
        assertEquals(1, m.size());
        assertEquals( Collections.emptyMap(), ZMap.dict(null, (Object)null ));
        m2 = ZMap.dict( null, new Object[]{ new Object[]{ m.entrySet().iterator().next()}}  );
        assertTrue( m2.containsKey(1));
        m2 = ZMap.dict( null, "hello!");
        assertFalse(m2.isEmpty());
        assertThrows( IllegalArgumentException.class, () -> ZMap.dict( null, new Object[]{ 1,2,3 }));

        m.clear();
        m.put(2,1);
        m.put(1,0);
        m2 = ZMap.sortedDict( Collections.emptyList(),  m, true );
        m.clear();
        assertEquals(2, m2.size());

    }

    @Test
    public void sortedDictTest(){
        ZMap sm  = (ZMap) ZMap.sortedDict( Collections.emptyList() , Arithmetic.INSTANCE );
        ZMap m = new ZMap();
        Stream.of(1,2,3,4,5).forEach( i -> {
            m.put(i,i);
            sm.put(i,i);
        });
        assertSame( Arithmetic.INSTANCE, sm.comparator());
        assertNull( m.comparator() );

        assertEquals(2 , sm.subMap(1,3).size() );
        assertSame( ZMap.EMPTY, m.subMap(1,3));

        assertEquals(2 , sm.subMap(1,true, 3, false).size() );
        assertSame( ZMap.EMPTY, m.subMap(1,true,3,false));

        assertEquals(2, sm.headMap(3).size());
        assertSame( ZMap.EMPTY, m.headMap(3));

        assertEquals(3, sm.headMap(3,true).size());
        assertSame( ZMap.EMPTY, m.headMap(3, true));

        assertEquals( 3, sm.tailMap(3).size() );
        assertSame( ZMap.EMPTY, m.tailMap(3));

        assertEquals( 2, sm.tailMap(3, false).size() );
        assertSame( ZMap.EMPTY, m.tailMap(3, false));

        assertEquals(1, sm.firstKey() );
        assertEquals( Function.NIL, m.firstKey() );

        assertEquals(5, sm.lastKey() );
        assertEquals( Function.NIL, m.lastKey() );

        assertEquals(1, sm.firstEntry().getKey() );
        assertNull(m.firstEntry() );

        assertEquals(5, sm.lastKey() );
        assertEquals( Function.NIL, m.lastKey() );

        assertEquals(5, sm.lastEntry().getKey() );
        assertNull(m.lastEntry() );

        assertEquals(4, sm.lowerEntry(5).getKey() );
        assertNull(  m.lowerEntry(5) );

        assertEquals(4, sm.lowerKey(5) );
        assertEquals( Function.NIL, m.lowerKey(5) );

        assertEquals(5, sm.floorEntry(5).getKey() );
        assertNull(  m.floorEntry(5) );

        assertEquals(5, sm.floorKey(5) );
        assertEquals( Function.NIL, m.floorKey(5) );

        assertEquals(5, sm.ceilingEntry(5).getKey() );
        assertNull(  m.ceilingEntry(5) );

        assertEquals(5, sm.ceilingKey(5) );
        assertEquals( Function.NIL, m.ceilingKey(5) );

        assertEquals(4, sm.higherEntry(3).getKey() );
        assertNull(  m.higherEntry(5) );

        assertEquals(4, sm.higherKey(3) );
        assertEquals( Function.NIL, m.higherKey(3) );

        assertEquals(5, sm.descendingMap().keySet().iterator().next() );
        assertEquals( ZMap.EMPTY, m.descendingMap() );

        assertEquals(5, sm.descendingKeySet().iterator().next() );
        assertEquals( ZSet.EMPTY, m.descendingKeySet() );

        assertEquals(1, sm.navigableKeySet().iterator().next() );
        assertEquals( ZSet.EMPTY, m.navigableKeySet() );

        assertEquals(1, sm.pollFirstEntry().getKey() );
        assertNull( m.pollFirstEntry() );
        assertEquals(5, sm.pollLastEntry().getKey() );
        assertNull( m.pollLastEntry() );
    }

    @Test
    public void relationTest(){
        Map m1 = new HashMap();
        m1.put(1,1);
        Map m2 = new HashMap();
        m2.put(2,2);
        assertEquals(ZCollection.Relation.INDEPENDENT, ZMap.relate( m1, m2 ));
    }

    @Test
    public void setOpTest(){
        Map m1 = new HashMap();
        m1.put(1,1);
        Map m2 = new HashMap();
        m2.put(1,2);
        assertEquals( 0,  ZMap.intersection(m1, m2).size() );
        assertEquals( 0, ZMap.intersectionMutable(m1,m2).size() );
        assertEquals( 0, ZMap.intersectionMutable(m2,m1).size() );

        // mutable union
        m1.put(1,1);
        m1.put(2,2);
        m2.put(1,1);
        m2.put(2,1);
        m2.put(3,3);
        assertEquals( 3, ZMap.unionMutable(m1,m2).size() );

        // mutable diff
        m1.clear();
        m2.clear();

        m1.put(1,1);
        m1.put(2,2);
        m2.put(1,1);
        m2.put(2,1);
        m2.put(3,2);
        assertEquals( 1, ZMap.differenceMutable(m1,m2).size() );

        // sym diff
        m1.clear();
        m2.clear();

        m1.put(1,1);
        m1.put(2,2);
        m2.put(2,1);
        m2.put(3,2);
        assertEquals( 3, ZMap.symmetricDifference(m1,m2).size() );

        m1.clear();
        m2.clear();

        m1.put(1,1);
        m1.put(2,2);
        m2.put(2,1);
        m2.put(3,2);
        assertEquals( 3, ZMap.symmetricDifferenceMutable(m1,m2).size() );

    }

    @Test
    public void initTest(){
        assertThrows( IllegalArgumentException.class, () -> new ZMap( new HashMap(), null, null  ));
        assertThrows( IllegalArgumentException.class, () -> new ZMap( new HashMap(), Collections.emptyList(), null  ));
        assertThrows( IllegalArgumentException.class, () -> new ZMap( new HashMap(), "foo", "bar"  ));
        assertThrows( IllegalArgumentException.class, () -> new ZMap( new HashMap(), Collections.emptyList(), "bar"  ));
        assertThrows( IllegalArgumentException.class, () -> new ZMap( new HashMap(), "foo", Collections.emptyList()  ));

        // copy test
        assertTrue( new ZMap( new HashMap()).copy().map instanceof  HashMap  );
        assertTrue( new ZMap( new LinkedHashMap()).copy().map instanceof  LinkedHashMap  );
        assertTrue( new ZMap( new TreeMap()).copy().map instanceof  TreeMap  );

        // constructor test
        assertTrue( new ZMap().map instanceof HashMap );
        Map<?,?> hm = new HashMap<>();
        Map<?,?> om = new LinkedHashMap<>();
        Map<?,?> sm = new TreeMap<>();

        // assign and do not clone - is this right behaviour? We do not know
        assertSame( hm, new ZMap( hm).map );
        assertSame( om, new ZMap( om).map );
        assertSame( sm, new ZMap( sm).map );

        // clone
        ZMap zm = new ZMap(sm);
        zm.put(1,1);
        ZMap zm2 = new ZMap(zm);
        assertEquals( zm.size(), zm2.size() );
        zm2.put(1,2);
        assertEquals( 1, zm.get(1) );
        assertEquals( 2, zm2.get(1) );

    }

    @Test
    public void passThroughMethodsTest(){
        ZMap zm = new ZMap();
        assertFalse( zm.containsValue(1) );
        zm.put(1,1);
        assertTrue( zm.containsValue(1) );
        assertFalse( zm.containsKey(2));
        Map m = new HashMap();
        m.put(2,2);
        zm.putAll(m);
        assertTrue( zm.containsValue(2) );
    }

    @Test
    public void compareFunctionTest(){
        ZMap zm = new ZMap();
        assertEquals(1, zm.compareTo(null));
        assertEquals(0, zm.compareTo(zm));
        assertThrows(UnsupportedOperationException.class, () -> zm.compareTo("foo-bar"));
        assertEquals(0, zm.compareTo( Collections.emptyList() ));
        assertFalse( zm.equals(null));
        assertEquals(0, zm.hashCode());
        assertTrue( zm.equals(Collections.emptyMap()));
        zm.put(1,1);
        assertFalse( zm.equals(Collections.emptyMap()));
    }

    @Test
    public void stringXFunctionTest(){
        ZMap zm = new ZMap();
        assertEquals("{}", zm.string());
        assertEquals("{}", zm.string("foo"));
    }

    @Test
    public void arithmeticFunctionsTest(){
        ZMap m = new ZMap();
        m.put(1,1);
        assertSame( m, m._add_( new Object[]{1,1} ) );
        Object o = m._add_( new Object[]{1,2} );
        assertNotSame( m, o  );
        assertSame( m, m._sub_( 4 ) );
        m.sub_mutable(1);
        assertEquals( 0, m.size());
        assertThrows( UnsupportedOperationException.class, () -> m._mul_( Function.NIL ));
        assertThrows( UnsupportedOperationException.class, () -> m._pow_( Function.NIL ));
        assertThrows( UnsupportedOperationException.class, () -> m.mul_mutable( Function.NIL ));
        assertThrows( UnsupportedOperationException.class, () -> m.div_mutable( Function.NIL ));

    }

    @Test
    public void logicFunctionsTest(){
        ZMap m = new ZMap();

        assertThrows( UnsupportedOperationException.class, () -> m._and_(Function.NIL));
        assertThrows( UnsupportedOperationException.class, () -> m._or_( Function.NIL ));
        assertThrows( UnsupportedOperationException.class, () -> m._xor_( Function.NIL ));

        assertThrows( UnsupportedOperationException.class, () -> m.and_mutable(Function.NIL));
        assertThrows( UnsupportedOperationException.class, () -> m.or_mutable( Function.NIL ));
        assertThrows( UnsupportedOperationException.class, () -> m.xor_mutable( Function.NIL ));

    }
}
