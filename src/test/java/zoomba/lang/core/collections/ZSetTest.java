package zoomba.lang.core.collections;

import org.junit.Test;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.ZCollection;
import zoomba.lang.core.types.ZRange;

import java.util.*;

import static org.junit.Assert.*;
import static zoomba.lang.core.collections.ZSet.EMPTY;

public class ZSetTest {

    @Test
    public void listFunctionsTest(){
        // regular
        assertEquals(Collections.emptySet(), ZSet.set( null, new ZRange.NumRange(1,1,1)));
        assertEquals(Collections.emptySet(), ZSet.set( null, (Object)null ));

        // ordered
        assertEquals(Collections.emptySet(), ZSet.orderedSet( null));
        assertEquals(Collections.emptySet(), ZSet.orderedSet( null, (Object)null ));
        Set s = ZSet.orderedSet( null, 2,1 );
        assertEquals(2, s.size());
        Iterator it = s.iterator();
        assertEquals(2, it.next());
        assertEquals(1, it.next());
        s = ZSet.orderedSet( null, 42 );
        assertEquals(1, s.size());
        it = s.iterator();
        assertEquals(42, it.next());

        // sorted
        assertEquals(Collections.emptySet(), ZSet.sortedSet( Collections.emptyList(), (Object)null ));
        s = ZSet.sortedSet( Collections.emptyList(), 2,1 );
        assertEquals(2, s.size());
        it = s.iterator();
        assertEquals(1, it.next());
        assertEquals(2, it.next());
        s = ZSet.sortedSet( Collections.emptyList(), 42 );
        assertEquals(1, s.size());
        it = s.iterator();
        assertEquals(42, it.next());
    }

    @Test
    public void sortedSetFunctionsTest(){
        ZSet z = new ZSet(Arrays.asList(1,2,3,4));
        ZSet zs = (ZSet) ZSet.sortedSet( Arrays.asList(Function.COLLECTOR_IDENTITY), 1,2,3,4) ;

        assertNull( z.comparator());
        assertNotNull( zs.comparator());

        // reverse functionality
        ZCollection zc = zs.reverse();
        assertTrue( zc instanceof ZSet );
        assertEquals(4, zc.iterator().next());
        zc = (ZCollection) ZSet.sortedSet( Collections.emptyList(), 1,2,3,4);
        zc = zc.reverse();
        assertTrue( zc instanceof ZSet );
        assertEquals(4, zc.iterator().next());


        assertEquals( EMPTY, z.subSet(3,4) );
        assertEquals( 1, zs.subSet(3,4).size() );

        assertEquals( EMPTY, z.subSet(3,true, 4, true) );
        assertEquals( 2, zs.subSet(3,true, 4, true).size() );

        assertEquals( EMPTY, z.headSet(2) );
        assertEquals( 1, zs.headSet(2).size() );

        assertEquals( EMPTY, z.headSet(2, true) );
        assertEquals( 2, zs.headSet(2, true).size() );

        assertEquals( EMPTY, z.tailSet(4) );
        assertEquals( 1, zs.tailSet(4).size() );

        assertEquals( EMPTY, z.tailSet(4, true) );
        assertEquals( 1, zs.tailSet(4, true).size() );

        assertEquals( Function.NIL, z.first() );
        assertEquals( 1, zs.first() );

        assertEquals( Function.NIL, z.last() );
        assertEquals( 4, zs.last() );

        assertEquals( Function.NIL, z.lower(4) );
        assertEquals( 3, zs.lower(4) );

        assertEquals( Function.NIL, z.higher(2) );
        assertEquals( 3, zs.higher(2) );

        assertEquals( Function.NIL, z.floor(2) );
        assertEquals( 2, zs.floor(2) );

        assertEquals( Function.NIL, z.ceiling(2) );
        assertEquals( 2, zs.ceiling(2) );

        assertEquals( EMPTY, z.descendingSet() );
        assertEquals( 4, zs.descendingSet().size() );

        assertEquals( Collections.emptyIterator(), z.descendingIterator() );
        assertEquals( 4, zs.descendingIterator().next() );

        assertEquals( Function.NIL, z.pollFirst() );
        assertEquals( 4, z.size() );
        assertEquals( 1, zs.pollFirst() );
        assertEquals( 3, zs.size() );

        assertEquals( Function.NIL, z.pollLast() );
        assertEquals( 4, z.size() );
        assertEquals( 4, zs.pollLast() );
        assertEquals( 2, zs.size() );

    }

    @Test
    public void copyTest(){
        final ZSet z = new ZSet();
        final ZSet zo = (ZSet) ZSet.orderedSet(null);
        final ZSet zs = (ZSet) ZSet.sortedSet(Collections.emptyList());
        assertTrue( ((ZSet)z.myCopy()).col instanceof HashSet );
        assertTrue( ((ZSet)zo.myCopy()).col instanceof LinkedHashSet );
        assertTrue( ((ZSet)zs.myCopy()).col instanceof TreeSet );

        assertTrue( ((ZSet)z.setCollector()).col instanceof HashSet );
        assertTrue( ((ZSet)zo.setCollector()).col instanceof LinkedHashSet );
        assertTrue( ((ZSet)zs.setCollector()).col instanceof TreeSet );

    }
}
