package zoomba.lang.core.interpreter;

import org.junit.Test;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.types.ZException;
import zoomba.lang.core.types.ZTypes;

import java.io.*;
import java.util.List;

import static org.junit.Assert.*;
import static zoomba.lang.core.interpreter.DSLPlugin.NANO_TO_SEC;

/**
 */
public class InterpreterScriptTest {

    final String BASIC = "samples/test/basic.zm";

    final String CONDITION = "samples/test/conditions.zm";

    final String ITERATORS = "samples/test/iterators.zm";

    final String FUNCTIONS = "samples/test/functions.zm";

    final String PREDICATES = "samples/test/predicates.zm";

    final String NUMBERS = "samples/test/numbers.zm";

    final String ARITHMETIC = "samples/test/arithmetic.zm";

    final String BIG_MATH_ARITHMETIC = "samples/test/big_math.zm";

    final String ATOMIC = "samples/test/atomic.zm";

    final String RETRY = "samples/test/retry.zm";

    final String IMPORT = "samples/test/import_test.zm";

    final String SCHED = "samples/test/schedulers.zm";

    final String PREC = "samples/test/precision_e.zm" ;

    final String MAP_LOGICAL = "samples/test/map_logical.zm" ;

    final String ZMB_PROXY = "samples/test/proxy_obj.zm" ;

    final String ZMB_REACTIVE = "samples/test/reactive.zm" ;

    final String ZMB_TIME_OUT = "samples/test/timed_out.zm" ;

    final String THREAD_IMPORT = "samples/test/thread_import/a.zm" ;

    final String DSL_TESTS = "samples/test/dsl.zm" ;

    final String REF_EXPR_TESTS = "samples/test/interpreter/ref_expr.zm" ;

    final String TRACES_TESTS = "samples/test/interpreter/traces.zm" ;

    final String TRACES_ERR_SCRIPT_TEST = "samples/test/interpreter/traces_err.zm" ;

    public static void printInts(Integer... args) {
        for (int i : args) {
            System.out.println(i);
        }
    }

    public static void printSomeInts(Integer x, Integer... args) {
        System.out.println(x);
        for (int i : args) {
            System.out.println(i);
        }
    }

    public static void danceWithScript(String scriptLocation) {
        returnDance(scriptLocation);
    }

    public static Function.MonadicContainer justDance(String scriptLocation) {
        System.out.println(ZTypes.orRaise( () -> "################ Running : " + new File(scriptLocation).getCanonicalPath()));
        ZScript script = new ZScript(scriptLocation, null);
        return script.execute();
    }

    public static Function.MonadicContainer returnDance(String scriptLocation) {
        Function.MonadicContainer mc = justDance(scriptLocation);
        if (mc.value() instanceof Throwable) {
            System.err.printf("%s\n", mc.value());
        }
        assertFalse(mc.value() instanceof Throwable);
        return mc;
    }

    @Test
    public void danceWithBasicFile() {
        danceWithScript(BASIC);
    }

    @Test
    public void danceWithConditionFile() {
        danceWithScript(CONDITION);
    }

    @Test
    public void danceWithIteratorFile() {
        danceWithScript(ITERATORS);
    }

    @Test
    public void danceWithFunctionsFile() {
        danceWithScript(FUNCTIONS);
    }

    @Test
    public void danceWithPredicatesFile() {
        danceWithScript(PREDICATES);
    }

    @Test
    public void danceWithNumbersFile() {
        danceWithScript(NUMBERS);
    }

    @Test
    public void danceWithArithmetic() {
        danceWithScript(ARITHMETIC);
    }

    @Test
    public void danceWithBigMath() {
        danceWithScript(BIG_MATH_ARITHMETIC);
    }

    @Test
    public void danceWithAtomic() {
        danceWithScript(ATOMIC);
    }

    @Test
    public void danceWithRetries() {
        danceWithScript(RETRY);
    }

    @Test
    public void danceWithImport() {
        Function.MonadicContainer mc = returnDance(IMPORT);
        assertEquals(42, mc.value());
    }

    @Test
    public void danceWithSchedulers() {
        Function.MonadicContainer mc = returnDance(SCHED);
        assertEquals(55, mc.value());
    }

    @Test
    public void danceWithPrecision() {
        Function.MonadicContainer mc = returnDance(PREC);
        assertEquals(true, mc.value());
    }

    @Test
    public void danceWithMapLogical() {
        Function.MonadicContainer mc = returnDance(MAP_LOGICAL);
        assertEquals(true, mc.value());
    }

    @Test
    public void danceWithProxy() {
        returnDance(ZMB_PROXY);
    }

    @Test
    public void danceWithReactive() {
        Function.MonadicContainer mc = returnDance(ZMB_REACTIVE);
        assertEquals(true, mc.value());
    }

    @Test
    public void danceWithTimeOut() {
        Function.MonadicContainer mc = returnDance(ZMB_TIME_OUT);
        assertEquals(true, mc.value());
    }

    @Test
    public void danceWithThreadImport() {
        Function.MonadicContainer mc = returnDance(THREAD_IMPORT);
        assertEquals(true, mc.value());
    }

    @Test
    public void variousDSLTest(){
        danceWithScript( DSL_TESTS );
    }

    @Test
    public void refExpressionTest(){
        danceWithScript( REF_EXPR_TESTS);
    }

    @Test
    public void tracesTest(){
        danceWithScript( TRACES_TESTS);
    }

    @Test
    public void traceWhenScriptError(){
        Object r = justDance(TRACES_ERR_SCRIPT_TEST).value();
        assertTrue( r instanceof ZException );
        final List<String> trace = ((ZException)r).stack() ;
        assertEquals( 3, trace.size() );
        assertTrue(trace.get(0).contains("bar"));
        assertTrue(trace.get(1).contains("foo"));
        assertTrue(trace.get(2).contains("xxx"));
    }

    @Test
    public void serialization() throws Exception {
        ZScript zs = new ZScript(BASIC,null);
        FileOutputStream fileOut =
                new FileOutputStream("basic.ser");
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        out.writeObject(zs);
        out.close();
        fileOut.close();
        // now read back again...
        FileInputStream fileIn = new FileInputStream("basic.ser");
        ObjectInputStream in = new ObjectInputStream(fileIn);
        ZScript zds = (ZScript) in.readObject();
        in.close();
        fileIn.close();
        // now check if body are same, that is all that matters
        assertEquals(zs.body(), zds.body());
    }

}
