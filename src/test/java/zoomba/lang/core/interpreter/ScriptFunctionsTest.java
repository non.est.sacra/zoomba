package zoomba.lang.core.interpreter;

import org.junit.Test;
import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.ZJVMFunctionalInterface;
import zoomba.lang.core.types.ZException;
import zoomba.lang.core.types.ZTypes;
import zoomba.lang.parser.ASTBlock;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.*;
import static org.junit.Assert.assertThrows;

public class ScriptFunctionsTest {

    public static AnonymousFunctionInstance.MapperLambda as(String code) {
        ZScript zs = new ZScript("{\n" + code + "\n}");
        ZInterpret zi = new ZInterpret(zs);
        zi.frames.add(ZContext.EMPTY_CONTEXT);
        return new AnonymousFunctionInstance.MapperLambda(zi, (ASTBlock) zs.script.jjtGetChild(0));
    }

    public static AnonymousFunctionInstance.PredicateLambda where(String code) {
        ZScript zs = new ZScript("{\n" + code + "\n}");
        ZInterpret zi = new ZInterpret(zs);
        zi.frames.add(ZContext.EMPTY_CONTEXT);
        return new AnonymousFunctionInstance.PredicateLambda(zi, (ASTBlock) zs.script.jjtGetChild(0));
    }

    public static <T> T  castTo(String code){
        ZScript zs = new ZScript(code);
        return (T)zs.execute().value();
    }

    @Test
    public void constructTest() {
        assertNotNull(as("$.o"));
        assertNotNull(where("false"));
    }

    @Test
    public void anonymousArgsGetSetTest() {
        Object r = as("[$.o,$.i]").execute(10, 42).runTimeExceptionOnError().value();
        assertTrue(r instanceof ZArray);
        assertEquals(42, ((ZArray) r).get(0));
        assertEquals(10, ((ZArray) r).get(1));
        r = as("[$.p, $.o,$.i]").execute(10, 42, Collections.emptySet()).runTimeExceptionOnError().value();
        assertTrue(r instanceof ZArray);
        assertSame(Collections.emptySet(), ((ZArray) r).get(0));
        assertEquals(42, ((ZArray) r).get(1));
        assertEquals(10, ((ZArray) r).get(2));
        AnonymousFunctionInstance.AnonymousArg args = new AnonymousFunctionInstance.AnonymousArg();
        assertEquals(5, args.size());
        assertFalse(args.isEmpty());
        assertFalse(args.containsKey("foo-bar"));
        assertFalse(args.containsValue("foo-bar"));
        assertEquals(Function.NIL, args.get("l"));
        assertEquals(Function.NIL, args.get("r"));
        assertEquals(Function.NIL, args.get("k"));
        assertEquals(Function.NIL, args.get("v"));
        assertEquals(Function.NIL, args.get("t"));


        args.setArgs(42, Arrays.asList(0,1));
        assertEquals(0, args.get(0));
        assertEquals(1, args.get(1));
        assertEquals(Arrays.asList(0,1), args.get("t"));

        args.setArgs(42, "hi".toCharArray());
        assertTrue(args.get("t").getClass().isArray());
        assertEquals('h', args.get(0));
        assertEquals('i', args.get(1));

        args.setArgs(42, "hi");
        assertEquals(Function.NIL, args.get("foo-bar"));
        args.setArgs(42, null);
        assertEquals(Function.NIL, args.get(0));

        args.put("p", 42);
        assertEquals(42, args.get("p"));
        assertEquals(Function.NIL, args.put("xxxx", 0 ));
    }

    @Test
    public void anonymousArgsOtherFunctionsTest(){
        AnonymousFunctionInstance.AnonymousArg args = new AnonymousFunctionInstance.AnonymousArg();
        assertEquals(Function.NIL, args.remove("x"));
        args.putAll(Collections.emptyMap());
        args.clear();
        assertSame(AnonymousFunctionInstance.AnonymousArg.KEY_SET, args.keySet());
        assertTrue( args.entrySet().size() > 0 );
    }

    @Test
    public void overrideFunctionsTest(){
        AnonymousFunctionInstance where = where("false");
        ZContext zc = new ZContext.FunctionContext();
        where.runContext(zc);
        assertTrue( ((ZContext.MapContext)where.runContext()).map.isEmpty() );
        assertEquals("{\nfalse\n}", where.body() );
        assertTrue(where.toString().contains("false") );
    }

    @Test
    public void mapperLambdaTest(){
        AnonymousFunctionInstance.MapperLambda as = as( "'hello!'");
        assertEquals( "#as", as.name() );
        assertEquals( "hello!", as.map() );
        assertEquals( 42, as("").map(0, 42) );
        as = as( "panic(true)");
        Consumer c = as.consumer(42);
        c.accept(10);
    }

    @Test
    public void predicateLambdaTest(){
        AnonymousFunctionInstance.PredicateLambda where = where( "false");
        assertFalse( where.accept() );
        assertEquals( "#where", where.name() );
        where.consumer( "hi").accept( "foo");
        where = where( "panic(true)");
        BiConsumer bc = where.biConsumer(42);
        bc.accept(10, 32);
    }

    @Test
    public void runContextTest(){
        AnonymousFunctionInstance.MapperLambda as = as( "'hello!'");
        as.execute();
        ZContext zc = new ZContext.ArgContext( List.of("hi"), List.of( Function.FAILURE ));
        as.runContext( zc );
        assertFalse( as.runContext().has("hi") );
    }

    @Test
    public void equitableArrayTest(){
        ZScriptMethod.EquitableArray arr = new ZScriptMethod.EquitableArray( new Object[]{ 1,2,3}) ;
        assertTrue( arr.equals(arr));
        assertFalse( arr.equals(null));
        assertFalse( arr.equals(42));

    }

    @Test
    public void methodTest(){
        assertThrows( RuntimeException.class, () -> new ZScript("def func( a, a){}"));
        ZScript zs = new ZScript( "x = def(a){a}" );
        Object o = zs.execute().value();
        assertTrue( o instanceof ZScriptMethod );
        ZScriptMethod id = (ZScriptMethod) o;
        assertEquals( "", id.name );

        // cache test
        id.cache(2);
        assertEquals( 2, id.cache() );
        id.cache(-10);
        assertEquals( 2, id.cache() );
        ZScriptMethod.ZScriptMethodInstance instance = id.instance( new ZInterpret(zs) );
        assertEquals( "", instance.name() );
        for ( int i=0; i < 10; i++) {
            assertEquals(i , instance.execute(i).value()) ;
        }
    }

    @Test
    public void scriptMethodTest(){
        assertThrows( IllegalArgumentException.class, () -> new ZScriptMethod(null, null, null ));
        ZScriptMethod sm = castTo("def(){42}" ) ;
        assertTrue( sm.isAnonymous() );
        sm = castTo("def x(){42}" ) ;
        assertFalse( sm.isAnonymous() );
        ZScriptMethod.ZScriptMethodInstance instance = castTo("def x(){ 42 } ; new(x)") ;
        assertEquals("x", instance.method().name );

        // failed error handling
        Object o = castTo("def err(){ panic(true) } ; f = def(){42} ; f.before += err; f.after += err; f()");
        assertEquals(42, o);
    }

    private Object useSupplier(Supplier s){
       return s.get();
    }

    private Object useBiFunction(BiFunction bf){
        return bf.apply(42,42);
    }

    private boolean useBiPredicate(BiPredicate bp){
        return bp.test(42,42);
    }

    private void useBiConsumer(BiConsumer bc){
        bc.accept(42,42);
    }

    @Test
    public void scriptMethodVoidFunctionalInterfaceTest(){
        try {
            // https://gitlab.com/non.est.sacra/zoomba/-/issues/147
            // void returning
            AnonymousFunctionInstance.MapperLambda as = as( "printf('.')");
            Stream.of(1).forEach( (Consumer<? super Integer>) as.coerce( Consumer.class) );
            useBiConsumer( (BiConsumer)as.coerce( BiConsumer.class) );
            // throws for the follows
            assertThrows( RuntimeException.class, () -> Stream.of(1).map(as).collect(Collectors.toList()) );
            assertThrows( RuntimeException.class, () -> Stream.of(1).
                    filter( (Predicate<? super Integer>) as.coerce( Predicate.class ) ).collect(Collectors.toList()) );
            assertThrows( RuntimeException.class, () -> ZTypes.orRaise( as.coerce( Callable.class) ) );
            assertThrows( RuntimeException.class, () -> useSupplier( as.coerce( Supplier.class) ) );
            assertThrows( RuntimeException.class, () -> useBiFunction( (BiFunction)as.coerce( BiFunction.class) ) );
            assertThrows( RuntimeException.class, () -> useBiPredicate( (BiPredicate)as.coerce( BiPredicate.class) ) );
            as.run(); // no error

        } finally {
            // ensure new line is added
            System.out.println();
        }
    }

    @Test
    public void scriptMethodErrorInFunctionalInterfaceTest(){
       // https://gitlab.com/non.est.sacra/zoomba/-/issues/147
        // void returning
        AnonymousFunctionInstance.MapperLambda as = as( "panic('Boom!')");
        final Class assertErrClass = ZException.ZRuntimeAssertion.class ;
        assertThrows(assertErrClass , () -> Stream.of(1).forEach( (Consumer<? super Integer>) as.coerce( Consumer.class) ) );
        assertThrows(assertErrClass, ()-> useBiConsumer( (BiConsumer)as.coerce( BiConsumer.class) ) );
        // throws for the follows
        assertThrows( assertErrClass, () -> Stream.of(1).map(as).collect(Collectors.toList()) );
        assertThrows( assertErrClass, () -> Stream.of(1).
                filter( (Predicate<? super Integer>) as.coerce( Predicate.class ) ).collect(Collectors.toList()) );
        assertThrows( assertErrClass, () -> ZTypes.orRaise( as.coerce( Callable.class) ) );
        assertThrows( assertErrClass, () -> useSupplier( as.coerce( Supplier.class) ) );
        assertThrows( assertErrClass, as::run);

    }
}
