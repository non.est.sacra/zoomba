package zoomba.lang.core.interpreter;

import org.junit.Test;
import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.Observable;
import zoomba.lang.parser.ZoombaNode;

import java.util.concurrent.Callable;
import java.util.function.Consumer;

import static org.junit.Assert.*;

public class ZAssertionTest {

    private final ZoombaNode zn = new ZScript("x=10").script;
    private final Observable<Boolean> obs = new Observable.ObservableBase<>() {
        @Override
        public Callable lazy() {
            return this::lastValue;
        }
    };
    private final Function anon = new Function() {
        @Override
        public String body() {
            return "";
        }

        @Override
        public MonadicContainer execute(Object... args) {
            return new MonadicContainerBase(new RuntimeException("boom"));
        }

        @Override
        public String name() {
            return "";
        }
    };

    @Test
    public void assertionTest(){
        assertFalse( ZAssertion.AssertionType.TEST.failed(true) );
        assertFalse( ZAssertion.AssertionType.PANIC.failed(false) );
        assertFalse( ZAssertion.AssertionType.ASSERT.failed(true) );

        assertTrue( ZAssertion.AssertionType.TEST.failed(false) );
        assertTrue( ZAssertion.AssertionType.PANIC.failed(true) );
        assertTrue( ZAssertion.AssertionType.ASSERT.failed(false) );

    }

    @Test
    public void assertionEventTest(){
        ZAssertion.AssertionEvent evt = new ZAssertion.AssertionEvent( "foo",
                ZAssertion.AssertionType.TEST, false, new Throwable(), ZArray.EMPTY_ARRAY );

        assertTrue( evt.toString().contains( "!!!"));
        evt = new ZAssertion.AssertionEvent( "foo",
                ZAssertion.AssertionType.TEST, true, new Throwable(), ZArray.EMPTY_ARRAY );
        assertFalse( evt.toString().contains( "!!!"));

    }

    @Test
    public void testAssertFunctionTest(){
        final ZAssertion.AssertionEvent[] wasCalled = new ZAssertion.AssertionEvent[] { null };
        Consumer<ZAssertion.AssertionEvent> c = (ae) -> wasCalled[0] = ae;
        ZAssertion za = new ZAssertion();
        za.eventListeners.add(c);

        obs.updateValue( false );

        za.testAssert( ZAssertion.AssertionType.TEST, zn , null, obs, "boom!" );
        assertNotNull( wasCalled[0] );
        wasCalled[0] = null;
        za.testAssert( ZAssertion.AssertionType.TEST, zn , anon, "boom!" );
        assertNotNull( wasCalled[0] );

        za.eventListeners.clear();
        final Consumer<ZAssertion.AssertionEvent> e = (ae) -> {
            wasCalled[0] = ae;
            throw new RuntimeException("boom!");
        };
        za.eventListeners.add(e);
        za.testAssert( ZAssertion.AssertionType.TEST, zn , null, obs, "boom!" );
        assertNotNull( wasCalled[0] );

        za.eventListeners.clear();

    }
}
