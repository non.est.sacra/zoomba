package zoomba.lang.core.interpreter;

import org.junit.Test;
import zoomba.lang.core.operations.Function;

import java.util.Arrays;

import static org.junit.Assert.*;
import static zoomba.lang.core.interpreter.ZContext.EMPTY_CONTEXT;

public class ZContextTest {

    @Test
    public void emptyContextTest(){
        assertSame( EMPTY_CONTEXT, EMPTY_CONTEXT.parent());
        assertFalse( EMPTY_CONTEXT.has("x"));
        assertFalse( EMPTY_CONTEXT.unSet("x"));
        assertSame( ZContext.NOTHING, EMPTY_CONTEXT.get("x"));
        EMPTY_CONTEXT.set("x", 42);
        EMPTY_CONTEXT.clear();
    }

    @Test
    public void argsContextTest(){
        ZContext.ArgContext argContext = new ZContext.ArgContext(0,1,2);
        assertSame( EMPTY_CONTEXT, argContext.parent());
        assertFalse( argContext.unSet("x"));
        assertSame( ZContext.NOTHING, argContext.get("x"));
        argContext.set("$", 10 );
        argContext.set("@ARGS", 10 );
        argContext.set("x", 10 );
        argContext = new ZContext.ArgContext( Arrays.asList("hhgg") , Arrays.asList( new Function.MonadicContainerBase(42)) );
        assertEquals( 42, argContext.get("hhgg").value() );
        argContext.set("hhgg", 100 );
        assertEquals( 100, argContext.get("hhgg").value() );
    }

    @Test
    public void mapContextTest(){
        ZContext.MapContext mc = new ZContext.MapContext();
        mc.put("x", 42);
        assertTrue(  mc.containsValue(42));
        assertEquals(1, mc.keySet().size());
        assertEquals(1, mc.values().size());
        assertEquals( 42, mc.remove("x"));

        assertSame( EMPTY_CONTEXT, mc.parent() );
        assertFalse( mc.unSet("x"));
        mc.put("x", 42);
        mc.clear();
        assertTrue(  mc.isEmpty());
    }
}
