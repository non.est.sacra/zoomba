package zoomba.lang.core.interpreter;

import org.junit.Test;
import zoomba.lang.core.operations.Function;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import static org.junit.Assert.*;

public class ZInterpretTest {

    static final String PARSE_FAILURE = "samples/test/interpreter/parse_fail" ;

    static final String IMPORT_SCRIPT = "samples/test/tbi" ;

    public static Function.MonadicContainer runScript(String text){
        return new ZScript(text).execute();
    }

    public static Object runSafe(String text){
        return runScript(text).runTimeExceptionOnError().value();
    }

    @Test
    public void typeOfTest(){
        assertEquals(Function.NIL.getClass(), ZInterpret.type());
        assertEquals(Function.NIL.getClass(), ZInterpret.type((Object) null));
        assertEquals(Class.class, ZInterpret.type(String.class));
    }

    @Test
    public void assignToNodeTests(){
        assertEquals( 42, runSafe("x = [1,2] ; #(x.0 ) = [ 42 ] ; x.0" )) ;
        // TODO later more - could not think much
    }

    @Test
    public void danceMasterTest(){
        ZScript zs = new ZScript("42");
        ZInterpret zi = new ZInterpret(zs);
        assertSame(zs, zi.script());
        assertSame( Function.NIL, runSafe("return" ) );
        assertEquals( false, runSafe( "del $x") );
        assertThrows( RuntimeException.class, () -> runSafe( "if(null){ println('null')}"));
        assertEquals( 1, runSafe( "x = false ; x ? 0 : 1 "));
        assertEquals( 0, runSafe( "x = true ; x ? 0 : 1 "));

        assertEquals(true, runSafe( "null .= null" ));
        assertEquals(false, runSafe( "0 .= null" ));
        assertEquals(false, runSafe( "null .= 0" ));
        assertEquals( true, runSafe( "def x(){} ; del x; y = x ; true"));

        zs = new ZScript("'42' =~ y");
        HashMap hm = new HashMap();
        Pattern y = Pattern.compile("\\d+");
        hm.put("y", y);
        assertEquals( true,  zs.eval( hm ).runTimeExceptionOnError().value() );
        zs = new ZScript("'42' !~ y");
        assertEquals( false,  zs.eval( hm ).runTimeExceptionOnError().value() );
    }

    @Test
    public void multipleReturnTest(){
        runSafe( "#(a,b,c) = null");
        runSafe( "#(a,b,c) = 'hello'");
        runSafe( "#(,b,c) = 'hello'");
        runSafe( "#(,b,c) = set(1,2)");

    }

    @Test
    public void loopConstructsTest(){
        assertEquals(Arrays.asList(0), runSafe("list([0:5]) as {  if ( $.i % 2 == 1 ) break; $. o }" ) );
        assertEquals(Arrays.asList(0,2), runSafe("list([0:3]) as {  if ( $.i % 2 == 1 ) continue; $. o }" ) );
        assertEquals( 42, runSafe( "while(true){ break(true){ 42 } }") );
        assertEquals( 42, runSafe( "i=0; while( i < 1){ i+=1; continue(true){ 42 } }") );
        assertEquals( 42, runSafe("for([0:4]); 42") ); // this makes sense
        assertEquals( 42, runSafe("for( x: [0:4] ); 42") ); // this does not make sense...
        assertEquals(false, runSafe("for(33); false"));
        assertEquals( 42, runSafe("for([0:1]){ break(true){ 42 }  }"));
        assertEquals( 42, runSafe("for([0:1]){ continue(true){ 42 }  }"));
        assertEquals( 1, runSafe("i =0; for( ;i<1; i+=1); i"));
        assertEquals( 42, runSafe("for( i=0; i<1;i+=1 ){ break(true){ 42 }  }"));
        assertEquals( 42, runSafe("for( i=0; i<1;i+=1 ){ continue(true){ 42 }  }"));

    }

    @Test
    public void scriptFailureTest(){
        final String script = String.format("import '%s' as I", PARSE_FAILURE);
        assertThrows( RuntimeException.class, () -> runSafe( script));
        assertThrows( RuntimeException.class, () -> runSafe( "1/0" ));
        assertThrows( RuntimeException.class, () -> runSafe( "x = 0; x += {:}" ));
        assertThrows( RuntimeException.class, () -> runSafe( "x = null ; x.toString()" ));
    }

    @Test
    public void arithmeticTest(){
        assertThrows( RuntimeException.class, () -> runSafe( "{:} ** 2") );
        assertThrows( RuntimeException.class, () -> runSafe( "{:} * 2") );
        assertThrows( RuntimeException.class, () -> runSafe( "{:} % 2") );
        assertThrows( RuntimeException.class, () -> runSafe( "!2") );

        assertEquals(-2, runSafe("~1"));
        assertEquals(1, runSafe("~(-2)"));
        assertThrows( RuntimeException.class, () -> runSafe( "~'foo'") );

    }

    @Test
    public void methodCallTest(){
        final String script = String.format("import '%s' as f; f() ; 42", IMPORT_SCRIPT);
        assertEquals(42, runSafe(script) );
        assertEquals(42, runSafe("def x(){42} ; def y(a){ a() }; y(x) ") );
        assertEquals(42, runSafe(" d = { 'm' : def(){ 42} } ; d.m() ") );

        assertThrows(RuntimeException.class, () ->runSafe(" d = { 'm' : def(){ 1/0 } } ; d.m() ") );
        assertThrows( RuntimeException.class, () -> runSafe("x=22 ; x()") );

        // method args issues
        assertThrows( RuntimeException.class, () -> runSafe("def x(a,b){ a + b }; x(a=10,12)") );
        assertThrows( RuntimeException.class, () -> runSafe("def x(a,b){ a + b }; x(a=10,@ARGS = [1,2])") );
        assertThrows( RuntimeException.class, () ->runSafe("def x(a,b){ a + b }; x( @ARGS = 'hello' )"));
        assertThrows( RuntimeException.class, () ->runSafe("def x(a,b){ a + b }; x( @ARGS = { 'a' : 10 } )"));
        assertThrows( RuntimeException.class, () ->runSafe("def x(a,b){ a + b }; x(10)"));

    }

    @Test
    public void constructObjectsTest(){
       Object o = runSafe( "def x : { a: 42 }; new ( 'x' ) ");
       assertTrue( o instanceof Map);
       final String script = String.format("import '%s' as f; new ( 'f.CrazyObj' )", IMPORT_SCRIPT);
       o = runSafe( script );
       assertTrue( o instanceof Map);
        assertThrows( RuntimeException.class,
                () -> runSafe("def x : { t : null } ; new (x, t = assert(false) )" ));
    }

    @Test
    public void referenceAccessTest(){
        assertEquals(0, runSafe( "([0,1,2])[0]"));
    }
}
