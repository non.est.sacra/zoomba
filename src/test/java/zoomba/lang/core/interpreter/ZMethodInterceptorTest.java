package zoomba.lang.core.interpreter;

import org.junit.Test;
import zoomba.lang.core.collections.BaseZCollection;
import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.collections.ZMap;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.sys.ZProcess;
import zoomba.lang.core.sys.ZThread;
import zoomba.lang.core.types.ZTypes;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.ThreadPoolExecutor;

import static org.junit.Assert.*;
import static zoomba.lang.core.interpreter.ZMethodInterceptor.DEFAULT_INTERCEPTOR;
import static zoomba.lang.core.interpreter.ZMethodInterceptor.Default;
import static zoomba.lang.core.interpreter.ZMethodInterceptor.Default.*;


public class ZMethodInterceptorTest {

    final static String SAMPLE_JSON_FILE = "samples/sample.json";
    final static String EMPTY_FILE = "samples/test/io/data-files/empty.csv" ;

    @Test
    public void jxFunctionsTest() {
        final Object js = ZTypes.json(SAMPLE_JSON_FILE, true);
        // path
        assertNull(Default.jxPath());
        assertNull(Default.jxPath(1));
        Object o = Default.jxPath( js, "//menuitem/value", false);
        assertFalse(  o instanceof Collection);
        o = Default.jxPath( js, "//menuitem/value", true);
        assertTrue(  o instanceof Collection);

        // pointers
        assertNull(Default.jxElement());
        assertNull(Default.jxElement(1));
        o = Default.jxElement( js, "//menuitem/value", false);
        assertFalse(  o instanceof Collection);
        o = Default.jxElement( js, "//menuitem/value", true);
        assertTrue(  o instanceof Collection);
    }

    @Test
    public void utilsMethodsTest(){
        assertEquals(Collections.emptyList(), Default.convertNoDoubleWrapSynchronized( null ) );
        assertTrue( Default.convertNoDoubleWrapSynchronized( new ArrayList<>()) instanceof List);
        assertTrue( Default.convertNoDoubleWrapSynchronized( new HashSet<>()) instanceof Set);
        assertTrue( Default.convertNoDoubleWrapSynchronized(
                new BaseZCollection.ZWrappedCollection(Collections.emptyList())) instanceof Collection);
        assertEquals(Collections.emptyList(), Default.convertNoDoubleWrapSynchronized( "hi" ) );
        assertEquals(Collections.emptyList(), Default.concurrent( Collections.emptyList() ) );

        assertTrue( Default.empty(ZArray.EMPTY_ARRAY));
        assertFalse( Default.empty( new Object[]{ 42 } ));
        assertFalse( Default.empty( new File( SAMPLE_JSON_FILE ) ));
        assertTrue( Default.empty( new File( EMPTY_FILE ) ));

        assertEquals(-1, Default.size() );
        assertEquals(0, Default.size(Function.NIL) );
        assertEquals(42, Default.size(42, 0) );
        assertEquals(42, Default.size(42, "foo") );
        assertThrows( UnsupportedOperationException.class, () -> Default.size(Function.TRUE) );
    }

    @Test
    public void readTest(){
        final InputStream original = System.in;
        Object res;
        try {
            final String hhgg = "42";
            // read from system in
            InputStream stream = new ByteArrayInputStream(hhgg.getBytes(StandardCharsets.UTF_8));
            System.setIn(stream);
            res = Default.read();
            System.setIn(original);
            assertEquals( hhgg, res);
            stream = new ByteArrayInputStream(new byte[]{});
            System.setIn(stream);
            res = Default.read();
            System.setIn(original);
            assertNull(res);
            // https://gitlab.com/non.est.sacra/zoomba/-/issues/129
            final String multiLineText = "a b c\nd e f" ;
            stream = new ByteArrayInputStream( multiLineText.getBytes() );
            System.setIn(stream);
            res = Default.read(false);
            System.setIn(original);
            assertEquals( multiLineText, res );

            stream = new ByteArrayInputStream( multiLineText.getBytes() );
            System.setIn(stream);
            res = Default.read(true);
            System.setIn(original);
            assertTrue( res instanceof List<?>);
            assertEquals( 2, ((List<?>) res).size());
            assertEquals( "a b c", ((List<?>) res).get(0));
            assertEquals( "d e f", ((List<?>) res).get(1));

        } finally {
            System.setIn(original);
        }

        // read from web
        res = Default.read("https://www.google.co.in", 10000, 10000, Collections.emptyMap() );
        assertNotNull(res);
    }

    @Test
    public void threadFunctionTest() throws InterruptedException {
        Runnable t = Default.thread( false, true, null, Function.TRUE );
        assertTrue( t instanceof ZThread );
        Thread.sleep(100);
        assertEquals( true, ((ZThread)t).value() ) ;
        assertThrows( IllegalArgumentException.class,
                () -> Default.thread( false, true, null, "hello" ) );

        assertTrue( Default.schedule( null, null) instanceof ThreadPoolExecutor);
    }

    @Test
    public void printFunctionsTest(){
        final PrintStream  original = System.out;

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream( baos );
        System.setOut(ps);
        Default.print(false, (Object[]) null);
        Default.print(false, (Object) null);
        System.setOut(original);
        assertEquals("null\nnull\n", baos.toString());
        Default.formatPrint(false);
    }

    @Test
    public void joinTest(){
        assertEquals( Collections.emptyList(), Default.join( Collections.emptyList()));
        assertEquals( Collections.emptyList(), Default.join( Collections.emptyList(), (Object) null ));
        assertEquals( Collections.emptyList(), Default.join( Collections.emptyList(), Collections.emptyList()));
        assertThrows( UnsupportedOperationException.class,
                ()-> Default.join( Collections.emptyList(), Collections.emptyList(), "hi" ) );
        assertThrows( UnsupportedOperationException.class,
                ()-> Default.join( Collections.emptyList(), "hi", "hi" ) );
    }

    @Test
    public void multiSetTest(){
        assertSame(ZMap.EMPTY, Default.multiSet( null, ZArray.EMPTY_ARRAY ));
        Function.NamedArgs na = new Function.NamedArgs();
        na.put("acc", "hi");
        assertThrows( IllegalArgumentException.class, () -> Default.accumulatorMap(na));
        na.clear();
        assertTrue( Default.accumulatorMap(na) instanceof HashMap );
    }

    @Test
    public void selectFunctionsTest(){
        assertSame( Collections.emptyList(), Default.select( ZArray.EMPTY_ARRAY, null, Collections.emptyList() ));
        assertSame( Collections.emptyList(), Default.select( new Object[]{ null }, null, Collections.emptyList() ));
        assertSame( Collections.emptyList(), Default.select( new Object[]{ Arrays.asList(1,2,3) }, null, Collections.emptyList() ));

        assertSame( Default.EMPTY_PARTITIONS, Default.partition( null, ZArray.EMPTY_ARRAY ));
        assertSame( Default.EMPTY_PARTITIONS, Default.partition(  null, new Object[]{ null } ));
        assertSame( Default.EMPTY_PARTITIONS, Default.partition(  null, new Object[]{ 1,2,3 } ));
        assertSame( Default.EMPTY_PARTITIONS, Default.partition( null, new Object[]{ Arrays.asList(1,2,3)} ));
    }

    @Test
    public void higherOrderFunctionsFoldVariantsTest(){
        assertSame( Function.NIL, Default.leftFold(null, Function.COLLECTOR_IDENTITY) );
        assertSame( Function.NIL, Default.leftFold(0, null) );

        assertSame( Function.NIL, Default.leftReduce(null, Function.COLLECTOR_IDENTITY) );
        assertSame( Function.NIL, Default.leftReduce(0, null) );

        assertSame( Function.NIL, Default.rightFold(null, Function.COLLECTOR_IDENTITY) );
        assertSame( Function.NIL, Default.rightFold(0, null) );

        assertSame( Function.NIL, Default.rightReduce(null, Function.COLLECTOR_IDENTITY) );
        assertSame( Function.NIL, Default.rightReduce(0, null) );

        assertSame( Function.NOTHING, Default.find(null, Function.COLLECTOR_IDENTITY) );
        assertSame( Function.NOTHING, Default.find(0, null) );

        assertEquals(-1, Default.leftIndex(null, null ));
        assertEquals(-1, Default.rightIndex(null, null ));
    }

    private Function.MonadicContainer intercept(String method, Object[] args, Function anon){
        ZScript zs = new ZScript("");
        ZInterpret zi = new ZInterpret(zs);
        return DEFAULT_INTERCEPTOR.intercept( null, zi,method, args, Arrays.asList(anon) , zs.script );
    }

    @Test
    public void interceptorFailuresTest(){
        assertSame( Function.FAILURE, intercept( FIND, ZArray.EMPTY_ARRAY, null ) );
        assertSame( Function.NOTHING, intercept( LREDUCE, ZArray.EMPTY_ARRAY, null ) );
        assertSame( Function.NOTHING, intercept( RREDUCE, ZArray.EMPTY_ARRAY, null ) );
        assertSame( Function.FAILURE, intercept( ASYNC, ZArray.EMPTY_ARRAY, null ) );
        assertSame( Function.FAILURE, intercept( MINMAX, ZArray.EMPTY_ARRAY, null ) );
    }

    @Test
    public void interceptorActualReturnsTest(){
        assertEquals( "{}",
                intercept( JSON_STRING, new Object[]{ new HashMap<>(), false, "" }, null ).value() );
        assertTrue(
                intercept( V_TASK, ZArray.EMPTY_ARRAY, Function.COLLECTOR_IDENTITY ).value() instanceof Runnable );
        assertTrue(
                intercept( POPEN, new Object[]{ "ls", "-al" }, null ).value() instanceof ZProcess);
    }
}
