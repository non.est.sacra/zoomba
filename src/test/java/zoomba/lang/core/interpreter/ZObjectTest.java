package zoomba.lang.core.interpreter;

import org.junit.Test;
import zoomba.lang.core.oop.ZObject;

import java.util.Collections;
import java.util.HashMap;

import static org.junit.Assert.*;

// putting it here cause OOP is sort of under default interpreter mechanism
public class ZObjectTest {

    public static ZObject getZObject(String def){
        Object o = ZInterpretTest.runSafe( def);
        assertTrue( o instanceof ZObject);
        return (ZObject)o;
    }

    @Test
    public void basicFunctionsTest(){
        ZObject zo = getZObject( "def x : { : }");
        assertEquals( "x", zo.get("name"));
        assertEquals( "x", zo.put("name", "xxxx"));
        assertEquals( "x", zo.get("name"));
        ZObject zo2 = getZObject( "def x : { : }");
        assertFalse( zo.equals( null ));
        assertFalse( zo.equals( new HashMap<>()));
        assertTrue( zo.equals( zo ));
        assertTrue( zo.equals( zo2 ));
        ZObject zo3 = getZObject( "def y: { : }");
        assertFalse( zo.equals( zo3 ));
        zo2.put("y", 42);
        assertFalse( zo.equals( zo2 ));
        assertFalse( zo2.equals( zo ));
        assertSame(Collections.emptyIterator(), zo.iterator());
    }

    @Test
    public void hashFunctionTest(){
        ZObject zo = getZObject( "def X : { : }; X.$hc = def() { 'foo' }; X ");
        assertThrows( IllegalStateException.class, zo::hashCode);
        zo = getZObject( "def X : { : }; X.$hc = def() { '42' }; X ");
        assertEquals( 42, zo.hashCode());
    }

    @Test
    public void stringFunctionTest(){
        ZObject zo = getZObject( "def X : { : }; X.$str = def() { 'foo' }; X ");
        assertEquals( "foo", zo.string());
        assertEquals( "foo", zo.string(1));
        zo = getZObject( "def X : { : }");
        assertEquals( "{}", zo.string(1));
    }

    @Test
    public void invalidFunctionTest(){
        final ZObject zo = getZObject( "def X : { : }");
        assertThrows( UnsupportedOperationException.class, () -> zo._add_(0));
        assertThrows( UnsupportedOperationException.class, () -> zo.add_mutable(0));
        final ZObject zo2 = getZObject( "def X : {:} ; X.$add = def(x){ x }; X");
        assertThrows( UnsupportedOperationException.class, () -> zo2.add_mutable(0));
    }

    @Test
    public void compareFunctionTest(){
        assertEquals( 1, getZObject( "def X : {:} ; X.$cmp = def(x){ false }; X").compareTo(10) );
        assertEquals(  -1, getZObject( "def X : {:} ; X.$cmp = def(x){ true }; X").compareTo(10) );
    }

    @Test
    public void logicalMutableFunctionTest(){
        final ZObject zo = getZObject( "def X : { v : false } ;" +
                " X.$and = def(x){  new (x, v = (x.v && $.v) ) }; " +
                " X.$or = def(x){  new (x, v = (x.v || $.v) ) }; " +
                " X.$xor = def(x){  new (x, v = (x.v ^ $.v) ) }; " +
                "X");
        zo.put("v", true);
        zo.and_mutable(zo);
        assertEquals( true,zo.get("v"));
        zo.or_mutable(zo);
        assertEquals( true,zo.get("v"));
        zo.xor_mutable(zo);
        assertEquals( false,zo.get("v"));

    }
}
