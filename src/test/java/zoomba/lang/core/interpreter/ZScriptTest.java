package zoomba.lang.core.interpreter;

import org.junit.Test;
import zoomba.lang.core.operations.Function;

import java.io.BufferedReader;
import java.io.StringReader;

import static org.junit.Assert.*;

public class ZScriptTest {

    static final String BAD_FILE = "samples/test/interpreter/parse_fail.zm" ;
    static final String PROTO_FILE = "samples/test/proto_class.zm" ;

    @Test
    public void basicFunctionsTest(){
        ZScript zs = new ZScript("i=42");
        assertTrue( zs.isMain() );
        assertNotNull( zs.assertion() );
        assertEquals("__INLINE__", zs.name());
        assertEquals(".", zs.baseDir());
        assertNull( zs.parent() );
        assertTrue( zs.inline());
        assertFalse( zs.toString().isEmpty() );
    }

    @Test
    public void utilityMethodsTest() throws Exception {
        assertEquals( "", ZScript.readerToString( new BufferedReader( new StringReader(""))) );
        assertNull( ZScript.cleanExpression(null));
        assertEquals( "", ZScript.cleanExpression( "" ) );
    }

    @Test
    public void initTest(){
        assertThrows( RuntimeException.class , () -> new ZScript("s="));
        assertThrows( RuntimeException.class , () -> new ZScript( "not-existing-file.zm", null ));
        assertThrows( RuntimeException.class , () -> new ZScript( BAD_FILE, null ));
    }

    @Test
    public void contextAccessMethodsTest(){
        ZScript zs = new ZScript( PROTO_FILE, null);
        zs.runContext( new ZContext.FunctionContext());
        zs.execute().runTimeExceptionOnError();
        Object o = zs.get("Complex") ;
        assertNotEquals(Function.NIL, o);
        assertEquals(Function.NIL, zs.get("foo-bar"));
        assertEquals( 42, zs.set("e", 42));
        assertEquals(Function.NIL, zs.set("foo-bar", 42 ));
        assertTrue( zs.trace().isEmpty() );
        zs.consumeTrace( (s) ->{} );
        assertFalse( zs.toString().isEmpty() );
    }
}
