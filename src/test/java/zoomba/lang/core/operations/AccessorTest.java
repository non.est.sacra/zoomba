package zoomba.lang.core.operations;

import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.ri.model.NodePointer;
import org.junit.Test;
import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.collections.ZList;
import zoomba.lang.core.collections.ZMap;
import zoomba.lang.core.types.ZException;
import zoomba.lang.core.types.ZTypes;

import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.*;
import java.util.function.Consumer;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static zoomba.lang.core.interpreter.ZMethodInterceptor.Default.myPath;
import static zoomba.lang.core.operations.ZJVMAccess.*;


/**
 */
public class AccessorTest {

    final Object[] arr = new Object[ ]{ 1, 2, 3, 4, 5, 6 } ;

    final List list = new ZList(arr);

    @Test
    public void testIntegerIndexAccessor(){
        Function.MonadicContainer c =  ZJVMAccess.getProperty(arr,0);
        assertFalse( c.isNil() );
        assertTrue( c.value() instanceof Integer );

        c =  ZJVMAccess.getProperty(arr,arr.length-1);
        assertFalse( c.isNil() );
        assertTrue( c.value() instanceof Integer );

        c =  ZJVMAccess.getProperty(arr,arr.length );
        assertTrue( c.isNil() );
        assertTrue( c == INVALID_PROPERTY );

        c =  ZJVMAccess.setProperty(arr, 0, 'A');
        assertFalse( c.isNil() );
        assertTrue( c == Function.SUCCESS );
        assertEquals( 'A', arr[0] );


        c = ZJVMAccess.getProperty(list,0);
        assertFalse( c.isNil() );
        assertTrue( c.value() instanceof Integer );

        c =  ZJVMAccess.setProperty(list, 1, 'B');
        assertFalse( c.isNil() );
        assertEquals( 'B', list.get(1) );


        c = ZJVMAccess.getProperty(list,"size" );
        assertFalse( c.isNil() );
        assertTrue( c.value() instanceof Integer );
        int size = (int)c.value() ;

        c =  ZJVMAccess.getProperty(list, size - 1 );
        assertFalse( c.isNil() );
        assertTrue( c.value() instanceof Integer );

        c =  ZJVMAccess.getProperty(list, size );
        assertTrue( c.isNil() );
        assertTrue( c instanceof ZException.MonadicException );
        assertTrue( ZException.MonadicException.throwable(c) instanceof IndexOutOfBoundsException );

    }

    @Test
    public void testErrInFieldAccessor(){
        Function.MonadicContainer c =  ZJVMAccess.FIELD_ACCESS.getValue(list,"bla");
        assertSame( INVALID_PROPERTY, c);
        c =  ZJVMAccess.FIELD_ACCESS.setValue(Arithmetic.INSTANCE,"NEGATIVE_INFINITY", 0);
        assertSame( Function.FAILURE , c);
    }

    @Test
    public void testErrInMethodAccessor(){
        Iterator dumb = new Iterator() {
            @Override
            public boolean hasNext() {
                return false;
            }

            @Override
            public Object next() {
                throw new NoSuchElementException();
            }
        };
        Function.MonadicContainer c =  METHOD_ACCESS.getValue(dumb,"next");
        assertTrue( c instanceof ZException.MonadicException );
        assertTrue( ZException.MonadicException.throwable(c) instanceof NoSuchElementException );
        Object obj = new Object(){
            public void foo(String val){
                throw new UnsupportedOperationException();
            }
        };
        c =  METHOD_ACCESS.setValue(obj,"foo", "bar");
        assertSame( Function.FAILURE, c );
    }

    @Test
    public void testIndexer(){

        final Object[] keysNum = new Object[ ]{ 1, 2, 3, 4, 5, 6 } ;

        final Object[] valuesChar = new Object[ ]{ 'a', 'b', 'c', 'd', 'e', 'f' } ;

        final Map map = new ZMap(new HashMap(), keysNum, valuesChar );

        Function.MonadicContainer c =  ZJVMAccess.getProperty(map,1);
        assertFalse( c.isNil() );
        assertTrue( c.value() instanceof Character );

        c = ZJVMAccess.getProperty(map,42);
        assertTrue( c.isNil() );
        assertTrue( c == INVALID_PROPERTY );

        c = ZJVMAccess.setProperty(map,42, "42" );
        assertFalse( c.isNil() );
        assertEquals("42", map.get(42) );

        c = ZJVMAccess.getProperty(map,"size");
        assertFalse( c.isNil() );
        assertEquals(c.value(), map.size());

        c = ZJVMAccess.getProperty(map,"keys");
        assertFalse( c.isNil() );
        assertEquals(c.value(), map.keySet());

        c = ZJVMAccess.getProperty(map,"values");
        assertFalse( c.isNil() );
        assertEquals(c.value(), map.values());

        c = ZJVMAccess.getProperty(map,"entries");
        assertFalse( c.isNil() );
        assertEquals(c.value(), map.entrySet());
    }

    @Test
    public void xpathTest(){
        Object root = ZTypes.json( "samples/large_json.json", true);
        JXPathContext context = JXPathContext.newContext(root);
        Iterator iter  = context.iteratePointers("//products");
        // all of these should be valid now...
        while ( iter.hasNext() ){
            NodePointer dp = (NodePointer) iter.next();
            String path = myPath(dp);
            // get new ptr
            NodePointer p = (NodePointer)context.getPointer(path);
            assertTrue(p.isActual());
        }
    }

    @Test
    public void orderingVarArgsTest(){
        Object foo = new Object(){
            public int firstCalled = 0;
            public int secondCalled = 0;

            public void printf(Integer i, String msg, Object... args){
                secondCalled++;
                System.out.print(i + " >> ");
                System.out.printf(msg, args);
            }

            public void printf(String msg, Object... args){
                firstCalled ++;
                System.out.printf(msg, args);
            }
        };
        // 1
        ZJVMAccess.callMethod( foo, "printf" , new Object[]{ "Hello, world %n" });
        assertEquals( 1, ZJVMAccess.getProperty( foo, "firstCalled").value() );
        // 1-1
        ZJVMAccess.callMethod( foo, "printf" , new Object[]{ "Hello, world : %s %n", "foo-bar" });
        assertEquals( 2, ZJVMAccess.getProperty( foo, "firstCalled").value());

        // 2-1
        ZJVMAccess.callMethod( foo, "printf" , new Object[]{ 0, "Hello, world : %s %n", "foo-bar" });
        assertEquals( 1, ZJVMAccess.getProperty( foo, "secondCalled").value());
        Throwable throwable = null;

        try {
            ZJVMAccess.callMethod( foo, "printf" , new Object[]{  });
        } catch (Throwable t){
            throwable = t;
        }
        assertNotNull(throwable);
    }

    interface Foo{
        default int value(){
            return 42;
        }
    }

    interface Bar{
        default int value(){
            return 41;
        }
    }

    @Test
    public void j8DefaultInterfaceTest(){
        Foo f = new Foo(){};
        Bar b = new Bar(){};
        Object r = ZJVMAccess.callMethod(f, "value", new Object[]{});
        assertEquals( 42,  r );
        r = ZJVMAccess.callMethod(b, "value", new Object[]{});
        assertEquals( 41,  r );
        Function.MonadicContainer mc = ZJVMAccess.getProperty(f, "value");
        assertEquals( 42,  mc.value() );
    }

    private boolean setSomethingWasCalled = false;

    public void setSomething(int x){
        System.out.println(x);
        setSomethingWasCalled = true;
    }

    private boolean formatWasCalled = false;

    public void format(String fmt, Object...args){
        System.out.printf(fmt,args);
        formatWasCalled = true ;
    }

    @Test
    public void jvmNumericArgsTest(){
        Object[] args = new Object[]{  new BigDecimal("42.90809809890890808") } ;
        Object r = ZJVMAccess.callMethod(Math.class, "abs", args);
        assertEquals( 42.9d , (double)r , 0.1d );
        r = ZJVMAccess.callMethod(Math.class, "signum", args);
        assertEquals( 1.0d , (double)r, 0.001 );
        // can it do null / void return
        r = ZJVMAccess.callMethod(this, "setSomething", new Object[]{ BigDecimal.valueOf(1)});
        assertNull(r);
        assertTrue(setSomethingWasCalled);

        // can it do null / void return
        r = ZJVMAccess.callMethod(this, "format", new Object[]{ "foo %s", "bar" });
        assertTrue(r instanceof Function.MonadicContainer);
        assertTrue(((Function.MonadicContainer) r).isNil());
        assertTrue(formatWasCalled);
    }

    @Test
    public void jvmFineTunedNumericMethodTest(){
         //Number abs(Number n)
        double x = 1.0;
        Math.abs((int)x);
         Object r = ZJVMAccess.callMethod(Math.class, "abs", new Object[] {1} );
         assertEquals( 1, r );
    }

    static class OverloadingIssue{

        public int function(int i, int  j){
            return 2;
        }
        public int function(int...args){
            return 42;
        }
        public int function(int i){
            return 1;
        }

        public int log(String fmt, Throwable err){
            return 3;
        }
        public int log(String data){
            return 1;
        }
        public int log(String fmt, Object data){
            return 2;
        }
    }

    @Test
    public void testVarArgsOverLoad(){
        final OverloadingIssue issue = new OverloadingIssue();
        Object r = ZJVMAccess.callMethod(issue, "function", new Object[]{});
        assertEquals( 42, r );
        r = ZJVMAccess.callMethod(issue, "function", new Object[]{ 42 });
        assertEquals( 1, r );
        r = ZJVMAccess.callMethod(issue, "function", new Object[]{ 42 , 420 });
        assertEquals( 2, r );
    }

    static final class StringFormatterIssue {
        public int log(String msg, Object...args){ return 42 ; }
        public int log(String msg){ return 0 ; }

    }

    @Test
    public void testStringFormatterArgsOverLoad(){
        final StringFormatterIssue issue = new StringFormatterIssue();
        Object r = ZJVMAccess.callMethod(issue, "log", new Object[]{ "hi!!"});
        assertEquals( 0, r );
        r = ZJVMAccess.callMethod(issue, "log", new Object[]{ "hi" , "hello!" });
        assertEquals( 42, r );
    }

    @Test
    public void testPerformance(){
        Object[] args = new Object[]{  new BigDecimal("42.90809809890890808") } ;
        final int times = 1000000;
        long start = System.currentTimeMillis();

        for ( int i=0; i< times ; i++ ) {
            Object r = ZJVMAccess.callMethod(Math.class, "sin", args);
        }
        long zDelta = System.currentTimeMillis() - start;
        start = System.currentTimeMillis();
        for ( int i=0; i< times; i++ ) {
            Object r = Math.sin(((Number)args[0]).doubleValue());
        }
        long jDelta = System.currentTimeMillis() - start;
        double ratio = zDelta / (double)jDelta;
        final boolean isRunningInCI = Boolean.TRUE.equals(
                ZTypes.bool(System.getProperty("inCI", "false"),false) );
        final int maxRatioAllowed = isRunningInCI ? 100: 25 ;
        System.out.println("Running in CI : " + isRunningInCI );
        System.out.printf("%d JVMAccess Method call (dynamic) zmb : %d , jvm (static) : %d , ratio: %.2f %n",
                times,  zDelta, jDelta, ratio );
        assertTrue( ratio < maxRatioAllowed );
    }

    @Test
    public void findClassFunctionTest(){
        Map<String,Class<?>> classDict = new HashMap<>();
        classDict.put("int", int.class);
        classDict.put("char", char.class);
        classDict.put("short", short.class);
        classDict.put("byte", byte.class);
        classDict.put("long", long.class);
        classDict.put("float", float.class);
        classDict.put("double", double.class);
        classDict.put("boolean", boolean.class);
        classDict.forEach( (k,v) -> {
            try {
                assertEquals( v, ZJVMAccess.findClass(k) );
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Test
    public void objectToDictFunctionTest(){
        assertNull( ZJVMAccess.dict(null));
        assertNotNull(ZJVMAccess.dict(Integer.valueOf(42)));
    }

    @Test
    public void constructClassTest(){
        assertThrows( RuntimeException.class, () -> ZJVMAccess.construct(42, null));
        assertEquals("", ZJVMAccess.construct(String.class, null) );
        assertEquals("", ZJVMAccess.construct(String.class, ZArray.EMPTY_ARRAY) );
    }

    static final class ZEventAwareImpl implements ZEventAware{

        public boolean injectErr = false;
        public boolean[] called = new boolean[] { false, false, false };

        public Object set(Object prop, Object v){
            return v;
        }

        public Object get(Object o){
            return o;
        }

        public void setSome(int v){
            called[2] = true;
        }


        public int setFoo(int v){
            return v;
        }

        public boolean method(boolean param){
            return param;
        }

        @Override
        public void after(ZEventArg after) {
            called[1] = true;
            if ( injectErr ) throw new RuntimeException("foo!");
        }

        @Override
        public void before(ZEventArg before) {
            called[0] = true;
            if ( injectErr ) throw new RuntimeException("foo!");
        }
    }

    @Test
    public void eventAwareTest(){
        ZEventAwareImpl impl = new ZEventAwareImpl();
        assertEquals(false, ZJVMAccess.callMethod( impl, "method", new Object[]{ false }) ) ;
        assertTrue( impl.called[0]);
        assertTrue( impl.called[1]);
        impl.injectErr = true;
        impl.called[0] = impl.called[1] = false;
        assertEquals(true, ZJVMAccess.callMethod( impl, "method", new Object[]{ true }) ) ;
        assertTrue( impl.called[0]);
        assertTrue( impl.called[1]);

    }

    @Test
    public void methodCallTest(){
        assertThrows( RuntimeException.class, () ->
                ZJVMAccess.callMethod(null, "foo", ZArray.EMPTY_ARRAY ) );
        assertEquals( String.class,
                ZJVMAccess.callMethod(Class.class, "forName", new Object[]{ "java.lang.String" } ) );
        assertTrue( ZJVMAccess.callMethod(System.out, "printf", new Object[]{ "%s %f %f %n" , "foo" , 42, 42 } ) instanceof PrintStream);
    }

    @Test
    public void matcherTest(){
        assertNull( ZJVMAccess.getField( null, null ) );
        assertNull( ZJVMAccess.findMatchingMethod( null, (x) -> false ));
        assertNull( ZJVMAccess.findMatchingMethod( Object.class, (x) -> false ));
    }

    static final class  FooBar{
        Object r = null;
        public void set(String name, Object v){
            if ( r != null ) throw new RuntimeException();
            r = v;
        }
    }

    @Test
    public void nonIntegerAccessorsTest(){
        assertEquals( INVALID_PROPERTY , FIELD_ACCESS.setValue( new Object(), "foo", "42"));
        assertEquals( INVALID_PROPERTY , METHOD_ACCESS.setValue( new Object(), "foo", "42"));

        ZEventAwareImpl zi = new ZEventAwareImpl();
        assertEquals( Function.SUCCESS , METHOD_ACCESS.setValue( zi, "some", 42));
        assertTrue( zi.called[2] );
        Function.MonadicContainer mc = METHOD_ACCESS.setValue( zi, "foo", 42) ;
        assertEquals(42, mc.value() );

        assertNull( PropertyBucket.getDuckGet(null) );
        assertNull( PropertyBucket.getDuckSet(null) );
        assertEquals( INVALID_PROPERTY , PROPERTY_BUCKET.getIndexedValue( null, null ) );
        assertEquals( INVALID_PROPERTY , PROPERTY_BUCKET.setIndexedValue( null, null, null  ) );
        assertEquals( INVALID_PROPERTY , PROPERTY_BUCKET.setIndexedValue( new Object(), "foo", null  ) );
        // now try map access
        Map<String,String> map = new HashMap<>();
        map.put("hi", "42");
        mc = PROPERTY_BUCKET.getIndexedValue( map, "hi");
        assertEquals("42", mc.value() );
        mc = PROPERTY_BUCKET.setIndexedValue( map, "hello", "42");
        assertTrue( map.containsKey("hello"));

        mc = PROPERTY_BUCKET.setIndexedValue( zi, "xxx", "42") ;
        assertEquals("42", mc.value() );

        FooBar fb = new FooBar();
        assertEquals( Function.SUCCESS, PROPERTY_BUCKET.setIndexedValue( fb, "xxx", 42) );
        assertEquals( 42, fb.r);
        assertEquals( Function.FAILURE, PROPERTY_BUCKET.setIndexedValue( fb, "xxx", 42) );

        assertEquals( INVALID_ACCESSOR, MAP_ACCESS.getIndexedValue( fb, "xxx") );
        assertEquals( INVALID_ACCESSOR, MAP_ACCESS.setIndexedValue( fb, "xxx", 45) );

    }

    @Test
    public void integerAccessorTest(){
        // array
        assertEquals( INVALID_ACCESSOR, ARRAY_ACCESS.getIndexedValue( "hello", 10.0d) );
        assertEquals( INVALID_ACCESSOR, ARRAY_ACCESS.getIndexedValue( "hello".toCharArray(), 10.0d) );
        assertEquals( INVALID_ACCESSOR, ARRAY_ACCESS.setIndexedValue( "hello", 10.0d, 10.0 ) );
        assertEquals( INVALID_ACCESSOR, ARRAY_ACCESS.setIndexedValue( "hello".toCharArray(), 10.0d, 10.0 ) );
        // neg indexed slice
        Function.MonadicContainer mc = ARRAY_ACCESS.getIndexedValue( "hello".toCharArray(), new Object[]{-2,-2} );
        assertFalse( mc.isNil() );
        assertEquals( INVALID_ACCESSOR, ARRAY_ACCESS.getIndexedValue( "hello".toCharArray(), new Object[]{-2,-3}) );
        assertEquals( INVALID_PROPERTY, ARRAY_ACCESS.setIndexedValue( "hello".toCharArray(), -20, 'z') );

        // list
        List<Integer> al = Arrays.asList(1,2,3,4);
        assertEquals( INVALID_ACCESSOR, LIST_ACCESS.getIndexedValue( "hello", 10.0d) );
        assertEquals( INVALID_ACCESSOR, LIST_ACCESS.setIndexedValue( "hello", 10.0d, 42) );

        assertEquals( INVALID_ACCESSOR, LIST_ACCESS.getIndexedValue( al, 10.0d) );
        assertEquals( INVALID_ACCESSOR, LIST_ACCESS.setIndexedValue( al, 10.0d, 10.0 ) );
        assertEquals( INVALID_ACCESSOR, LIST_ACCESS.setIndexedValue( al, 10.0d, 10.0 ) );
        // neg indexed slice
        mc = LIST_ACCESS.getIndexedValue( al, new Object[]{-2,-2} );
        assertFalse( mc.isNil() );
        mc = LIST_ACCESS.getIndexedValue( al, -1 );
        assertEquals(4, mc.value());

        mc = LIST_ACCESS.setIndexedValue( al, -1 , 42 );
        assertEquals(4, mc.value());
        assertEquals( 42L, al.get(3).longValue() );

        assertEquals( INVALID_ACCESSOR, LIST_ACCESS.getIndexedValue( al, new Object[]{-2,-3}) );
        assertEquals( INVALID_PROPERTY, LIST_ACCESS.setIndexedValue( al, -20, 200 ) );
        assertEquals( INVALID_PROPERTY, LIST_ACCESS.setIndexedValue( al, 20, 200 ) );

    }

    @Test
    public void genericPropertyTest(){
        // bunch of get
        assertThrows( NullPointerException.class, () -> ZJVMAccess.getProperty( null, "foo") );
        assertThrows( NullPointerException.class, () -> ZJVMAccess.setProperty( null, "foo", "bar") );
        assertEquals( 5, ZJVMAccess.getProperty( "hello".toCharArray(), "length" ).value() );
        assertEquals( INVALID_PROPERTY, ZJVMAccess.getProperty( "hello", 20 ) );
        assertEquals( "o", ZJVMAccess.getProperty( "hello", new Object[]{ -1,-1} ).value() );
        assertEquals( INVALID_PROPERTY, ZJVMAccess.getProperty( "hello", new Object[]{ -100,-100}) );

        // bunch of set
        assertEquals( INVALID_PROPERTY, ZJVMAccess.setProperty( Function.NIL, "foo", "bar") );
        ZEventAwareImpl zi = new ZEventAwareImpl();
        assertEquals( Function.SUCCESS , ZJVMAccess.setProperty( zi, "some", 42));
        assertTrue( zi.called[2] );
        Function.MonadicContainer mc = ZJVMAccess.setProperty( zi, "foo", 42) ;
        assertEquals(42, mc.value() );

        FooBar fb = new FooBar();
        assertEquals( Function.SUCCESS, ZJVMAccess.setProperty( fb, "xxx", 42) );
        assertEquals( 42, fb.r);
        assertEquals( Function.FAILURE, ZJVMAccess.setProperty( fb, "xxx", 42) );

    }

    @Test
    public void zJvmFunctionalCoercionTest(){
        ZJVMFunctionalInterface uv = Function.COLLECTOR_IDENTITY.coerce( Consumer.class);
        assertTrue( uv instanceof ZJVMFunctionalInterface.UniParam);
        assertEquals( Integer.MAX_VALUE, uv.definedParams() );
    }
}
