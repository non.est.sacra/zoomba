package zoomba.lang.core.operations;

import org.junit.Test;
import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.types.ZNumber;
import zoomba.lang.core.types.ZRange;
import zoomba.lang.core.types.ZString;

import java.util.*;

import static org.junit.Assert.*;

public class ArithmeticTest {

    @Test
    public void infinityTest(){
        assertThrows( UnsupportedOperationException.class, Arithmetic.POSITIVE_INFINITY::intValue );
        assertThrows( UnsupportedOperationException.class, Arithmetic.POSITIVE_INFINITY::floatValue );
        assertThrows( UnsupportedOperationException.class, Arithmetic.POSITIVE_INFINITY::longValue );
        assertThrows( UnsupportedOperationException.class, Arithmetic.POSITIVE_INFINITY::doubleValue );
        assertEquals(-1, Arithmetic.INSTANCE.compare( Arithmetic.NEGATIVE_INFINITY, 0 ) );
        assertEquals("-inf", Arithmetic.NEGATIVE_INFINITY.toString() );
    }

    @Test
    public void unsupportedOperationTest(){
        assertThrows( UnsupportedOperationException.class, () -> { throw Arithmetic.INSTANCE.UNSUPPORTED_OPERATION(null, null, "+") ; });
        assertThrows( UnsupportedOperationException.class, () -> { throw Arithmetic.INSTANCE.UNSUPPORTED_OPERATION(null, 0, "+") ; });
        assertThrows( UnsupportedOperationException.class, () -> { throw Arithmetic.INSTANCE.UNSUPPORTED_OPERATION(0, null, "+") ; });
    }

    @Test
    public void addFunctionTest(){
        assertThrows( UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.add(Function.NIL, 0 ) );
        ZString zs = new ZString("");
        assertSame( zs, Arithmetic.INSTANCE.addMutable(zs, 42 ));
        assertEquals("42", zs.toString());
        assertThrows( UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.addMutable(Function.NIL, 0 ) );
    }

    @Test
    public void subtractFunctionTest(){
        assertThrows( UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.sub(Function.NIL, 0 ) );

        ZNumber zn = new ZNumber(0);
        assertSame( zn, Arithmetic.INSTANCE.subMutable(zn, 42 ));
        assertEquals(-42, zn.actual());

        List<Integer> l = new ArrayList<>(Arrays.asList(1,2,4));
        assertSame( l, Arithmetic.INSTANCE.subMutable(l, 1 ));
        assertEquals(2, l.size());

        assertThrows( UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.subMutable(Function.NIL, 0 ) );
    }

    @Test
    public void mulFunctionTest(){
        assertThrows( UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.div(Function.NIL, 0 ) );

        ZNumber zn = new ZNumber(42);
        assertSame( zn, Arithmetic.INSTANCE.mulMutable(zn, 1 ));
        assertEquals(42, zn.actual());

    }

    @Test
    public void divFunctionTest(){
        assertThrows( UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.div(Function.NIL, 0 ) );

        ZNumber zn = new ZNumber(42);
        assertSame( zn, Arithmetic.INSTANCE.divMutable(zn, 42 ));
        assertEquals(1, zn.actual());

    }

    @Test
    public void compareFunctionTest(){
        assertEquals(0, Arithmetic.INSTANCE.compare(null, null));
        assertThrows(UnknownFormatConversionException.class, () -> Arithmetic.INSTANCE.compare(null, 0));
        assertThrows(UnknownFormatConversionException.class, () -> Arithmetic.INSTANCE.compare(0, null));
        assertEquals(-1, Arithmetic.INSTANCE.compare(0, Arithmetic.POSITIVE_INFINITY));
        assertEquals(1, Arithmetic.INSTANCE.compare(0, Arithmetic.NEGATIVE_INFINITY));

        assertEquals(0, Arithmetic.INSTANCE.compare(new HashMap<>(), new TreeMap<>()));
        assertEquals(0, Arithmetic.INSTANCE.compare(new HashSet<>(), ZArray.EMPTY_ARRAY ));

        // equality
        assertTrue( Arithmetic.INSTANCE.eq( true, true) );
        assertTrue( Arithmetic.INSTANCE.eq( false, false) );
        assertFalse( Arithmetic.INSTANCE.eq( true, false) );
        assertFalse( Arithmetic.INSTANCE.eq( false, true) );

        assertTrue( Arithmetic.INSTANCE.eq( true, "true") );
        assertTrue( Arithmetic.INSTANCE.eq( false, "false") );
        assertFalse( Arithmetic.INSTANCE.eq( true, "false") );
        assertFalse( Arithmetic.INSTANCE.eq( false, "true") );

    }

    @Test
    public void andTest(){
        // and
        assertEquals( false, Arithmetic.INSTANCE.and( false, true) );
        assertEquals( false, Arithmetic.INSTANCE.and( false, false) );
        assertEquals( true, Arithmetic.INSTANCE.and( true, true) );
        assertEquals( false, Arithmetic.INSTANCE.and( true, false) );
        assertEquals( false, Arithmetic.INSTANCE.and( true, "false") );
        assertEquals( true, Arithmetic.INSTANCE.and( true, "true") );

        assertEquals( false, Arithmetic.INSTANCE.andMutable( false, true) );
        assertEquals( false, Arithmetic.INSTANCE.andMutable( false, false) );
        assertEquals( true, Arithmetic.INSTANCE.andMutable( true, true) );
        assertEquals( false, Arithmetic.INSTANCE.andMutable( true, false) );
        assertEquals( false, Arithmetic.INSTANCE.andMutable( true, "false") );
        assertEquals( true, Arithmetic.INSTANCE.andMutable( true, "true") );

        ZNumber zn = new ZNumber(42);
        assertSame( zn, Arithmetic.INSTANCE.andMutable(zn, 42 ));
        assertEquals(42, zn.actual());

    }

    @Test
    public void orTest(){
        // or
        assertEquals( true, Arithmetic.INSTANCE.or( true, false) );
        assertEquals( true, Arithmetic.INSTANCE.or( false, true) );
        assertEquals( false, Arithmetic.INSTANCE.or( false, false) );
        assertEquals( false, Arithmetic.INSTANCE.or( false, "false") );
        assertEquals( true, Arithmetic.INSTANCE.or( true, "false") );


        assertEquals( true, Arithmetic.INSTANCE.orMutable( true, false) );
        assertEquals( true, Arithmetic.INSTANCE.orMutable( false, true) );
        assertEquals( false, Arithmetic.INSTANCE.orMutable( false, false) );
        assertEquals( false, Arithmetic.INSTANCE.orMutable( false, "false") );
        assertEquals( true, Arithmetic.INSTANCE.orMutable( true, "false") );

        ZNumber zn = new ZNumber(42);
        assertSame( zn, Arithmetic.INSTANCE.orMutable(zn, 0 ));
        assertEquals(42, zn.actual());
    }

    @Test
    public void xorTest(){
        // xor
        ZNumber zn = new ZNumber(1);
        assertSame( zn, Arithmetic.INSTANCE.xorMutable(zn, 0 ));
        assertEquals(1, zn.actual());
    }

    @Test
    public void notTest(){
        assertThrows( UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.not(Function.NIL) );
        // not
        ZNumber zn = new ZNumber(1);
        assertEquals( -1, Arithmetic.INSTANCE.not(zn));
    }

    @Test
    public void modTest(){
        assertThrows( UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.mod(Function.NIL, 0) );
        assertThrows( UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.mod(0, Function.NIL ) );

        assertThrows( UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.modMutable(Function.NIL, 0) );
        assertThrows( UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.modMutable(0, Function.NIL ) );

        // mod
        ZNumber zn = new ZNumber(4);
        assertSame( zn, Arithmetic.INSTANCE.modMutable(zn, 2 ));
        assertEquals(0, zn.actual());
    }

    @Test
    public void dividesTest(){
        assertFalse(Arithmetic.INSTANCE.divides(Function.NIL, 0));
        assertFalse(Arithmetic.INSTANCE.divides(0, Function.NIL));
        assertTrue(Arithmetic.INSTANCE.divides(2, 4));
        assertTrue(Arithmetic.INSTANCE.divides(2.0, 3));
        assertTrue(Arithmetic.INSTANCE.divides(2, 3.0));
    }

    @Test
    public void containsInTest(){
        // in
        assertFalse(Arithmetic.INSTANCE.in(0, null));
        assertFalse(Arithmetic.INSTANCE.in(Function.NIL, Function.NIL));

        // stats with
        assertFalse(Arithmetic.INSTANCE.startsWith("", null));
        assertFalse(Arithmetic.INSTANCE.startsWith(Function.NIL, Function.NIL));
        assertFalse(Arithmetic.INSTANCE.startsWith(Arrays.asList(1,2,3), Arrays.asList(1,3)));
        assertTrue(Arithmetic.INSTANCE.startsWith(Arrays.asList(1,2,3), Arrays.asList(1,2)));

        // ends with
        assertFalse(Arithmetic.INSTANCE.endsWith("", null));
        assertFalse(Arithmetic.INSTANCE.endsWith(Function.NIL, Function.NIL));
        assertFalse(Arithmetic.INSTANCE.endsWith(Arrays.asList(1,2,3), Arrays.asList(1,3)));
        assertTrue(Arithmetic.INSTANCE.endsWith(Arrays.asList(1,2,3), Arrays.asList(2,3)));

        // in Order
        assertFalse(Arithmetic.INSTANCE.inOrder(Function.NIL, new HashMap<>()));
        assertFalse(Arithmetic.INSTANCE.inOrder( Function.NIL, new HashSet<>()));

        assertFalse(Arithmetic.INSTANCE.inOrder( null, "hello"));
        assertTrue(Arithmetic.INSTANCE.inOrder( "el", "hello"));
        assertFalse(Arithmetic.INSTANCE.inOrder( "foo", "hello"));

        assertTrue(Arithmetic.INSTANCE.inOrder( Arrays.asList(2,3), Arrays.asList(1,2,3,4).toArray() ));
        assertFalse(Arithmetic.INSTANCE.inOrder( Arrays.asList(2,4), Arrays.asList(1,2,3,4).toArray() ));
        assertFalse(Arithmetic.INSTANCE.inOrder(Function.NIL, Function.NIL));
    }

    @Test
    public void addToAccumulatorFunctionTest(){
        List<Integer> li = new ArrayList<>();

        Iterable iter = () -> new ZRange.NumRange(0,5);
        assertTrue( Arithmetic.addToAccumulator( iter, li) );
        assertEquals(5, li.size());
        li.clear();

        assertTrue( Arithmetic.addToAccumulator( iter.iterator(), li) );
        assertEquals(5, li.size());
        li.clear();

        assertTrue( Arithmetic.addToAccumulator( "hello".toCharArray(), li) );
        assertEquals(5, li.size());
        li.clear();
        assertFalse( Arithmetic.addToAccumulator( Function.NIL, li) );
    }

    @Test
    public void catenationTest(){
        assertThrows( UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.catenation( Function.NIL, Collections.emptyList() ) );
        assertThrows( UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.catenation(  Collections.emptyList() , Function.NIL) );

        assertThrows( UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.mutableCatenation( Function.NIL, Collections.emptyList() ) );
        assertThrows( UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.mutableCatenation(  Collections.emptyList() , Function.NIL) );
    }

    @Test
    public void collectionDifferenceTest(){

        assertThrows( UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.collectionDifference( Function.NIL, Collections.emptyList() ) );
        assertThrows( UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.collectionDifference( Collections.emptyList(), Function.NIL ) );
        assertThrows( UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.collectionDifference( Collections.emptyMap(), Collections.emptyList() ) );

        List<Integer> l = new ArrayList<>( Arrays.asList(1,2,3,4,5,6));
        Iterable<Integer> iterable = () -> new ZRange.NumRange(1,4);
        Arithmetic.INSTANCE.mutableCollectionDifference( l, iterable);
        assertEquals(3, l.size());

        Map<Integer,Integer> m = new HashMap<>();
        m.put(1,1);
        m.put(2,2);
        Map<Integer,Integer> o = new HashMap<>();
        o.put(1,1);
        Arithmetic.INSTANCE.mutableCollectionDifference( m, o);
        assertEquals(1, o.size());

        assertThrows( UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.mutableCollectionDifference( m, Collections.emptyList() ) );
        assertThrows( UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.mutableCollectionDifference( Function.NIL, Collections.emptyList() ) );
        assertThrows( UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.mutableCollectionDifference( Collections.emptyList(), Function.NIL ) );
        assertThrows( UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.mutableCollectionDifference( Collections.emptyMap(), Collections.emptyList() ) );

    }

    @Test
    public void errorThrowingOperationsTest(){
        List l = Collections.unmodifiableList( Collections.emptyList() );
        assertThrows(UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.subMutable( l , 1 ));
        assertThrows(UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.mul( Function.NIL , 1 ));
        assertThrows(UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.mulMutable( Function.NIL , 1 ));
        assertThrows(UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.divMutable( Function.NIL , 1 ));
        assertThrows(UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.pow( Function.NIL , 1 ));
        assertThrows(UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.add( Collections.emptyMap(), Collections.emptyList() ));

        assertThrows(UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.divMod( l , 1 ));
        assertThrows(UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.divMod( 1 , l ));

        // cmp operations
        assertFalse( Arithmetic.INSTANCE.lt( Function.NIL , 1 ) );
        assertFalse( Arithmetic.INSTANCE.le( Function.NIL , 1 ) );
        assertFalse( Arithmetic.INSTANCE.ge( Function.NIL , 1 ) );

        // logical
        assertThrows(UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.and( Function.NIL , 1 ));
        assertThrows(UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.andMutable( Function.NIL , 1 ));
        assertThrows(UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.or( Function.NIL , 1 ));
        assertThrows(UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.orMutable( Function.NIL , 1 ));
        assertThrows(UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.xor( Function.NIL , 1 ));
        assertThrows(UnsupportedOperationException.class, () -> Arithmetic.INSTANCE.xorMutable( Function.NIL , 1 ));

        assertFalse( Arithmetic.addToAccumulator( l,l) );

    }
}
