package zoomba.lang.core.operations;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigInteger;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class DecoratorTest {

    final java.util.function.Function<Integer,Integer> errFunc = (x) -> { throw new RuntimeException(); } ;

    final java.util.function.Function<Integer,Integer> id = (x) -> x ;

    final java.util.function.Function<Integer,Integer> slowFunc = (x) -> {
        BigInteger bi = BigInteger.ZERO ;
        for ( long l =0; l < Long.MAX_VALUE; l++){
            for ( long m =0; m < Long.MAX_VALUE; m++){
                bi = bi.add( BigInteger.valueOf(l)).add( BigInteger.valueOf(m));
                if ( Thread.interrupted() ) {
                    throw new RuntimeException();
                }
            }
        }
        return 42;
    } ;

    @Test
    public void nopRetryTest(){
        assertFalse(Retry.NOP.can());
        assertEquals( 0, Retry.NOP.tries() );
        assertFalse(Retry.NOP.trace());
        Retry.NOP.trace(false);
        Retry.NOP.numTries(0);
        Retry.NOP.errorHandler(null );
        assertEquals(Long.MAX_VALUE,  Retry.NOP.interval());
        assertSame( id, Retry.NOP.function(id));
        java.util.function.Predicate<Integer> nz = (x) -> x != 0 ;
        assertNotSame( nz, Retry.NOP.predicate(nz));
        assertSame( Retry.NOP, Retry.fromConfig( Map.of("strategy", "foo-bar")) );
        assertSame( Retry.NOP, Retry.fromConfig(Collections.emptyMap()));

    }

    @Test
    public void failureRetryTest(){
        Throwable th = new Throwable("foo!");
        Retry.Failure failure = Retry.Failure.failure( th );
        assertSame(th, failure.error());
        assertTrue( failure.when().compareTo(Instant.now()) < 0);

        Retry.MaximumRetryExceededException ex = new Retry.MaximumRetryExceededException( Retry.NOP, List.of(failure));
        assertTrue( ex.getMessage().contains( "retry") );
    }

    @Test
    public void errorHandlerRetryTest(){
        Map config = Map.of( "err", Retry.ErrorHandler.TRACE );
        Retry retry = Retry.fromConfig( config );
        PrintStream original = System.err ;
        ByteArrayOutputStream bos = new ByteArrayOutputStream( 2048 ) ;
        PrintStream es = new PrintStream( bos );
        System.setErr(es);
        java.util.function.Function<Integer,Integer> withRetry = retry.function(errFunc);
        assertThrows( Throwable.class, () -> withRetry.apply(10) ) ;
        System.setErr(original);
        assertTrue( bos.toString().contains("Retry") );
    }

    @Test
    public void interruptSleepRetryTest() throws Exception {
        Map config = Map.of( "debug", true , "interval", 10000 , "max", 2 );
        Retry retry = Retry.fromConfig( config );
        java.util.function.Function<Integer,Integer> withRetry = retry.function(errFunc);
        PrintStream original = System.err ;
        ByteArrayOutputStream bos = new ByteArrayOutputStream( 2048 ) ;
        PrintStream es = new PrintStream( bos );
        System.setErr(es);
        Runnable r = () -> withRetry.apply(0) ;
        Thread t = new Thread(r);
        t.start();
        t.interrupt();
        Thread.sleep(100);
        System.setErr(original);
        assertTrue( bos.toString().contains("Possible Timeout") );
        assertEquals( 1, retry.tries() );
    }

    @Test
    public void randomRetryStrategy(){
        Map config = Map.of( "strategy", "random", "interval", 10 , "max", 2 );
        Retry retry = Retry.fromConfig( config );
        java.util.function.Function<Integer,Integer> withRetry = retry.function(errFunc);
        Retry.MaximumRetryExceededException th = assertThrows( Retry.MaximumRetryExceededException.class, () -> withRetry.apply(0 ));
        // remember, first one is the try, not a retry
        assertEquals(3, th.failures.size());
        // test that these were in random intervals? How ?
    }

    @Test
    public void expRetryStrategy(){
        Map config = Map.of( "strategy", "exp", "interval", 10 , "max", 3 );
        Retry retry = Retry.fromConfig( config );
        java.util.function.Function<Integer,Integer> withRetry = retry.function(errFunc);
        Retry.MaximumRetryExceededException th = assertThrows( Retry.MaximumRetryExceededException.class, () -> withRetry.apply(0 ));
        // remember, first one is the try, not a retry
        assertEquals(4, th.failures.size());
        // test that these were in exp intervals? How ?
        long d1 = th.failures.get(1).when().toEpochMilli() - th.failures.get(0).when().toEpochMilli();
        long d2 = th.failures.get(2).when().toEpochMilli() - th.failures.get(1).when().toEpochMilli();
        long d3 = th.failures.get(3).when().toEpochMilli() - th.failures.get(2).when().toEpochMilli();
        assertTrue( d1 < d2 );
        assertTrue( d2 < d3 );
    }

    @Test
    public void otherThreadTimeOutTest(){
        assertSame( errFunc, Timeout.fromConfig(Map.of()).function( errFunc ) );
        Timeout to = Timeout.fromConfig( Map.of( "interval", 10L, "inline", false ) );
        // regular usage
        final java.util.function.Function<Integer,Integer> t_id = to.function( id );
        assertEquals( 42, (int)t_id.apply(42) );
        // in case of error
        final java.util.function.Function<Integer,Integer> t_errFunc = to.function( errFunc );
        assertThrows( RuntimeException.class, () -> t_errFunc.apply(42));
        // in case of timeout
        final java.util.function.Function<Integer,Integer> t_slowFunc = to.function( slowFunc );
        assertThrows( RuntimeException.class, () -> t_slowFunc.apply(42));
    }

    @Test
    public void currentThreadTimeOutTest(){
        Timeout to = Timeout.fromConfig( Map.of( "interval", 10L, "inline", true ) );
        // regular usage
        final java.util.function.Function<Integer,Integer> t_id = to.function( id );
        assertEquals( 42, (int)t_id.apply(42) );
        // in case of error
        final java.util.function.Function<Integer,Integer> t_errFunc = to.function( errFunc );
        assertThrows( RuntimeException.class, () -> t_errFunc.apply(42));
        // in case of timeout
        final java.util.function.Function<Integer,Integer> t_slowFunc = to.function( slowFunc );
        assertThrows( RuntimeException.class, () -> t_slowFunc.apply(42));
    }
}
