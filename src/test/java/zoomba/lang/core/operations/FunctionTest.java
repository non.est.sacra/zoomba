package zoomba.lang.core.operations;

import org.junit.Test;
import zoomba.lang.core.interpreter.ZContext;
import zoomba.lang.core.types.ZTypes;

import java.util.*;

import static org.junit.Assert.*;

public class FunctionTest {

    @Test
    public void exceptionReturnTest(){
        assertTrue( Function.runTimeException( new Exception()).getCause() instanceof Exception );
        RuntimeException re = new RuntimeException();
        assertSame( re, Function.runTimeException( re));
        assertTrue( Function.runTimeException( new Exception()).getCause() instanceof Exception );
        Function.Void.runTimeExceptionOnError();
        Function.MonadicContainer mc = new Function.MonadicContainerBase(re);
        assertThrows( RuntimeException.class, mc::runTimeExceptionOnError);
        assertSame(mc,  mc.whenNilRaise( re) );
        assertSame(mc,  mc.whenNil( () ->{} ) );
        assertThrows( RuntimeException.class, () -> Function.Void.whenNilRaise( re) );
    }

    @Test
    public void nilTest(){
        assertThrows( UnknownFormatConversionException.class, () -> new SingleProjection(0).apply( Function.NIL ));
        assertEquals("nil", Function.NIL.toString());
    }

    @Test
    public void namedArgTest(){
        final Function.NamedArgs na = new Function.NamedArgs();
        assertTrue( na.doWhenAbsent( "x", (x)-> na.put(x,42)) );
        assertFalse( na.doWhenAbsent( "x", (x)-> na.put(x,42)) );
        // TODO need to find why this does not run
        // boolean b = na.typed("x", true);
        // assertTrue( b);
    }

    @Test
    public void wrapperFunctionTest(){
        final boolean retVoid[] = { false };

        Function f = new Function() {
            @Override
            public String body() {
                return "body";
            }

            @Override
            public MonadicContainer execute(Object... args) {
                return retVoid[0]? Void : new MonadicContainerBase(42);
            }

            @Override
            public String name() {
                return "name";
            }
        };

        Function.Mapper w = Function.Mapper.from( f);
        assertEquals("body", w.body());
        assertEquals("name", w.name());
        assertEquals( 42, w.map() );

        retVoid[0] = true;
        assertEquals( "hi", w.map("hi") );
        assertNotNull( w.map() );

        assertSame( Function.COLLECTOR_IDENTITY, Function.Mapper.from( Function.COLLECTOR_IDENTITY ) );

        Function.Predicate p = new Function.Predicate() {
            @Override
            public boolean accept(Object... args) {
                return false;
            }

            @Override
            public String body() {
                return "";
            }

            @Override
            public MonadicContainer execute(Object... args) {
                return null;
            }

            @Override
            public String name() {
                return "";
            }
        };
        assertSame( p, Function.Predicate.from(p) );
    }

    @Test
    public void monadTest(){
        Optional<?> opt = new Function.MonadicContainerBase(42).asOptional();
        assertTrue(opt.isPresent());
        assertFalse(Function.Void.asOptional().isPresent());
        assertTrue( Function.Void.equals( Function.Void ));
        assertFalse( Function.Void.equals( null ));
        assertEquals( 0, Function.Void.hashCode() );
        assertEquals( 0, new Function.MonadicContainerBase(null).hashCode() );
        assertEquals( 42, new Function.MonadicContainerBase(42).hashCode() );
        assertEquals( "<42>", new Function.MonadicContainerBase(42).toString() );

    }

    @Test
    public void contextFunctionTest(){
        Function.TRUE.runContext(ZContext.EMPTY_CONTEXT);
        assertEquals(ZContext.EMPTY_CONTEXT, Function.TRUE.runContext() );
    }

    private static class CmpFunc implements Function, Comparator{
        @Override
        public int compare(Object o1, Object o2) {
            return Arithmetic.INSTANCE.compare(o1,o2);
        }

        @Override
        public String body() {
            return "";
        }

        @Override
        public MonadicContainer execute(Object... args) {
            return Void;
        }

        @Override
        public String name() {
            return "";
        }
    }

    @Test
    public void otherInterfacesTest(){
        // single projection
        assertEquals( "SingleProjection", Function.COLLECTOR_IDENTITY.name() );
        assertEquals( "return @ARGS[1] ;", Function.COLLECTOR_IDENTITY.body() );
        assertEquals( 42, Function.COLLECTOR_IDENTITY.map(0,42,null,null) );

        // const function
        assertEquals( "const", Function.TRUE.name() );
        assertEquals( "def _const_(){ true }", Function.TRUE.body() );

    }
}
