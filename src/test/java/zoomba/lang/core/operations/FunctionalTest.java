package zoomba.lang.core.operations;

import org.junit.Test;
import zoomba.lang.core.interpreter.InterpreterScriptTest;

import java.util.concurrent.Callable;
import java.util.function.*;

public class FunctionalTest {

    public static class Functional{

        public Object function( java.util.function.Function f, Object o){
            return f.apply(o);
        }
        public boolean predicate( java.util.function.Predicate f, Object o){
            return f.test(o);
        }
        public void runnable( Runnable r){
            r.run();
        }
        public Object callable( Callable c) throws Exception{
            return c.call();
        }
        public void consumer( Consumer c, Object o){
            c.accept(o);
        }
        public Object supplier(Supplier s){
            return s.get();
        }

        public Object biFunction(BiFunction f, Object o1, Object o2){
            return f.apply(o1,o2);
        }
        public boolean biPredicate(BiPredicate f, Object o1, Object o2){
            return f.test(o1,o2);
        }
        public void biConsumer(BiConsumer c, Object o1, Object o2){
            c.accept(o1,o2);
        }
    }

    final String JVM_FUNCTIONAL_NON_ANON_METHOD_TESTS = "samples/test/interpreter/functional_non_anon.zm" ;

    final String JVM_FUNCTIONAL_ANON_METHOD_TESTS = "samples/test/interpreter/functional_anon.zm" ;

    @Test
    public void jvmFunctionalArgWithZMBMethodsTest(){
        InterpreterScriptTest.danceWithScript( JVM_FUNCTIONAL_NON_ANON_METHOD_TESTS);
    }

    @Test
    public void jvmFunctionalArgWithZMBAnonMethodsTest(){
        InterpreterScriptTest.danceWithScript( JVM_FUNCTIONAL_ANON_METHOD_TESTS);
    }
}
