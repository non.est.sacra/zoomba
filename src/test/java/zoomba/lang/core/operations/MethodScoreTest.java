package zoomba.lang.core.operations;

import org.junit.Assert;
import org.junit.Test;

import java.io.PrintStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Locale;
import java.util.PriorityQueue;

public class MethodScoreTest {

    @Test
    public void scoreTestNumeric(){
        PriorityQueue<MethodScore> pq = MethodScore.scoreMethods(Math.class, "abs", new Object[]{ 1 });
        Assert.assertNotNull(pq.peek());
        Assert.assertTrue( pq.peek().toString().contains("int") );

        pq = MethodScore.scoreMethods(Math.class, "abs", new Object[]{ 1.0f });
        Assert.assertNotNull(pq.peek());
        Assert.assertTrue( pq.peek().toString().contains("float") );

        pq = MethodScore.scoreMethods(Math.class, "abs", new Object[]{ 1L});
        Assert.assertNotNull(pq.peek());
        Assert.assertTrue( pq.peek().toString().contains("long") );

        pq = MethodScore.scoreMethods(Math.class, "abs", new Object[]{ 1.0D });
        Assert.assertNotNull(pq.peek());
        Assert.assertTrue( pq.peek().toString().contains("double") );

        pq = MethodScore.scoreMethods(Math.class, "abs", new Object[]{BigInteger.ONE});
        Assert.assertNotNull(pq.peek());
        Assert.assertTrue( pq.peek().toString().contains("long") );

        pq = MethodScore.scoreMethods(Math.class, "abs", new Object[]{BigDecimal.ONE});
        Assert.assertNotNull(pq.peek());
        Assert.assertTrue( pq.peek().toString().contains("double") );
    }

    @Test
    public void scoreTestVarArgs(){
        PriorityQueue<MethodScore> pq = MethodScore.scoreMethods(String.class, "format", new Object[]{ "foo" });
        Assert.assertEquals(1, pq.size());
        Assert.assertFalse( pq.peek().toString().contains("Locale") );

        pq = MethodScore.scoreMethods(String.class, "format", new Object[]{ null });
        Assert.assertEquals(1, pq.size());
        Assert.assertFalse( pq.peek().toString().contains("Locale") );

        pq = MethodScore.scoreMethods(String.class, "format", new Object[]{Locale.ROOT , "foo" });
        Assert.assertEquals(1, pq.size());
        Assert.assertTrue( pq.peek().toString().contains("Locale") );
    }

    @Test
    public void testVarArgsOverLoad(){
        PriorityQueue<MethodScore> pq =
                MethodScore.scoreMethods(AccessorTest.OverloadingIssue.class, "function", new Object[]{});
        MethodScore res = pq.peek();
        Assert.assertNotNull(res);
        Assert.assertTrue( res.method.isVarArgs() );

        pq = MethodScore.scoreMethods(AccessorTest.OverloadingIssue.class, "function", new Object[]{ 42 });
        res = pq.peek();
        Assert.assertNotNull(res);
        Assert.assertFalse( res.method.toString().contains("[]") );
        Assert.assertFalse( res.method.toString().contains(",") );

        pq = MethodScore.scoreMethods(AccessorTest.OverloadingIssue.class, "function", new Object[]{ 42 , 420 });
        res = pq.peek();
        Assert.assertNotNull(res);
        Assert.assertTrue( res.method.toString().contains(",") );
    }

    @Test
    public void testPreciseCustomObjectArgsOverLoad(){
        PriorityQueue<MethodScore> pq =
                MethodScore.scoreMethods(AccessorTest.OverloadingIssue.class, "log", new Object[]{});
        Assert.assertTrue(pq.isEmpty());

        pq = MethodScore.scoreMethods(AccessorTest.OverloadingIssue.class, "log", new Object[]{ "foo bar" });
        MethodScore res = pq.peek();
        Assert.assertNotNull(res);
        Assert.assertFalse( res.toString().contains(",") );

        pq = MethodScore.scoreMethods(AccessorTest.OverloadingIssue.class, "log", new Object[]{ "foo" , "bar" });
        res = pq.peek();
        Assert.assertNotNull(res);
        Assert.assertTrue( res.toString().contains("Object") );

        pq = MethodScore.scoreMethods(AccessorTest.OverloadingIssue.class, "log", new Object[]{ "foo" , new RuntimeException() });
        res = pq.peek();
        Assert.assertNotNull(res);
        Assert.assertTrue( res.toString().contains("Throwable") );
    }

    @Test
    public void testLoggingMethodSignature(){
        PriorityQueue<MethodScore> pq =
                MethodScore.scoreMethods(AccessorTest.StringFormatterIssue.class, "log", new Object[]{"hello!!"});
        Assert.assertEquals( 2, pq.size() );
        MethodScore res = pq.peek();
        Assert.assertNotNull(res);
        Assert.assertFalse(res.toString().contains("Object") );

        pq = MethodScore.scoreMethods(AccessorTest.StringFormatterIssue.class, "log", new Object[]{"hello!!", "world!"});
        Assert.assertEquals( 1, pq.size() );
        res = pq.peek();
        Assert.assertNotNull(res);
        Assert.assertTrue(res.toString().contains("Object") );
    }

    @Test
    public void testJVMObjectOverLoad(){
        PriorityQueue<MethodScore>
        pq = MethodScore.scoreMethods(PrintStream.class, "println", new Object[]{ new RuntimeException() });
        MethodScore res = pq.peek();
        Assert.assertNotNull(res);
        Assert.assertTrue( res.toString().contains("Object") );

        pq = MethodScore.scoreMethods(PrintStream.class, "println", new Object[]{ "foo bar" });
        res = pq.peek();
        Assert.assertNotNull(res);
        Assert.assertTrue( res.toString().contains("String") );
    }

    @Test
    public void testConstructorOverLoad(){
        PriorityQueue<MethodScore>
                pq = MethodScore.scoreConstructors( String.class, new Object[]{ } );
        MethodScore res = pq.peek();
        Assert.assertNotNull(res);
        Assert.assertTrue( res.toString().contains("()") );

        pq = MethodScore.scoreConstructors( String.class, new Object[]{ "foo bar" } );
        res = pq.peek();
        Assert.assertNotNull(res);
        Assert.assertTrue( res.toString().contains("(java.lang.String)") );
    }

    @Test
    public void testNoSuchMethod(){
        Throwable exception = Assert.assertThrows(NoSuchMethodError.class, () ->{
            MethodScore.scoreMethods(Integer.class, "println", new Object[]{ new RuntimeException() });
        });
        Assert.assertNotNull(exception);
        Assert.assertTrue(exception.getMessage().contains("println"));
    }

    @Test
    public void testPerformance(){
        final int times = 1000000;
        long start = System.currentTimeMillis();

        for ( int i=0; i< times ; i++ ) {
            MethodScore.scoreMethods(PrintStream.class, "format", new Object[]{ String.valueOf(i) });
        }
        long zDelta = System.currentTimeMillis() - start;
        System.out.printf("%d JVMAccess Method scoring zmb (PrintStream::println) : %d %n", times , zDelta);
        Assert.assertTrue( zDelta < 777 ) ; // thala for a reason
    }

    @Test
    public void nullArgForPrimitiveTest(){
        int s = MethodScore.parameterScore( new Class[]{ int.class }, false, new Object[]{ null } );
        Assert.assertEquals( MethodScore.IMPOSSIBLE, s );
    }

    @Test
    public void constructScoreKeyWithNullArgsTest(){
        MethodScore.ScoreKey ms = new MethodScore.ScoreKey( Object.class, "foobar", new Object[]{ null, false, null });
        Assert.assertTrue( ms.equals( ms ) );
        Assert.assertFalse( ms.equals( 42 ) );
        String s = MethodScore.formatInvalidArgMessage( ms.clazz, ms.methodName, new Object[]{ null, false, null } );
        Assert.assertTrue( s.contains("null"));
    }
}
