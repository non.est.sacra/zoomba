package zoomba.lang.core.operations;

import org.junit.Test;
import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.io.ZFileSystem;
import zoomba.lang.core.types.ZTypes;

import java.nio.file.Files;
import java.util.*;
import java.util.regex.Pattern;

import static org.junit.Assert.*;

public class ZMatrixTest {

    @Test
    public void tupleInitTest(){
        assertSame( ZMatrix.Tuple.EMPTY, ZMatrix.Tuple.tuple());
        assertThrows( IllegalArgumentException.class, () -> new ZMatrix.Tuple(null));
        assertEquals(Arrays.asList(42) , new ZMatrix.Tuple(42).values() );
        assertThrows( IllegalArgumentException.class, () -> new ZMatrix.Tuple(null));
        assertThrows( UnsupportedOperationException.class, () -> new ZMatrix.Tuple(Collections.emptyMap(), Arrays.asList(1,2,3)));
        assertThrows( UnsupportedOperationException.class, () -> new ZMatrix.Tuple(Collections.emptyList(), Arrays.asList(1,2,3)));
    }

    @Test
    public void tupleFunctionsTest(){
        ZMatrix.Tuple.EMPTY.clear();
        assertSame(  Collections.emptyList(), ZMatrix.Tuple.EMPTY.project());
        assertFalse( ZMatrix.Tuple.EMPTY.containsKey("foo"));
        assertFalse( ZMatrix.Tuple.EMPTY.containsKey(0));
        assertFalse( ZMatrix.Tuple.EMPTY.containsKey(-1));
        assertFalse( ZMatrix.Tuple.EMPTY.containsKey(1));
        assertFalse( ZMatrix.Tuple.EMPTY.containsValue(1));
        assertFalse( ZMatrix.Tuple.EMPTY.containsKey('a'));


        assertSame( Function.NIL, ZMatrix.Tuple.EMPTY.get(0));
        assertSame( Function.NIL, ZMatrix.Tuple.EMPTY.get(-1));
        assertSame( Function.NIL, ZMatrix.Tuple.EMPTY.get(1));
        assertSame( Function.NIL, ZMatrix.Tuple.EMPTY.get("foo"));
        assertSame( Function.NIL, ZMatrix.Tuple.EMPTY.get('a'));

        assertSame( Function.NIL, ZMatrix.Tuple.EMPTY.put("foo", "foo"));
        assertSame( Function.NIL, ZMatrix.Tuple.EMPTY.remove("foo"));

        ZMatrix.Tuple.EMPTY.putAll( Collections.emptyMap());
        assertFalse( ZMatrix.Tuple.EMPTY.equals(null));

    }

    @Test
    public void matrixIteratorTest(){
        ZMatrix.MatrixIterator iter = new ZMatrix.MatrixIterator( ZMatrix.EMPTY_MATRIX );
        assertFalse(iter.hasNext());
        assertThrows( UnsupportedOperationException.class , iter::next);
    }

    @Test
    public void emptyMatrixTest(){
        assertEquals( Collections.emptyMap(), ZMatrix.EMPTY_MATRIX.columns());
        assertEquals( 0, ZMatrix.EMPTY_MATRIX.rows());
        assertEquals( Collections.emptyList(), ZMatrix.EMPTY_MATRIX.select(null));
        assertEquals( -1, ZMatrix.EMPTY_MATRIX.index(null));
        assertEquals( Collections.emptyIterator(), ZMatrix.EMPTY_MATRIX.iterator());
        assertEquals( "[]", ZMatrix.EMPTY_MATRIX.toString());
    }

    @Test
    public void matrixLoaderTest(){
        final Pattern emptyPattern = Pattern.compile(".*\\.empty\\..*");

        ZMatrix.ZMatrixLoader zml = new ZMatrix.ZMatrixLoader() {
            @Override
            public Pattern loadPattern() {
                return emptyPattern;
            }

            @Override
            public ZMatrix load(String path, Object... args) {
                return ZMatrix.EMPTY_MATRIX;
            }
        };
        assertTrue( ZMatrix.Loader.add( zml ) );
        assertFalse( ZMatrix.Loader.add( zml ) );

        assertTrue( ZMatrix.Loader.remove( zml ) );
        assertFalse( ZMatrix.Loader.remove( zml ) );
        assertSame( ZMatrix.EMPTY_MATRIX, ZMatrix.Loader.load( "foo.empty.bar"));

        assertSame( ZMatrix.EMPTY_MATRIX, ZMatrix.Loader.load());
        ZMatrix m = ZMatrix.Loader.load( Arrays.asList( Arrays.asList("a","b" ), Arrays.asList(1,2)  ));
        assertNotSame( ZMatrix.EMPTY_MATRIX, m);
    }

    @Test
    public void baseZMatrixIOFunctionsTest() {
        final ZMatrix mInvalid = ZMatrix.Loader.load( Arrays.asList( Arrays.asList("a", "a" ), Arrays.asList(1,2)  ));
        assertThrows( UnsupportedOperationException.class, mInvalid::columns);
        assertFalse( mInvalid.add(null));
        assertFalse( mInvalid.addAll(Collections.emptyList()));
        mInvalid.clear();
        assertFalse( mInvalid.contains(null));
        assertFalse( mInvalid.containsAll(Collections.emptyList()));
        assertFalse( mInvalid.remove(null));
        assertFalse( mInvalid.removeAll(Collections.emptyList()));
        assertFalse( mInvalid.retainAll(Collections.emptyList()));
        assertThrows( UnsupportedOperationException.class, mInvalid::toArray);

        ZMatrix m = ZMatrix.Loader.load( Arrays.asList( Arrays.asList("a", "b" ), Arrays.asList(1,2)  ));
        Object[] a = m.toArray(ZArray.EMPTY_ARRAY);
        assertEquals( 1, a.length);
        assertTrue( a[0] instanceof ZMatrix.Tuple );
        String tmpFile = ZTypes.orRaise( () -> Files.createTempDirectory("zmatrix-").toString() + ".m.tsv" );
        ((ZMatrix.BaseZMatrix)m).write(tmpFile);
        ZFileSystem.ZFile file = ZFileSystem.file(tmpFile);
        assertTrue( file.file.length() > 0 );
    }

    @Test
    public void baseZMatrixHigherOrderFunctionsTest(){
        ZMatrix.BaseZMatrix m = (ZMatrix.BaseZMatrix) ZMatrix.Loader.load( Arrays.asList(
                Arrays.asList("a","b" ),
                Arrays.asList(1,2), Arrays.asList(3,4), Arrays.asList(5,6)   ));
        Map<String, List<Integer>> keys = m.keys( Function.TRUE );
        assertEquals( 1, keys.size());
        assertEquals( 3, keys.get("true").size());
        assertEquals(-1,  m.index( Function.FALSE ) );
        assertEquals(0,  m.index( Function.TRUE ) );
        // var args
        keys = m.keys( new Object[] { Function.TRUE } );
        assertEquals( 1, keys.size());
        keys = m.keys();
        assertEquals( 3, keys.size());

        m = (ZMatrix.BaseZMatrix) ZMatrix.Loader.load( Arrays.asList(
                Arrays.asList("a","b" ),
                Arrays.asList(1,2), Arrays.asList(1,2), Arrays.asList(5,6)   ));
        keys = m.keys();
        assertEquals( 2, keys.size());
    }
}
