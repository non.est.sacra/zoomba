package zoomba.lang.core.operations;

import org.junit.Test;
import zoomba.lang.core.interpreter.ZInterpret;
import zoomba.lang.core.interpreter.ZScript;
import zoomba.lang.core.types.ZNumber;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.Consumer;

import static org.junit.Assert.*;

public class ZObservableTest {

    boolean throwError = false;
    boolean useObj = true;
    final RuntimeException ex = new RuntimeException("boom!");
    final Callable<Object> _laz = () -> {
        if ( throwError ) throw ex;
        if ( useObj ) return Long.valueOf(42);
        return null;
    };


    Observable<Object> obs = new Observable.ObservableBase<>() {

        @Override
        public Callable<Object> lazy() {
            return _laz;
        }

        @Override
        public List<Consumer<Throwable>> errorObservers() {
            super.errorObservers().add( (x) ->{
                assertSame(ex, x);
            });
            return errorListeners;
        }
    };

    @Test
    public void valueTest(){
        throwError = true;
        assertThrows( RuntimeException.class, () -> obs.value() );
    }

    @Test
    public void arithmeticLogicTest(){
        assertEquals( Long.valueOf(42), obs.comparable() );
        assertTrue(obs.numeric() instanceof ZNumber);
        assertEquals( 42, ZNumber.narrowNumber( obs.numeric()) );

        assertEquals( 42, obs._add_(0) );
        assertEquals( 42, obs._sub_(0) );
        assertEquals( 0, obs._mul_(0) );
        assertEquals( 21, obs._div_(2) );
        assertEquals( 42, obs._pow_(1)  );

        assertEquals( 42, obs._abs_() );
        assertEquals( -42, obs._not_() );
        assertEquals( 0, obs._mod_(2) );

        assertTrue( obs._and_(0) instanceof Integer );
        assertTrue( obs._or_(0) instanceof Integer );
        assertTrue( obs._xor_(0) instanceof Integer );

        assertEquals( 42, obs.actual() );
        assertEquals( 42, obs.ceil() );
        assertEquals( 42, obs.floor() );

        assertEquals(BigInteger.valueOf(42), obs.bigInteger() );
        assertEquals(BigDecimal.valueOf(42), obs.bigDecimal() );

        assertFalse( obs.fractional() );

        assertThrows( UnsupportedOperationException.class, () -> obs.add_mutable(0) );
        assertThrows( UnsupportedOperationException.class, () -> obs.sub_mutable(0) );
        assertThrows( UnsupportedOperationException.class, () -> obs.mul_mutable(0) );
        assertThrows( UnsupportedOperationException.class, () -> obs.div_mutable(1) );
        assertThrows( UnsupportedOperationException.class, () -> obs.mod_mutable(2) );

        assertThrows( UnsupportedOperationException.class, () -> obs.and_mutable(0) );
        assertThrows( UnsupportedOperationException.class, () -> obs.or_mutable(1) );
        assertThrows( UnsupportedOperationException.class, () -> obs.xor_mutable(2) );

        assertThrows( UnsupportedOperationException.class, () -> obs.not_mutable() );

        useObj = false;
        assertThrows( RuntimeException.class, () -> obs.comparable() );
    }

    @Test
    public void equalTest(){
        assertNotEquals( Long.valueOf(42).hashCode(), obs.hashCode() );
        assertFalse( obs.equals(null));
        assertFalse( obs.equals(42));
        assertTrue( obs.equals(obs));
        Observable o1 = new Observable.ObservableBase() {
            @Override
            public Callable lazy() {
                return _laz;
            }
        };
        assertTrue( obs.equals(o1));
        Observable o2 = new Observable.ObservableBase() {
            @Override
            public Callable lazy() {
                return null;
            }
        };
        assertFalse( obs.equals(o2));
    }

    @Test
    public void errorTest(){
        ZScript zs = new ZScript("#def{ panic(true, 'boom!') }");
        Object o = zs.execute().runTimeExceptionOnError().value();
        assertTrue( o instanceof Observable.ZObservable);
        Observable.ZObservable obs = (Observable.ZObservable)o;
        obs.error( Function.COLLECTOR_IDENTITY );
        assertThrows( zoomba.lang.core.types.ZException.Panic.class, obs::value);
        assertTrue( obs.toString().contains("#eval-error#"));
    }
}
