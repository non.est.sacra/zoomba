package zoomba.lang.core.operations;

import org.junit.Test;
import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.types.ZTypes;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import static org.junit.Assert.*;

public class ZRandomTest {

    public static final Function IDENTITY = new Function() {

        @Override
        public int definedParams(){
            return 1 ;
        }

        @Override
        public String body() {
            return "@ARGS[0]";
        }

        @Override
        public MonadicContainer execute(Object... args) {
            if ( args.length == 0 ) return Void ;
            return new MonadicContainerBase(args[0]);
        }

        @Override
        public String name() {
            return "identity";
        }
    };

    @Test
    public void randomFunctionTest(){
        assertSame(ZRandom.RANDOM, ZRandom.random());
        assertSame(ZRandom.RANDOM, ZRandom.random((Object) null));
        assertNull(ZRandom.shuffle(null));
    }

    @Test
    public void hashFunctionTest(){
        assertEquals("", ZRandom.hash());
        assertEquals("5a8dd3ad0756a93ded72b823b19dd877", ZRandom.hash("hello!"));
        assertEquals("686578", ZRandom.hash("hex", "hex")); // hex of hex
        assertThrows( IllegalArgumentException.class, () -> ZRandom.hash("hmac", "foo") );
        assertEquals("48",  ZRandom.hash("foo-bar", "0" ));
    }

    @Test
    public void selectFunctionTest(){
        assertEquals( ZTypes.NilEnum.NIL , ZRandom.RANDOM.select( ZTypes.NilEnum.NIL ) );
        assertSame( ZRandom.RANDOM, ZRandom.RANDOM.select( Function.NIL ) );
        assertSame( ZRandom.RANDOM, ZRandom.RANDOM.select( Function.NIL, 0) );
        assertEquals( ZTypes.NilEnum.NIL , ZRandom.RANDOM.select( ZTypes.NilEnum.NIL , 1 ) );
        assertEquals( Arrays.asList(1,3,4) , ZRandom.RANDOM.select( Arrays.asList(1,3,4) , 3 ) );
        assertSame( ZRandom.RANDOM, ZRandom.RANDOM.select( Function.NIL, 3) );
    }

    @Test
    public void shuffleNoListTest(){
        Object o = 42;
        assertSame( o, ZRandom.shuffle(o));
    }

    @Test
    public void numFunctionTest(){
        assertTrue( ZRandom.RANDOM.num() instanceof Integer);
        assertTrue( ZRandom.RANDOM.num(0L) instanceof Long);
        assertTrue( ZRandom.RANDOM.num(true) instanceof Double);
    }

    @Test
    public void sortFunctionTest(){
        assertSame( ZArray.EMPTY_ARRAY, ZRandom.sortAscending( null, null ));
        assertSame( ZArray.EMPTY_ARRAY, ZRandom.sortAscending( ZArray.EMPTY_ARRAY, null ));
        assertNull( ZRandom.sortAscending( new Object[]{ null }, null ));
        assertEquals( true, ZRandom.sortAscending( new Object[]{ true }, null ));
        Object[] arr = new Object[]{1,2,3,4};
        assertArrayEquals(arr , (Object[]) ZRandom.sortAscending( new Object[]{ 4,2,1,3 }, null ) );
    }

    @Test
    public void sumFunctionTest(){
        assertEquals( 0, ZRandom.sum( null, new Object[]{} ));
        assertEquals( 0, ZRandom.sum( null, new Object[]{ ZArray.EMPTY_Z_ARRAY } ));
        assertEquals( 10, ZRandom.sum( null, new Object[]{ 1,2,3,4 } ));
    }

    @Test
    public void minMaxFunctionTest(){
        assertSame( ZArray.EMPTY_ARRAY, ZRandom.minMax( null, new Object[]{} ));
        assertSame( ZArray.EMPTY_ARRAY, ZRandom.minMax( null, new Object[]{ ZArray.EMPTY_Z_ARRAY } ));
    }

    @Test
    public void tokenTest(){
        assertEquals("", ZRandom.tokens(null, null) );
        assertEquals("", ZRandom.tokens(null, ZArray.EMPTY_ARRAY) );

        Pattern p = Pattern.compile("\\w+");
        // https://stackoverflow.com/questions/10055034/java-comparing-two-pattern-objects
        assertEquals( p.pattern(), ZRandom.tokens(null, new Object[]{ "\\w+" }).toString() );
        assertEquals( p.pattern(), ZRandom.tokens(null, new Object[]{ p }).toString() );

        assertTrue( ZRandom.tokens(null, new Object[]{ "1234", "\\d" }) instanceof List);
        assertTrue( ZRandom.tokens(Collections.emptyList(), new Object[]{ "1234", "\\d" }) instanceof List);

    }

    @Test
    public void binarySearchFunctionTest(){
        // basic function
        assertEquals(-1, ZRandom.binarySearchBasic(null, null));
        assertEquals( 0, ZRandom.binarySearchBasic(Arrays.asList(1,2,3,4), 1 ));
        assertEquals(-1, ZRandom.binarySearchBasic(42, null));
        assertEquals( 2, ZRandom.binarySearchBasic(Arrays.asList(1,2,3,4).toArray(), 3));
        assertEquals( 1, ZRandom.binarySearchBasic("abc".toCharArray(), 'b'));

        // now complex ones
        assertEquals(-1, ZRandom.binarySearch(null, null));
        assertEquals(-1, ZRandom.binarySearch(ZArray.EMPTY_ARRAY, null));
        assertEquals(-1, ZRandom.binarySearch(new Object[]{ null, 42 }, null));
        assertEquals(-1, ZRandom.binarySearch(new Object[]{ 42, 42 }, null));


        assertEquals(1, ZRandom.binarySearch(new Object[]{ Arrays.asList(1,2,3) , 2}, IDENTITY));
        assertEquals(1, ZRandom.binarySearch(new Object[]{ Arrays.asList(1,2,3).toArray() , 2}, IDENTITY));
        assertEquals(1, ZRandom.binarySearch(new Object[]{ "abc".toCharArray() , 'b' }, IDENTITY));
        assertEquals(-1, ZRandom.binarySearch(new Object[]{ "boom" , 'b' }, IDENTITY));
    }
}
