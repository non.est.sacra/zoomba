package zoomba.lang.core.sys;

import org.junit.Test;
import zoomba.lang.core.interpreter.InterpreterScriptTest;

public class SysScriptsTest {

    final String THREADING = "samples/test/sys/sys_thread.zm" ;

    final String PROCESS_MAC = "samples/test/sys/sys_proc.zm" ;

    final String PROCESS_WIN = "samples/test/sys/sys_proc_win.zm" ;

    final String PROCESS_LINUX = "samples/test/sys/sys_proc_linux.zm" ;

    @Test
    public void testThreadFunctions()  {
        InterpreterScriptTest.danceWithScript( THREADING );
    }

    @Test
    public void testProcessFunctions()  {
        // different for Unix and Windows...
        final String osHint = System.getProperty("os.name").toLowerCase() ;
        final String actualPath;
        if ( osHint.contains("win")  ){
            actualPath = PROCESS_WIN ;
        } else if ( osHint.contains("mac")) {
            actualPath = PROCESS_MAC;
        }else {
            actualPath = PROCESS_LINUX ;
        }
        System.out.println("Detected that the process path should be : " + actualPath );
        InterpreterScriptTest.danceWithScript( actualPath );
    }
}
