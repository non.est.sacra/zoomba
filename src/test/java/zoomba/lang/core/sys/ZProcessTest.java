package zoomba.lang.core.sys;


import org.junit.Test;
import zoomba.lang.core.io.ZFileSystem;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class ZProcessTest {

    @Test
    public void currentProcessTest() throws Exception {
        ZProcess.CurrentProcess cp = new ZProcess.CurrentProcess();
        assertNotEquals(0, cp.pid);
        assertEquals( -1, cp.getErrorStream().read());
        assertEquals( -1, cp.getInputStream().read());
        cp.getOutputStream().write( 42 );
        assertEquals(0, cp.waitFor() );
        assertEquals(0, cp.exitValue() );
        cp.destroy();
    }

    @Test
    public void procInfoTest(){
        Map<String,Object> config = new HashMap<>();
        config.put(ZProcess.ZProcInfo.ARGS, new String[] { "ls", "-al"} );
        ZProcess.ZProcInfo info = new ZProcess.ZProcInfo( config );
        assertFalse( info.redirectInput );
        assertFalse( info.redirectOutput );
        assertFalse( info.redirectError );
        config.put(ZProcess.ZProcInfo.REDIRECT, Collections.emptyMap() );
        info = new ZProcess.ZProcInfo( config );
        assertFalse( info.redirectInput );
        assertFalse( info.redirectOutput );
        assertFalse( info.redirectError );
    }

    @Test
    public void redirectedProcessTest() throws Exception{

        final String prefix =  "zmb_ls_" + System.currentTimeMillis();
        final String tmp = Files.createTempDirectory(prefix).toAbsolutePath().toString();
        final String inputFile = Paths.get(tmp, "stdin.txt").toString();
        final String outFile = Paths.get(tmp, "stdout.txt").toString();
        final String errFile = Paths.get(tmp, "stderr.txt").toString();
        final String catData = "hello!" ;
        ZFileSystem.write( inputFile, catData);

        Map<String,Object> config = new HashMap<>();
        config.put(ZProcess.ZProcInfo.ARGS, new String[] { "cat" } );
        Map<String,String> redirects = new HashMap<>();

        redirects.put( ZProcess.ZProcInfo.STDIN, inputFile );
        redirects.put( ZProcess.ZProcInfo.STDOUT, outFile );
        redirects.put( ZProcess.ZProcInfo.STDERR, errFile );
        config.put(ZProcess.ZProcInfo.REDIRECT, redirects);

        ZProcess.ZProcInfo info = new ZProcess.ZProcInfo( config );
        assertTrue( info.redirectInput );
        assertTrue( info.redirectOutput );
        assertTrue( info.redirectError );
        ZProcess proc = info.start();
        assertNotNull(proc.process);
        assertNotEquals( 0, proc.process.pid() );
        assertEquals(0, proc.status().code.intValue() );
        String redirectedData = ZFileSystem.read( inputFile ).toString();
        assertEquals(catData, redirectedData );
    }

    @Test
    public void pOpenFunctionTest(){
        assertEquals( ZProcess.CurrentProcess.class, ZProcess.popen().process.getClass() );
        ZProcess p =  ZProcess.popen("ls", "-al");

        p.pollWait(10);
        assertEquals( 0, p.process.exitValue() );

        Map<String,Object> m = new HashMap<>();
        m.put(ZProcess.ZProcInfo.ARGS, new String[]{ "ls", "-al" });
        p =  ZProcess.popen(m);
        p.waitOn(1000L);
        assertEquals( 0, p.process.exitValue() );


        p =  ZProcess.popen( Arrays.asList("ls", "-al") );
        p.waitOn(1000L);
        assertEquals( 0, p.process.exitValue() );

        p =  ZProcess.popen( (Object) new String[]{ "ls", "-al" });
        p.waitOn(1000L);
        assertEquals( 0, p.process.exitValue() );

        p =  ZProcess.popen("ls", Arrays.asList("-al"));
        assertEquals( 0, p.status(1000).code.intValue() );

        p =  ZProcess.popen("ls", new String[]{ "-al" });
        p.waitOn(-1L);
        assertEquals( 0, p.process.exitValue() );
    }

    @Test
    public void waitAndDestroyTest(){
        ZProcess p = ZProcess.popen("ls", "/", "-alR");
        assertFalse( p.waitOn(1000) );
        assertTrue( p.process.isAlive() );
        assertTrue(p.kill());
    }
}
