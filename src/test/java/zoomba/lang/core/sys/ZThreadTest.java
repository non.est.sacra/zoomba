package zoomba.lang.core.sys;

import org.junit.Test;
import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.operations.Function;

import static org.junit.Assert.*;

public class ZThreadTest {
    @Test
    public void virtualExecutorTest(){
        ZThread.getExecutor(-1);
    }

    @Test
    public void threadRunTest() throws Exception{
        // Good case
        ZThread zt = new ZThread( Function.TRUE );
        assertThrows( IllegalStateException.class, zt::thread );
        assertTrue( zt.isNil() );
        assertFalse( zt.isFinished() );
        zt.join(100);
        assertFalse( zt.isAlive() );
        zt.start();
        zt.start();
        assertTrue( zt.isAlive() );
        zt.join();
        assertFalse( zt.isAlive() );
        assertTrue( zt.isFinished() );
        assertEquals(true, zt.value());
        assertNotNull(zt.thread());

        // Ugly case, with error
        Function f = new Function() {
            @Override
            public String body() {
                return "";
            }

            @Override
            public MonadicContainer execute(Object... args) {
                throw new RuntimeException("foo!");
            }

            @Override
            public String name() {
                return "";
            }
        };
        final ZThread zt1 = new ZThread( f );
        zt1.start();
        Thread.sleep( 400 );
        assertTrue( zt1.isFinished() );
        assertThrows( Exception.class, zt1::call);
    }

    @Test
    public void schedulerCreationErrorTest(){
        assertThrows( IllegalArgumentException.class, () -> ZThread.SchedulerInputConfig.fromArgs(null, null, new Object[]{1} ));
    }

    @Test
    public void pollFunctionTest(){
        assertTrue( ZThread.poll(null, ZArray.EMPTY_ARRAY ) );
        assertFalse( ZThread.poll(Function.FALSE, new Object[]{ 4, 10L } ) );
    }
}
