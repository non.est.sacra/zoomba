package zoomba.lang.core.types;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.Assert.*;

public class CircularBufferTest {

    public CircularBuffer<Integer> createBuf(int n){
        CircularBuffer<Integer> cb = new CircularBuffer<>(n);
        assertEquals(0, cb.size());
        assertTrue( cb.isEmpty() );
        //System.out.println(cb);
        final int maxLen = 2*n + n/2 ;
        for(int i=0; i< maxLen; i++ ){
            cb.add(i);
            //System.out.println(cb);
        }
        for ( int i=0; i < n;i++){
            assertFalse( cb.contains(i) );
        }
        assertFalse( cb.isEmpty() );
        assertEquals( cb.bufSize(), cb.size() );
        Integer[] arr = cb.toArray( new Integer[n]) ;
        for ( int i=0; i < n;i++){
            assertEquals( (maxLen - 1-i), (int)arr[n-i-1] );
        }
        return cb;
    }

    @Test
    public void createAndPopulateTest(){
        createBuf(2);
        createBuf(5);
        createBuf(19);
        createBuf(100);
    }

    @Test
    public void queueFunctionsTest(){
        CircularBuffer<Integer> cb = new CircularBuffer<>(3);
        cb.offer(0);
        assertEquals( Integer.valueOf(0), cb.peek() );
        assertEquals( Integer.valueOf(0), cb.element() );
        cb.offer(1);
        assertEquals( Integer.valueOf(0), cb.peek() );
        assertEquals( Integer.valueOf(0), cb.poll() );
        assertEquals( Integer.valueOf(1), cb.remove() );
        assertNull(cb.poll());
        assertNull(cb.peek());
        assertThrows( NoSuchElementException.class, cb::remove);
        assertThrows( NoSuchElementException.class, cb::element);
    }

    @Test
    public void collectionsFunctionsTest(){
        CircularBuffer<Integer> cb = new CircularBuffer<>(3);
        cb.addAll(List.of(1,2,3,4));
        assertEquals(Integer.valueOf(2), cb.peek() );
        assertTrue( cb.contains(2));
        assertTrue( cb.contains(3));
        assertTrue( cb.containsAll(List.of(2,3,4)));
        assertFalse( cb.containsAll(List.of(2,1,4)));
        assertThrows( UnsupportedOperationException.class, () -> cb.remove(10));
        assertThrows( UnsupportedOperationException.class, () -> cb.removeAll(Collections.emptyList()));
        assertThrows( UnsupportedOperationException.class, () -> cb.retainAll(Collections.emptyList()));
        assertEquals( 3, cb.toArray( new Integer[]{} ).length );
        assertEquals( 6, cb.toArray( new Integer[]{10,11,12,13,14,15} ).length );
        CircularBuffer<Integer> c = new CircularBuffer<>(1);
        assertThrows( NoSuchElementException.class, () -> c.iterator().next());

    }

    @Test
    public void stdObjectFunctionsTest(){
        CircularBuffer<Integer> c1 = createBuf(3);
        CircularBuffer<Integer> c2 = createBuf(3);
        assertEquals(c1, c2);
        CircularBuffer<Integer> c3 = createBuf(4);
        assertNotEquals(c3, c1);
        assertNotEquals(c3, ZTypes.NilEnum.NIL);
        assertNotEquals(c3, null);
        assertEquals(Integer.valueOf(4), c1.poll() ) ;
        assertNotEquals(c1, c2);
        assertEquals("<*5,6_>", c1.toString() );
        assertEquals(Arrays.hashCode(new int[]{5,6}), c1.hashCode()) ;
        c1.add(10);
        assertNotEquals(c1, c2);
    }
}
