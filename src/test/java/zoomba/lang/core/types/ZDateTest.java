package zoomba.lang.core.types;


import org.junit.Test;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.temporal.TemporalAmount;
import java.util.Date;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

public class ZDateTest {

    @Test
    public void instantTest(){
        ZDate dt = new ZDate();
        assertSame( dt._instant, dt.timeStamp() );
        Date cp = dt.deepCopy();
        assertEquals( dt.date(), cp);
        assertSame( dt.time, dt.getTime() );
        dt = new ZDate("+09");
        assertNotEquals( new Date(), dt.date() );
        assertThrows( IllegalArgumentException.class, () -> new ZDate("foo-bar"));
    }

    @Test
    public void timeFunctionTest(){
        LocalDateTime lt = LocalDateTime.now();
        assertEquals( lt, ZDate.time( lt ).time );
        ZDate dt = new ZDate();
        assertEquals( dt.date(), ZDate.time( dt.date() ).date() );
        assertNotNull( ZDate.time( dt.date(), "Z") );
        assertNull(  ZDate.time( dt.date(), false ) );
        assertNull(  ZDate.time( false ) );
    }

    @Test
    public void stringFunctionTest(){
        assertTrue( new ZDate().string( "yyyy").startsWith("202") );
        assertTrue( new ZDate().string( false).startsWith("202") );
        ZDate dt = new ZDate( "1900", "yyyy") ;
        assertTrue( dt.string("yyyy-MM-dd HH:mm:ss.SSS" ).contains(".") );
    }

    @Test
    public void zoneIdConversion(){
        Date dt = new Date();
        Date utc = new ZDate().date("+09");
        assertNotEquals( dt, utc);
    }

    @Test
    public void arithmeticOnTimeTest(){
        // immutables
        assertThrows( IllegalArgumentException.class, () -> new ZDate()._add_( "XX") );
        assertThrows( IllegalArgumentException.class, () -> new ZDate()._add_( "PX11") );
        assertThrows( IllegalArgumentException.class, () -> new ZDate()._sub_( "XX") );
        assertThrows( IllegalArgumentException.class, () -> new ZDate()._sub_( "PX11") );
        ZDate dt = new ZDate();
        assertTrue( String.valueOf( dt._sub_( dt.date())).startsWith("PT0.00") ) ;
        // invalid ops
        assertThrows( UnsupportedOperationException.class, () -> new ZDate()._mul_(1) );
        assertThrows( UnsupportedOperationException.class, () -> new ZDate()._div_(1) );
        assertThrows( UnsupportedOperationException.class, () -> new ZDate()._pow_(1) );
        assertThrows( UnsupportedOperationException.class, () -> new ZDate().mul_mutable(1) );
        assertThrows( UnsupportedOperationException.class, () -> new ZDate().div_mutable(1) );
        // mutable
        assertThrows( IllegalArgumentException.class, () -> new ZDate().add_mutable( "XX") );
        assertThrows( IllegalArgumentException.class, () -> new ZDate().sub_mutable( "PX11") );
        dt = new ZDate();
        Date clone = dt.date();
        dt.sub_mutable(Duration.ofMillis(42));
        assertEquals( clone.getTime() - 42L , dt.date().getTime());
        dt.add_mutable(Duration.ofMillis(42));
        assertEquals( clone.getTime() , dt.date().getTime());
    }

    @Test
    public void compareTest(){
        ZDate dt = new ZDate();
        assertTrue( dt.equals(dt));
        Date clone = dt.deepCopy();
        assertTrue( dt.equals(clone));
        assertFalse( dt.equals(null));
        assertTrue( dt.equals( dt.time));
        assertThrows( IllegalArgumentException.class, () -> dt.equals("foobar") );
    }

    @Test
    public void issue_102_Test(){
        assertThrows( UnsupportedOperationException.class, () -> ZDate.checkAmbiguity(Period.parse("P1Y")));
        assertThrows( UnsupportedOperationException.class, () -> ZDate.checkAmbiguity(Period.parse("P1M")));
        assertThrows( UnsupportedOperationException.class, () -> ZDate.checkAmbiguity(Period.parse("P1Y1M")));
        TemporalAmount ta = Duration.parse("P1000D") ;
        assertSame( ta, ZDate.checkAmbiguity(ta));
    }
}
