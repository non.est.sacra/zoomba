package zoomba.lang.core.types;

import org.junit.Assert;
import org.junit.Test;
import zoomba.lang.core.collections.ZSet;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.*;

/**
 */
public class ZNumberTest {

    @Test
    public void testDefaultMathContext(){
        Assert.assertEquals(MathContext.DECIMAL128.getPrecision(), ZNumber.precision() );
    }

    @Test
    public void deepCopyTest(){
        ZNumber zn = new ZNumber("42424242424242424242424242424242424242424242");
        ZNumber zc = (ZNumber) zn.deepCopy();
        Assert.assertNotSame( zn.bigInteger, zc.bigInteger );
        Assert.assertEquals( zn.bigInteger, zc.bigInteger );

        zn = new ZNumber(12345);
        zc = (ZNumber) zn.deepCopy();
        Assert.assertEquals( zn.lValue, zc.lValue );
    }

    @Test
    public void testAssignment(){
        ZNumber zn = new ZNumber(0);

        assertTrue( zn.actual() instanceof Integer );
        zn = new ZNumber(0.00);

        assertTrue( zn.actual() instanceof Double );

        // test auto assign
        zn = new ZNumber("0.121010011104111973947987917179719219798783798473974398");
        assertTrue( zn.actual() instanceof BigDecimal);

        zn = new ZNumber("0.121010011100000");
        assertTrue( zn.actual() instanceof Double);

        zn = new ZNumber(1134234229);
        assertTrue( zn.actual() instanceof Integer );

        zn = new ZNumber(1134234229998989L);
        assertTrue( zn.actual() instanceof Long );

        zn = new ZNumber("1134234229998989979473948759348759834793");
        assertTrue( zn.actual() instanceof BigInteger);

        ZNumber n0 = new ZNumber(0);
        ZNumber d0 = new ZNumber(0.0);
        assertEquals(n0, d0);

        ZNumber d01 = new ZNumber(0.01);
        assertNotEquals(n0, d01);
        assertNotEquals(d0, d01);

        assertEquals( n0.compareTo(d0) , 0 );
        assertEquals( d0.compareTo(d01) , -1 );
        assertEquals( n0.compareTo(d01) , -1 );

        // populate method test
        Long l = 42L;
        Assert.assertEquals( (long)l, new ZNumber(l.byteValue() ).lValue );
        Assert.assertEquals( (long)l, new ZNumber(l.shortValue()).lValue );
        Assert.assertEquals( (long)l, new ZNumber(l.intValue()).lValue );
        Assert.assertEquals( (long)l, new ZNumber(l).lValue );

        // double and float
        Double d = 42.0d;
        Assert.assertEquals( d, new ZNumber( d.floatValue() ).doubleValue() , 0.0001d);
        Assert.assertEquals( d, new ZNumber( d ).doubleValue() , 0.0001d );

        Assert.assertEquals( BigDecimal.ONE, new ZNumber( ZRational.ONE ).bigDecimal );

        Date dt = new Date();
        Assert.assertEquals( dt.getTime(), new ZNumber( dt).lValue );

    }

    @Test
    public void absTest(){

        ZNumber n0 = new ZNumber(0);
        ZNumber d0 = new ZNumber(0.0);

        assertEquals( 0, n0._abs_() );
        assertEquals( 0.0, d0._abs_() );

        ZNumber d01 = new ZNumber(0.01);
        assertTrue( d01._abs_().doubleValue() - 0.01 < 0.0001);
        d01 = new ZNumber(-0.01);
        assertTrue( d01._abs_().doubleValue() > 0 );
        assertTrue( d01._abs_().doubleValue() - 0.01 < 0.0001);

        ZNumber zn = new ZNumber("0.121010011104111973947987917179719219798783798473974398");
        assertTrue( zn._abs_() instanceof BigDecimal);

        assertEquals( 10, new ZNumber(BigInteger.TEN.negate())._abs_() );
    }

    @Test
    public void additionTest(){
        ZNumber n0 = new ZNumber(0);
        ZNumber d0 = new ZNumber(0.0);
        assertEquals( 0.0, d0._add_(n0));
        assertEquals( 0.0, n0._add_(d0));
        long ln = -123213131312311131L ;
        ZNumber zln = new ZNumber(ln);
        // must upcast?
        Object r = zln._add_(n0);
        assertEquals( ln , r);
        // Now plenty of tests to go by
        BigInteger res = BigInteger.valueOf( Long.MAX_VALUE ).add( BigInteger.ONE);
        Assert.assertEquals( res, new ZNumber(Long.MAX_VALUE)._add_(1) );
        // lef int, right fractional
        Assert.assertEquals( 1.2d, new ZNumber(1)._add_(0.2d) );
        // lef fractional, right int
        Assert.assertEquals( 1.2d, new ZNumber(0.2d)._add_(1) );

    }

    @Test
    public void subtractionTest(){
        Assert.assertEquals( 2, new ZNumber(3)._sub_(1));
        // Now plenty of tests to go by
        BigInteger res = BigInteger.valueOf( Long.MIN_VALUE ).subtract( BigInteger.ONE);
        Assert.assertEquals( res, new ZNumber(Long.MIN_VALUE)._sub_(1) );
        // lef int, right fractional
        Assert.assertEquals( 1.8d, new ZNumber(2)._sub_(0.2d) );
        // lef fractional, right int
        Assert.assertEquals( -0.8d, new ZNumber(0.2d)._sub_(1) );
    }


    @Test
    public void multiplicationTest(){
        int i = 6 ;
        ZNumber n = new ZNumber(7);
        Object o = n._mul_(i);
        assertEquals(42,o);
        n = new ZNumber(7.0);
        o = n._mul_(i);
        assertEquals(42.0,o);
        o = n._mul_(6.00);
        assertEquals(42.0,o);
        BigInteger bi = BigInteger.valueOf(Long.MAX_VALUE).multiply( BigInteger.TWO);
        Assert.assertEquals( bi,  new ZNumber(Long.MAX_VALUE)._mul_(2));
    }

    @Test
    public void divisionTest(){
        int i = 6 ;
        ZNumber n = new ZNumber(42);
        Object o = n._div_(i);
        assertEquals(7,o);

        n = new ZNumber(42.0);
        o = n._div_(6);
        assertEquals(7.0,o);

        ZNumber a = new ZNumber("0.11213131010801823013821093221097319873211381981111109099080921");
        ZNumber b = new ZNumber("0.9809809809890119018230981203981309173891481640872630164012746127");
        o = a._div_(b);
        assertTrue( o instanceof BigDecimal );

        Assert.assertEquals( 1.0d,  new ZNumber(1.0)._div_(1));
        Assert.assertEquals( 1.0d,  new ZNumber(1)._div_(1.0));

        Assert.assertEquals( 1,  new ZNumber(BigInteger.ONE)._div_(1));
        Assert.assertEquals( 1,  new ZNumber(1)._div_(BigInteger.ONE));

        ZNumber zn = new ZNumber(4);
        zn.sub_mutable(2);
        Assert.assertEquals( 2, zn.lValue );
    }

    @Test
    public void logicTest(){
        ZNumber n = new ZNumber(1);
        n.and_mutable(0);
        assertEquals(0, n.intValue());

        n.or_mutable(1);
        assertEquals(1, n.intValue());

        n.xor_mutable(0);
        assertEquals(1, n.intValue());

        n.mod_mutable( 2);
        assertEquals(1, n.intValue());

        n.not_mutable();
        // this should work out
        assertEquals(-1, n.intValue());
        // now the mutable logic
        n.sub_mutable(-1);
        assertEquals(0, n.intValue());

        n = new ZNumber(BigInteger.ONE);
        n.and_mutable(0);
        assertEquals(0, n.intValue());

        n = new ZNumber(BigInteger.ONE);
        n.or_mutable(1);
        assertEquals(1, n.intValue());

        n = new ZNumber(BigInteger.ONE);
        n.xor_mutable(0);
        assertEquals(1, n.intValue());

        n = new ZNumber(BigInteger.ONE);
        n.mod_mutable( 2);
        assertEquals(1, n.intValue());

        n = new ZNumber(BigInteger.ONE);
        n.not_mutable();
        // this should work out
        assertEquals(-1, n.intValue());

    }

    @Test
    public void negationTest(){
        Assert.assertEquals( 1, new ZNumber(-1)._not_() );
        Assert.assertEquals( 0, new ZNumber(0)._not_() );
        Assert.assertEquals( BigInteger.valueOf(Long.MIN_VALUE).negate(), new ZNumber(Long.MIN_VALUE)._not_() );
        Assert.assertEquals( -9223372036854775807L, new ZNumber(Long.MAX_VALUE)._not_() );
    }

    @Test
    public void testExtended(){
        ZNumber n = new ZNumber(1.1);
        assertEquals(1, n.floor() );
        assertEquals(2, n.ceil() );
        // test hash code
        Set s = ZSet.set( null, new ZNumber(1), new ZNumber(1) );
        assertEquals( s.size(), 1);
        s.clear();
        s = ZSet.set( null, new ZNumber(1), n );
        assertEquals( s.size(), 2);
    }

    @Test
    public void cielTest(){
        ZNumber zn = new ZNumber(0.1);
        assertEquals(1, zn.ceil());
        zn = new ZNumber(2.1);
        assertEquals(3, zn.ceil());
        zn = new ZNumber(1.5);
        assertEquals(2, zn.ceil());
        zn = new ZNumber(1.7);
        assertEquals(2, zn.ceil());

        zn = new ZNumber(-2.1);
        assertEquals(-2, zn.ceil());
        zn = new ZNumber(-2.0);
        assertEquals(-2, zn.ceil());
        zn = new ZNumber(0.0);
        assertEquals(0, zn.ceil());
        Assert.assertEquals( 1, new ZNumber(1).ceil() );

        // now the static function
        Long l = 41L;
        Assert.assertEquals( l.byteValue(), ZNumber.ceil(l.byteValue()));
        Assert.assertEquals( l.shortValue(), ZNumber.ceil(l.shortValue()));
        Assert.assertEquals( l.intValue(), ZNumber.ceil(l.intValue()));
        Assert.assertEquals( l, ZNumber.ceil(l));
        BigInteger bi = BigInteger.valueOf(l);
        Assert.assertEquals( bi, ZNumber.ceil(bi));

        Assert.assertEquals( 42, ZNumber.ceil(41.1f) );
        Assert.assertEquals( 42, ZNumber.ceil(41.1d) );

        BigDecimal bd = new BigDecimal( "41.22") ;
        Assert.assertEquals( 42, ZNumber.ceil(bd) );

    }

    @Test
    public void floorTest(){
        ZNumber zn = new ZNumber(0.1);
        assertEquals(0, zn.floor());
        zn = new ZNumber(0.5);
        assertEquals(0, zn.floor());

        zn = new ZNumber(2.1);
        assertEquals(2, zn.floor());
        zn = new ZNumber(-0.1);
        assertEquals(-1, zn.floor());
        zn = new ZNumber(-1.0);
        assertEquals(-1, zn.floor());

        zn = new ZNumber(-2.1);
        assertEquals(-3, zn.floor());
        zn = new ZNumber(-2.0);
        assertEquals(-2, zn.floor());
        zn = new ZNumber(0.0);
        assertEquals(0, zn.floor());

        Assert.assertEquals( 1, new ZNumber(1).floor() );
        // now the static function
        Long l = 42L;
        Assert.assertEquals( l.byteValue(), ZNumber.floor(l.byteValue()));
        Assert.assertEquals( l.shortValue(), ZNumber.floor(l.shortValue()));
        Assert.assertEquals( l.intValue(), ZNumber.floor(l.intValue()));
        Assert.assertEquals( l, ZNumber.floor(l));
        BigInteger bi = BigInteger.valueOf(l);
        Assert.assertEquals( bi, ZNumber.floor(bi));

        Assert.assertEquals( 42, ZNumber.floor(42.1f) );
        Assert.assertEquals( 42, ZNumber.floor(42.1d) );

        BigDecimal bd = new BigDecimal( "42.22") ;
        Assert.assertEquals( 42, ZNumber.floor(bd) );

    }

    @Test
    public void roundingTest(){
        Number rn = ZNumber.round(0.2314, 2 );
        assertEquals( 4,rn.toString().length());
        assertEquals(0.23, rn);
        rn = ZNumber.round(0.2514, 1 );
        assertEquals( 3,rn.toString().length());
        assertEquals(0.3, rn);
        Long l = 42L;
        Assert.assertEquals( l.byteValue(), ZNumber.round( l.byteValue() ) );
        Assert.assertEquals( l.shortValue(), ZNumber.round( l.shortValue() ) );
        Assert.assertEquals( l.intValue(), ZNumber.round( l.intValue() ) );
        Assert.assertEquals( l, ZNumber.round( l ) );
        BigInteger bi = BigInteger.valueOf(l);
        Assert.assertSame( bi, ZNumber.round(bi) );

        Assert.assertEquals( l.intValue(), ZNumber.round( new ZNumber(l) ) );
        Assert.assertEquals( l.intValue(), ZNumber.round( 42.001, -1 ) );
    }

    @Test
    public void fastExponentiationTest(){
        // BigInt, long
        Assert.assertThrows( IllegalArgumentException.class , () ->{
            ZNumber.fastExp(BigInteger.valueOf(1), -1);
        }) ;
        Assert.assertEquals( BigInteger.ONE, ZNumber.fastExp( BigInteger.TEN, 0 ));
        // BigDec, long
        Assert.assertEquals( BigDecimal.ONE, ZNumber.fastExp( BigDecimal.TEN, 0 ));
        Assert.assertEquals( BigDecimal.ONE, ZNumber.fastExp( BigDecimal.ONE, Long.MAX_VALUE ));
        Assert.assertEquals( new BigDecimal("0.001"), ZNumber.fastExp( BigDecimal.TEN, -3 ));
    }

    @Test
    public void powerTest(){
        ZNumber zn = new ZNumber(12331201231.12321312312301301d);
        long pl = 0 ;
        Object o = zn._pow_(pl);
        assertEquals( 1.0d, o );
        zn = new ZNumber("121213123182012810280129824109481098");
        o = zn._pow_(pl);
        assertEquals( 1, o );

        // -ve power
        ZNumber n = new ZNumber(2.0);
        Number p = (Number) n._pow_(-1);
        assertEquals(0.5,p);
        // 0 power
        n = new ZNumber(2.0);
        p = (Number) n._pow_(0);
        assertEquals(1.0, p );
        // fractional num , integral power
        n = new ZNumber(1.1);
        p = (Number) n._pow_(2);
        assertEquals(1.21, p );
        // fractional power
        n = new ZNumber(4.0);
        p = (Number) n._pow_(0.5);
        assertEquals(2.0, p );
    }

    @Test
    public void integerValueTest(){
        ZNumber zn = new ZNumber(1.0 );
        Assert.assertTrue( zn.integerValued() );
        zn = new ZNumber(12121.0 );
        Assert.assertTrue( zn.integerValued() );
        zn = new ZNumber(-1211.0 );
        Assert.assertTrue( zn.integerValued() );
        zn = new ZNumber(-11.0 );
        Assert.assertTrue( zn.integerValued() );
        // fractional
        zn = new ZNumber(-11.1 );
        Assert.assertFalse( zn.integerValued() );
    }

    @Test
    public void modTest(){
        Assert.assertEquals( 0, new ZNumber(3)._mod_( 2.0 ));
        Assert.assertEquals( 0, new ZNumber(2.0)._mod_( 3 ));
        Assert.assertThrows( IllegalArgumentException.class, () ->{
           new ZNumber(1)._mod_(0);
        });
        Assert.assertTrue( new ZNumber(0).isZero() );
        Assert.assertTrue( new ZNumber(0L).isZero() );
        Assert.assertTrue( new ZNumber(0.00d).isZero() );
        Assert.assertTrue( new ZNumber(BigInteger.ZERO).isZero() );
        Assert.assertTrue( new ZNumber(BigDecimal.ZERO).isZero() );
    }

    @Test
    public void narrowNumberTest(){
        Long l = 42L;
        Assert.assertEquals( l.byteValue(), ZNumber.narrowNumber( l.byteValue() ) );
        Assert.assertEquals( l.shortValue(), ZNumber.narrowNumber( l.shortValue() ) );
        Assert.assertEquals( l.intValue(), ZNumber.narrowNumber( l.intValue() ) );
        Assert.assertEquals( l.intValue(), ZNumber.narrowNumber( l ) );
        BigInteger bi = BigInteger.valueOf(l);
        Assert.assertSame( 42, ZNumber.narrowNumber(bi) );

        Assert.assertEquals( l.intValue(), ZNumber.narrowNumber( new ZNumber(l) ) );
        Assert.assertEquals( l.intValue(), ZNumber.narrowNumber( new BigDecimal(l) ) );
    }

    /**
     * <a href="https://math.tools/calculator/base/10-62">...</a>
     */
    @Test
    public void toRadixConversionTest(){
        Assert.assertEquals( "0", ZNumber.radix(BigInteger.valueOf(0), 50 ));
        Assert.assertEquals( "10", ZNumber.radix(BigInteger.valueOf(50), 50 ));
        Assert.assertEquals( "-10", ZNumber.radix(BigInteger.valueOf(-50), 50 ));

        Assert.assertEquals( "j8oomjdGeneLBR85B", ZNumber.radix(new BigInteger("129100981290121212131231121999"), 52 ));
        Assert.assertEquals( "2Z8kMb4FAaEMgIgaA9ULYjVDF", ZNumber.radix(new BigInteger("408908098908790712907831290387190827301987"), 52 ));

        Assert.assertEquals( "10", ZNumber.radix(BigInteger.valueOf(62), 62 ));
        Assert.assertEquals( "-10", ZNumber.radix(BigInteger.valueOf(-62), 62 ));

        Assert.assertEquals( "2htso7V2orzNciEkJ", ZNumber.radix(new BigInteger("129100981290121212131231121999"), 62 ));
        Assert.assertEquals( "UkYUqKHu6QjUi4E5OP", ZNumber.radix(new BigInteger("90890123103198039123219998878797"), 62 ));

        Assert.assertEquals( BigInteger.TEN, ZNumber.intRadix("10", 10 ));
        Assert.assertThrows( NumberFormatException.class, () -> ZNumber.intRadix("10", 80 ));
        Assert.assertEquals( "10" ,  ZNumber.radix(BigInteger.TEN, 80 ));

    }

    @Test
    public void fromRadixConversionTest(){
        Assert.assertEquals( "0", ZNumber.intRadix("0", 50 ).toString() );
        Assert.assertEquals( "50", ZNumber.intRadix("10", 50 ).toString() );
        Assert.assertEquals( "-50", ZNumber.intRadix("-10", 50 ).toString() );

        Assert.assertEquals( "129100981290121212131231121999",  ZNumber.intRadix("j8oomjdGeneLBR85B", 52 ).toString());
        Assert.assertEquals( "90890123103198039123219998878797",  ZNumber.intRadix("UkYUqKHu6QjUi4E5OP", 62 ).toString());

        Exception ex = Assert.assertThrows( IllegalArgumentException.class , () ->{
            ZNumber.intRadix("*", 45);
        });
        Assert.assertNotNull(ex);
    }

    @Test
    public void integerFunctionTest(){
        // regular integer...
        Assert.assertNull( ZNumber.integer( new Object[]{ } ) );
        Assert.assertEquals( 1, ZNumber.integer(Map.of( "value", "1" )));
        Long l = 42L;
        Assert.assertEquals( l.byteValue(), ZNumber.integer( l.byteValue()) );
        Assert.assertEquals( l.shortValue(), ZNumber.integer( l.shortValue()) );
        Assert.assertEquals( l.intValue(), ZNumber.integer( l.intValue()) );
        Date dt = new Date();
        Assert.assertEquals( dt.getTime(), ZNumber.integer( dt ) );
        // INT...
        ZDate zd = new ZDate();
        Assert.assertNull( ZNumber.INT() );
        Assert.assertEquals( zd.toNano(), ZNumber.INT( zd ) );

        // integer() function
        ZNumber zn = new ZNumber(1);
        Assert.assertEquals(1, zn.integer());
        zn = new ZNumber("4242424242424242424242424242424242424242");
        Assert.assertSame(zn.bigInteger, zn.integer());
        zn = new ZNumber("42424242424242424242424242424.24242424242");
        Assert.assertEquals(zn.bigInteger(), zn.integer());

        // couple of other coercions
        Assert.assertEquals(0, ZNumber.integer( ZTypes.NilEnum.NIL));
        Assert.assertEquals((int)'a', ZNumber.integer( 'a'));
        Assert.assertEquals(1, ZNumber.integer( true));
        Assert.assertEquals(0, ZNumber.integer( false));

    }

    @Test
    public void floatFunctionTest(){
        // regular floating...
        Assert.assertNull( ZNumber.floating( new Object[]{ } ) );
        Assert.assertEquals( 97.0, ZNumber.floating( 'a') );
        Assert.assertEquals( 97.0f, ZNumber.floating( 97.f) );
        Assert.assertEquals( 97.0d, ZNumber.floating( 97.d) );
        Long l = 42L;
        Assert.assertEquals( l.doubleValue(), ZNumber.floating( l.byteValue()) );
        Assert.assertEquals( l.doubleValue(), ZNumber.floating( l.shortValue()) );
        Assert.assertEquals( l.doubleValue(), ZNumber.floating( l.intValue()) );
        Assert.assertEquals( l.doubleValue(), ZNumber.floating( l ) );
        BigInteger bi = BigInteger.valueOf(42);
        BigDecimal bd = new BigDecimal(bi);
        Assert.assertEquals( 42.0d, ZNumber.floating( bi ) );
        Assert.assertEquals( bd, ZNumber.floating( bd ) );

        Assert.assertEquals( 1.0, ZNumber.floating(ZRational.ONE) );
        Assert.assertEquals( 1.3d, ZNumber.floating(new ZRational(1,2), 1.3d) );

        Assert.assertNull( ZNumber.floating( new ZRational(1,2) )  );

        // FLOAT
        Assert.assertNull( ZNumber.FLOAT( ) );
        ZDate zDate = new ZDate();
        assertEquals( zDate.toNano().toString(), ZNumber.FLOAT(zDate).toString());

        // floating() function
        ZNumber zn = new ZNumber(1);
        Assert.assertEquals(1.0D, zn.floating());
        zn = new ZNumber("4242424242424242424242424242424242424242");
        Assert.assertEquals(zn.bigDecimal(), zn.floating()) ;
        zn = new ZNumber("42424242424242424242424242424.24242424242");
        Assert.assertSame(zn.bigDecimal, zn.floating());

        zn = new ZNumber(Long.MAX_VALUE);
        Assert.assertEquals(zn.bigDecimal(), zn.floating()) ;

        zn = new ZNumber(Integer.MAX_VALUE);
        double d = Integer.MAX_VALUE ;
        Assert.assertEquals(d, zn.floating()) ;


        // numeric
        Assert.assertNull( ZNumber.number() );
        Assert.assertEquals( 42, ZNumber.number( "xxx", 42 ) );
        Assert.assertEquals( 42.0d, ZNumber.number( "xxx", 42.0d ) );
    }

    @Test
    public void numberInterfaceTest(){
        ZNumber zn = new ZNumber(42);
        Assert.assertEquals( 42, zn.intValue() );
        Assert.assertEquals( 42L, zn.longValue() );
        Assert.assertEquals( 42.0d, zn.doubleValue() , 0.0001d);
        Assert.assertEquals( 42.0f, zn.floatValue() , 0.0001f);

        zn = new ZNumber(BigInteger.valueOf(42) );
        Assert.assertEquals( 42, zn.intValue() );
        Assert.assertEquals( 42L, zn.longValue() );
        Assert.assertEquals( 42.0d, zn.doubleValue() , 0.0001d);
        Assert.assertEquals( 42.0f, zn.floatValue() , 0.0001f);

        zn = new ZNumber(42.01d );
        Assert.assertEquals( 42, zn.intValue() );
        Assert.assertEquals( 42L, zn.longValue() );
        Assert.assertEquals( 42.01d, zn.doubleValue() , 0.0001d);
        Assert.assertEquals( 42.01f, zn.floatValue() , 0.0001f);

    }

    @Test
    public void compareTest(){
        ZNumber zn = new ZNumber(1);
        Assert.assertTrue( zn.equals( zn ) );
        Assert.assertTrue( zn.equals( new ZNumber(1) ) );
        Assert.assertTrue( zn.equals( 1 ) );
        Assert.assertFalse( zn.equals( null) );

        // now compare
        Assert.assertEquals(0, zn.compareTo(zn) );
        Assert.assertEquals(0, zn.compareTo(new ZNumber(1)) );
        Assert.assertThrows( IllegalArgumentException.class, ( ) ->{
            zn.compareTo(null);
        });
    }

    @Test
    public void signFunctionTest(){
        Assert.assertEquals(-1, new ZNumber(-1).sign() );
        Assert.assertEquals(0, new ZNumber(0).sign() );
        Assert.assertEquals(1, new ZNumber(1).sign() );
        Assert.assertEquals(0, new ZNumber(BigInteger.ZERO).sign() );
        Assert.assertEquals(0, new ZNumber(BigDecimal.ZERO).sign() );
        // now sign() function
        Assert.assertEquals(0, ZNumber.sign( new Object[]{ } ));
        Assert.assertEquals(-1, ZNumber.sign( -1 ));
        Assert.assertEquals(1, ZNumber.sign( 1 ));
        Assert.assertEquals(0, ZNumber.sign( 0 ));
    }

    @Test
    public void logTest(){
        Assert.assertEquals( Math.E, ZNumber.log().doubleValue() , 0.00000001 );
    }

    @Test
    public void stringTest(){
        final String ds = "1.20345789" ;
        ZNumber zn = new ZNumber( ds );
        Assert.assertEquals( "1.203", zn.string("#.###") );
        Assert.assertEquals( ds, zn.string(true) );

        zn = new ZNumber(5);
        Assert.assertEquals( "101", zn.string(2) );
        Assert.assertEquals( "101", zn.string(2,2) );
        Assert.assertEquals( "101", zn.string(2,3) );
        Assert.assertEquals( "0101", zn.string(2,4) );
        Assert.assertEquals( "00101", zn.string(2,5) );
    }

    @Test
    public void convertNumericTest(){
        Double d = 42.42d ;
        Assert.assertEquals( d, ZNumber.convertNum( d, double.class ));
        Assert.assertEquals( d, ZNumber.convertNum( d, Double.class ));

        Assert.assertEquals( d.floatValue(), ZNumber.convertNum( d, float.class ));
        Assert.assertEquals( d.floatValue(), ZNumber.convertNum( d, Float.class ));

        Assert.assertEquals( d.intValue(), ZNumber.convertNum( d, int.class ));
        Assert.assertEquals( d.intValue(), ZNumber.convertNum( d, Integer.class ));

        Assert.assertEquals( d.longValue(), ZNumber.convertNum( d, long.class ));
        Assert.assertEquals( d.longValue(), ZNumber.convertNum( d, Long.class ));

        Assert.assertEquals( d.shortValue(), ZNumber.convertNum( d, short.class ));
        Assert.assertEquals( d.shortValue(), ZNumber.convertNum( d, Short.class ));

        Assert.assertEquals( d.byteValue(), ZNumber.convertNum( d, byte.class ));
        Assert.assertEquals( d.byteValue(), ZNumber.convertNum( d, Byte.class ));

        Assert.assertEquals( new BigDecimal(d.toString()), ZNumber.convertNum( d, BigDecimal.class ));
        Assert.assertEquals( BigInteger.ZERO, ZNumber.convertNum( new ZNumber(0), BigInteger.class ));

        Assert.assertEquals( ZRational.ZERO, ZNumber.convertNum( ZRational.ZERO, ZRational.class ));

    }

    @Test
    public void longToFloatTest(){
        ZNumber zn1 = new ZNumber( Long.MAX_VALUE );
        ZNumber zn2 = new ZNumber( zn1.floating() ) ;
        assertEquals( zn1.actual(), zn2.integer() );

        zn1 = new ZNumber( Long.MIN_VALUE );
        zn2 = new ZNumber( zn1.floating() ) ;
        assertEquals( zn1.actual(), zn2.integer() );
    }

    static final ZNumber.BigMath math = ZNumber.BigMath.INSTANCE ;

    @Test
    public void bdFunctionTest(){
        assertEquals(BigDecimal.ONE,  ZNumber.BigMath.BD((byte)1));
        assertEquals(BigDecimal.ONE,  ZNumber.BigMath.BD((short)1));
        assertEquals(BigDecimal.ONE,  ZNumber.BigMath.BD(1));
        assertEquals(BigDecimal.ONE,  ZNumber.BigMath.BD(1L));
        assertEquals(BigDecimal.ONE,  ZNumber.BigMath.BD( BigInteger.ONE ));
        assertEquals(BigDecimal.ONE,  ZNumber.BigMath.BD( BigDecimal.ONE ));
    }
}
