package zoomba.lang.core.types;

import org.junit.Assert;
import org.junit.Test;
import zoomba.lang.core.operations.Function;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

public class ZRangeTest {

    @Test
    public void xArgTest(){
        Assert.assertTrue( ZXRange.hasXtype( new Object[] {BigInteger.ZERO, 0 } ));
        Assert.assertTrue( ZXRange.hasXtype( new Object[] {BigDecimal.ZERO, 1 } ));
        Assert.assertTrue( ZXRange.hasXtype( new Object[] {0.1f , true} ));
        Assert.assertTrue( ZXRange.hasXtype( new Object[] {0.1D , false } ));
        Assert.assertFalse( ZXRange.hasXtype( new Object[] {1 , false } ));
    }

    @Test
    public void x2ArgConstructorTest(){
        ZXRange r = ZXRange.zxRange( 1.1, 0.9 );
        Assert.assertEquals(1, r.size() );

        r = ZXRange.zxRange( 1.0, 1.0 );
        Assert.assertEquals(0, r.size() );

        r = ZXRange.zxRange( 1, 1 );
        Assert.assertEquals(0, r.size() );
    }

    @Test
    public void xObjectNILTest(){
        Assert.assertEquals(Function.NIL, ZXRange.zxRange( 1.1, 0.9 ).object(10) );
        ZXRange r = ZXRange.zxRange( 1.1, 0.9, -0.1 );
        Assert.assertEquals( Function.NIL, r.get(-4));
        Assert.assertEquals( Function.NIL, r.get(4));

        r = ZXRange.zxRange( 0.9, 1.1, 0.1 );
        Assert.assertEquals( Function.NIL, r.get(-4));
        Assert.assertEquals( Function.NIL, r.get(4));
    }

    @Test
    public void xReverseTest(){
        ZXRange r = ZXRange.zxRange( 1.1, 1.9, 0.5 );
        r.toEnd();
        Assert.assertFalse( r.hasNext() );
        Assert.assertTrue( r.hasPrevious() );
        Assert.assertEquals( 1.6, r.previous() );
        Assert.assertTrue( r.hasPrevious() );
        Assert.assertEquals( 1.1, r.previous() );
        Assert.assertFalse( r.hasPrevious() );


        r = ZXRange.zxRange( 3.1, 2.2, -0.5 );
        r.toEnd();
        Assert.assertFalse( r.hasNext() );
        Assert.assertTrue( r.hasPrevious() );
        Assert.assertEquals( 2.1, r.previous() );
        Assert.assertTrue( r.hasPrevious() );
        Assert.assertEquals( 2.6, r.previous() );
        Assert.assertTrue( r.hasPrevious() );
        Assert.assertEquals( 3.1, r.previous() );
        Assert.assertFalse( r.hasPrevious() );

        // Boundary check
        r = ZXRange.zxRange(1.1,1.1, 0 );
        Assert.assertFalse( r.hasNext() );
        Assert.assertFalse( r.hasPrevious() );
    }

    @Test
    public void semiClosedTest(){
        Assert.assertTrue( new ZRange.NumRange(0,2 ).isSemiClosed() );
        Assert.assertTrue( new ZRange.DateRange(new Date(),new Date() ).isSemiClosed() );
        Assert.assertFalse( new ZRange.CharRange('a', 'b' ).isSemiClosed() );
    }

    @Test
    public void toStringTest(){
        Assert.assertEquals( "[0:9223372036854775807:1]", new ZRange.NumRange(0,Long.MAX_VALUE ).toString() );
        Assert.assertEquals( "[a:b:1]", new ZRange.CharRange('a', 'b' ).toString() );
        Assert.assertNotNull( new ZRange.DateRange(new Date(),new Date() ).toString() );
    }

    @Test
    public void numReverseTest(){
        ZRange r = new ZRange.NumRange(0,4, 3 );
        r.toEnd();
        Assert.assertFalse( r.hasNext() );
        Assert.assertTrue( r.hasPrevious() );
        Assert.assertEquals( 3, r.previous() );
        Assert.assertTrue( r.hasPrevious() );
        Assert.assertEquals( 0, r.previous() );
        Assert.assertFalse( r.hasPrevious() );

        r = new ZRange.NumRange(4,-1, -2 );
        r.toEnd();
        Assert.assertFalse( r.hasNext() );
        Assert.assertTrue( r.hasPrevious() );
        Assert.assertEquals( 0, r.previous() );
        Assert.assertTrue( r.hasPrevious() );
        Assert.assertEquals( 2, r.previous() );
        Assert.assertTrue( r.hasPrevious() );
        Assert.assertEquals( 4, r.previous() );
        Assert.assertFalse( r.hasPrevious() );

    }

    @Test
    public void charRangeStringTest(){
        ZRange.CharRange r = new ZRange.CharRange('a','c');
        Assert.assertNull( r.string );
        Assert.assertNull( r.zs );
        Assert.assertEquals( "abc", r.string() );
        Assert.assertNotNull( r.string );
        Assert.assertEquals( "abc", r.string() ); // test cache
        Assert.assertEquals( 3, r.size() );
        Assert.assertNotNull( r.zs );
        Assert.assertEquals( 3, r.size() ); // test cache
    }

    @Test
    public void charReverseTest(){
        ZRange.CharRange r = new ZRange.CharRange('a','c');
        Assert.assertFalse(r.hasPrevious());
        r.toEnd();
        Assert.assertFalse( r.hasNext() );
        Assert.assertTrue( r.hasPrevious() );
        Assert.assertEquals( 'c', r.previous() );
    }

    @Test
    public void dateReverseTest(){
        final ZDate dt = new ZDate();
        final ZDate last = (ZDate) dt._add_( 1000L );
        ZRange.DateRange r = new ZRange.DateRange( dt, last, "PT1H");
        Assert.assertEquals(1L, r.zSize() );
        Assert.assertFalse( r.hasPrevious() );
        r.toEnd();
        Assert.assertTrue( r.hasPrevious() );
        Assert.assertEquals( dt.date().getTime(),  ((Date)r.previous()).getTime() );
    }

    @Test
    public void exclusiveRangeGetTest(){
        ZRange r = new ZRange.NumRange(0,4, 3 );
        Assert.assertEquals(0,  r.get( 0 ));
        Assert.assertEquals(3,  r.get( -1 ));
        Assert.assertEquals(Function.NIL,  r.get( 10 ));
        Assert.assertEquals(Function.NIL,  r.get( -10 ));

        r = new ZRange.NumRange(4,1, -2 );
        Assert.assertEquals(4,  r.get( 0 ));
        Assert.assertEquals(2,  r.get( -1 ));
        Assert.assertEquals(Function.NIL,  r.get( 3 ));
        Assert.assertEquals(Function.NIL,  r.get( -5 ));
    }

    @Test
    public void inclusiveRangeGetTest(){
        ZRange.CharRange r = new ZRange.CharRange('a', 'c');
        Assert.assertEquals('a',  r.get( 0 ));
        Assert.assertEquals('b',  r.get( 1 ));
        Assert.assertEquals('c',  r.get( 2 ));
        Assert.assertEquals('c',  r.get( -1 ));
    }

    @Test
    public void rangeListContainsTest(){
        ZRange.NumRange r = new ZRange.NumRange(1,1, 0);
        List l = r.asList();
        Assert.assertEquals(0, l.size());
        Assert.assertTrue(l.isEmpty());

        // contains test
        r = new ZRange.NumRange(1,5, 2);
        l =  r.asList();

        Assert.assertEquals(2, l.size());
        Assert.assertFalse(l.isEmpty());
        Assert.assertTrue( l.contains( 1) );
        Assert.assertTrue( l.contains( 3) );

        Assert.assertFalse( l.contains( 4) );
        Assert.assertFalse( l.contains( "bla bla" ) );

        // Index Of
        Assert.assertEquals(0, l.indexOf(1));
        Assert.assertEquals(1, l.indexOf(3));
        Assert.assertEquals(1, l.lastIndexOf(3));

        Assert.assertEquals(-1, l.indexOf(0));

        // decreasing list
        r = new ZRange.NumRange(4,0, -3);
        l =  r.asList();
        Assert.assertEquals(2, l.size());

        Assert.assertTrue( l.contains( 4) );
        Assert.assertTrue( l.contains( 1) );

        Assert.assertFalse( l.contains( -1) );
        Assert.assertFalse( l.contains( 5 ) );

    }

    @Test
    public void rangeListDummyOpsTest(){
        List<Integer> l = new ZRange.NumRange(0,3).asList();
        Object[] arr = l.toArray( new Object[3] );
        Assert.assertNotNull(arr);
        Assert.assertEquals( 3, arr.length );
        Assert.assertFalse( l.remove(null) );
        Assert.assertFalse( l.addAll( Arrays.asList(1,2) ) );
        Assert.assertFalse( l.addAll( 0, Arrays.asList(1,2) ) );
        Assert.assertFalse( l.removeAll( Arrays.asList(1,2) ) );
        Assert.assertFalse( l.retainAll( Arrays.asList(1,2) ) );
        l.clear();
        Assert.assertEquals(3, l.size()); // do not remove it, clear has no effect on this list
        ListIterator li = l.listIterator();
        Assert.assertEquals(0, li.next() );
        li = l.listIterator(0);
        Assert.assertEquals(0, li.next() );
        li = l.listIterator(1);
        Assert.assertEquals(1, li.next() );
        Assert.assertThrows( IndexOutOfBoundsException.class, () -> l.listIterator(-2) );
        Assert.assertThrows( IndexOutOfBoundsException.class, () -> l.listIterator(12) );

        // sublist is not implemented TODO
        Assert.assertTrue( l.subList(0,1).isEmpty());

        Assert.assertThrows( UnsupportedOperationException.class, () -> l.set(0,12) );
        Assert.assertThrows( UnsupportedOperationException.class, () -> l.add(0,12) );
    }

    @Test
    public void callDummyOpsTest(){
        ZRange zr = new ZRange.NumRange(0,10);
        zr.remove();
        zr.set("x");
        zr.add("x");
        Assert.assertEquals(0, zr.nextIndex());
        Assert.assertEquals(-1, zr.previousIndex());
    }
}
