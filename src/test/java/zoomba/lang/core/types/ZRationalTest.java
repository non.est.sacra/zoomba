package zoomba.lang.core.types;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;

public class ZRationalTest {

    @Test
    public void initTest(){
        ZRational zr = new ZRational(0);
        Assert.assertEquals( BigInteger.ZERO, zr.getDividend());
        Assert.assertEquals( BigInteger.ONE, zr.getDivisor());

        zr = new ZRational(1);
        Assert.assertEquals( BigInteger.ONE, zr.getDividend());
        Assert.assertEquals( BigInteger.ONE, zr.getDivisor());

        zr = new ZRational(-1);
        Assert.assertEquals( BigInteger.ONE.negate(), zr.getDividend());
        Assert.assertEquals( BigInteger.ONE, zr.getDivisor());

        zr = new ZRational(-4,4);
        Assert.assertEquals( BigInteger.ONE.negate(), zr.getDividend());
        Assert.assertEquals( BigInteger.ONE, zr.getDivisor());

        zr = new ZRational(12,15);
        Assert.assertEquals( BigInteger.valueOf(4), zr.getDividend());
        Assert.assertEquals( BigInteger.valueOf(5), zr.getDivisor());

        zr = new ZRational(12,-15);
        Assert.assertEquals( BigInteger.valueOf(-4), zr.getDividend());
        Assert.assertEquals( BigInteger.valueOf(5), zr.getDivisor());

        zr = new ZRational(1,-10);
        Assert.assertEquals( BigInteger.valueOf(-1), zr.getDividend());
        Assert.assertEquals( BigInteger.TEN, zr.getDivisor());
    }

    @Test
    public void addTest(){
        ZRational zr = new ZRational( 0 ).add( new ZRational(0));
        Assert.assertEquals( BigInteger.ZERO, zr.getDividend());
        Assert.assertEquals( BigInteger.ONE, zr.getDivisor());

        zr = new ZRational( 1,2 ).add( new ZRational(1,2));
        Assert.assertEquals( BigInteger.ONE, zr.getDividend());
        Assert.assertEquals( BigInteger.ONE, zr.getDivisor());

        zr = new ZRational( 1,2 ).add( new ZRational(-1,2));
        Assert.assertEquals( BigInteger.ZERO, zr.getDividend());
        Assert.assertEquals( BigInteger.ONE, zr.getDivisor());

        zr = new ZRational( 1 ).add( new ZRational(1));
        Assert.assertEquals( BigInteger.TWO, zr.getDividend());
        Assert.assertEquals( BigInteger.ONE, zr.getDivisor());

        zr = new ZRational( 3 , 2).add( new ZRational(3,2));
        Assert.assertEquals( BigInteger.valueOf(3), zr.getDividend());
        Assert.assertEquals( BigInteger.ONE, zr.getDivisor());
    }

    @Test
    public void subtractTest(){
        ZRational zr = new ZRational( 0 ).subtract( new ZRational(0));
        Assert.assertEquals( BigInteger.ZERO, zr.getDividend());
        Assert.assertEquals( BigInteger.ONE, zr.getDivisor());

        zr = new ZRational( 1,2 ).subtract( new ZRational(1,2));
        Assert.assertEquals( BigInteger.ZERO, zr.getDividend());
        Assert.assertEquals( BigInteger.ONE, zr.getDivisor());

        zr = new ZRational( 1,2 ).subtract( new ZRational(-1,2));
        Assert.assertEquals( BigInteger.ONE, zr.getDividend());
        Assert.assertEquals( BigInteger.ONE, zr.getDivisor());

        zr = new ZRational( 1 ).subtract( new ZRational(1));
        Assert.assertEquals( BigInteger.ZERO, zr.getDividend());
        Assert.assertEquals( BigInteger.ONE, zr.getDivisor());

        zr = new ZRational( 3 , 4).subtract( new ZRational(1,4));
        Assert.assertEquals( BigInteger.ONE, zr.getDividend());
        Assert.assertEquals( BigInteger.TWO, zr.getDivisor());
    }

    @Test
    public void multiplyTest(){
        ZRational zr = new ZRational( 0 ).multiply( new ZRational(0));
        Assert.assertEquals( BigInteger.ZERO, zr.getDividend());
        Assert.assertEquals( BigInteger.ONE, zr.getDivisor());

        zr = new ZRational( 1,2 ).multiply( new ZRational(1,2));
        Assert.assertEquals( BigInteger.ONE, zr.getDividend());
        Assert.assertEquals( BigInteger.valueOf(4), zr.getDivisor());

        zr = new ZRational( 1,2 ).multiply( new ZRational(-1,2));
        Assert.assertEquals( BigInteger.ONE.negate(), zr.getDividend());
        Assert.assertEquals( BigInteger.valueOf(4), zr.getDivisor());

        zr = new ZRational( 1 ).multiply( new ZRational(1));
        Assert.assertEquals( BigInteger.ONE, zr.getDividend());
        Assert.assertEquals( BigInteger.ONE, zr.getDivisor());

        zr = new ZRational( 3 , 2).multiply( new ZRational(2,3));
        Assert.assertEquals( BigInteger.ONE, zr.getDividend());
        Assert.assertEquals( BigInteger.ONE, zr.getDivisor());
    }

    @Test
    public void divisionTest(){
        ZRational zr = new ZRational( 0 ).divide( new ZRational(1));
        Assert.assertEquals( BigInteger.ZERO, zr.getDividend());
        Assert.assertEquals( BigInteger.ONE, zr.getDivisor());

        zr = new ZRational( 1,2 ).divide( new ZRational(1,2));
        Assert.assertEquals( BigInteger.ONE, zr.getDividend());
        Assert.assertEquals( BigInteger.ONE, zr.getDivisor());

        zr = new ZRational( 1,2 ).divide( new ZRational(-1,2));
        Assert.assertEquals( BigInteger.ONE.negate(), zr.getDividend());
        Assert.assertEquals( BigInteger.ONE, zr.getDivisor());

        zr = new ZRational( 1,2 ).divide( new ZRational(2));
        Assert.assertEquals( BigInteger.ONE, zr.getDividend());
        Assert.assertEquals( BigInteger.valueOf(4), zr.getDivisor());

        zr = new ZRational( 3 , 2).divide( new ZRational(2,3));
        Assert.assertEquals( BigInteger.valueOf(9), zr.getDividend());
        Assert.assertEquals( BigInteger.valueOf(4), zr.getDivisor());
    }

    @Test
    public void bigDecimalTest(){
        ZRational zr = new ZRational( 0 );
        Assert.assertEquals(BigDecimal.ZERO, zr.bigDecimal() );

        zr = new ZRational( 1 );
        Assert.assertEquals(BigDecimal.ONE, zr.bigDecimal() );

        zr = new ZRational( 1,2 );
        Assert.assertEquals( new BigDecimal("0.5") , zr.bigDecimal() );

        zr = new ZRational( 1,3 );
        Assert.assertEquals( -1, new BigDecimal("0.3333").compareTo( zr.bigDecimal() ) );
        Assert.assertEquals( 1, new BigDecimal("0.3334").compareTo( zr.bigDecimal() ) );

        // Now construct one with BigDecimal
        zr = new ZRational( BigDecimal.ONE );
        Assert.assertEquals( BigInteger.ONE,  zr.getDividend());
        Assert.assertEquals( BigInteger.ONE,  zr.getDivisor());

        zr = new ZRational( new BigDecimal("2.000") );
        Assert.assertEquals( BigInteger.TWO,  zr.getDividend());
        Assert.assertEquals( BigInteger.ONE,  zr.getDivisor());

        zr = new ZRational( new BigDecimal("-2.001") );
        Assert.assertEquals( BigInteger.valueOf(-2001),  zr.getDividend());
        Assert.assertEquals( BigInteger.valueOf(1000),  zr.getDivisor());
    }

    @Test
    public void copyTest(){
        ZRational zr = ZRational.ONE.copy();
        Assert.assertNotSame(zr.getDividend(), ZRational.ONE.getDividend());
        Assert.assertNotSame(zr.getDivisor(), ZRational.ONE.getDivisor());
    }

    @Test
    public void negateTest(){
        Assert.assertEquals( BigInteger.ONE, new ZRational(-1).negate().getDividend());
        Assert.assertEquals( BigInteger.ONE.negate(), ZRational.ONE.negate().getDividend());
    }

    @Test
    public void compareToTest(){
        Assert.assertFalse( ZRational.ZERO.equals(null)); // do not change it, equality test
        Assert.assertFalse( ZRational.ZERO.equals(0)); // do not change it, type equality test
        Assert.assertTrue(ZRational.ZERO.equals( ZRational.ZERO) ); // do not change this, same ref test
        Assert.assertEquals(ZRational.ZERO, new ZRational(0) );
        Assert.assertEquals(ZRational.ONE, new ZRational(1) );
        Assert.assertEquals(-1, ZRational.ZERO.compareTo(new ZRational(1)) );
        Assert.assertEquals(1, ZRational.ONE.compareTo(ZRational.ZERO) );

    }

    @Test
    public void toStringTest(){
        Assert.assertEquals( "1", ZRational.ONE.toString() );
        Assert.assertEquals( "1/2", new ZRational(1,2).toString() );
    }

    @Test
    public void numberTest(){
        Assert.assertEquals(1, ZRational.ONE.intValue() );
        Assert.assertEquals(1L, ZRational.ONE.longValue() );
        Assert.assertEquals(1.0f, ZRational.ONE.floatValue(), 0.0001f );
        Assert.assertEquals(1.0d, ZRational.ONE.doubleValue(), 0.0001d );
    }
}
