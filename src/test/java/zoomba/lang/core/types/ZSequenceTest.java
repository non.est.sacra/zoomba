package zoomba.lang.core.types;

import org.junit.Test;
import zoomba.lang.core.operations.Function;

import java.util.Arrays;

import static org.junit.Assert.*;

public class ZSequenceTest {

    @Test
    public void basicTest(){
        ZSequence zs = new ZSequence.BaseSequence( Function.TRUE );
        assertTrue( zs.hasNext() );
        zs.retain(1);
        assertEquals( true, zs.next() );
        assertEquals( true, zs.next() );
        assertEquals( true, zs.next() );
        assertNotEquals(zs, zs.yield());
        final ZSequence zs1 = zs;
        assertThrows( IllegalArgumentException.class, () -> zs1.get(-1));
        assertNotEquals( 0, zs.hashCode() );
        assertEquals( true, zs.head());

        zs = new ZSequence.BaseSequence( Function.NOP );
        assertTrue( zs.hasNext() );
        assertThrows( UnsupportedOperationException.class, zs::next);
    }

    @Test
    public void creationTest(){
        assertSame( ZSequence.NULL_SEQUENCE, ZSequence.BaseSequence.sequence(null) );
        ZSequence zs = ZSequence.BaseSequence.sequence(Function.FALSE);
        assertEquals( false, zs.next() );
        zs = ZSequence.BaseSequence.sequence( Function.constant(0) , Arrays.asList(1,2,3));
        assertEquals( 0, zs.next() );
        zs = ZSequence.BaseSequence.sequence( Function.singleProjection(0), 0 );
        assertEquals( 1, ((Number)zs.next()).intValue() );
        assertEquals( 2, ((Number)zs.next()).intValue() );

        assertTrue( zs.equals(zs));
        assertFalse( zs.equals(null));
        assertFalse( zs.equals(42));
        assertFalse( zs.equals( ZSequence.NULL_SEQUENCE ));

    }
}
