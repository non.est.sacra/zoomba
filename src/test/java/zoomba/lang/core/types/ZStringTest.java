package zoomba.lang.core.types;

import org.junit.Assert;
import org.junit.Test;
import zoomba.lang.core.io.ZWeb;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.collections.ZList;
import zoomba.lang.core.collections.ZSet;

import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import static org.junit.Assert.*;

/**
 */
public class ZStringTest {

    static final String HELLO = "Hello, World!" ;

    static final StringBuilder HELLO_BUF = new StringBuilder( HELLO );

    static final StringBuilder HELLO_BUILDER = new StringBuilder( HELLO );

    @Test
    public void deepCopyTest(){
        ZString zs = new ZString("hi!");
        ZString cp = (ZString) zs.deepCopy();
        Assert.assertNotSame( zs.buffer , cp.buffer );
    }

    @Test
    public void stringTest(){
        ZString zs = new ZString("hi!");
        Assert.assertEquals( zs.toString(), zs.string() );
    }

    @Test
    public void testAssignment(){
        ZString zs = new ZString((Object) null);
        assertFalse( zs.isEmpty() );

        zs = new ZString();
        assertTrue(zs.isEmpty());

        zs  = new ZString(HELLO);
        assertFalse( zs.isEmpty() );

        zs  = new ZString(HELLO_BUF);
        assertFalse( zs.isEmpty() );

        zs  = new ZString(HELLO_BUILDER);
        assertFalse( zs.isEmpty() );

        zs  = new ZString(HELLO.toCharArray() );
        assertFalse( zs.isEmpty() );

        zs = new ZString((CharSequence) null);
        assertTrue( zs.isEmpty() );

        zs = new ZString((char[]) null);
        assertTrue( zs.isEmpty() );
    }

    @Test
    public void testCollection(){
        ZString zs = new ZString(HELLO);
        int i = 0 ;
        for ( Object c : zs){
            assertTrue( c instanceof Character );
            i++;
        }
        assertEquals( zs.length() , zs.size());
        assertEquals( i , zs.size());

        ListIterator li = zs.listIterator(zs.size());
        i = zs.size() ;
        while ( li.hasPrevious() ){
            Object c = li.previous();
            assertTrue( c instanceof Character );
            --i;
        }
        assertEquals(0,i);
        assertTrue( li.hasNext() );
        ZList l = new ZList(zs);
        i = l.rightIndex(Function.TRUE );
        assertEquals( i, zs.size() - 1 );
        i = l.leftIndex(Function.TRUE );
        assertEquals( i , 0 );

        ZSet s = new ZSet(zs);
        assertEquals( s.size() , 10 );

    }

    @Test
    public void testArithmetic(){
        ZString zs = new ZString(HELLO_BUILDER);
        assertEquals( 0 , zs.compareTo( HELLO ) );
        assertEquals( 0 , zs.compareTo( zs ) );
        assertEquals( 0 , zs.compareTo( HELLO_BUF ) );
        assertEquals( 0 , zs.compareTo( HELLO_BUILDER ) );
        assertEquals( 0 , zs.compareTo( HELLO.toCharArray() ));

        int previousSize = zs.size();
        zs.add_mutable(" Everyone seems to be happy, but not file");
        assertTrue(  previousSize < zs.size() ) ;
        String p = (String) zs._pow_(0) ;
        assertTrue(p.isEmpty());

        // start with a palindrome
        zs = new ZString( "malayalam" );

        p = (String) zs._pow_(1) ;
        assertEquals( 0 , zs.compareTo( p ));

        p = (String) zs._pow_(2) ;
        assertEquals( 2 * zs.size() , p.length() );

        p = (String) zs._pow_(-1) ;
        assertEquals( 0 , zs.compareTo(p));

        p = (String) zs._pow_(-2) ;
        assertEquals( 2 * zs.size() , p.length() );

    }

    @Test
    public void urlEncodeParamsTest() throws Exception {
        String res = ZWeb.payLoad(Map.of());
        Assert.assertEquals("", res);
        res = ZWeb.payLoad(Map.of("x", 42 ));
        Assert.assertEquals("x=42", res);
        res = ZWeb.payLoad(Map.of("x", 42 , "Y", 52));
        String[] arr = res.split("&");
        Assert.assertEquals(2, arr.length);
        res = ZWeb.payLoad(Map.of("x", 42 , "y", 52, "Z", "Hello, World"));
        arr = res.split("&");
        Assert.assertEquals(3, arr.length);
    }

    @Test
    public void charSequenceIteratorTest(){
        ZString.CharSequenceIterator it = new ZString.CharSequenceIterator("hello!");
        Assert.assertTrue( it.hasNext() );
        Assert.assertEquals('h', it.next() );
        Assert.assertEquals( 1, it.nextIndex() );
        Assert.assertEquals( -1, it.previousIndex() );
        // no op really
        it.remove();
        it.set('f');
        it.add('a');
    }

    @Test
    public void variousPassThroughTest(){
        ZString  zs = new ZString("hello,there!") ;
        Assert.assertEquals( 'h', zs.charAt(0) );
        Assert.assertEquals( "hello", zs.subSequence(0,5).toString() );
        Assert.assertEquals( 2, zs.indexOf("ll") );
        Assert.assertEquals( 10, zs.lastIndexOf("e") );
        Assert.assertEquals( zs.toString().hashCode(), zs.hashCode() );
        Assert.assertEquals( Arrays.asList( 'h', 'e'), zs.subList( 0,2) );
    }

    @Test
    public void unsupportedOperationsTest(){
        ZString zs = new ZString("hi!");
        assertThrows( UnsupportedOperationException.class, () ->  zs._sub_("a") );
        assertThrows( UnsupportedOperationException.class, () ->  zs.sub_mutable("a") );
        assertThrows( UnsupportedOperationException.class, () ->  zs._mul_("a") );
        assertThrows( UnsupportedOperationException.class, () ->  zs.mul_mutable("a") );
        assertThrows( UnsupportedOperationException.class, () ->  zs._div_("a") );
        assertThrows( UnsupportedOperationException.class, () ->  zs.div_mutable("a") );
    }

    @Test
    public void collectionIgnoreOperationsTest(){
        ZString zs = new ZString("hi!");
        assertFalse(zs.remove("a"));
        assertFalse(zs.removeAll(Arrays.asList(1,2,3)));
        assertFalse(zs.addAll(Arrays.asList(1,2,3)));
        assertFalse(zs.addAll(0, Arrays.asList(1,2,3)));
        assertFalse(zs.containsAll( Arrays.asList(1,2,3)) );
        assertFalse(zs.retainAll(Arrays.asList(1,2,3)) );
        assertNull(zs.remove(1));
        zs.add(0,'x');
    }

    @Test
    public void opsTest(){
        ZString zs = new ZString("hello") ;
        // add
        ZString res = (ZString) zs._add_( 42);
        assertEquals( "hello42", res.toString() );
        assertNotEquals( "hello42", zs.toString() );
        // pow
        assertThrows( IllegalArgumentException.class , ( ) -> zs._pow_( "hi!") );
        // compareTo
        assertEquals(0, new ZString("ab").compareTo( new char[]{ 'a', 'b' } ));
        assertEquals(1, new ZString("ab").compareTo( null ));

        assertThrows( UnsupportedOperationException.class , ( ) -> zs.compareTo( 1234) );
        // contains
        assertFalse( zs.contains(42) );
        // add
        assertTrue( zs.add(",hey!"));
        assertEquals( "hello,hey!", zs.toString() );
        assertEquals('h', zs.set(0, 123));
        // clear
        zs.clear();
        res.clear();
        assertEquals("", zs.toString() );
        assertEquals("", res.toString() );
        assertTrue(zs.equals("") );
        assertFalse( zs.equals(null)) ;
    }

    @Test
    public void utf_8_Basic_reverseTest(){
        List<String> somePoints = List.of("\uD835\uDD7D", "\uD835\uDD38", "\uD801\uDC00", "\uD801\uDCB0" ,
                "\uD840\uDC00" , "\uD83C\uDF0D" );
        somePoints.forEach( s -> {
            ZString zs = new ZString(s);
            String rs = zs._pow_(-1).toString();
            //System.out.printf("Original : %s , Reversed %s %n", s,rs);
            assertEquals(s, rs);
        });
    }

    @Test
    public void utf_8_Grapheme_reverseTest(){
        assertThrows( IllegalArgumentException.class, () -> new ZString("hello")._pow_("foo"));
        assertThrows( IllegalArgumentException.class, () -> new ZString("hello")._pow_(1.34));

        // for the love of ... well you can guess from the flags
        List<String> somePoints = List.of("\uD83C\uDDEE\uD83C\uDDF3", "\uD83C\uDDEE\uD83C\uDDF9" ,
                "\uD83C\uDDFA\uD83C\uDDF8" ,
                "\uD83C\uDDFA\uD83C\uDDF8" , "abc", "def" ,"❤\uFE0F");
        somePoints.forEach( s -> {
            ZString zs = new ZString(s);
            String rs = zs._pow_(-1.0).toString();
            //System.out.printf("Original : %s , Reversed %s %n", s,rs);
            assertEquals(s, rs);
        });

        String s = "a b \uD83C\uDDEE\uD83C\uDDF9 cde \uD83C\uDDEE\uD83C\uDDF3 x" ;
        String rs = "x \uD83C\uDDEE\uD83C\uDDF3 edc \uD83C\uDDEE\uD83C\uDDF9 b a" ;
        assertEquals(rs, new ZString(s)._pow_(-1.0) );

        s = "a b cde \uD83C\uDDEE\uD83C\uDDF3 x" ;
        rs = "x \uD83C\uDDEE\uD83C\uDDF3 edc b a" ;
        assertEquals(rs, new ZString(s)._pow_(-1.0) );

        s = "abcde\uD83C\uDDEE\uD83C\uDDF3xyz" ;
        rs = "zyx\uD83C\uDDEE\uD83C\uDDF3edcba" ;
        assertEquals(rs, new ZString(s)._pow_(-1.0) );

        s = "I❤\uFE0F\uD83C\uDDEE\uD83C\uDDF3" ;
        rs = "\uD83C\uDDEE\uD83C\uDDF3❤\uFE0FI" ;
        //System.out.printf("Original: %s , Reversed: %s %n", s, rs);
        assertEquals(rs, new ZString(s)._pow_(-1.0) );

    }
}
