package zoomba.lang.core.types;


import org.junit.Assert;
import org.junit.Test;
import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.collections.ZList;
import zoomba.lang.core.interpreter.ZAssertion;
import zoomba.lang.core.interpreter.ZMethodInterceptor;
import zoomba.lang.core.operations.Arithmetic;
import zoomba.lang.core.operations.Function;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Callable;

import static org.junit.Assert.*;
import static zoomba.lang.core.operations.Function.NIL;

public class ZTypesTest {

    @Test
    public void safeFunctionTest(){
        Callable<Boolean> c = () -> { throw new IllegalArgumentException(); };
        assertTrue( ZTypes.safeOrDefault(c , true ));
        ZTypes.LOG_ERR = true;
        assertFalse( ZTypes.safeOrDefault(c , false ));
        ZTypes.LOG_ERR = false;
        // test the error
        assertThrows( RuntimeException.class, () -> ZTypes.orRaise( () -> 1/0 ));
    }

    @Test
    public void isaTest(){
        assertTrue( ZTypes.isa( null, null , null ) );
        assertFalse( ZTypes.isa( "x", null , null ) );
        assertFalse( ZTypes.isa( null, "foo" , null ) );
        Long l = 42L;
        assertTrue( ZTypes.isa( l, "int" , null ) );
        assertTrue( ZTypes.isa( l.intValue(), "int" , null ) );
        assertTrue( ZTypes.isa( l.shortValue(), "int" , null ) );
        assertTrue( ZTypes.isa( l.byteValue(), "int" , null ) );
        assertTrue( ZTypes.isa(BigInteger.valueOf(l), "int" , null ) );
        assertTrue( ZTypes.isa(new ZNumber(l), "int" , null ) );

        assertTrue( ZTypes.isa( l.floatValue(), "float" , null ) );
        assertTrue( ZTypes.isa( l.doubleValue(), "float" , null ) );
        assertTrue( ZTypes.isa( new BigDecimal(l), "float" , null ) );
        assertTrue( ZTypes.isa(new ZNumber("42.0"), "float" , null ) );

        assertTrue( ZTypes.isa(ZTypes.NilEnum.NIL, "enum" , null ) );

        ZDate dt = new ZDate();
        assertTrue( ZTypes.isa(dt, "dt" , null ) );
        assertTrue( ZTypes.isa(dt.date(), "dt" , null ) );
        assertTrue( ZTypes.isa(dt.time, "dt" , null ) );
        assertTrue( ZTypes.isa(dt._instant, "dt" , null ) );
        assertTrue( ZTypes.isa(Duration.ofMillis(1), "dt" , null ) );
        assertTrue( ZTypes.isa(dt, "dt" , null ) );
    }

    @Test
    public void boolCastTest(){
        assertNull( ZTypes.bool() );
        assertNull( ZTypes.bool((Object) null) );
        assertNull( ZTypes.bool("x", Arrays.asList("x")) );
        assertFalse( ZTypes.bool( 'x', "xy".toCharArray() ) );
        assertNull( ZTypes.bool( 'x', null ) );
    }

    @Test
    public void stringCastTest(){
        assertEquals("",  ZTypes.string(Collections.emptyList()));
        assertEquals("",  ZTypes.string(null, null, ""));
        assertEquals( "0|1|2|3",  ZTypes.string( Function.COLLECTOR_IDENTITY, new ZRange.NumRange(0,4), "|") );
        assertEquals( "0|1|2|3",  ZTypes.string( Function.COLLECTOR_IDENTITY, new int[]{0,1,2,3}, "|") );
        assertEquals( "a|b|c",  ZTypes.string( Function.COLLECTOR_IDENTITY, "abc", "|") );
        assertEquals( "",  ZTypes.string( Function.COLLECTOR_IDENTITY, "", "|") );
        int cnt[] = new int[1];
        boolean[] retNil = new boolean[1];
        retNil[0] = false;
        cnt[0] = 0;
        Function bf = new Function() {
            @Override
            public String body() {
                return "";
            }

            @Override
            public MonadicContainer execute(Object... args) {
                cnt[0]++;
                if ( cnt[0] % 2 == 1 ){
                   if ( retNil[0] ){
                       return new ZException.Break(NIL) ;
                   }
                    return new ZException.Break("x") ;
                }
                return new MonadicContainerBase(args[1]);
            }
            @Override
            public String name() {
                return "";
            }
        };
        assertEquals( "x",  ZTypes.string( bf, "abc", "|") );
        cnt[0] = 1;
        assertEquals( "a|x",  ZTypes.string( bf, "abc", "|") );
        retNil[0] = true;
        cnt[0] = 0;
        assertEquals( "",  ZTypes.string( bf, "abc", "|") );
        cnt[0] = 1;
        assertEquals( "a",  ZTypes.string( bf, "abc", "|") );
    }

    @Test
    public void stringCastTestVarArgs(){
        assertEquals("",  ZTypes.string());
        assertEquals("null",  ZTypes.string((Object) null));
        ZString zs = new ZString("foo");
        assertEquals(zs.toString(),  ZTypes.string(zs));
        assertNotNull( ZTypes.string(new Date()) );
        assertEquals("inf", ZTypes.string(Arithmetic.POSITIVE_INFINITY) );
        assertEquals("42", ZTypes.string(new ZNumber(42)) );
        assertEquals( "1|2|3",  ZTypes.string(  Arrays.asList(1,2,3), "|", Function.COLLECTOR_IDENTITY) );
        assertEquals("[ 1, 2, 3 ]", ZTypes.string( Arrays.asList(1,2,3), true,true) );
        assertEquals("{}", ZTypes.string( new HashMap<>(), true ) );

        assertEquals("0.01", ZTypes.string("%d", 0.01d ) );
        assertEquals("42", ZTypes.string("%f", 42 ) );
    }

    @Test
    public void priorityQueueTest(){
        assertNotNull(ZTypes.pqueue(null ));
        Comparator<?> cmp = (o1, o2) -> 0;
        assertNotNull(ZTypes.pqueue( null, cmp  ));
        assertNotNull(ZTypes.pqueue( null, "foo bar"  ));
        assertNotNull(ZTypes.pqueue( Function.COLLECTOR_IDENTITY) );
    }

    @Test
    public void jsonStringConversionTest(){
        ZDate dt = new ZDate();
        Map<String,Object> map = new HashMap<>();

        Map<Integer,Integer> nm = new HashMap<>();
        nm.put(0,0);
        nm.put(1,1);
        map.put("nm", nm);

        map.put("zn", new ZNumber(42));
        map.put("num", 42.00D );
        map.put("bool", true);

        map.put("ca", "hello".toCharArray() );
        map.put("eoa", new Object[]{ } );
        map.put("zdt", dt );
        map.put("dt", dt.date() );
        map.put("lt", dt.time );
        map.put("di", dt._instant );
        String res = ZTypes.jsonString( map );
        assertNotNull(res);
    }

    @Test
    public void jsonStringFormattingTest(){
        Map<String,Object> em = Collections.emptyMap();
        assertEquals( "{}", ZTypes.jsonFormatted( em, false, "" ));
        assertEquals( "{}", ZTypes.jsonFormatted( em, true, "" ));
        assertEquals( "{\n\t\n}", ZTypes.jsonFormatted( em));
    }

    @Test
    public void jsonConversionTest(){
        assertEquals(Collections.emptyMap(), ZTypes.json());
        assertNull(ZTypes.json((Object) null));
        Object r = ZTypes.json("https://jsonplaceholder.typicode.com/users", true );
        assertNotNull(r);
        assertEquals( Collections.emptyList(), ZTypes.json( Collections.emptySet() ) );
        assertThrows( RuntimeException.class, () -> ZTypes.json("src/test/java/zoomba/lang/core/types/ZTypesTest.java", true));
        assertNull(ZTypes.json( 42) );
    }

    @Test
    public void yamlStringConversionTest(){
        assertEquals("", ZTypes.yamlString());
        assertEquals("- a\n- b\n", ZTypes.yamlString( "a", "b") );
        assertThrows( RuntimeException.class, () -> ZTypes.yamlString((Object) null));
    }

    @Test
    public void yamlConversionTest(){
        assertNull( ZTypes.yaml( ) );
        assertThrows( RuntimeException.class, () -> ZTypes.yaml("@") );
        final String lines = "--- # Indented Block\n" +
                "  name: John Smith\n" +
                "  age: 33\n" +
                "--- # Inline Block\n" +
                "{name: John Smith, age: 33}" ;
        assertNotNull( ZTypes.yaml( lines) );
        assertThrows( RuntimeException.class, () -> ZTypes.yaml("src/test/java/zoomba/lang/core/types/foobar.java", true));
    }

    @Test
    public void enumConversionTest(){
        assertThrows( IllegalArgumentException.class, () -> ZTypes.enumCast("foo.bar" , "x" ) );
        assertEquals(ZTypes.NilEnum.NIL, ZTypes.enumCast("java.lang.String", "x") );
        assertEquals(ZTypes.NilEnum.NIL, ZTypes.enumCast(false) );
        assertEquals( ZAssertion.AssertionType.TEST, ZTypes.enumCast(ZAssertion.AssertionType.class, "TEST") );
    }

    @Test
    public void loadJarTest(){
        Assert.assertEquals(Function.FAILURE,  ZTypes.loadJar("foo-bar.jar" ) );
        final String jLinePath = System.getenv("HOME") + "/.m2/repository/org/jline/jline/3.26.3/" ;
        Assert.assertEquals(Function.FAILURE,  ZTypes.loadJar(jLinePath ) );
        Assert.assertEquals(Function.FAILURE,  ZTypes.loadJar() );
    }

    @Test
    public void miscTest(){
        assertTrue( ZTypes.equals( null, null ));
        assertFalse( ZTypes.equals( null, 10 ));
        assertFalse( ZTypes.equals( 10, null ));
        Object[] foo = new Object[] { } ;
        assertSame( foo, ZTypes.shiftArgsLeft(foo) );
    }

    @Test
    public void rangeTest(){
        ZRange zr = ZTypes.range( new ZDate(), new ZDate(), "PT1H");
        assertTrue( zr instanceof ZRange.DateRange );
        assertSame( ZRange.EMPTY_RANGE, ZTypes.range());
        assertSame( ZRange.EMPTY_RANGE, ZTypes.range(true));
    }

    @Test
    public void raiseTest(){
        assertThrows(  ZException.ZTerminateException.class, ZTypes::bye);
        assertThrows(  ZAssertionException.class, () -> ZTypes.raise("java.lang.String") );
        assertThrows(  ZAssertionException.class, () -> ZTypes.raise("java.lang.Exception", 1,2,3) );
    }

    @Test
    public void xmlFunctionsTest() throws Exception {
        ZXml xml = ZXml.xml("pom.xml", true);
        Object o = xml.root.get("/project/version") ;
        assertTrue( o instanceof ZXml.ZXmlElement );
        assertSame( NIL,((ZXml.ZXmlElement) o).get("foobar") );
        assertSame( NIL,((ZXml.ZXmlElement) o).get("@foobar") );

        assertEquals( "4.0.0", xml.xpath("//modelVersion") );
        assertEquals( "4.0.0", xml.root.xpath("//modelVersion") );

        assertNull( xml.element("//xxx") );
        assertEquals(0, xml.elements("//xxx").size());
        assertEquals("42", xml.xpath("//xxx", "42"));

        assertNull( ZXml.xml());
        assertNotEquals(0, xml.root.hashCode());
        assertTrue( xml.root.equals( xml.root ));
        assertFalse( xml.root.equals( null ));
        assertFalse( xml.root.equals( 42 ));

        ZXml xml2 = ZXml.xml("pom.xml", true);
        assertTrue( xml.equals( xml2 )); // why???
        assertFalse( xml.equals( 42 )); // why???
        assertFalse( xml.equals( null )); // why???

        Object js = xml2.json();
        assertSame( js, xml2.json() );
        Object bk = ZMethodInterceptor.Default.jxPath( ZXml.xml("samples/books.xml",true) , "//book[@id='bk101']/genre");
        assertEquals("Computer", bk);
        bk = ZMethodInterceptor.Default.jxPath( ZXml.xml("samples/books.xml",true) , "//book/genre", true);
        assertTrue(bk instanceof List);
        assertTrue(((List<?>) bk).get(0) instanceof String);

        bk = ZMethodInterceptor.Default.jxElement( ZXml.xml("samples/books.xml",true) , "//book", false);
        assertTrue(bk instanceof ZXml.ZXmlElement);
        assertTrue(bk.toString().contains("<"));

        bk = ZMethodInterceptor.Default.jxElement( ZXml.xml("samples/books.xml",true) , "//book", true);
        assertTrue(((List<?>) bk).get(0) instanceof ZXml.ZXmlElement);

        assertNotEquals(0, xml2.hashCode());

    }

    @Test
    public void proxyFunctionsTest() {
        assertNull( ZTypes.jvmProxy(null) );
        assertNull( ZTypes.jvmProxy(null, new Object[]{ null }) );
        // anon null, Function passed as arg
        assertNotNull( ZTypes.jvmProxy(null, BigInteger.ZERO, Function.COLLECTOR_IDENTITY) );
        assertNotNull( ZTypes.jvmProxy(null, BigInteger.ZERO, true , Function.COLLECTOR_IDENTITY) );
        assertNotNull( ZTypes.jvmProxy(null, BigInteger.ZERO, false , Function.COLLECTOR_IDENTITY) );
        // anon is passed
        assertNull( ZTypes.jvmProxy(Function.COLLECTOR_IDENTITY ) );
        assertNotNull( ZTypes.jvmProxy(Function.COLLECTOR_IDENTITY , BigInteger.ZERO ) );
        assertNotNull( ZTypes.jvmProxy(Function.COLLECTOR_IDENTITY , BigInteger.ZERO , false) );
        assertNotNull( ZTypes.jvmProxy(Function.COLLECTOR_IDENTITY , BigInteger.ZERO , true) );

    }

    @Test
    public void queueFunctionsTest(){
        assertTrue( ZTypes.queue(ZArray.EMPTY_ARRAY) instanceof ArrayDeque );
        assertTrue( ZTypes.queue(new Object[]{ "bla" } ) instanceof ZList);
        assertTrue( ZTypes.queue(new Object[]{ "-10" } ) instanceof ZList);
        assertTrue( ZTypes.queue(new Object[]{ 42 } ) instanceof CircularBuffer );
        assertTrue( ZTypes.queue(new Object[]{ 42 , false } ) instanceof CircularBuffer );
        assertTrue( ZTypes.queue(new Object[]{ 42 , true } ) instanceof ArrayBlockingQueue);
        assertTrue( ZTypes.queue(new Object[]{ 42 , true , false } ) instanceof ArrayBlockingQueue);
        assertTrue( ZTypes.queue(new Object[]{ 42 , true , true } ) instanceof ArrayBlockingQueue);

    }
}
