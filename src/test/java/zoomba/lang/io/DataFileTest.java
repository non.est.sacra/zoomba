package zoomba.lang.io;

import org.junit.BeforeClass;
import org.junit.Test;
import zoomba.lang.core.io.ZXlsxDataFile;
import zoomba.lang.core.operations.ZMatrix;

import java.util.Map;

import static org.junit.Assert.*;

public class DataFileTest {

    // please edit this file for more input using Gnumeric app and nothing else
    static final String EXCEL_FILE = "samples/test/io/data-files/Test.xlsx" ;

    static final String MULTILINE_CELL_CSV = "samples/test/io/data-files/multiline_data.tsv" ;

    static ZXlsxDataFile excl ;

    @BeforeClass
    public static void initFile() throws Exception {
        excl = new ZXlsxDataFile( EXCEL_FILE );
    }

    @Test
    public void xlsxMatrixTest(){
        Map<String,String[][]> sheetData = excl.sheets();
        assertEquals( 2, sheetData.size());
        assertThrows(UnsupportedOperationException.class, () -> excl.matrix("foo-bar"));
        ZMatrix m = excl.matrix("Test");
        assertNotNull( m.row(0));
        assertNull( m.row(10));
    }

    @Test
    public void dataLoaderTest() {
        ZMatrix m = ZXlsxDataFile.XlsxDataLoader.LOADER.load(EXCEL_FILE, "Test", true);
        assertNotNull(m);
        assertNull( ZXlsxDataFile.XlsxDataLoader.LOADER.load(EXCEL_FILE));
        assertThrows(UnsupportedOperationException.class, () ->
                ZXlsxDataFile.XlsxDataLoader.LOADER.load("foo-bar", "Test", true));
    }

    @Test
    public void multilineDataTest(){
        ZMatrix m = ZMatrix.Loader.load(MULTILINE_CELL_CSV );
        assertEquals( 2, m.dataSize() );
        assertEquals( "Hi", m.tuple(1).get("h1") );

        m = ZMatrix.Loader.load(MULTILINE_CELL_CSV , "\t", true , "\n", '"' );
        assertEquals( 3, m.dataSize() );
        assertFalse( m.tuple(0).containsKey("h1"));
        assertTrue( m.tuple(0).containsKey("_1"));
        assertEquals( "h1", m.tuple(0).get("_1") );
        assertEquals( "Hi", m.tuple(1).get("_1") );

        m = ZMatrix.Loader.load(MULTILINE_CELL_CSV , Map.of("esn", true , "nh", true ) );
        assertEquals( 3, m.dataSize() );
        assertFalse( m.tuple(0).containsKey("h1"));
        assertTrue( m.tuple(0).containsKey("_1"));
        assertEquals( "h1", m.tuple(0).get("_1") );
        assertEquals( "Hi", m.tuple(1).get("_1") );

    }
}
