package zoomba.lang.io;

import org.junit.Test;
import zoomba.lang.core.interpreter.InterpreterScriptTest;

public class IOScriptsTest {

    final String FILE_BASIC = "samples/test/io/file_basic.zm" ;

    final String DATABASE = "samples/test/io/sys_db.zm" ;

    final String IO = "samples/test/io/sys_io.zm" ;

    final String MATRIX = "samples/test/io/sys_matrices.zm" ;

    final String WEB = "samples/test/io/sys_web.zm" ;

    @Test
    public void testFiles()  {
        InterpreterScriptTest.danceWithScript( FILE_BASIC );
    }

    @Test
    public void testDBFunctions() {
        // Load out the h2 stuff properly
        InterpreterScriptTest.danceWithScript( DATABASE );
    }

    @Test
    public void testIOFunctions()  {
        InterpreterScriptTest.danceWithScript( IO );
    }

    @Test
    public void testMatricesFunctions()  {
        InterpreterScriptTest.danceWithScript( MATRIX );
    }

    @Test
    public void testWebFunctions()  {
        InterpreterScriptTest.danceWithScript( WEB );
    }

}
