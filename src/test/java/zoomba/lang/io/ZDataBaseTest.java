package zoomba.lang.io;

import org.junit.Test;
import zoomba.lang.core.io.ZDataBase;
import zoomba.lang.core.operations.ZMatrix;

import java.sql.SQLException;
import java.util.Collections;
import java.util.Map;

import static org.junit.Assert.*;

public class ZDataBaseTest {

    public static final String DB_CON_CONFIG = "samples/test/io/db_con.json" ;

    @Test
    public void resultSetMatrixTest(){
        ZDataBase.ResultSetMatrix m = new ZDataBase.ResultSetMatrix(null);
        assertThrows( NullPointerException.class, m::dataColumns);
        assertThrows( UnsupportedOperationException.class, ()-> m.row(10));
        assertEquals( -1, m.rows());
    }

    @Test
    public void zDBConnectionTest(){
        ZDataBase.ZDBConnection con = new ZDataBase.ZDBConnection( "x", Collections.emptyMap());
        assertEquals("", con.driver());
        assertEquals("", con.user());
        assertEquals("", con.pass());
        RuntimeException re = assertThrows( RuntimeException.class, con::connection);
        assertTrue( re.getCause() instanceof ClassNotFoundException );
        con = new ZDataBase.ZDBConnection( "x", Map.of("driver", "java.lang.String" ));
        re = assertThrows( RuntimeException.class, con::connection);
        assertTrue( re.getCause() instanceof SQLException);
    }

    @Test
    public void execTest(){
        ZDataBase db = new ZDataBase( DB_CON_CONFIG );
        IllegalArgumentException e = assertThrows( IllegalArgumentException.class, ()-> db.exec("foobar", "foobar"));
        final String nonRSQuery = "CREATE SCHEMA __zoomba__" ;
        ZMatrix m = db.matrix( "local_h2", nonRSQuery);
        assertSame( ZMatrix.EMPTY_MATRIX, m );
        RuntimeException re = assertThrows( RuntimeException.class, ()-> db.exec("local_h2", "bla bla bla"));
        assertTrue( re.getCause() instanceof SQLException);
    }
}
