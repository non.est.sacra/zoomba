package zoomba.lang.io;

import org.junit.Assert;
import org.junit.Test;
import zoomba.lang.core.io.ZFileSystem;
import zoomba.lang.core.operations.Function;

import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.stream.Collectors;

public class ZFSTest {

    final static String THIS_FILE = "src/test/java/zoomba/lang/io/ZFSTest.java" ;
    final static String THIS_DIR = "src/test/java/zoomba/lang/io" ;

    final static String SAMPLES = "samples" ;
    final static String SAMPLES_TEST = "samples/test" ;

    public static Path createTmpFile(String fileName, String text) throws Exception {
        String tmpdir = System.getProperty("java.io.tmpdir");
        Path p = Paths.get(tmpdir, fileName);
        return Files.write( p, text.getBytes(StandardCharsets.UTF_8) );
    }

    @Test
    public void nameAndExtensionTest(){
        ZFileSystem.ZFile zFile = ZFileSystem.file( THIS_FILE) ;
        Assert.assertTrue( zFile.file.exists() );
        Assert.assertEquals( ".java", zFile.extension() );
        Assert.assertEquals( "ZFSTest", zFile.baseName() );
    }

    @Test
    public void parentChildTest(){
        ZFileSystem.ZFile sampleRoot = ZFileSystem.file( SAMPLES_TEST) ;
        Assert.assertTrue( sampleRoot.file.isDirectory() );
        Assert.assertFalse( sampleRoot.children().isEmpty() );
        Assert.assertEquals(SAMPLES, sampleRoot.parent().baseName() );
        Assert.assertTrue( ZFileSystem.file("foo-bar" ).children().isEmpty() ) ;
        Assert.assertTrue( ZFileSystem.file(THIS_FILE ).children().isEmpty() ) ;
    }

    @Test
    public void pathNameTest() throws Exception {
        ZFileSystem.ZFile sampleRoot = ZFileSystem.file( SAMPLES_TEST) ;
        Assert.assertTrue( sampleRoot.canon().startsWith("/") );
        final String rel = sampleRoot.relative();
        Assert.assertEquals( SAMPLES_TEST, rel );
    }

    @Test
    public void walkTest() throws Exception {
        ZFileSystem.ZFile sampleRoot = ZFileSystem.file( SAMPLES_TEST + "/perf" )  ;
        Assert.assertTrue( sampleRoot.file.isDirectory() );
        sampleRoot.descendants().forEach(zf -> {
            if ( zf.file.isDirectory() ) return;
            Assert.assertEquals( ".zm", zf.extension() );
        });
    }

    @Test
    public void copyTest() throws Exception {
        Path p = createTmpFile( "hello.txt", "Hello, World!");
        ZFileSystem.ZFile file = ZFileSystem.file(p.toString());
        Assert.assertTrue( file.file.exists() );
        ZFileSystem.ZFile copy = file.copy( file.parent(), "hello2.txt" );
        Assert.assertTrue( copy.file.exists() );
        // check they have same data ?
        Assert.assertEquals( file.string(), copy.string() );
        file.file.delete();
        copy.file.delete();
    }

    @Test
    public void moveTest() throws Exception {
        final String text = "Hello, World!" ;
        Path p = createTmpFile( "hi.txt", text);
        ZFileSystem.ZFile file = ZFileSystem.file(p.toString());
        Assert.assertTrue( file.file.exists() );
        Assert.assertEquals( text, file.string());
        ZFileSystem.ZFile move = file.move( file.parent(), "hi2.txt" );
        Assert.assertFalse( file.file.exists() );
        Assert.assertEquals( text, move.string() );
        move.file.delete();
    }

    @Test
    public void readFunctionTest() throws Exception {
        final String data = (String) ZFileSystem.read( THIS_FILE);
        Assert.assertNotNull( data );
        Assert.assertEquals( data, ZFileSystem.read( THIS_FILE , false ) );
        Assert.assertEquals(Function.NIL, ZFileSystem.read( ) );
        Assert.assertEquals(Function.NIL, ZFileSystem.read( 42,42) );
        Assert.assertTrue(Array.getLength( ZFileSystem.file(THIS_FILE).bytes() ) > 0 );
    }

    @Test
    public void packTest() throws Exception {
        String dir = Files.createTempDirectory("zip").toAbsolutePath().toString();
        final String zipDir = Paths.get(dir, "zio.zip").toAbsolutePath().toString() ;
        ZFileSystem.ZFile myDir = ZFileSystem.file(THIS_DIR);
        myDir.pack( zipDir );
        final File zF = new File(zipDir);
        Assert.assertTrue(zF.exists());
        Assert.assertTrue(zF.length() > 0 );
    }

    @Test
    public void writeFunctionTest() throws Exception {
        String dir = Files.createTempDirectory("write").toAbsolutePath().toString();
        // empty file write
        Path p = ZFileSystem.write( dir + "/empty.txt");
        File f = new File(p.toString());
        Assert.assertTrue(f.exists());
        Assert.assertEquals(0, f.length() );
        // create many dir and  write
        p = ZFileSystem.write( dir + "/a/b/c/file.txt", "hello!", false, true );
        f = new File(p.toString());
        Assert.assertTrue(f.exists());
        Assert.assertTrue(f.length() > 0 );

        Assert.assertThrows( UnsupportedOperationException.class, ZFileSystem::write);
        Assert.assertThrows( RuntimeException.class, () -> ZFileSystem.write( dir + "/a/b/d/file.txt", "hello!") );
        Assert.assertThrows( RuntimeException.class, () -> ZFileSystem.write( dir + "/a/b/d/file.txt", "hello!", false, false ) );
    }

    @Test
    public void openFunctionTest() throws Exception {
        Assert.assertSame( System.in, ZFileSystem.open() );
        String dir = Files.createTempDirectory("open").toAbsolutePath().toString();
        // empty file write
        Closeable cl = ZFileSystem.open( new File(dir + "/empty.txt") ,"wb" );
        Assert.assertTrue( cl instanceof FileOutputStream);
        cl.close();
        cl = ZFileSystem.open( dir + "/empty.txt" ,"bw" );
        Assert.assertTrue( cl instanceof FileOutputStream);
        cl.close();

        cl = ZFileSystem.open( dir + "/empty.txt" ,"br" );
        Assert.assertTrue( cl instanceof FileReader);
        cl.close();

        cl = ZFileSystem.open( dir + "/empty.txt" ,"rb" );
        Assert.assertTrue( cl instanceof FileReader);
        cl.close();

        Assert.assertThrows( RuntimeException.class, () -> ZFileSystem.open( dir + "/non-existing.txt" ));
    }

    @Test
    public void zFileFunctionTest() throws Exception {
        ZFileSystem.ZFile f = ZFileSystem.file("/foo.bar");
        Assert.assertThrows( IllegalArgumentException.class, f::iterator);
        Assert.assertEquals("", f.canon() );
        f = ZFileSystem.file(THIS_FILE);
        Assert.assertEquals(Collections.emptyList(), f.children() );
        final ZFileSystem.ZFile zf = f;
        Assert.assertThrows( IllegalArgumentException.class, () -> zf.relative("/foobar"));
        String dir = Files.createTempDirectory("file-ops").toAbsolutePath().toString();
        ZFileSystem.ZFile dirFS = ZFileSystem.file(dir);
        Assert.assertEquals(Collections.emptyList(), dirFS.children() );
        final String cpFile = dir + "/cp-file" ;
        ZFileSystem.ZFile cp = f.copy(cpFile);
        Assert.assertTrue( cp.file.exists() );
        cp = f.copy(dirFS);
        Assert.assertTrue( cp.file.exists() );
        final String renameFile = dir + "/ren-file" ;
        Assert.assertTrue( cp.rename( renameFile ) );
        Assert.assertFalse( cp.file.exists() );
        ZFileSystem.ZFile moved = ZFileSystem.file( renameFile );
        Assert.assertTrue(moved.file.exists());
        moved = moved.move( dir + "/1" );
        Assert.assertTrue(moved.file.exists());
        dir = Files.createTempDirectory("file-ops-move").toAbsolutePath().toString();
        moved = moved.move( ZFileSystem.file(dir) );
        Assert.assertTrue(moved.file.exists());
    }

    @Test
    public void zFileDescendantsTest() throws Exception {
        ZFileSystem.ZFile f = ZFileSystem.file("foo-bar");
        Assert.assertTrue( f.descendants().collect(Collectors.toList()).isEmpty());
        f = ZFileSystem.file(THIS_FILE);
        Assert.assertEquals( 1, f.descendants().collect(Collectors.toList()).size() );
    }

    @Test
    public void equalityTest(){
        ZFileSystem.ZFile f = ZFileSystem.file(THIS_FILE);
        Assert.assertTrue( f.equals(f));
        Assert.assertFalse( f.equals(null));
        Assert.assertFalse( f.equals(42));
    }

    @Test
    public void attributesTest() throws Exception {
        ZFileSystem.ZFile f = ZFileSystem.file(THIS_FILE);
        Assert.assertFalse( f.get("isOther") );
    }
}
