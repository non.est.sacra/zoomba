package zoomba.lang.io;


import org.junit.Test;
import zoomba.lang.core.io.ZWeb;

import java.net.URL;
import java.util.Collections;

import static org.junit.Assert.*;


public class ZWebTest {

    @Test
    public void constructTest() throws Exception {
        new ZWeb("https://google.co.in");
        assertThrows( IllegalArgumentException.class, () -> new ZWeb("foo-bar"));
        new ZWeb(new URL("https://google.co.in") );
        assertThrows( IllegalArgumentException.class, () -> new ZWeb(42));
    }

    @Test
    public void readTest() throws Exception {
        assertArrayEquals(new byte[]{}, ZWeb.readStream(null));
        // Go Postman go, at least this is very useful from Postman
        ZWeb zw = new ZWeb("https://postman-echo.com");
        zw.conTO = 4000;
        zw.readTO = 4000;
        // TODO rewrite with latest JDK
        //assertEquals( 200, zw.send("patch", "/patch", Collections.emptyMap(),"").status );
        //assertEquals( 200, zw.send("connect", "/connect", Collections.emptyMap(),"").status );
        assertEquals( 200, zw.send("head", "/head", Collections.emptyMap(),"").status );
        assertEquals( 200, zw.send("put", "/put", Collections.emptyMap(),"").status );
        assertEquals( 405, zw.send("trace", "/trace", Collections.emptyMap(),"").status );
        assertEquals( 200, zw.send("options", "/options", Collections.emptyMap(),"").status );
        assertEquals( 200, zw.send("delete", "/delete", Collections.emptyMap(),"").status );
    }
}
