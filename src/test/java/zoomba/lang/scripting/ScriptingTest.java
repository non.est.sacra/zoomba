package zoomba.lang.scripting;

import org.junit.Test;
import zoomba.lang.core.oop.ZObject;

import javax.script.*;
import java.io.StringReader;

import static org.junit.Assert.*;

public class ScriptingTest {

    private final ZScriptEngine engine = new ZScriptEngine();

    @Test
    public void javaxScripting() throws Exception {
        ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
        ScriptEngine engine = scriptEngineManager.getEngineByName("zoomba");
        assertNotNull(engine);
        Object result = engine.eval("2+2");
        assertEquals(4,result);
        Bindings bindings = engine.createBindings();
        bindings.put("x", 2 );
        bindings.put("y", 2 );
        result = engine.eval("x+y", bindings);
        assertEquals(4,result);
        ScriptContext scriptContext = engine.getContext();
        scriptContext.setAttribute("x", 2, ScriptContext.ENGINE_SCOPE );
        scriptContext.setAttribute("y", 2, ScriptContext.GLOBAL_SCOPE );
        result = engine.eval("x+y");
        assertEquals(4,result);
        CompiledScript compiledScript = ((Compilable)engine).compile("x+y");
        assertEquals( engine, compiledScript.getEngine());
        result = compiledScript.eval(bindings);
        assertEquals(4,result);
        // function invoke ?
        engine.eval("def add(a,b){ a + b }",scriptContext);
        result = ((Invocable)engine).invokeFunction("add", 2, 2);
        assertEquals(4,result);
        // method invoke ?
        Object zo = engine.eval(new StringReader("def ZO{ def add(a,b){ a + b } } ; new(ZO) ") ,scriptContext);
        assertNotNull(zo);
        assertTrue(zo instanceof ZObject);
        result = ((Invocable)engine).invokeMethod(zo,"add", 2, 2);
        assertEquals(4,result);
        final ZObject zObj = (ZObject)zo;
        assertThrows( NoSuchMethodException.class, () -> ((Invocable)engine).invokeMethod(zObj,"neg") );
        // now attributes stuff
        assertEquals( 2, scriptContext.getScopes().size());
        assertNotNull(scriptContext.getAttribute("x"));
        final int scope = scriptContext.getAttributesScope("x");
        assertEquals(ScriptContext.ENGINE_SCOPE , scope);
        assertNotNull(scriptContext.getAttribute("y", ScriptContext.GLOBAL_SCOPE));
        scriptContext.removeAttribute("y", ScriptContext.GLOBAL_SCOPE);
        assertNull(scriptContext.getAttribute("y", ScriptContext.GLOBAL_SCOPE));
        assertNull(scriptContext.getAttribute("y"));
        assertEquals(-1, scriptContext.getAttributesScope("y"));
        // now let's eval a script... from external file
        zo = engine.eval("@samples/test/date.zm",scriptContext);
        assertNotNull(zo);
        compiledScript = ((Compilable)engine).compile("@samples/test/date.zm");
        assertNotNull(compiledScript);
        engine.put("y",10);
        assertEquals(10,engine.get("y"));
    }

    @Test
    public void scriptEngineFactoryTest(){
        ZScriptEngineFactory factory = new ZScriptEngineFactory();
        factory.getEngineName();
        factory.getEngineVersion();
        factory.getLanguageName();
        factory.getLanguageVersion();

        assertNull( factory.getParameter("foo"));
        assertNotNull( factory.getMethodCallSyntax( "ab", "method", "a","b","c" ));
        assertNotNull( factory.getOutputStatement( "hello" ));
        assertNotNull( factory.getProgram( "s='hello'" ));

        assertFalse( factory.getNames().isEmpty() );
        assertFalse( factory.getMimeTypes().isEmpty() );
        assertFalse( factory.getExtensions().isEmpty() );

        assertEquals(ZScriptEngineFactory.INSTANCE, engine.getFactory() );
    }

    @Test
    public void scriptContextTest(){
        ZScriptContext zc = new ZScriptContext();
        assertNull( zc.getAttribute("foo", 0 ) );
        assertNull( zc.removeAttribute("foo", 0 ) );
        zc.setReader(null);
        zc.setWriter(null);
        zc.setErrorWriter(null);
        engine.setContext(null);
        assertNotNull( engine.getContext() );
        engine.setContext(engine.getContext());
        assertNull( engine.getBindings(10));
    }

    @Test
    public void compiledScripTest() throws ScriptException {
        StringReader reader = new StringReader("s ( y");
        assertThrows(ScriptException.class, () -> engine.compile(reader));
        CompiledScript cs = engine.compile("s = x");
        assertThrows(ScriptException.class, cs::eval);
        cs = engine.compile("assert(false, 'boom!')");
        assertThrows(ScriptException.class, cs::eval);
        StringReader reader2 = new StringReader("true");
        cs = engine.compile(reader2);
        assertEquals( true, cs.eval()); ;
    }

    @Test
    public void evalTest() throws ScriptException {
        assertEquals(true, engine.eval( new StringReader("true")));
        Bindings b = new SimpleBindings();
        b.put("x", true);
        assertEquals( true, engine.eval( "x", b  ));
        assertEquals( true, engine.eval( new StringReader("x"), b  ));

        assertThrows(ScriptException.class, ()-> engine.eval("x"));
    }

    @Test
    public void invokeMethodTest() throws Exception {
        assertThrows( ScriptException.class, () -> engine.invokeMethod( null, "foo" ));
        assertThrows( NoSuchMethodException.class, () -> engine.invokeMethod( "foo", "foo" ));
        assertEquals("hi", engine.invokeMethod("hi", "toString"));
    }

    @Test
    public void invokeFunction() throws Exception{
        Object o = engine.eval( "def x(a){ a } ; x(42) ", new SimpleBindings());
        assertEquals( 42, o );
        o = engine.invokeFunction("x", 84);
        assertEquals( 84, o );
        assertThrows( NoSuchMethodException.class, () -> engine.invokeFunction("foo-bar" ));
    }

    @Test
    public void interfaceTest(){
        assertThrows( UnsupportedOperationException.class , () -> engine.getInterface(String.class) );
        assertThrows( UnsupportedOperationException.class , () -> engine.getInterface("hi", String.class) );
    }
}
