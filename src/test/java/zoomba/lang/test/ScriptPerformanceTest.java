package zoomba.lang.test;

import org.junit.Test;
import zoomba.lang.core.operations.Function;

import static org.junit.Assert.assertTrue;
import static zoomba.lang.core.interpreter.DSLPlugin.NANO_TO_SEC;
import static zoomba.lang.core.interpreter.InterpreterScriptTest.returnDance;

public class ScriptPerformanceTest {

    final String PERFORMANCE = "samples/test/perf/perf.zm";

    final String PERF_FACT = "samples/test/perf/factorial.zm";

    final String PERF_WHILE_SUM = "samples/test/perf/while_iter_sum.zm";

    final String PERF_LAMBDA = "samples/test/perf/lambda_iter.zm";

    final String PERF_POW = "samples/test/perf/pow.zm";

    @Test
    public void whileSumTest(){
        returnDance(PERF_WHILE_SUM);
    }

    @Test
    public void lFoldTest(){
        returnDance(PERF_LAMBDA);
    }

    @Test
    public void factorialTest(){
        returnDance(PERF_FACT);
    }

    @Test
    public void powerTest(){
        returnDance(PERF_POW);
    }

    int testJava() {
        int j = 0;
        for (int i = 0; i < 1000000; i++) {
            j = i;
        }
        return j;
    }

    public static final int MAX_LIMIT = 200;

    @Test
    public void dancePerformance() {
        Function.MonadicContainer mc = returnDance(PERFORMANCE);
        Number num = (Number) mc.value();
        long cur = System.nanoTime();
        testJava();
        long delta = System.nanoTime() - cur;
        double doubleDeltaInSec = delta * NANO_TO_SEC;
        System.out.printf("Java %s (sec): ZoomBA %s (sec)\n", doubleDeltaInSec, mc.value());
        double ratio = num.doubleValue() / doubleDeltaInSec;
        System.out.printf("Java is %s times faster than ZoomBA with limit %d\n", ratio, MAX_LIMIT);
        assertTrue((int) ratio < MAX_LIMIT);
    }
}
